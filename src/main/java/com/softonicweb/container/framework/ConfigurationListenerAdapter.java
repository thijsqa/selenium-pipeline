package com.softonicweb.container.framework;

import org.testng.IConfigurationListener;
import org.testng.ITestResult;

/**
 * An adapter that provides an empty implementation of an IConfigurationListener interface.
 *
 * @author alfredo.lopez
 */
public class ConfigurationListenerAdapter implements IConfigurationListener {

  @Override
  public void onConfigurationSuccess(ITestResult itr) {
  }

  @Override
  public void onConfigurationFailure(ITestResult itr) {
  }

  @Override
  public void onConfigurationSkip(ITestResult itr) {
  }
}
