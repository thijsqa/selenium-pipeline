package com.softonicweb.container.framework;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;

/**
 * Formats all logging events as HTML.
 *
 * @author ivan.solomonoff
 */
class HtmlResultsFormatter implements ResultsFormatter {

  private static final int HTML_MAX_COLUMNS = 7;
  private static final int RESOURCE_PREVIEW_WIDHT = 200;

  private static final String URL_PATH_SEPARATOR = "/";
  private static final String PATH_SEPARATOR = File.separator;

  private static final String CSS_CLASS_FAILED = "status_failed";
  private static final String CSS_CLASS_PASSED = "status_passed";
  private static final String CSS_CLASS_COMMENT = "status_comment";
  private static final String CSS_CLASS_TEXTAREA = "status_textarea";
  private static final String CSS_CLASS_SPECIAL = "status_special";
  private static final String CSS_CLASS_DONE = "status_done";
  private static final String CSS_CLASS_METHOD = "method";

  private static final String TOOL_TIPP_MESSAGE_TIME_DELTA =
      "time delta reporting is alpha and subject to change";

  private static final SimpleDateFormat LOGGING_DATETIME_FORMAT =
      new SimpleDateFormat("HH:mm:ss dd-MM-yyyy");

  private static final SimpleDateFormat LOGGING_TIME_FORMAT = new SimpleDateFormat("HH:mm:ss");

  // customized copy from org.openqa.selenium.server.htmlrunner.HTMLTestResults
  private static final String HTML_HEADER = "<html>\n"
      + "<head>"
      + "<meta content=\"text/html; charset={0}\" http-equiv=\"content-type\">"
      + "<meta content=\"cache-control\" http-equiv=\"no-cache\">"
      + "<meta content=\"pragma\" http-equiv=\"no-cache\">"
      + "<style type=\"text/css\">\n"
      + "body, table '{'\n"
      + "    font-family: Verdana, Arial, sans-serif;\n"
      + "    font-size: 12;\n"
      + "'}'\n"
      + "\n"
      + "table '{'\n"
      + "    border-collapse: collapse;\n"
      + "    border: 1px solid #ccc;\n"
      + "'}'\n"
      + "\n"
      + "th, td '{'\n"
      + "    padding-left: 0.3em;\n"
      + "    padding-right: 0.3em;\n"
      + "'}'\n"
      + "\n"
      + "a '{'\n"
      + "    text-decoration: none;\n"
      + "'}'\n"
      + "\n"
      + "."
      + CSS_CLASS_METHOD
      + " '{'\n"
      + "    font-style: italic;\n"
      + "'}'\n"
      + "\n"
      + ".selected '{'\n"
      + "    background-color: #ffffcc;\n"
      + "'}'\n"
      + "\n"
      + "."
      + CSS_CLASS_DONE
      + " '{'\n"
      + "    background-color: #eeffee;\n"
      + "'}'\n"
      + "\n"
      + "."
      + CSS_CLASS_PASSED
      + " '{'\n"
      + "    background-color: #ccffcc;\n"
      + "'}'\n"
      + "\n"
      + "."
      + CSS_CLASS_FAILED
      + " '{'\n"
      + "    background-color: #ffcccc;\n"
      + "'}'\n"
      + "\n"
      + "."
      + CSS_CLASS_SPECIAL
      + " '{'\n"
      + "    background-color: #eeffff;\n"
      + "'}'\n"
      + "\n"
      + "."
      + CSS_CLASS_COMMENT
      + " '{'\n"
      + "    background-color: #ffffcc;\n"
      + "'}'\n"
      + "."
      + CSS_CLASS_TEXTAREA
      + " '{'\n"
      + "    background-color: #ffffff;\n"
      + "    width: 100%;\n"
      + "'}'\n"
      + "\n"
      + ".breakpoint '{'\n"
      + "    background-color: #cccccc;\n"
      + "    border: 1px solid black;\n"
      + "'}'\n"
      + "</style>\n"
      + "<title>Test results</title></head>\n"
      + "<body>\n"
      + " <span style=\"font-size:9px;font-family:arial,verdana,sans-serif;\">"
      + "Softonic QA - HTML Test Report"
      + "</span>"
      + "<h1>Test results for: {1}</h1>";

  private static final String HTML_TABLE_HEADER = "<tr>"
      + "<td><b>Time</b></td>"
      + "<td><b>Command</b></td>"
      + "<td><b>Parameter-1</b></td>"
      + "<td><b>Parameter-2</b></td>"
      + "<td><b>Result</b></td>"
      + "<td><b>Duration[ms]</b></td>"
      + "<td><b>Calling-Class with line number</b></td>"
      + "</tr>\n";

  private static final String HTML_METRICS = "<table>\n"
      + "<tr><td>test suite:</td><td>{0}</td></tr>\n"
      + "<tr><td>test environment:</td><td>{1}</td></tr>\n"
      + "<tr><td>environment revision:</td><td> {2} </td></tr>\n"
      + "<tr><td>framework revision:</td><td> {3} </td></tr>\n"
      + "<tr><td>device:</td><td>{4}</td></tr>\n"
      + "<tr><td>test-started:</td><td>{5}</td></tr>\n"
      + "<tr><td>test-finished:</td><td>{6}</td></tr>\n"
      + "<tr><td>test-duration [secs]:</td><td>{7,number,#} s</td></tr>\n"
      + "{8}\n"
      + "</table>\n";

  private static final String HTML_SEPARATOR = "<tr><td colspan=\"{0}\"><hr></td></tr>\n";

  private static final String HTML_COMMENT =
      "<tr class=\"{0}\"><td>{3}</td><td colspan=\"{1}\">{2}</td></tr>\n";

  private static final String HTML_METHOD = "<tr class=\"{0}\"><td colspan=\"{1}\">{2}</td></tr>\n";

  private static final String HTML_TEXTAREA = "<tr><td></td>{3}<td colspan=\"{1}\">"
      + "<textarea rows=\"6\" class=\"{0}\">{2}</textarea></td></tr>\n";

  private static final String HTML_FOOTER = "</tbody></table></body></html>";

  private static final String HTML_SPECIAL =
      "<span style=\"font-size:9px;font-family:arial,verdana,sans-serif;\">{0}</span>";

  private static final String HTML_RESOURCE_ROW = "<tr class=\"{0}\">"
      + "<td colspan=\"{1}\" valign=\"center\" align=\"center\" halign=\"center\">{2}</td>"
      + "<td>{3,number,#}</td>"
      + "<td>{4}</td>"
      + "</tr>\n";

  private static final String HTML_RESOURCE_IMG = "<a href=\"{0}\">"
      + "<img src=\"{1}\" width=\"{2}\""
      + " alt=\"Selenium Screenshot\""
      + " title=\"Selenium Screenshot\"/>"
      + "<br/>{3}</a>";

  private static final String HTML_EMPTY_COLUMN = "<td>&nbsp;</td>";

  private final Writer resultsWriter;
  private final ISuiteEnvironment suiteEnvironment;

  private String resultFileEncoding = "UTF-8";
  private String resourcesBaseUri = "";
  private String resourcesPath = ".";

  /**
   * Write results with an arbitrary encoding.
   * <p>Be sure to create the writer with the correct encoding
   * <p>Note: resultFileEncoding is only used to set a corresponding meta-tag
   * in the resulting HTML-file
   *
   * <p>For Example:
   * <code>new BufferedWriter(new OutputStreamWriter(new FileOutputStream
   * ("myResultFile.html"), "UTF-8")</code>
   *
   * @param resultsWriter writer with resultFileEncoding set.
   * @param resultFileEncoding any encoding supported by the running JVM
   */
  public HtmlResultsFormatter(Writer resultsWriter, String resultFileEncoding,
      ISuiteEnvironment suiteEnvironment) {
    this.resultsWriter = resultsWriter;
    this.resultFileEncoding = resultFileEncoding;
    this.suiteEnvironment = suiteEnvironment;
  }

  /** {@inheritDoc} */
  @Override
  public String getResourceBaseUri() {
    return resourcesBaseUri;
  }

  /** {@inheritDoc} */
  @Override
  public void setResourceBaseUri(String resourceBaseUri) {
    resourcesBaseUri = (resourceBaseUri == null) ? "" : resourceBaseUri;
  }

  /** {@inheritDoc} */
  @Override
  public String getResourcesPath() {
    return resourcesPath;
  }

  /**
   * Default location is current working dir (".") If another
   * (filesystem-) location is desired use this setter.
   *
   * {@inheritDoc}
   */
  @Override
  public void setResourcesPath(String path) {
    resourcesPath = new File(path).getAbsolutePath();
  }

  private String getResultFileEncoding() {
    return resultFileEncoding;
  }

  /** {@inheritDoc} */
  @Override
  public void logCommandEvent(LogItem logItem) {
    if (!logItem.isExcludeFromLogging()) {
      String resultClass = logItem.isCommandSuccessful() ? CSS_CLASS_DONE : CSS_CLASS_FAILED;
      if ("screenshot".equals(logItem.getCommandName())) {
        logToWriter(formatScreenshot(logItem, resultClass));
      } else {
        logToWriter(formatCommandAsHtml(logItem, resultClass, ""));
      }
    }
  }

  /** {@inheritDoc} */
  @Override
  public void logSpecialCommandEvent(LogItem logItem) {
    if (!logItem.isExcludeFromLogging()) {
      logToWriter(formatCommandAsHtml(logItem, CSS_CLASS_SPECIAL, ""));
    }
  }

  /** {@inheritDoc} */
  @Override
  public void logBooleanCommandEvent(LogItem logItem) {
    String toolTippMessage = "";
    String resultClass = "";
    if (logItem.isCommandSuccessful()) {
      resultClass = CSS_CLASS_PASSED;
    } else {
      resultClass = CSS_CLASS_FAILED;
    }

    logToWriter(formatCommandAsHtml(logItem, resultClass, toolTippMessage));
  }

  /** {@inheritDoc} */
  @Override
  public void logCommentEvent(LogItem logItem) {
    logText(logItem, HTML_COMMENT, CSS_CLASS_COMMENT);
  }

  /** {@inheritDoc} */
  @Override
  public void logTextArea(LogItem logItem) {
    logText(logItem, HTML_TEXTAREA, CSS_CLASS_TEXTAREA);
  }

  /** {@inheritDoc} */
  @Override
  public void logMethodEvent(LogItem logItem) {
    logText(logItem, HTML_METHOD, CSS_CLASS_METHOD);
  }

  private void logText(LogItem logItem, String htmlCode, String cssClass) {
    String[] logItemArgs = getCorrectedArgsArray(logItem, 2, "");
    String textToBeLogged = logItemArgs[0];
    String additionalInformation = logItemArgs[1];
    logToWriter(MessageFormat.format(htmlCode, cssClass, HTML_MAX_COLUMNS - 1,
        textToBeLogged + extraInformationLogEvent(additionalInformation),
        LOGGING_TIME_FORMAT.format(logItem.getCmdStartMillis())));
  }

  /** {@inheritDoc} */
  @Override
  public void logSeparator() {
    logToWriter(MessageFormat.format(HTML_SEPARATOR, HTML_MAX_COLUMNS));
  }

  /** {@inheritDoc} */
  @Override
  public void logHeaderEvent(TestMetrics metrics) {
    logToWriter(formatHeader(metrics));
  }

  /** {@inheritDoc} */
  @Override
  public void logFooterEvent() {
    logSeparator();
    logToWriter(HTML_FOOTER);
  }

  private String formatMetrics(TestMetrics metrics) {
    String failedCommandsRow = "";
    if (metrics.getFailedCommands() > 0) {
      failedCommandsRow = "<tr class=\""
          + CSS_CLASS_FAILED
          + "\"><td>failed commands:</td><td>"
          + metrics.getFailedCommands()
          + "</td></tr>\n";
      if (StringUtils.isNotBlank(metrics.getLastFailedCommandMessage())) {
        failedCommandsRow = failedCommandsRow
            + "<tr class=\""
            + CSS_CLASS_FAILED
            + "\"><td>last failed message:</td><td>"
            + metrics.getLastFailedCommandMessage()
            + "</td></tr>\n";
      } else {
        System.err.println("WARNING: NO LastFailedCommandMessage");
      }
    }

    final int secondsInMilliseconds = 1000;

    return MessageFormat.format(HTML_METRICS,
        suiteEnvironment.getSuite().getName(),
        suiteEnvironment.getSuite().getEnvironment(),
        metrics.getServerRevision(),
        metrics.getFrameworkRevision(),
        metrics.getDevice(),
        LOGGING_DATETIME_FORMAT.format(metrics.getStartTimeStamp()),
        LOGGING_DATETIME_FORMAT.format(metrics.getEndTimeStamp()),
        metrics.getTestDuration() / secondsInMilliseconds,
        failedCommandsRow);
  }

  private String formatHeader(TestMetrics metrics) {
    String header = MessageFormat.format(HTML_HEADER, getResultFileEncoding(),
        metrics.getTestName())
        + "\n"
        + formatMetrics(metrics)
        + "<table border=\"1\"><tbody>"
        + HTML_TABLE_HEADER;

    return header;
  }

  private String extraInformationLogEvent(String extraInformation) {
    String result = "";
    if (null != extraInformation && !extraInformation.equals("")) {
      result = MessageFormat.format(HTML_SPECIAL, extraInformation);
    }

    return result;
  }

  private String formatScreenshot(LogItem logItem, String resultClass) {
    // if screenshot could not be written there should be something like a SeleniumException
    return MessageFormat.format(HTML_RESOURCE_ROW, resultClass, HTML_MAX_COLUMNS - 2,
        formatScreenshotFileImgTag(logItem.getArgs()[0]), logItem.getDeltaMillis(),
        logItem.getCallingClass());
  }

  /**
   * Format img HTML tag.
   * Link has to be relative on any system
   *
   * @param absFsPathToScreenshot Absolute path to saved screenshot on the
   *                              local filesystem
   * @return formatted HTML img tag
   */
  private String formatScreenshotFileImgTag(String absFsPathToScreenshot) {
    String screenshotRelativeUrl;
    String screenshotPathNormalized =
        absFsPathToScreenshot.replace(PATH_SEPARATOR, URL_PATH_SEPARATOR);
    String screenShotName = screenshotPathNormalized.substring(
        screenshotPathNormalized.lastIndexOf(URL_PATH_SEPARATOR) + URL_PATH_SEPARATOR.length());

    if (getResourceBaseUri().isEmpty()) {
      screenshotRelativeUrl = screenShotName;
    } else {
      screenshotRelativeUrl = getResourceBaseUri().endsWith("/")
          ? getResourceBaseUri() + screenShotName
              : getResourceBaseUri() + URL_PATH_SEPARATOR + screenShotName;
    }

    return MessageFormat.format(HTML_RESOURCE_IMG, screenshotRelativeUrl, screenshotRelativeUrl,
        RESOURCE_PREVIEW_WIDHT, screenShotName);
  }

  private String formatCommandAsHtml(LogItem logItem, String resultClass, String toolTippMessage) {
    StringBuilder htmlWrappedCommand = new StringBuilder();
    htmlWrappedCommand.append("<tr class=\""
        + resultClass
        + "\" title=\""
        + toolTippMessage
        + "\" alt=\""
        + toolTippMessage
        + "\">"
        + "<td>"
        + LOGGING_TIME_FORMAT.format(logItem.getCmdStartMillis())
        + "</td>"
        + "<td>"
        + StringEscapeUtils.escapeXml(logItem.getCommandName())
        + "</td>");
    int writtenColumns = 0;
    if (logItem.getArgs() != null) {
      for (int i = 0; i < logItem.getArgs().length; i++) {
        writtenColumns++;
        htmlWrappedCommand.append("<td>" + StringEscapeUtils.escapeXml(logItem.getArgs()[i])
            + "</td>");
      }
    }
    // put empty columns if parameters are missing
    htmlWrappedCommand.append(generateEmptyColumns(
        HTML_MAX_COLUMNS - writtenColumns - (HTML_MAX_COLUMNS - 2)));

    htmlWrappedCommand.append("<td>"
        + StringEscapeUtils.escapeXml(logItem.getResult())
        + "</td>"
        + "<td title=\""
        + TOOL_TIPP_MESSAGE_TIME_DELTA
        + "\" alt=\""
        + TOOL_TIPP_MESSAGE_TIME_DELTA
        + "\">"
        + logItem.getDeltaMillis()
        + "</td><td>"
        + logItem.getCallingClass()
        + "</td></tr>\n");

    return htmlWrappedCommand.toString();
  }

  private void logToWriter(String formattedLogEvent) {
    try {
      resultsWriter.write(formattedLogEvent);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * Generate empty HTML columns.
   *
   * @param numColsToGenerate num of columns to be generated
   * @return generated, empty columns in one string
   */
  private static String generateEmptyColumns(int numColsToGenerate) {
    StringBuilder result = new StringBuilder();
    for (int i = 0; i < numColsToGenerate; i++) {
      result.append(HTML_EMPTY_COLUMN);
    }

    return result.toString();
  }

  /**
   * Presets the args array with default values if the are absent.
   * <p>Creates if necessary: - new array - default values for args array as
   * many as presetNumArgs.
   *
   * @param logItem args array to be taken from here
   * @param presetNumArgs number of array elements to be preset
   * @param defaultValue default value to be used for missing array elements
   * @return array containing previous values and filled with default ones,
   *               will be created if null previously
   */
  private static String[] getCorrectedArgsArray(LogItem logItem, int presetNumArgs,
      String defaultValue) {
    String[] currentArgs;
    if (logItem == null || logItem.getArgs() == null) {
      currentArgs = new String[] {};
    } else {
      currentArgs = logItem.getArgs();
    }
    String[] newArgs = new String[presetNumArgs];
    for (int i = 0; i < currentArgs.length; i++) {
      newArgs[i] = currentArgs[i];
    }
    for (int i = currentArgs.length; i < presetNumArgs; i++) {
      newArgs[i] = defaultValue;
    }

    return newArgs;
  }
}
