package com.softonicweb.container.framework;

import java.util.Locale;

/**
 * Model for a specific environment instance.
 *
 * @author ivan.solomonoff
 */
public final class Instance {

  public static final Locale DEFAULT_LOCALE = Localization.EN;
  public static final String DEFAULT_NAME = "";
  public static final int DEFAULT_ID = -1;
  public static final String DEFAULT_BASE_URL = "";

  private final String name;
  private final int id;
  private final String baseUrl;
  private final Locale locale;

  public Instance(String name, int id, String baseUrl, Locale locale) {
    this.name = name;
    this.id = id;
    this.baseUrl = baseUrl;
    this.locale = locale;
  }

  public Instance(String name, String id, String baseUrl, String locale) {
    this.name = name;
    this.id = Integer.parseInt(id);
    this.baseUrl = baseUrl;
    this.locale = Localization.fromString(locale);
  }

  public Instance(String name, int id, String baseUrl) {
    this(name, id, baseUrl, DEFAULT_LOCALE);
  }

  public Instance(int id, String baseUrl) {
    this(DEFAULT_NAME, id, baseUrl);
  }

  public Instance() {
    this(DEFAULT_NAME, DEFAULT_ID, DEFAULT_BASE_URL, DEFAULT_LOCALE);
  }

  public String getName() {
    return name;
  }

  public Locale getLocale() {
    return locale;
  }

  public int getId() {
    return id;
  }

  public String getBaseUrl() {
    return baseUrl;
  }
}
