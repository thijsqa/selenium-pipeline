package com.softonicweb.container.framework;

import java.util.List;

import com.softonicweb.container.framework.ISuite;
import com.softonicweb.container.framework.ISuiteEnvironment;

/**
 * Stores common suite environment variables and reporting configuration.
 *
 * @author ivan.solomonoff
 */
public final class SuiteEnvironment implements ISuiteEnvironment {

  private static SuiteEnvironment instance = null;

  public static SuiteEnvironment getInstance() {
    if (instance == null) {
      instance = new SuiteEnvironment();
    }
    return instance;
  }

  private ISuite suite;
  private Parameters parameters;
  private String reportsDir;
  private String screenshotsDir;
  private String testOutputDir;
  private String reportsBaseUrl;
  private String downloadsFolder;
  private String serverRevision;
  private String detailedReportUrl;

  private SuiteEnvironment() {
  }

  @Override
  public String getReportsDir() {
    return reportsDir;
  }

  @Override
  public void setReportsDir(String directory) {
    this.reportsDir = directory;
  }

  @Override
  public String getScreenshotsDir() {
    return screenshotsDir;
  }

  @Override
  public void setScreenshotsDir(String directory) {
    this.screenshotsDir = directory;
  }

  @Override
  public String getTestOutputDir() {
    return testOutputDir;
  }

  @Override
  public void setTestOutputDir(String directory) {
    this.testOutputDir = directory;
  }

  @Override
  public String getReportsBaseUrl() {
    return reportsBaseUrl;
  }

  @Override
  public void setReportsBaseUrl(String url) {
    this.reportsBaseUrl = url;
  }

  @Override
  public String getDownloadsFolder() {
    return downloadsFolder;
  }

  @Override
  public void setDownloadsFolder(String directory) {
    this.downloadsFolder = directory;
  }

  @Override
  public String getServerRevision() {
    return serverRevision;
  }

  @Override
  public void setServerRevision(String revision) {
    this.serverRevision = revision;
  }

  @Override
  public String getDetailedReportUrl() {
    return detailedReportUrl;
  }

  @Override
  public void setDetailedReportUrl(String url) {
    this.detailedReportUrl = url;
  }

  @Override
  public ISuite getSuite() {
    return suite;
  }

  @Override
  public void setSuite(ISuite suiteArg) {
    this.suite = suiteArg;
  }

  @Override
  public List<Instance> getInstances() {
    return suite.getInstances();
  }

  @Override
  public String[] getPlatforms() {
    return parameters.getDevices().split(ISuite.ARRAY_SEPARATOR);
  }

  public Parameters getParameters() {
    return parameters;
  }

  public void setParameters(Parameters parameters) {
    this.parameters = parameters;
  }
}
