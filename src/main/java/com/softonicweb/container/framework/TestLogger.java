package com.softonicweb.container.framework;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import com.softonicweb.container.framework.FileUtil;

/**
 * Provides formatted logging support.
 *
 * @author ivan.solomonoff
 */
public final class TestLogger {

  private static final InheritableThreadLocal<TestLogger> THREAD_LOCAL =
      new InheritableThreadLocal<TestLogger>() {
    @Override
    protected TestLogger initialValue() {
      // Creates a TestLogger object and presets its initial test metrics. Ensures session is clean.
      return new TestLogger().resetLogsAndMetrics().presetTestMetrics();
    }
  };

  private static final InheritableThreadLocal<List<LogItem>> LOG_EVENTS =
      new InheritableThreadLocal<List<LogItem>>() {
    @Override
    protected List<LogItem> initialValue() {
      return new ArrayList<LogItem>();
    }
  };

  private static final InheritableThreadLocal<TestMetrics> TEST_METRICS =
      new InheritableThreadLocal<TestMetrics>() {
    @Override
    protected TestMetrics initialValue() {
      return new TestMetrics();
    }
  };

  private TestLogger() {
  }

  public static TestLogger getInstance() {
    return THREAD_LOCAL.get();
  }

  private TestMetrics getTestMetrics() {
    return TEST_METRICS.get();
  }

  private List<LogItem> getLogEvents() {
    return LOG_EVENTS.get();
  }

  // A singleton object is not supposed to be cloned.
  @Override
  public Object clone() throws CloneNotSupportedException {
    throw new CloneNotSupportedException();
  }

  public static void cleanup() {
    LOG_EVENTS.remove();
    TEST_METRICS.remove();
    THREAD_LOCAL.remove();
  }

  /** Useful when reusing the session for example with Selenium. */
  public TestLogger resetLogsAndMetrics() {
    getLogEvents().clear();
    getTestMetrics().resetMetrics();

    return this;
  }

  private void presetFormatter(ResultsFormatter formatter, ISuiteEnvironment suiteEnvironment) {
    String baseUri = "../screenshots";
    formatter.setResourceBaseUri(baseUri);
    formatter.setResourcesPath(suiteEnvironment.getScreenshotsDir());
  }

  private TestLogger presetTestMetrics() {
    getTestMetrics().setFrameworkRevision(getFrameworkRevision());
    getTestMetrics().setStartTimeStamp(System.currentTimeMillis());
    return this;
  }

  private String getFrameworkRevision() {
    URLClassLoader cl = (URLClassLoader) getClass().getClassLoader();
    try {
      URL url = cl.findResource("META-INF/MANIFEST.MF");
      Manifest manifest = new Manifest(url.openStream());
      Attributes attr = manifest.getMainAttributes();
      String build = attr.getValue("Build-Number");
      String timestamp = attr.getValue("Build-Timestamp");
      return (build + " [" + timestamp + "]");
    } catch (IOException e) {
      // Upon errors, we will print "Revision: unknown" in reports.
      return "unknown";
    }
  }

  /**
   * Post-processes logs using the formatter and writes them to a file.
   *
   * <p>TODO(ivan.solomonoff): In the future we may want to choose the
   * ResultFormatter to be used.
   */
  public void finalize(File resultFileNameAndPath, String testName,
      ISuiteEnvironment suiteEnvironment, ITestEnvironment testEnvironment) {
    getTestMetrics().setTestName(testName);
    getTestMetrics().setServerRevision(suiteEnvironment.getServerRevision());
    getTestMetrics().setDevice(testEnvironment.getPlatform().toString());

    OutputStreamWriter osw = null;
    try {
      osw = new OutputStreamWriter(new FileOutputStream(resultFileNameAndPath), FileUtil.ENCODING);
      BufferedWriter buffer = new BufferedWriter(osw);
      PrintWriter writer = new PrintWriter(buffer);

      ResultsFormatter formattter =
          new HtmlResultsFormatter(writer, FileUtil.ENCODING, suiteEnvironment);
      presetFormatter(formattter, suiteEnvironment);
      new LogEventsPostProcessor(formattter, getLogEvents(), getTestMetrics());
      getLogEvents().clear(); // ensure log events are clean in next run
      writer.close();
      buffer.close();
      osw.close();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * Logs a command.
   *
   * @param commandName command representing the action to be logged
   * @param args arguments of the executed command, if any
   * @param result string representing the command output or result
   * @param cmdStartMillis command runtime timestamp
   */
  public void log(String commandName, String[] args, String result,
      long cmdStartMillis) {
    LogItem currentCommand = presetLogItem(commandName, args, result, cmdStartMillis,
        System.currentTimeMillis());

    currentCommand.setCallingClass(getRealCallingClassWithLineNumberAsString(
        getCurrentCallingClassAsStackTraceElement()));
    String sourceMethodName = "unknown";
    StackTraceElement classOrNull = getCurrentCallingClassAsStackTraceElement();
    if (classOrNull != null) {
      sourceMethodName = classOrNull.getMethodName();
    }
    currentCommand.setSourceMethod(sourceMethodName);

    getLogEvents().add(currentCommand);
  }

  /**
   * Logs an Exception.
   *
   * @param commandName command which possibly is responsible for the exception
   * @param args arguments which possibly are responsible for the exception
   * @param exception the exception thrown, can be null
   * @param cmdStartMillis command runtime timestamp
   */
  public void logException(String commandName, String[] args, Throwable exception,
      long cmdStartMillis) {
    String resultContent;
    String errorMessage;
    if (exception != null) {
      resultContent = exception.getClass().getName() + " - " + exception.getMessage();
      errorMessage = exception.getMessage();
    } else {
      resultContent = "FAIL";
      if (args.length > 0) {
        errorMessage = args[0];
      } else {
        errorMessage = "ERROR: real error-msg could not be determined";
      }
    }

    log(commandName, args, resultContent, cmdStartMillis);
    getTestMetrics().incFailedCommands();
    getTestMetrics().setLastFailedCommandMessage(errorMessage);
  }

  public void logComment(final String comment) {
    long cmdStartMillis = System.currentTimeMillis();
    log("comment", new String[] { comment, "" }, "", cmdStartMillis);
  }

  public void logText(final String text) {
    long cmdStartMillis = System.currentTimeMillis();
    log("textarea", new String[] { text, "" }, "", cmdStartMillis);
  }

  public void logUrl(final String precedingText, final String url) {
    long cmdStartMillis = System.currentTimeMillis();
    log("comment", new String[] {
        precedingText + " <a href=\"" + url + "\"" + ">" + url + "</a>", "" }, "", cmdStartMillis);
  }

  public void logSeparator() {
    long cmdStartMillis = System.currentTimeMillis();
    log("separator", new String[] { "", "" }, "", cmdStartMillis);
  }

  public void logAssertionSuccess(String assertionName, String assertionMessage,
      String assertionCondition) {
    String[] loggingArgs = new String[] { "Verified NOT: \"" + assertionMessage + "\"",
        assertionCondition };

    log(assertionName, loggingArgs, "PASS", System.currentTimeMillis());
  }

  public void logAssertionFailure(String assertionName, String assertionMessage,
      String assertionCondition) {
    String[] loggingArgs = new String[] { assertionMessage, assertionCondition };

    logException(assertionName, loggingArgs, null, System.currentTimeMillis());
  }

  public void logError(String errorMessage) {
    logException("FAIL!", new String[] { errorMessage, "" }, null, System.currentTimeMillis());
  }

  public void logScreenshot(IScreenshooter screenshooter) {
    logResource(screenshooter.getScreenshot());
  }

  public void logResource(String resourcePath) {
    log("screenshot", new String[] { resourcePath }, "", System.currentTimeMillis());
  }

  /** When a test failed, logs the throwable and captures a screenshot */
  public void logTestFailureWithScreenshot(IScreenshooter screenshooter, Throwable throwable) {
    String[] loggingArgs = new String[] { "Test FAILED.", "" };
    logSeparator();
    logException("", loggingArgs, throwable, System.currentTimeMillis());
    logScreenshot(screenshooter);
  }

  private LogItem presetLogItem(String commandName, String[] args, String result,
      long cmdStartMillis, long cmdEndMillis) {
    LogItem logitem = new LogItem();
    logitem.setCommandName(commandName);
    logitem.setArgs(args);
    logitem.setResult(result);
    logitem.setCmdStartMillis(cmdStartMillis);
    logitem.setCmdEndMillis(cmdEndMillis);

    return logitem;
  }

  private StackTraceElement getCurrentCallingClassAsStackTraceElement() {
    StackTraceElement[] testElements = Thread.currentThread().getStackTrace();
    StackTraceElement currentCallingClassAsStackTraceElement = null;
    for (int i = 0; i < testElements.length; i++) {
      StackTraceElement stackTraceElement = testElements[i];
      // First appearance of a call from the tests package should lead to the source of the problem.
      // The previous should lead to the faulty page object or framework code.
      if (stackTraceElement.getClassName().matches("com\\.softonic\\.\\w+\\.tests.+")) {
        currentCallingClassAsStackTraceElement = testElements[i - 1];
        break;
      }
    }

    return currentCallingClassAsStackTraceElement;
  }

  private String getRealCallingClassWithLineNumberAsString(StackTraceElement stackTraceElement) {
    if (stackTraceElement != null) {
      return stackTraceElement.getClassName() + "#" + stackTraceElement.getLineNumber();
    } else {
      return "";
    }
  }
}
