package com.softonicweb.container.framework;

import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.testng.reporters.TestHTMLReporter;

/**
 * Contains util methods to be used in HTML reports.
 *
 * @author ivan.solomonoff
 */
public class HtmlReporterUtil extends TestHTMLReporter {

  /** Starts HTML stream. */
  public static void startHtml(PrintWriter out, String suiteName, String reportFilename) {
    out.println("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN http://www.w3.org/TR/xhtml11/DTD"
        + "/xhtml11.dtd\">");
    out.println("<html xmlns=\"http://www.w3.org/1999/xhtml\">");
    out.println("<head>");
    out.println("<title>" + suiteName + " - " + reportFilename + "</title>");
    out.println("<meta http-equiv=\"Content-Security-Policy\" content=\"default-src * "
        + "'unsafe-inline'; style-src 'self' 'unsafe-inline'; media-src *\">");
    out.println("<style type=\"text/css\">");
    out.println("table.info_table td,table.passed td,table.info_table th,table.param td,"
        + "table.param th,table.passed th,table.failed td,table.failed th {");
    out.println("border:1px solid #000099;padding:.25em .5em .25em .5em");
    out.println("}");
    out.println("table caption,table.info_table,table.param,table.passed,table.failed {margin-"
        + "bottom:10px;border:1px solid #000099;border-collapse:collapse;empty-cells:show;}");
    out.println("table.param th {vertical-align:bottom}");
    out.println("td.numi,th.numi,td.numi_attn,td.numi_warn,td.numi_skip,td.numi_passed {");
    out.println("text-align:right");
    out.println("}");
    out.println("tr.total td {font-weight:bold}");
    out.println("table caption {");
    out.println("text-align:center;font-weight:bold;");
    out.println("}");
    out.println("table.passed tr.stripe td,table tr.passedodd td, table.param td.numi_passed "
        + "{background-color: #ccffcc;}");
    out.println("table.passed td,table tr.passedeven td, table.param tr.stripe td.numi_passed "
        + "{background-color: #ddffdd;}");
    out.println("table.passed tr.stripe td,table tr.skippedodd td, table.param td.numi_skip "
        + "{background-color: #ffffcc;}");
    out.println("table.passed td,table tr.skippedeven td, table.param tr.stripe td.numi_skip "
        + "{background-color: #ffffdd;}");
    out.println("table.passed tr.stripe td,table tr.retriedodd td, table.param td.numi_warn "
        + "{background-color: #FCD5B4;}");
    out.println("table.passed td,table tr.retriedeven td, table.param tr.stripe td.numi_warn "
        + "{background-color: #FCDDB4;}");
    out.println("table.failed tr.stripe td,table tr.failedodd td,table.param td.numi_attn "
        + "{background-color: #ff9999;}");
    out.println("table.failed td,table tr.failedeven td,table.param tr.stripe td.numi_attn "
        + "{background-color: #ffaaaa;}");
    out.println("table.failed td, table tr.configuration td, table tr.failedlight td,table.param "
        + "tr.stripe td.numi_attn {background-color: #ffcccc;}");
    out.println("tr.stripe td,tr.stripe th {background-color: #E6EBF9;}");
    out.println("p.totop {font-size:85%;text-align:center;border-bottom:2px black solid}");
    out.println("div.shootout {padding:2em;border:3px #4854A8 solid}");
    out.println("</style>");
    out.println("</head>");
    out.println("<body>");
    out.println("<h2>" + new SimpleDateFormat("EEE, d MMM yyyy - HH:mm:ss")
    .format(Calendar.getInstance().getTime()) + "</h2>");
    out.println("<h3>Softonic QA Test Results</h3>");
  }
}
