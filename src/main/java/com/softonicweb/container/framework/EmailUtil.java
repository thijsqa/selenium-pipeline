package com.softonicweb.container.framework;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * Util class for sending test result emails.
 *
 * @author ivan.solomonoff
 */
public final class EmailUtil {

  private EmailUtil() {
  }

  /**
   * @param ccRecipient the recipient in copy, or null if not necessary.
   */
  public static void sendEmail(String recipient, String[] ccRecipients, String subject, String body,
      boolean isHtml) {
    Properties props = new Properties();
    props.setProperty("mail.transport.protocol", "smtp");
    props.setProperty("mail.smtp.port", "465");
    props.setProperty("mail.smtp.auth", "true");
    props.setProperty("mail.smtp.ssl.enable", "true");
    props.setProperty("mail.host", "email-smtp.eu-west-1.amazonaws.com");
    props.setProperty("mail.user", "AKIAIF3ZBCTRON6V3DIQ");
    props.setProperty("mail.password", "AsIxcBfQDZjD51OpJi0cBYrKY8ZKiagMtfK1OzidXJ9s");

    Session mailSession = Session.getInstance(props, new javax.mail.Authenticator() {
      protected PasswordAuthentication getPasswordAuthentication() {
          return new PasswordAuthentication("AKIAIF3ZBCTRON6V3DIQ",
              "AsIxcBfQDZjD51OpJi0cBYrKY8ZKiagMtfK1OzidXJ9s");
        }
      });

    try {
      Transport transport = mailSession.getTransport();

      MimeMessage message = new MimeMessage(mailSession);
      message.setSubject(subject, "UTF-8");
      message.setContent("This is an automated email. Do not reply to this address.\n\n" + body,
          (isHtml ? "text/html; charset=utf-8" : "text/plain; charset=utf-8"));
      message.setFrom(new InternetAddress("qa-auto@softonic.com"));
      message.addRecipient(Message.RecipientType.TO, new InternetAddress(recipient));
      if (ccRecipients != null) {
        InternetAddress[] ccAddresses = new InternetAddress[ccRecipients.length];
        for (int i = 0; i < ccRecipients.length; i++) {
          ccAddresses[i] = new InternetAddress(ccRecipients[i]);
        }
        message.addRecipients(Message.RecipientType.CC, ccAddresses);
      }

      transport.connect();
      transport.sendMessage(message, message.getAllRecipients());

      transport.close();
    } catch (Exception e) {
      System.out.println("Unable to send email with subject: " + subject + "\n" + e.getMessage());
    }
  }
}
