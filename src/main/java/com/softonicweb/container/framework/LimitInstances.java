package com.softonicweb.container.framework;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation for restricting the Suite instances a test method supports.
 *
 * @author ivan.solomonoff
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD, ElementType.TYPE })
public @interface LimitInstances {

  String[] restrictTo();
}
