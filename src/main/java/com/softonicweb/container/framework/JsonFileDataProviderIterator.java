package com.softonicweb.container.framework;

import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.Iterator;

import com.softonicweb.container.framework.HttpUtil;

/**
 * Provides data fom a given .json file for the given method.
 * <p>Fist column will be always the line index.
 *
 * @author ivan.solomonoff
 */
public class JsonFileDataProviderIterator extends JsonDataProviderIterator implements
    Iterator<Object> {

  /**
   * Class Constructor to build the Data Provider from a local json file.
   *
   * @param [Method] method
   * @param [String] jsonPath
   */
  public JsonFileDataProviderIterator(Method method, String jsonResourceText) {
    super(method, jsonResourceText);
  }

  /**
   * Class constructor to build the Data Provider from a remote json file.
   *
   * @param [Method] method
   * @param [URL] csvUrl
   * @param [char] delimiter
   * @param [String] commentChar
   * @throws IOException
   */
  public JsonFileDataProviderIterator(Method method, URL jsonUrl) throws IOException {
    super(method, new HttpUtil(jsonUrl.toString()).getSourceAsString());
  }
}
