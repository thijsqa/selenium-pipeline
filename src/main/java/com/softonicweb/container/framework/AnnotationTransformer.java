package com.softonicweb.container.framework;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import org.testng.IAnnotationTransformer;
import org.testng.annotations.ITestAnnotation;

import com.softonicweb.container.framework.ISuiteEnvironment;

/**
 * Implementation of TestNG's annotation transformer.
 *
 * @author ivan.solomonoff
 */
public class AnnotationTransformer implements IAnnotationTransformer  {

  private static final int DEFAULT_TEST_METHOD_TIMEOUT = 180000;

  private final ISuiteEnvironment suiteEnvironment;

  public AnnotationTransformer(ISuiteEnvironment suiteEnvironment) {
    this.suiteEnvironment = suiteEnvironment;
  }

  @Override
  @SuppressWarnings("rawtypes")
  public synchronized void transform(ITestAnnotation annotation, Class testClass,
      Constructor testConstructor, Method testMethod) {
    if (annotation.getTimeOut() == 0) { // timeout not set
      annotation.setTimeOut(DEFAULT_TEST_METHOD_TIMEOUT);
    }

    if (suiteEnvironment.getSuite().getNumRetries() > 0) {
      annotation.setRetryAnalyzer(com.softonicweb.container.framework.RetryAnalyzer.class);
      RetryAnalyzer.setNumRetries(suiteEnvironment.getSuite().getNumRetries());
    }
  }
}
