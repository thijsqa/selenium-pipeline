package com.softonicweb.container.framework;

import java.util.List;

/**
 * SuiteEnvironment interface.
 *
 * @author ivan.solomonoff
 */
public interface ISuiteEnvironment {

  String getReportsDir();

  void setReportsDir(String dir);

  String getScreenshotsDir();

  void setScreenshotsDir(String dir);

  String getTestOutputDir();

  void setTestOutputDir(String dir);

  String getReportsBaseUrl();

  void setReportsBaseUrl(String url);

  String getDownloadsFolder();

  void setDownloadsFolder(String dir);

  String getServerRevision();

  void setServerRevision(String revision);

  String getDetailedReportUrl();

  void setDetailedReportUrl(String url);

  ISuite getSuite();

  void setSuite(ISuite suite);

  Parameters getParameters();

  void setParameters(Parameters parameters);

  List<Instance> getInstances();

  String[] getPlatforms();
}
