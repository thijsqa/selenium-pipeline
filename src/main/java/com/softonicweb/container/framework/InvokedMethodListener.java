package com.softonicweb.container.framework;

import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ITestResult;

import com.softonicweb.container.framework.TestNgUtil;

/**
 * Implementation for IInvokedMethodListener.
 *
 * @author ivan.solomonoff
 */
public class InvokedMethodListener implements IInvokedMethodListener {

  @Override
  public void beforeInvocation(IInvokedMethod method, ITestResult testResult) {
    // nothing to do.
  }

  @Override
  public void afterInvocation(IInvokedMethod method, ITestResult testResult) {
    if ((testResult.getStatus() == ITestResult.FAILURE) && TestNgUtil.hasRetriesLeft(testResult)) {
      // used for us to detect retried tests
      testResult.setStatus(ITestResult.SUCCESS_PERCENTAGE_FAILURE);
    } else if (testResult.getStatus() == ITestResult.SUCCESS) {
      TestNgUtil.resetRetryCount(testResult); // ensure value is cleaned for this thread
    }
  }

}
