package com.softonicweb.container.framework;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;

import net.sf.json.JSON;
import net.sf.json.JSONSerializer;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.htmlparser.Parser;
import org.htmlparser.filters.CssSelectorNodeFilter;
import org.htmlparser.util.NodeList;
import org.htmlparser.util.ParserException;

/**
 * Util class for querying HTTP traffic.
 *
 * @author ivan.solomonoff
 */
public class HttpUtil {

  private HttpURLConnection httpConnection;

  public HttpUtil(String url) {
    try {
      httpConnection = getConnection(url);
      httpConnection.connect();
    } catch (IOException e) {
      throw new RuntimeException(e.getMessage(), e);
    }
  }

  public HttpUtil(String url, String acceptLanguage) {
    try {
      httpConnection = getConnection(url);
      httpConnection.setRequestProperty("Accept-Language", acceptLanguage);
      httpConnection.connect();
    } catch (IOException e) {
      throw new RuntimeException(e.getMessage(), e);
    }
  }

  public HttpUtil(String url, String user, String password, String requestMethod) {
    try {
      httpConnection = getConnection(url);
      httpConnection.setRequestMethod(requestMethod);
      String userpassword = user + ":" + password;
      byte[] encodedAuthorization = Base64.encodeBase64(userpassword.getBytes());
      httpConnection.setRequestProperty("Authorization",
          "Basic " + new String(encodedAuthorization));
    } catch (Exception e) {
      throw new RuntimeException(e.getMessage(), e);
    }
  }

  public HttpUtil(String url, String user, String password, File file) {
    this(url, user, password, "POST");
    try {
      httpConnection.setDoOutput(true); // Triggers POST.
      httpConnection.setRequestProperty("Content-type", "application/octet-stream");
      httpConnection.setRequestProperty("Content-length", String.valueOf(file.length()));
      OutputStream connectionOs = null;
      InputStream fileIs = null;
      try {
        connectionOs = httpConnection.getOutputStream();
        fileIs = new FileInputStream(file);
        IOUtils.copy(fileIs, connectionOs);
      } finally {
        if (connectionOs != null) {
          connectionOs.close();
        }
        if (fileIs != null) {
          fileIs.close();
        }
      }
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  private HttpURLConnection getConnection(String url) {
    try {
      URLEncoder.encode(url, FileUtil.ENCODING);
      URL iurl = new URL(url);
      return (HttpURLConnection) iurl.openConnection();
    } catch (Exception e) {
      throw new RuntimeException(e.getMessage(), e);
    }
  }

  public HttpResource getContentProperties() {
    return new HttpResource(httpConnection.getContentType(), httpConnection.getContentLength(),
        httpConnection.getHeaderField("Server"), getResponseCode());
  }

  public String getCurrentUrl() {
    return httpConnection.getURL().toString();
  }

  public String getResponseMessage() {
    try {
     return httpConnection.getResponseMessage();
    } catch (IOException e) {
      return e.getMessage();
    }
  }

  public String getResponseCode() {
    try {
      return String.valueOf(httpConnection.getResponseCode());
    } catch (IOException e) {
      return e.getMessage();
    }
  }

  public String getSourceAsString() {
    StringBuffer source = new StringBuffer();
    InputStreamReader in;
    try {
      in = new InputStreamReader(getHttpInputStream(), FileUtil.ENCODING);
      BufferedReader r = new BufferedReader(in);
      String line;
      while ((line = r.readLine()) != null) {
        source.append(line);
        source.append("\n");
      }
    } catch (IOException e) {
      throw new RuntimeException("Input/Output Error: " + e.getMessage());
    }

    return source.toString();
  }

  public NodeList getHtmlNodeList() {
    String source = getSourceAsString();
    return getHtmlNodeList(source);
  }

  public NodeList getHtmlNodeList(String source) {
    Parser parser = null;
    try {
      parser = new Parser(source);
      return parser.parse(null);
    } catch (ParserException e) {
      throw new RuntimeException(e);
    }
  }

  public JSON getJson() {
    String source = getSourceAsString();
    return getJson(source);
  }

  public JSON getJson(String source) {
    return JSONSerializer.toJSON(source);
  }

  public static String encodeUrl(String url) {
    try {
      return URLEncoder.encode(url, "ISO-8859-1");
    } catch (UnsupportedEncodingException e) {
      throw new RuntimeException(e);
    }
  }

  public static String decodeUrl(String url) {
    try {
      return URLDecoder.decode(url, "ISO-8859-1");
    } catch (UnsupportedEncodingException e) {
      throw new RuntimeException(e);
    }
  }

  public static NodeList getNodesByCss(NodeList nodeList, String cssLocator) {
    return nodeList.extractAllNodesThatMatch(new CssSelectorNodeFilter(cssLocator), true);
  }

  private boolean isRedirectionNeeded(int status) {
    final int multipleChoicesCode = 300;
    final int temporaryRedirectCode = 307;
    final int reservedCode = 306;
    return (status >= multipleChoicesCode && status <= temporaryRedirectCode
        && status != reservedCode && status != HttpURLConnection.HTTP_NOT_MODIFIED);
  }

  // Due to the JRE's HTTP/HTTPS redirection policy, redirection between HTTP and HTTPS
  // is not automatically followed. Manual redirection must be implemented in these cases.
  private InputStream getHttpInputStream() throws IOException {
     InputStream in = httpConnection.getInputStream();
     if (!isRedirectionNeeded(httpConnection.getResponseCode())) {
       return in;
     }

     HttpURLConnection tmpHttpConnection = httpConnection;
     boolean redir = false;
     int redirects = 0;
     do {
       URL base = tmpHttpConnection.getURL();
       String loc = tmpHttpConnection.getHeaderField("Location");
       URL target = null;
       if (loc != null) {
         target = new URL(base, loc);
       }
       tmpHttpConnection.disconnect();
       // Redirection should should be limited to 5 redirections at most.
      final int redirectionLimit = 5;
      if (target == null || redirects >= redirectionLimit) {
          throw new SecurityException("Illegal URL redirect");
       }

       tmpHttpConnection = (HttpURLConnection) target.openConnection();
       tmpHttpConnection.setInstanceFollowRedirects(false);
       in = tmpHttpConnection.getInputStream();
       if (isRedirectionNeeded(tmpHttpConnection.getResponseCode())) {
         redir = true;
         redirects++;
       }
     } while (redir);
     httpConnection = tmpHttpConnection;
     return in;
  }
}
