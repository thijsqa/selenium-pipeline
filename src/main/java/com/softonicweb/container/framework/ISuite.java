package com.softonicweb.container.framework;

import java.util.List;

/**
 * Suite interface for suite-related properties.
 *
 * @author ivan.solomonoff
 */
public interface ISuite {

  String CONFIG_FILE = "config.json";

  String SUITE_KEY = "suite";

  String NAME_KEY = "name";
  String TIMEOUT_KEY = "timeout";
  String THREADS_KEY = "threads";
  String RETRIES_KEY = "retries";
  String DEVICES_KEY = "devices";

  String ENVIRONMENTS_KEY = "environments";
  String INSTANCE_KEY = "instance";
  String PROJECT_ID_KEY = "projectId";
  String BASE_URL_KEY = "baseUrl";
  String LOCALE_KEY = "locale";

  String DEFAULT_NAME = "Unnamed Suite";
  int DEFAULT_TIMEOUT = 900000;  // 15 minutes
  int DEFAULT_NUM_THREADS = 1;
  int DEFAULT_NUM_RETRIES = 0;
  String DEFAULT_DEVICE = DefaultClient.HTTP.toString();

  String ARRAY_SEPARATOR = ",";

  String getName();

  int getTimeout();

  int getNumThreads();

  int getNumRetries();

  Environment getEnvironment();

  List<Instance> getInstances();
}
