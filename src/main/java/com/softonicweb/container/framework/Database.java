package com.softonicweb.container.framework;

import java.sql.SQLException;
import java.util.List;

/**
 * Provides connectivity towards a specific database.
 *
 * @author alfredo.lopez
 */
public abstract class Database {

  protected final Environment environment;
  protected final String instance;

  public Database(String instance, Environment environment) {
    this.instance = instance;
    this.environment = environment;
  }

  public Database(Environment environment) {
    this.environment = environment;
    instance = null;
  }

  /**
   * Provides a list of restricted environments. These are environments where
   * access to the database is only available from Kitchen.
   *
   * @return list of restricted environments
   */
  public abstract List<Environment> restrictedEnvironments();

  /**
   * Get the database profile for current instance / environment.
   *
   * @return the database Profile
   */
  public abstract Profile getProfile();

  public Connection getConnection() throws SQLException, com.jcraft.jsch.JSchException {
    Connection connection;
    if (restrictedEnvironments().contains(environment)) {
      connection = new TunnelConnection(getProfile());
    } else {
      connection = new DirectConnection(getProfile());
    }
    return connection;
  }

  /**
   * A Database profile storing database name, host, user and password
   * for a given instance and or environment.
   *
   * @author alfredo.lopez
   */
  public static class Profile {
    private final String name;
    private final String host;
    private final String user;
    private final String password;

    public Profile(String databaseName, String host, String user, String password) {
      name = databaseName;
      this.host = host;
      this.user = user;
      this.password = password;
    }

    public String getName() {
      return name;
    }

    public String getHost() {
      return host;
    }

    public String getUser() {
      return user;
    }

    public String getPassword() {
      return password;
    }
  }
}
