package com.softonicweb.container.framework;

/**
 * Account interface.
 *
 * @author ivan.solomonoff
 */
public interface IAccount {

  String getUsername();

  String getEmail();

  String getPassword();

  String getFirstname();

  String getLastname();
}
