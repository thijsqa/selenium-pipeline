package com.softonicweb.container.framework;

import java.io.File;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Hashtable;

import org.apache.log4j.Logger;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.softonicweb.container.framework.Database.Profile;

/**
 * Connection to a database server over an outgoing SSH tunnel connection.
 *
 * @author alfredo.lopez
 */
public class TunnelConnection extends AbstractConnection {

  private static final Logger LOGGER = Logger.getLogger(TunnelConnection.class);

  private static final String LOCAL_HOST = "127.0.0.1";
  private static final int LOCAL_PORT = 3333;

  // Kitchen
  private static final String SERVER_HOST = "172.20.4.45";
  private static final String SERVER_USER = "SOFTONIC+qauser";
  private static final String SERVER_USER_KEY_PATH = System.getProperty("kitchenKey");

  // The mysql port on the remote database server
  private static final int REMOTE_PORT = 3306;

  private final String remoteHost;
  private Session tunnelSession;

  public TunnelConnection(Profile dbProps) throws SQLException, com.jcraft.jsch.JSchException {
    remoteHost = dbProps.getHost();
    try {
      initTunnel();
    } catch (com.jcraft.jsch.JSchException e) {
      close(); // deallocate port to avoid further "cannot be bound" JschExpeption.
      throw new com.jcraft.jsch.JSchException("Secure tunnel connection to '"
          + dbProps.getHost() +  "' failed.", e);
    }

    String jdbcUrl = "jdbc:mysql://" + LOCAL_HOST + ":" + LOCAL_PORT + "/" + dbProps.getName();
    try {
      setConnection(DriverManager.getConnection(jdbcUrl, dbProps.getUser(), dbProps.getPassword()));
    } catch (SQLException e) {
      throw new SQLException("JDBC connection to " + jdbcUrl + " failed.", e);
    }
  }

  @Override
  public void close() {
    super.close();
    closeTunnel();
  }

  public String getLocalHost() {
    return LOCAL_HOST;
  }

  public int getLocalPort() {
    return LOCAL_PORT;
  }

  public String getServerHost() {
    return SERVER_HOST;
  }

  public String getServerUser() {
    return SERVER_USER;
  }

  public String getServerUserKeyPath() {
    return SERVER_USER_KEY_PATH;
  }

  public String getRemoteHost() {
    return remoteHost;
  }

  public int getRemotePort() {
    return REMOTE_PORT;
  }

  private void closeTunnel() {
    try {
      tunnelSession.delPortForwardingL(LOCAL_PORT);
    } catch (com.jcraft.jsch.JSchException e) {
      LOGGER.warn("Deleting fowarding port failed", e);
    } finally {
      tunnelSession.disconnect();
    }
  }

  private void initTunnel() throws com.jcraft.jsch.JSchException {
    if (SERVER_USER_KEY_PATH == null) {
      throw new RuntimeException("System property 'kitchenKey' has not been set.");
    }

    File privateKey = new File(SERVER_USER_KEY_PATH);
    if (!privateKey.exists()) {
      throw new RuntimeException("Private key not found in " + SERVER_USER_KEY_PATH);
    }

    JSch jsch = new JSch();
    jsch.addIdentity(privateKey.getAbsolutePath());
    tunnelSession = jsch.getSession(SERVER_USER, SERVER_HOST);

    // Workaround: we trust Kitchen's identity,
    // otherwise we need to provide the 'known_hosts' file.
    Hashtable<String, String> config = new Hashtable<String, String>();
    config.put("StrictHostKeyChecking", "no");
    tunnelSession.setConfig(config);

    // Try to free up port in case previous tunnel connection was not closed.
    try {
      tunnelSession.delPortForwardingL(LOCAL_PORT);
      LOGGER.warn("Previous forwarding port deleted.");
    } catch (com.jcraft.jsch.JSchException e) {
      LOGGER.info("No previous forwarding port needs to be deleted.");
    }

    tunnelSession.setPortForwardingL(LOCAL_PORT, remoteHost, REMOTE_PORT);
    tunnelSession.connect();
  }
}
