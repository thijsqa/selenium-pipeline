package com.softonicweb.container.framework;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation for setting up a start url into test methods.
 *
 * @author ivan.solomonoff
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD, ElementType.TYPE })
public @interface StartUrl {

  String relativeUrl() default "";

  String subdomain() default "";
}
