package com.softonicweb.container.framework;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Iterator;

import com.softonicweb.container.framework.FileUtil;

/**
 * Provides data fom a given .csv file for the given method.
 * <p>Fist column will be always the line index.
 *
 * @author ivan.solomonoff
 */
public class CsvFileDataProviderIterator extends CsvDataProviderIterator implements
    Iterator<Object> {

  /**
   * Class Constructor to build the Data Provider from a local csv file.
   *
   * @param [Method] method
   * @param [String] csvPath
   * @param [char] delimiter
   * @param [String] commentChar
   * @throws FileNotFoundException
   */
  public CsvFileDataProviderIterator(Method method, InputStreamReader csvContent, char delimiter,
      String commentChar) throws FileNotFoundException {
    super(method, csvContent, delimiter, commentChar);
  }

  /**
   * Class constructor to build the Data Provider from a remote csv file.
   *
   * @param [Method] method
   * @param [URL] csvUrl
   * @param [char] delimiter
   * @param [String] commentChar
   * @throws IOException
   */
  public CsvFileDataProviderIterator(Method method, URL csvUrl, char delimiter, String commentChar)
      throws IOException {
    super(method, new InputStreamReader(csvUrl.openStream(), Charset.forName(FileUtil.ENCODING)),
        delimiter, commentChar);
  }
}
