package com.softonicweb.container.framework;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.log4j.PropertyConfigurator;
import org.testng.TestNG;
import org.testng.annotations.Test;
import org.testng.reporters.XMLReporter;
import org.testng.xml.XmlClass;
import org.testng.xml.XmlInclude;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;

import com.metapossum.utils.scanner.reflect.ClassesInPackageScanner;
import com.softonicweb.container.framework.AnnotationTransformer;
import com.softonicweb.container.framework.AtomicClassRun;
import com.softonicweb.container.framework.LimitInstances;
import com.softonicweb.container.framework.ISuite;
import com.softonicweb.container.framework.ISuiteEnvironment;
import com.softonicweb.container.framework.InvokedMethodListener;
import com.softonicweb.container.framework.SoftonicTestListenerAdapter;
import com.softonicweb.container.framework.HtmlStackTraceReporter;
import com.softonicweb.container.framework.DataGenerator;
import com.softonicweb.container.framework.EmailUtil;
import com.softonicweb.container.framework.FileUtil;

/**
 * Runs TestNG programatically.
 *
 * @author ivan.solomonoff
 */
public final class Runner {

  // private static final Logger LOGGER = Logger.getLogger(Runner.class);

  private static final String CONFIG_FILE = "config.json";
  private static final String TEST_CONFIG_FILE = "test_config.json";
  private static final int JIRA_ID = 4;

  private static final ISuiteEnvironment SUITE_ENVIRONMENT = SuiteEnvironment.getInstance();

  private static Parameters parameters;
  private static String environment;
  private static String instances;

  private static final Map<String, String[]> SUITE_MAPPING;
  static {
    SUITE_MAPPING = new HashMap<String, String[]>();
    SUITE_MAPPING.put("WEB", new String[] { "jenkins-web.softonic.one" });
    SUITE_MAPPING.put("SADS_TRACKER_SERVICE", new String[] { "jenkins-web.softonic.one" });
    SUITE_MAPPING.put("API_WEB", new String[] { "jenkins-mobile.softonic.one" });
    SUITE_MAPPING.put("REDIRECTION_CHECKER", new String[] { "jenkins-mobile.softonic.one" });
    SUITE_MAPPING.put("FDH", new String[] { "jenkins-ads.softonic.one" });
    SUITE_MAPPING.put("SADS", new String[] { "jenkins-ads.softonic.one" });
    SUITE_MAPPING.put("DISPLAY_ADS", new String[] { "jenkins-ads.softonic.one" });
    SUITE_MAPPING.put("QA_GRID", new String[] { "qa-grid.sofonic.one" });
    SUITE_MAPPING.put("SAFE_URLSv4", new String[] { "jenkins-eng.softonic.one" });
    SUITE_MAPPING.put("APPCRAWLR", new String[] { "jenkins-eng.softonic.one" });
    SUITE_MAPPING.put("API_APPCRAWLR", new String[] { "jenkins-eng.softonic.one" });
    SUITE_MAPPING.put("GLOBAL_REDIRECTER", new String[] { "jenkins-eng.softonic.one" });
    SUITE_MAPPING.put("WEBSEARCH", new String[] { "jenkins-eng.softonic.one" });
    SUITE_MAPPING.put("MOBILE", new String[] { "jenkins-mobile.softonic.one" });
    SUITE_MAPPING.put("XROBOTS", new String[] { "jenkins-eng.softonic.one" });
    SUITE_MAPPING.put("STATUSCODE", new String[] { "jenkins-eng.softonic.one" });
    SUITE_MAPPING.put("DENSITY", new String[] { "jenkins-ads.softonic.one" });
    SUITE_MAPPING.put("SHEETDENSITY", new String[] { "jenkins-ads.softonic.one" });
  }

  private Runner() {
  }

  /** Main-entry class when executing from JARs */
  public static void main(String[] args) {
    execute();
  }

  /** Entry when executing from the modules (IDE) */
  public static void execute() {
    setupLogger();

    Properties props = Runner.loadProperties("environment.properties");
    String branch = props.getProperty("environment.branch");
    boolean isMaster = branch.equals("MASTER");
    String configFile = isMaster ? CONFIG_FILE : TEST_CONFIG_FILE;

    setupSuiteEnvironment(configFile, isMaster);

    boolean isTestPassed = runTests();

    // Add -DnoEmail to skip emailing reports
    if (parameters.getNoEmail() == null) {
      sendMail(isTestPassed, isMaster, branch);
    }

    System.exit(0);
  }

  public static void setupSuiteEnvironment(String configFile, boolean isMaster) {
    String config = FileUtil.getResource(configFile);

    parameters = new Parameters(config, isMaster);
    environment = parameters.getEnvironment().toLowerCase();
    instances = parameters.getInstance().toLowerCase();

    ISuite suite = new Suite(config, environment, instances, parameters.getBaseUrl());
    SUITE_ENVIRONMENT.setSuite(suite);
    SUITE_ENVIRONMENT.setParameters(parameters);

    String timestamp = DataGenerator.getTimestamp();
    String jenkinsBaseUrl = getJenkinsUrl();
    String baseReportsDir = parameters.getBaseReportsDir();
    String testReportsDir = baseReportsDir + "/" + suite.getName() + "/"
        + environment + "/" + timestamp + "/";
    new File(testReportsDir).mkdirs();
    SUITE_ENVIRONMENT.setReportsDir(testReportsDir);

    String reportsHost = isMaster ? "http://" + jenkinsBaseUrl + "/job/mnt/ws/reports"
        : baseReportsDir;
    String reportsBaseUrl =
        reportsHost + testReportsDir.split("reports")[1].replaceAll("\\\\", "/");

    SUITE_ENVIRONMENT.setReportsBaseUrl(reportsBaseUrl);
    SUITE_ENVIRONMENT.setScreenshotsDir(testReportsDir + "screenshots");
    SUITE_ENVIRONMENT.setTestOutputDir(baseReportsDir + "/test_data");
    SUITE_ENVIRONMENT.setDownloadsFolder(testReportsDir + "downloads");
    SUITE_ENVIRONMENT.setDetailedReportUrl(reportsBaseUrl
        + HtmlStackTraceReporter.DETAILED_REPORT_FILENAME);
  }

  private static String getJenkinsUrl() {
    String suiteName = String.valueOf(SUITE_ENVIRONMENT.getSuite().getName());
    String[] jenkinsUrl = SUITE_MAPPING.get(suiteName);
    if (jenkinsUrl == null) {
      throw new RuntimeException("No data for instance: " + suiteName);
    }
    String url = jenkinsUrl[0];
    if (jenkinsUrl.length > 1) {
      url += jenkinsUrl[1];
    }
    if (SUITE_ENVIRONMENT.getParameters().getExecution().equals("DISPLAY")
        || SUITE_ENVIRONMENT.getParameters().getExecution().equals("CONTEXTUAL")
        || SUITE_ENVIRONMENT.getParameters().getExecution().equals("DENSITY")
        || SUITE_ENVIRONMENT.getParameters().getExecution().equals("SHEETDENSITY")) {
      url = "jenkins-ads.softonic.one";
    }
    return url;
  }

  public static void setupLogger() {
    Properties props = loadProperties("log4j.properties");
    PropertyConfigurator.configure(props);
  }

  public static void sendMail(boolean isTestPassed, boolean isMaster, String branch) {
    String recipient = "qa-auto@softonic.com";
    String result = isTestPassed ? " [OK]" : " [KO]";
    String subject = new String();
    String suiteName = SUITE_ENVIRONMENT.getSuite().getName();
    String device = Arrays.toString(SUITE_ENVIRONMENT.getPlatforms());
    String serverHost = SUITE_ENVIRONMENT.getParameters().getSeleniumHost();
    if (SUITE_ENVIRONMENT.getParameters().getExecution().equals("VOLATILE")) {
      String volatileUrl = SUITE_ENVIRONMENT.getParameters().getBaseUrl();
      String volatileId =
          volatileUrl.substring(volatileUrl.indexOf("com.") + JIRA_ID,
              volatileUrl.indexOf(".noodle"));
      subject =
          "Automated " + suiteName + " tests in volatile" + " "
              + volatileId.toUpperCase(Locale.ENGLISH) + " "
              + instances.toUpperCase(Locale.ENGLISH) + " " + device + result;
    } else if (SUITE_ENVIRONMENT.getParameters().getExecution().equals("VOLATILE_SOLUTIONS")) {
      String solutionsVolatileUrl = SUITE_ENVIRONMENT.getParameters().getBaseUrl();
      String solutionsVolatileId =
          solutionsVolatileUrl.substring(solutionsVolatileUrl.indexOf("p://") + JIRA_ID,
              solutionsVolatileUrl.indexOf(".solutions"));
      subject =
          "Automated " + suiteName + " tests in volatile" + " "
              + solutionsVolatileId.toUpperCase(Locale.ENGLISH) + " "
              + instances.toUpperCase(Locale.ENGLISH) + " " + device + result;
    } else if (SUITE_ENVIRONMENT.getParameters().getExecution().equals("24x7")) {
      // Fetch the Git Tag Version from production
      String revision = RevisionFetcher.fetchGitTagVersion();
      subject =
          "Automated " + suiteName + " tests in " + environment.toUpperCase(Locale.ENGLISH) + " "
              + instances.toUpperCase(Locale.ENGLISH) + " " + serverHost + " " + device + result
              + revision;
    } else {
      subject =
          "Automated " + suiteName + " tests in " + environment.toUpperCase(Locale.ENGLISH) + " "
              + instances.toUpperCase(Locale.ENGLISH) + " " + device + result;
    }

    String bodyFile = SUITE_ENVIRONMENT.getReportsDir() + "/emailable-report.html";
    String body = FileUtil.getTextFromFile(new File(bodyFile));

    String[] ccRecipients = null;
    String qaTestEmailList = "qa-test@softonic.com";

    String emailRecipients = parameters.getEmailTo();
    if (!emailRecipients.isEmpty()) {
      ccRecipients =
          (String[]) ArrayUtils.add(emailRecipients.split(ISuite.ARRAY_SEPARATOR), qaTestEmailList);
    } else {
      ccRecipients = new String[] { qaTestEmailList };
    }

    if (!isMaster) { // devel
      subject = branch.toUpperCase(Locale.ENGLISH) + ": " + subject;
    }
    EmailUtil.sendEmail(recipient, ccRecipients, subject, body, true);
  }

  public static Properties loadProperties(String propertyFileName) {
    Properties props = new Properties();
    try {
      props.load(ClassLoader.getSystemResourceAsStream(propertyFileName));
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return props;
  }

  private static boolean runTests() {
    ISuite suite = SUITE_ENVIRONMENT.getSuite();
    List<XmlSuite> suites = new ArrayList<XmlSuite>();
    suites.add(createXmlSuite(suite));

    TestNG testng = new TestNG();
    testng.setXmlSuites(suites);
    testng.setUseDefaultListeners(false);

    // Provides the test result information to be collected by Jenkins.
    XMLReporter xmlReporter = new XMLReporter();
    xmlReporter.setOutputDirectory(".");
    testng.addListener(xmlReporter);

    // Provides the custom EmailableReporter and the StackTraceReported handling retries
    testng.addListener(new SoftonicTestListenerAdapter());

    // Auto-sets test timeout and retry if Suite has retries > 0.
    testng.addListener(new AnnotationTransformer(SUITE_ENVIRONMENT));
    // Sets proper test status based on the retries left
    testng.addListener(new InvokedMethodListener());

    testng.setOutputDirectory(SUITE_ENVIRONMENT.getReportsDir());
    testng.run();

    boolean isPassed = (System.getProperty("failed.tests") != null)
        ? (System.getProperty("failed.tests").equals("0")
            && (Integer.parseInt(System.getProperty("passed.tests")) > 0))
            : false;

        return isPassed;
  }

  private static XmlSuite createXmlSuite(ISuite singleSuite) {
    final int verboseAndHrmNumberOfthreads = 3;
    XmlSuite suite = new XmlSuite();
    suite.setName("Softonic QA - " + singleSuite.getName() + " tests");
    suite.setVerbose(verboseAndHrmNumberOfthreads);
    suite.setParallel("methods");

    final int suiteThreadCount = singleSuite.getNumThreads();
    final int mediumGridThreadCount = 9;
    suite.setDataProviderThreadCount(suiteThreadCount);
    // we run medium grid instance with maximum 9 threads
    if (SuiteEnvironment.getInstance().getParameters().getSeleniumHost().equals("EU_Frankfurt")
        || SuiteEnvironment.getInstance().getParameters().getSeleniumHost()
            .equals("Asia_Southeast_Singapore")) {
      suite.setThreadCount(mediumGridThreadCount);
    } else {
      suite.setThreadCount(suiteThreadCount);
    }
    suite.setTimeOut(String.valueOf(singleSuite.getTimeout()));

    suite.setConfigFailurePolicy("continue");

    List<XmlClass> xmlClasses = null;
    String className = parameters.getClassName();
    if (!className.isEmpty()) {
      xmlClasses = getClassesFromArguments(className);
    } else {
      xmlClasses = getTestClasses();
    }

    createSuiteXmlTests(suite, xmlClasses, singleSuite);

    // It is actually useful to have the TestNG XML Manifest printed to the console.
    System.out.println(suite.toXml());

    return suite;
  }

  @SuppressWarnings("unchecked")
  private static List<XmlTest> createSuiteXmlTests(XmlSuite suite, List<XmlClass> xmlClasses,
      ISuite singleSuite) {
    Map<AtomicClassRun, List<XmlClass>> annotatedXmlClassesMap =
        new HashMap<AtomicClassRun, List<XmlClass>>();
    List<XmlClass> nonAnnotatedXmlClasses = new LinkedList<XmlClass>();

    // Distribute classes in a map according to its AtomicTestRun annotation
    for (XmlClass xmlClass : xmlClasses) {
      AtomicClassRun annotation =
          (AtomicClassRun) xmlClass.getSupportClass().getAnnotation(AtomicClassRun.class);
      if (annotation == null) {
        nonAnnotatedXmlClasses.add(xmlClass);
      } else {
        if (!annotatedXmlClassesMap.containsKey(annotation)) {
          annotatedXmlClassesMap.put(annotation, new LinkedList<XmlClass>());
        }
        annotatedXmlClassesMap.get(annotation).add(xmlClass);
      }
    }

    return createSortedXmlTestList(
        annotatedXmlClassesMap, nonAnnotatedXmlClasses, suite, singleSuite);
  }

  private static List<AtomicClassRun> sortAnnotations(Set<AtomicClassRun> annotationsSet) {
    Comparator<AtomicClassRun> comparator = new Comparator<AtomicClassRun>() {
      @Override
      public int compare(AtomicClassRun annotation1, AtomicClassRun annotation2) {
        return annotation1.priority() - annotation2.priority();
      }
    };

    List<AtomicClassRun> annotations = new LinkedList<AtomicClassRun>(annotationsSet);
    Collections.sort(annotations, comparator);
    return Collections.unmodifiableList(annotations);
  }

  private static List<XmlTest> createSortedXmlTestList(
      Map<AtomicClassRun, List<XmlClass>> annotatedXmlClassesMap,
      List<XmlClass> nonAnnotatedXmlClasses, XmlSuite xmlSuite, ISuite suite) {
    // XmlTests are sorted first according to the priority stated in the annotation.
    // Not annotated tests are added at the end in a separate XmlTest
    // (this last XmlTest is named like the suite)
    List<XmlTest> sortedXmlTests = new LinkedList<XmlTest>();
    List<AtomicClassRun> sortedAnnotations = sortAnnotations(annotatedXmlClassesMap.keySet());
    for (AtomicClassRun annotation : sortedAnnotations) {
      // Configure and add a new XmlTest for each map entry following the given order
      List<XmlClass> annotatedXmlClasses = annotatedXmlClassesMap.get(annotation);
      XmlTest annotatedXmlTest = createSuiteXmlTest(
          xmlSuite, suite, annotation, annotatedXmlClasses);
      sortedXmlTests.add(annotatedXmlTest);
    }

    // Configure and add the default XmlTest as the last
    // (this XmlTest contains classes that were not annotated with 'AtomicClassRun')
    XmlTest nonAnnotatedXmlTest = createSuiteXmlTest(xmlSuite, suite, null, nonAnnotatedXmlClasses);
    sortedXmlTests.add(nonAnnotatedXmlTest);
    return sortedXmlTests;
  }

  private static boolean belongsToTargetExecution(Method m, String executions) {
    if (!executions.isEmpty()) {
      String[] groupArray = m.getAnnotation(Test.class).groups();
      return isAnyTestGroupInExecutions(groupArray, executions);
    } else {
      return true;
    }
  }

  private static boolean isAnyTestGroupInExecutions(String[] groups, String executions) {
    List<String> executionList = Arrays.asList(executions.toLowerCase().split(","));
    List<String> groupList = new ArrayList<String>();
    for (String group : groups) {
      groupList.add(group.toLowerCase());
    }
    return CollectionUtils.containsAny(groupList, executionList);
  }

  private static XmlTest createSuiteXmlTest(XmlSuite xmlSuite, ISuite suite,
      AtomicClassRun annotation, List<XmlClass> xmlClasses) {
    XmlTest xmlTest = new XmlTest(xmlSuite);
    xmlTest.setName(getXmlTestName(annotation, suite.getName()));

    if (!parameters.getTestName().isEmpty()) {
      xmlTest.setBeanShellExpression(
          "String[] testNames = System.getProperty(\"test.name\").split(\",\");"
              + "return java.util.Arrays.asList(testNames).contains(method.getName());");
    } else {
      String executions = parameters.getExecution();
      // Exclude methods with limited instances that do not apply to this run.
      if (!instances.isEmpty()) {
        for (XmlClass xmlClass : xmlClasses) {
          List<XmlInclude> xmlInclude = new ArrayList<XmlInclude>();
          List<String> xmlExclude = new ArrayList<String>();
          List<String> instancesToRun =
              Arrays.asList(instances.toUpperCase(Locale.ENGLISH).split(","));
          Class<?> testClass = xmlClass.getSupportClass();
          if (testClass.isAnnotationPresent(LimitInstances.class)) {
            String[] restrictedToArray = testClass.getAnnotation(LimitInstances.class).restrictTo();
            List<String> restrictedTo = new ArrayList<String>();
            for (String restInstance : restrictedToArray) {
              restrictedTo.add(restInstance.toUpperCase(Locale.ENGLISH));
            }
            for (Method m : xmlClass.getSupportClass().getMethods()) {
              if (m.isAnnotationPresent(Test.class) && belongsToTargetExecution(m, executions)) {
                if (CollectionUtils.containsAny(restrictedTo, instancesToRun)) {
                  xmlInclude.add(new XmlInclude(m.getName()));
                } else {
                  xmlExclude.add(m.getName());
                }
              }
            }
          } else {
            for (Method m : xmlClass.getSupportClass().getMethods()) {
              if (m.isAnnotationPresent(Test.class) && belongsToTargetExecution(m, executions)) {
                if (m.isAnnotationPresent(LimitInstances.class)) {
                  List<String> restrictedTo =
                      Arrays.asList(m.getAnnotation(LimitInstances.class).restrictTo());
                  if (CollectionUtils.containsAny(restrictedTo, instancesToRun)) {
                    xmlInclude.add(new XmlInclude(m.getName()));
                  } else {
                    xmlExclude.add(m.getName());
                  }
                } else {
                  xmlInclude.add(new XmlInclude(m.getName()));
                }
              }
            }
          }
          xmlClass.setIncludedMethods(xmlInclude);
          xmlClass.setExcludedMethods(xmlExclude);
        }
      }

      if (!executions.isEmpty()) {
        for (String execution : executions.split(",")) {
          xmlTest.addIncludedGroup(execution);
        }
      }
    }
    xmlTest.setClasses(xmlClasses);

    return xmlTest;
  }

  private static String getXmlTestName(AtomicClassRun annotation, String defaultName) {
    if (annotation == null) {
      return defaultName;
    }

    return annotation.name() + annotation.priority();
  }

  private static List<XmlClass> getClassesFromArguments(String className) {
    List<XmlClass> classes = new ArrayList<XmlClass>();
    String[] classArray = className.split(",");
    for (String classStr : classArray) {
      XmlClass singleClass = new XmlClass(classStr);
      classes.add(singleClass);
    }

    return classes;
  }

  private static List<XmlClass> getTestClasses() {
    List<XmlClass> classes = new ArrayList<XmlClass>();
    String basePackage = "com.softonic";
    Class<?>[] classArray = findClassesRecursively(basePackage);
    for (Class<?> className : classArray) {
      if (className.toString().endsWith("Test")) {
        XmlClass singleClass = new XmlClass(className);
        classes.add(singleClass);
      }
    }

    return classes;
  }

  private static Class<?>[] findClassesRecursively(String fromPackage) {
    Set<Class<?>> classesSet;
    try {
      classesSet = new ClassesInPackageScanner().scan(fromPackage);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }

    ArrayList<Class<?>> testClassesList = new ArrayList<Class<?>>();
    Iterator<Class<?>> it = classesSet.iterator();
    while (it.hasNext()) {
      Class<?> c = it.next();
      if (isTestAnnotationPresentInClass(c) || isTestAnnotationPresentInMethods(c)) {
        testClassesList.add(c);
      }
    }

    Class<?>[] classes = testClassesList.toArray(new Class<?>[testClassesList.size()]);

    return classes;
  }

  private static boolean isTestAnnotationPresentInClass(Class<?> testClass) {
    return testClass.isAnnotationPresent(org.testng.annotations.Test.class)
        || testClass.isAnnotationPresent(org.testng.annotations.Factory.class);
  }

  private static boolean isTestAnnotationPresentInMethods(Class<?> testClass) {
    for (Method method : testClass.getMethods()) {
      if ((method.isAnnotationPresent(org.testng.annotations.Test.class))
          || method.isAnnotationPresent(org.testng.annotations.Factory.class)) {
        return true;
      }
    }
    return false;
  }
}
