package com.softonicweb.container.framework;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Softonic2 database.
 *
 * @author alfredo.lopez
 */
public class Softonic2Db extends Database {

  private static final Environment[] RESTRICTED_ENVIRONMENTS = { Environment.PRODUCTION };

  private static final Map<String, Profile> PRODUCTION_PROPERTIES = new HashMap<String, Profile>();
  static {
    PRODUCTION_PROPERTIES.put("BR", new Profile(
        "softonic2pt", "brrepdb0.ext.bcn.softonic.lan", "bruserr", "Pgyh43F34"));
    PRODUCTION_PROPERTIES.put("DE", new Profile(
        "softonic2de", "derepdb0.ext.bcn.softonic.lan", "deuserr", "sdas7Glws"));
    PRODUCTION_PROPERTIES.put("EN", new Profile(
        "softonic2en", "enrepdb0.ext.bcn.softonic.lan", "enuser1_01", "bmcvn3490"));
    PRODUCTION_PROPERTIES.put("ES", new Profile(
        "softonic2", "esrepdb0.ext.bcn.softonic.lan", "esuserr", "kdfjwei83w"));
    PRODUCTION_PROPERTIES.put("FR", new Profile(
        "softonic2fr", "frrepdb0.ext.bcn.softonic.lan", "fruser1", "jdsjYws"));
    PRODUCTION_PROPERTIES.put("IT", new Profile(
        "softonic2it", "itrepdb0.ext.bcn.softonic.lan", "ituser1", "jd151ws"));
    PRODUCTION_PROPERTIES.put("JP", new Profile(
        "softonic2jp", "jprepdb0.ext.bcn.softonic.lan", "jpuserr", "Gr5Se4In3Up2Sh1MoNw"));
    PRODUCTION_PROPERTIES.put("NL", new Profile(
        "softonic2nl", "nlrepdb0.ext.bcn.softonic.lan", "nluserr", "cAFRdvEvIONNdA"));
    PRODUCTION_PROPERTIES.put("PL", new Profile(
        "softonic2pl", "plrepdb0.ext.bcn.softonic.lan", "pluser2", "dFs4t69"));
  }

  private static final Map<String, Profile> QA_PROPERTIES = new HashMap<String, Profile>();
  static {
    QA_PROPERTIES.put("BR", new Profile(
        "softonic2pt", "brdbw01st.ofi.softonic.lan", "bruserr", "Pgyh43F34"));
    QA_PROPERTIES.put("DE", new Profile(
        "softonic2de", "dedbw01st.ofi.softonic.lan", "deuserr", "sdas7Glws"));
    QA_PROPERTIES.put("EN", new Profile(
        "softonic2en", "endbw01st.ofi.softonic.lan", "enuser1_01", "bmcvn3490"));
    QA_PROPERTIES.put("ES", new Profile(
        "softonic2", "esdbw01st.ofi.softonic.lan", "esuserr", "kdfjwei83w"));
    QA_PROPERTIES.put("FR", new Profile(
        "softonic2fr", "frdbw01st.ofi.softonic.lan", "fruser1", "jdsjYws"));
    QA_PROPERTIES.put("IT", new Profile(
        "softonic2it", "itdbw01st.ofi.softonic.lan", "ituser1", "jd151ws"));
    QA_PROPERTIES.put("JP", new Profile(
        "softonic2jp", "jpdbw01st.ofi.softonic.lan", "jpuserr", "Gr5Se4In3Up2Sh1MoNw"));
    QA_PROPERTIES.put("NL", new Profile(
        "softonic2nl", "nldbw01st.ofi.softonic.lan", "nluserr", "cAFRdvEvIONNdA"));
    QA_PROPERTIES.put("PL", new Profile(
        "softonic2pl", "pldbw01st.ofi.softonic.lan", "pluser2", "dFs4t69"));
  }

  private Profile profile;

  public Softonic2Db(String instance, Environment environment) {
    super(instance, environment);
  }

  @Override
  public Profile getProfile() {
    if (profile == null) {
      switch (environment) {
        case PRODUCTION:
          profile = PRODUCTION_PROPERTIES.get(instance);
          break;
        case STAGING:
          profile = QA_PROPERTIES.get(instance);
          break;
      case INTEGRATION:
      case DEVELOPMENT:
        default:
          throw new UnsupportedOperationException();
      }
    }

    return profile;
  }

  @Override
  public List<Environment> restrictedEnvironments() {
    return Arrays.asList(RESTRICTED_ENVIRONMENTS);
  }
}
