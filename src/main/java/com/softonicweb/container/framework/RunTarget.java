package com.softonicweb.container.framework;

/**
 * The target of the RemoteWebDriver session.
 *
 * @author ivan.solomonoff
 */
public enum RunTarget {
  EU_FRANKFURT("35.158.65.233", "4444"),
  US_WEST_OREGON("35.166.39.78", "4444"),
  ASIA_SOUTHEAST_SINGAPORE("18.136.191.235", "4444"),
  SAUCE_LABS("saucelabs", ""),
  BROWSERSTACK("browserstack", ""),
  LOCALHOST("localhost", "4444"),
  TEST_VM("172.20.x.x", "5555");

  private final String host;
  private final String port;

  private RunTarget(String host, String port) {
    this.host = host;
    this.port = port;
  }

  public String getPort() {
    return port;
  }

  public String getHost() {
    return host;
  }
}
