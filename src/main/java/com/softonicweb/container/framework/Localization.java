package com.softonicweb.container.framework;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Contains data related to i18n.
 *
 * @author ivan.solomonoff
 */
public final class Localization {

  public static final Locale EN = new Locale("en", "EN");
  public static final Locale ES = new Locale("es", "ES");
  public static final Locale FR = Locale.FRANCE;
  public static final Locale IT = Locale.ITALY;
  public static final Locale DE = Locale.GERMANY;
  public static final Locale BR = new Locale("pt_br", "BR");
  public static final Locale PL = new Locale("pl", "PL");
  public static final Locale NL = new Locale("nl", "NL");
  public static final Locale JP = Locale.JAPAN;
  public static final Locale CN = new Locale("zh-cn", "CN");
  public static final Locale RU = new Locale("ru_RU", "RU");
  public static final Locale TR = new Locale("tr_TR", "TR");
  public static final Locale KO = Locale.KOREA;
  public static final Locale AR = new Locale("ar", "AR");
  public static final Locale ID = new Locale("id_ID", "ID");
  public static final Locale VI = new Locale("vi", "VI");
  public static final Locale TH = new Locale("th", "TH");

  /**
   * List of available countries so that can be used in a switch statement.
   */
  public static enum Country {
    EN, ES, FR, IT, DE, BR, PL, NL, JP, CN, RU, TR, KO, AR, ID, VI, TH;

    public Locale toLocale() {
      switch (this) {
      case EN:
        return Localization.EN;
      case BR:
        return Localization.BR;
      case CN:
        return Localization.CN;
      case DE:
        return Localization.DE;
      case ES:
        return Localization.ES;
      case FR:
        return Localization.FR;
      case IT:
        return Localization.IT;
      case JP:
        return Localization.JP;
      case NL:
        return Localization.NL;
      case PL:
        return Localization.PL;
      case RU:
        return Localization.RU;
      case TR:
        return Localization.TR;
      case KO:
        return Localization.KO;
      case AR:
        return Localization.AR;
      case ID:
        return Localization.ID;
      case VI:
        return Localization.VI;
      case TH:
        return Localization.TH;
      default:
        throw new IllegalArgumentException("Unsupported country: " + toString());
      }
    }
  };

  private static final Map<String, Locale> STRING_TO_LOCALE = new HashMap<String, Locale>();
  static {
    for (Country country : Country.values()) {
      STRING_TO_LOCALE.put(country.toString(), country.toLocale());
    }
  }

  private Localization() {
    // prevents construction outside of this class.
  }

  public static Locale fromString(String country) {
    return STRING_TO_LOCALE.get(country.toUpperCase(Locale.ENGLISH));
  }
}
