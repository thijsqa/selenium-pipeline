package com.softonicweb.container.framework;


/**
 * Stores information related to Mobile devices.
 *
 * @author ivan.solomonoff
 */
public enum MobileDevices implements IPlatform {
  ANDROID_6_0_1("Samsung Galaxy S6 Emulator", "Android", "6.0", "Samsung", "Galaxy S 6",
      "Mozilla/5.0 (Linux; Android 6.0.1; SM-G920V Build/MMB29K) AppleWebKit/537.36 "
      + "(KHTML, like Gecko) Chrome/52.0.2743.98 Mobile Safari/537.36"),
  ANDROID("Android", "Android", "", "HTC", "Desire",
      "Mozilla/5.0 (Linux; U; Android 2.2; en-us; Nexus One Build/FRF91) AppleWebKit/533.1 "
          + "(KHTML, like Gecko) Version/4.0 Mobile Safari/533.1"),
  ANDROID_4_4_PHONE("LG Nexus 4 Emulator", "Android", "4.4", "LG", "Nexus 4",
      "Mozilla/5.0 (Linux; Android 4.4.2; en-us; Nexus 4 Build/KOT49H) AppleWebKit/535.19 "
          + "(KHTML, like Gecko) Chrome/18.0.1025.166 Mobile Safari/535.19"),
  ANDROID_4_4_TABLET("Google Nexus 7 HD Emulator", "Android", "4.4", "Google", "Nexus 7 HD",
      "Mozilla/5.0 (Linux; U; Android 4.4.4; en-us; Nexus 7 Build/KTU84P) AppleWebKit/537.16 "
          + "(KHTML, like Gecko) Version/4.0 Safari/537.16"),
  ANDROID_4_3_PHONE("Samsung Galaxy S3 Emulator", "Android", "4.3", "Samsung", "Galaxy S 3",
      "Mozilla/5.0 (Linux; U; Android 4.3; nl-nl; SAMSUNG GT-I9300/I9300XXUGML2 Build/JSS15J) "
          + "AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30"),
  ANDROID_4_3_TABLET("Google Nexus 7C Emulator", "Android", "4.3", "Google", "Nexus 7C",
      "Mozilla/5.0 (Linux; U; Android 4.3; en-us; Galaxy Nexus Build/ICL53F) AppleWebKit/534.30"
          + " (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30"),
  ANDROID_4_2_PHONE("Samsung Galaxy Nexus Emulator", "Android", "4.2", "Samsung", " Galaxy Nexus",
      "Mozilla/5.0 (Linux; U; Android 4.2; en-us; Galaxy Nexus Build/ICL53F) AppleWebKit/534.30"
          + " (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30"),
  ANDROID_4_2_TABLET("Samsung Galaxy Tab 3 Emulator", "Android", "4.2", "Samsung", "Galaxy Tab 3",
       "Mozilla/5.0 (Linux; U; Android 4.2.2; en-us; GT-P5210 Build/JDQ39) AppleWebKit/534.30"
           + " (KHTML, like Gecko) Version/4.0 Safari/534.30"),
  IPAD_IOS_7("iPad Simulator", "iOS", "7.0", "Apple", "iPad",
      "Mozilla/5.0 (iPhone; CPU iPhone OS 5_0_1 like Mac OS X) AppleWebKit/534.46 (KHTML, like "
          + "Gecko) Version/5.1 Mobile/9A405 Safari/7534.48.3"),
  IPAD_IOS_6("iPad Simulator", "iOS", "6.0", "Apple", "iPad",
      "Mozilla/5.0 (iPhone; CPU iPhone OS 6_0_1 like Mac OS X) AppleWebKit/534.46 (KHTML, like "
          + "Gecko) Version/5.1 Mobile/9A405 Safari/7534.48.3"),
  IPHONE_IOS_4("iPhone Simulator", "iOS", "4.2.1", "Apple", "iPhone",
      "Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_2_1 like Mac OS X; en-us) AppleWebKit/533.17.9 "
          + "(KHTML, like Gecko) Version/5.0.2 Mobile/8C148 Safari/6533.18.5"),
  IPHONE_IOS_7("iPhone Simulator", "iOS", "7.0", "Apple", "iPhone",
      "Mozilla/5.0 (iPhone; CPU iPhone OS 7_0_2 like Mac OS X) AppleWebKit/537.51.1 "
          + "(KHTML, like Gecko) Version/7.0 Mobile/11A4449d Safari/9537.53"),
  IPHONE_IOS_6("iPhone Simulator", "iOS", "6", "Apple", "iPhone",
      "Mozilla/5.0 (iPhone; CPU iPhone OS 6_0_1 like Mac OS X) AppleWebKit/534.46 (KHTML, like "
          + "Gecko) Version/5.1 Mobile/9A405 Safari/7534.48.3"),
  BLACKBERRY("Blackberry", "Blackberry", "", "Blackberry", "9330",
      "Mozilla/5.0 (BlackBerry; U; BlackBerry 9900; en-US) AppleWebKit/534.11+ (KHTML, like "
          + "Gecko) Version/7.0.0 Mobile Safari/534.11+"),
  SYMBIAN("Symbian", "Symbian", "", "Nokia", "Asha 311",
      "Mozilla/5.0 (SymbianOS/9.4; U; Series60/5.0 Nokia5800d-1/21.0.025; Profile/MIDP-2.1 "
          + "Configuration/CLDC-1.1 ) AppleWebKit/413 (KHTML, like Gecko) Safari/413"),
  WINPHONE7("Windows Phone", "Windows Phone", "", "", "",
      "Mozilla/4.0 (compatible; MSIE 7.0; Windows Phone OS 7.0; Trident/3.1; IEMobile/7.0) "
          + "Asus;Galaxy6"),
  OTHER("Windows Mobile", "Windows Mobile", "", "", "",
      "Mozilla/4.0(compatible; MSIE 6.0; Windows NT 5.1; Motorola_ES405B_19103; Windows Phone "
          + "6.5.3.5)"),
  DEFAULT("", "", "", "", "", "");

  private String userAgent;
  private String deviceName;
  private String os;
  private String brand;
  private String model;
  private String osVersion;

  private MobileDevices(String deviceName, String os, String osVersion, String brand, String model,
      String userAgent) {
    this.userAgent = userAgent;
    this.deviceName = deviceName;
    this.os = os;
    this.osVersion = osVersion;
    this.brand = brand;
    this.model = model;
  }

  public String getDeviceName() {
    return deviceName;
  }

  public String getOs() {
    return os;
  }

  public String getOsVersion() {
    return osVersion;
  }

  public String getBrand() {
    return brand;
  }

  public String getModel() {
    return model;
  }

  public String getUserAgent() {
    return userAgent;
  }

  public String getDeviceUrlString() {
    String url = this.os;
    if (this.os.equals("iOS")) {
      url = "iphone";
    } else if (os.equals("Windows Phone")) {
      url = "windows-phone-7";
    }
    return url.toLowerCase();
  }
}
