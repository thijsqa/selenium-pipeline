package com.softonicweb.container.framework;

import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.LineNumberReader;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.imageio.ImageIO;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.BOMInputStream;
import org.apache.log4j.Logger;

import com.softonicweb.container.framework.OSValidator;
import com.softonicweb.container.framework.WaitUtil.WaitCondition;

/**
 * Contains helper methods for handling files and file content.
 *
 * @author ivan.solomonoff
 */
public final class FileUtil {

  public static final String ENCODING = "UTF-8";

  private static final Logger LOGGER = Logger.getLogger(FileUtil.class);
  private static final char[] ILLEGAL_CHARACTERS = {
    '/', '\n', '\r', '\t', '\0', '\f', '`', '?', '*', '\\', '<', '>', '|', '\"', ':'
  };
  private static final String WINDOWS_TEMP_FOLDER = "c:\\Temp\\";

  private FileUtil() {

  }

  public static String replaceInvalidChars(String fileName) {
    for (char c : ILLEGAL_CHARACTERS) {
      fileName = fileName.replace(c, '-');
    }

    return fileName;
  }

  public static String cleanupStringNewLines(String dirtyString) {
    return dirtyString.replace("\r", "").trim();
  }

  public static void writeFile(String filePath, String text) {
    try {
      File file = new File(filePath);
      new File(file.getParent()).mkdirs();
      FileOutputStream fileos = new FileOutputStream(file);
      OutputStreamWriter output = new OutputStreamWriter(fileos, ENCODING);
      BufferedWriter writer = new BufferedWriter(output);
      writer.write(text);
      writer.close();
    } catch (IOException e) {
      LOGGER.error("Unable to write file: " + filePath + "\n" + e.getMessage());
    }
  }

  /** Read the contents of the given file in UTF-8. */
  public static String getTextFromFile(File file) {
    StringBuilder text = new StringBuilder();
    String nl = System.getProperty("line.separator");
    Scanner scanner;
    try {
      scanner = new Scanner(new BOMInputStream(new FileInputStream(file), false), ENCODING);
    } catch (FileNotFoundException e) {
      throw new RuntimeException("File not found: " + file);
    }
    try {
      while (scanner.hasNextLine()) {
        text.append(scanner.nextLine() + nl);
      }
    } finally {
      scanner.close();
    }

    return text.toString();
  }

  public static void decodeAndWriteImage(String screenshotString, String fileFullPath) {
    try {
      byte[] imageBytes = Base64.decodeBase64(screenshotString.getBytes("UTF-8"));
      BufferedImage image = ImageIO.read(new ByteArrayInputStream(imageBytes));

      File file = new File(fileFullPath);
      ImageIO.write(image, "png", file);
    } catch (Exception e) {
      LOGGER.error("Unable to write screenshot: " + e);
    }
  }

  public static void decodeAndWriteImage(BufferedImage screenshotImage, String fileFullPath) {
    try {
      File file = new File(fileFullPath);
      ImageIO.write(screenshotImage, "png", file);
    } catch (IOException e) {
      LOGGER.error("Unable to write screenshot: " + e);
    }
  }

  /** @return text contained at {lineIndex} or null if line doesn't exist */
  public static String readLineFromFile(String filePath, int lineIndex) {
    List<String> lines = readAllLinesFromFile(filePath);
    if (lineIndex >= lines.size()) {
      return null;
    } else {
      return lines.get(lineIndex);
    }
  }

  public static List<String> readAllLinesFromFile(String filePath) {
    List<String> theLines = new ArrayList<String>();
    LineNumberReader lineNumberReader = null;
    String line = null;

    try {
      FileReader reader = new FileReader(filePath);
      lineNumberReader = new LineNumberReader(reader);

      line = lineNumberReader.readLine();
      while (line != null) {
        theLines.add(line);
        line = lineNumberReader.readLine();
      }
    } catch (FileNotFoundException ex) {
      throw new RuntimeException("File not found!: " + filePath);
    } catch (IOException ex) {
      throw new RuntimeException(ex);
    } finally {
      // Close the BufferedWriter
      try {
        if (lineNumberReader != null) {
          lineNumberReader.close();
        }
      } catch (IOException ex) {
        LOGGER.error("Failed to close the buffered writer. " + ex);
      }
    }
    return theLines;
  }

  public static void copyFile(File in, File out) {
    try {
      FileUtils.copyFile(in, out);
    } catch (IOException e) {
      LOGGER.error("Unable to copy file: " + in.getName() + "\n" + e.getMessage());
    }
  }

  public static void writeStringWriterToFile(StringWriter writer, File fileNameAndPath) {
    try {
      OutputStreamWriter fos = new OutputStreamWriter(
          new FileOutputStream(fileNameAndPath), ENCODING);
      fos.write(writer.getBuffer().toString());
      fos.close();
    } catch (IOException ioe) {
      throw new RuntimeException(
          "Unable to write file: " + fileNameAndPath + "\n" + ioe.getMessage());
    }
  }

  public static void deleteFilesInDir(File dir) {
    if (!dir.isDirectory()) {
      throw new IllegalArgumentException("Path '" + dir.getPath() + "' is not a directory");
    }

    for (File file : dir.listFiles()) {
      if (file.isFile()) {
        file.delete();
      }
    }
  }

  public static void deleteFile(File file) {
    if (file != null && file.exists()) {
      if (!file.delete()) {
        file.deleteOnExit();
      }
    }
  }

  public static void waitForFileDeleted(final File file, int seconds) throws Exception {
    WaitCondition waitCondition = new WaitCondition() {
      @Override
      public boolean isTrue() {
        return !file.exists();
      }
    };
    try {
      WaitUtil.waitFor(waitCondition, seconds);
    } catch (TimeoutException e) {
      throw new Exception(file + " not deleted.", e);
    }
  }

  public static void waitForFileCreated(final File file, int seconds) {
    WaitCondition waitCondition = new WaitCondition() {
      private long lastLength;
      private long lastLenghtTimestamp;

      @Override
      public boolean isTrue() {
        // if it is a regular file, wait until detected file size reaches a constant value
        final int fileSizeReadyMillis = 2000;
        if (file.exists()) {
          if (file.isDirectory()) {
            return true;
          } else if (file.length() > 0) {
            if (file.length() != lastLength) {
              lastLength = file.length();
              lastLenghtTimestamp = System.currentTimeMillis();
            } else if (System.currentTimeMillis() - lastLenghtTimestamp >= fileSizeReadyMillis) {
              return true;
            }
          }
        }
        return false;
      }
    };

    try {
      WaitUtil.waitFor(waitCondition, seconds);
    } catch (TimeoutException e) {
      throw new TimeoutException("File: "  + file + " not created.", e);
    }
  }

  public static int getNumberLines(String filePath) {
    return readAllLinesFromFile(filePath).size();
  }

  public static File extractResource(String resourcePath, String folder) {
    LOGGER.info("Extracting resource: " + resourcePath + " to folder: " + folder);
    InputStream in = FileUtil.class.getClassLoader().getResourceAsStream(resourcePath);
    File outputFile = new File(folder + resourcePath);
    outputFile.getParentFile().mkdirs();
    try {
      FileOutputStream out = new FileOutputStream(outputFile);
      IOUtils.copy(in, out);
      in.close();
      out.close();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    return outputFile;
  }

  public static String getResource(String path) {
    InputStream is = FileUtil.class.getClassLoader().getResourceAsStream(path);
    try {
      return IOUtils.toString(is, FileUtil.ENCODING);
    } catch (IOException e) {
      throw new RuntimeException("Unable to find resource: " + path);
    }
  }

  public static InputStream getResourceStream(String path) {
    return FileUtil.class.getClassLoader().getResourceAsStream(path);
  }

  public static String getTempFileFolder() {
    if (OSValidator.isWindows()) {
      // we had problems using java.io.tmpdir, maybe due to its length
      return WINDOWS_TEMP_FOLDER;
    } else {
      return System.getProperty("java.io.tmpdir") + "/";
    }
  }
}
