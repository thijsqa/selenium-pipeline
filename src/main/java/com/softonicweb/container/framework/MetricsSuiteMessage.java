package com.softonicweb.container.framework;

import org.testng.remote.strprotocol.SuiteMessage;

/**
 * A <code>IStringMessage</code> implementation for suite running events.
 *
 * @author <a href='mailto:the_mindstorm[at]evolva[dot]ro'>Alexandru Popescu</a>
 */
public class MetricsSuiteMessage extends SuiteMessage {

  private static final long serialVersionUID = 1L;
  protected long mStartMillis;
  protected long mEndMillis;

  public MetricsSuiteMessage(String suiteName, boolean startSuiteRun,
      int methodCount, long startMillis, long endMillis) {
    super(suiteName, startSuiteRun, methodCount);
    mStartMillis = startMillis;
    mEndMillis = endMillis;
  }

  public long getEndMillis() {
    return mEndMillis;
  }

  public long getStartMillis() {
    return mStartMillis;
  }
}
