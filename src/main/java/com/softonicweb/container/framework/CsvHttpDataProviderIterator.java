package com.softonicweb.container.framework;

import java.io.StringReader;
import java.lang.reflect.Method;
import java.util.Iterator;

import org.apache.commons.lang.StringEscapeUtils;

import com.softonicweb.container.framework.HttpUtil;

/**
 * Provides data fom a given .csv for the given method.
 * <p>Fist column will be always the line index.
 *
 * @author ivan.solomonoff
 */
public class CsvHttpDataProviderIterator extends CsvDataProviderIterator
    implements Iterator<Object> {

  public CsvHttpDataProviderIterator(Method method, String url, String user, String password,
      char delimiter, String commentChar) {
    super(method, new StringReader(
        StringEscapeUtils.unescapeHtml(
            new HttpUtil(url, user, password, "GET").getSourceAsString())), delimiter, commentChar);
  }
}
