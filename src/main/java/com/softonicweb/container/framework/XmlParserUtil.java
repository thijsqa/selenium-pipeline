package com.softonicweb.container.framework;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * Contains helper methods for parsing data from xml files.
 *
 * @author ivan.solomonoff
 */
public final class XmlParserUtil {

  private static final String VALUE_ATTRIBUTE = "value";
  private static final Logger LOGGER = Logger.getLogger(XmlParserUtil.class);

  private XmlParserUtil() {
    // Util class
  }

  public static Document parseDocument(String s) {
    InputSource is = new InputSource();
    is.setCharacterStream(new StringReader(s));

    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
    DocumentBuilder db;
    try {
      db = dbf.newDocumentBuilder();
    } catch (ParserConfigurationException e) {
      LOGGER.error("ParserConfigurationException: " + e.getMessage());
      throw new RuntimeException("ParserConfigurationException: " + e.getMessage());
    }
    Document doc;
    try {
      doc = db.parse(is);
    } catch (SAXException e) {
      LOGGER.error("ParserConfiguratioSAXExceptionnException: " + e.getMessage());
      throw new RuntimeException("SAXException: " + e.getMessage());
    } catch (IOException e) {
      LOGGER.error("IOException: " + e.getMessage());
      throw new RuntimeException("IOException: " + e.getMessage());
    }
    doc.getDocumentElement().normalize();

    return doc;
  }

  public static Document parseLocalFile(String contentFile) {
    File file = new File(contentFile);
    try {
      DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder documentBuilder;
      documentBuilder = documentBuilderFactory.newDocumentBuilder();
      Document document = documentBuilder.parse(file);
      document.getDocumentElement().normalize();
      return document;
    } catch (ParserConfigurationException e) {
      LOGGER.error("ParserConfigurationException: " + e.getMessage());
      throw new RuntimeException("ParserConfigurationException: " + e.getMessage());
    } catch (SAXException e) {
      LOGGER.error("SAXException: " + e.getMessage());
      throw new RuntimeException("SAXException: " + e.getMessage());
    } catch (IOException e) {
      LOGGER.error("IOException: " + e.getMessage());
      throw new RuntimeException("IOException: " + e.getMessage());
    }
  }

  public static Document parseDocument(InputStream is) {
    try {
      DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
      DocumentBuilder db = dbf.newDocumentBuilder();
      Document doc = db.parse(is);
      doc.getDocumentElement().normalize();
      return doc;
    } catch (Exception e) {
      LOGGER.error("Exception: " + e.getMessage());
      throw new RuntimeException("Exception: " + e.getMessage());
    }
  }

  public static Element getNodeElement(Element element, String tagName) {
    return (Element) element.getElementsByTagName(tagName).item(0);
  }

  public static String getElementAttribute(Element element, String attribute) {
    return element.getAttribute(attribute);
  }

  public static String getChildNodeValue(Element element, String tagName) {
    return getFirstChildValue(getNodeElement(element, tagName));
  }

  public static String getFirstChildValue(Element element) {
    return element.getFirstChild().getNodeValue();
  }

  public static String getValueAttribute(Element element) {
    return element.getAttribute(VALUE_ATTRIBUTE);
  }

  public static void generateXsd(String xmlFileName) {
    String input = xmlFileName + ".xml";
    String output = xmlFileName + ".xsd";

    new XmlTrangDriver().run(new String[] { input, output });
  }

  public static void validateXML(String source, String xsdFullPath) {
    File xsdFile = new File(xsdFullPath);
    SchemaFactory factory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
    try {
      Schema schema = factory.newSchema(xsdFile);
      Validator validator = schema.newValidator();
      byte[] bytes = source.getBytes(FileUtil.ENCODING);
      Source parsedSource = new StreamSource(new ByteArrayInputStream(bytes));
      validator.validate(parsedSource);
    } catch (SAXException e) {
      // ignore data type errors
      if (!e.getMessage().contains("is not a valid value for")) {
        LOGGER.error("SAXException: " + e.getMessage());
        throw new RuntimeException("SAXException" + e.getMessage());
      }
    } catch (IOException e) {
      LOGGER.error("IOException: " + e.getMessage());
      throw new RuntimeException("IOException: " + e.getMessage());
    }
  }

  public static String cleanXMLFile(final String path) throws IOException {
    FileInputStream stream = new FileInputStream(new File(path));
    try {
      FileChannel fc = stream.getChannel();
      MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
      /* Instead of using default, pass in a decoder. */
      return Charset.defaultCharset().decode(bb).toString();
    } finally {
      stream.close();
    }
  }
}
