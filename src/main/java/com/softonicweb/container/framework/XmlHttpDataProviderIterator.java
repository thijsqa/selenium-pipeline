package com.softonicweb.container.framework;

import java.lang.reflect.Method;
import java.util.Iterator;

import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.Converter;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import com.softonicweb.container.framework.HttpUtil;
import com.softonicweb.container.framework.XmlParserUtil;

/**
 * Lazily feeds data provider tests with data taken from an XML export.
 * <p>Fist column will be always the line index.
 *
 * @author ivan.solomonoff
 */
public class XmlHttpDataProviderIterator implements Iterator<Object> {

  protected int currentIndex = -1;
  protected String[] last = null;
  protected final Class<?>[] parameterTypes;

  protected Document doc;
  protected NodeList nodeList;

  private final Converter[] parameterConverters;

  public XmlHttpDataProviderIterator(Method method, String url) {
    String source = new HttpUtil(url).getSourceAsString();
    doc = XmlParserUtil.parseDocument(source);

    parameterTypes = method.getParameterTypes();
    int len = parameterTypes.length;
    parameterConverters = new Converter[len];
    for (int i = 0; i < len; i++) {
      parameterConverters[i] = ConvertUtils.lookup(parameterTypes[i]);
    }
  }

  @Override
  public boolean hasNext() {
    return currentIndex < (nodeList.getLength() - 1);
  }

  @Override
  public Object next() {
    String[] next;
    if (last != null) {
      next = last;
    } else {
      next = getNext();
    }
    last = null;
    Object[] args = parseValues(next);

    return args;
  }

  /** This method is to be implemented in a superclass. **/
  protected String[] getNext() {
    return null;
  }

  private Object[] parseValues(String[] values) {
    int len = values.length;
    Object[] result = new Object[len];
    for (int i = 0; i < len; i++) {
      result[i] = parameterConverters[i].convert(parameterTypes[i], values[i]);
    }

    return result;
  }

  @Override
  public void remove() {
    throw new UnsupportedOperationException();
  }
}
