package com.softonicweb.container.framework;

import org.testng.remote.strprotocol.IRemoteTestListener;
import org.testng.remote.strprotocol.TestMessage;
import org.testng.remote.strprotocol.TestResultMessage;

/**
 * Adapter class for IRemoteTestListener.
 *
 * @author alfredo.lopez
 */
public class RemoteTestAdapter implements IRemoteTestListener {

  @Override
  public void onFinish(TestMessage tm) { } // empty method

  @Override
  public void onStart(TestMessage tm) { } // empty method

  @Override
  public void onTestFailedButWithinSuccessPercentage(TestResultMessage trm) { } // empty method

  @Override
  public void onTestFailure(TestResultMessage trm) { } // empty method

  @Override
  public void onTestSkipped(TestResultMessage trm) { } // empty method

  @Override
  public void onTestStart(TestResultMessage trm) { } // empty method

  @Override
  public void onTestSuccess(TestResultMessage trm) { } // empty method
}
