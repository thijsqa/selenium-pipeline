package com.softonicweb.container.framework;

import java.lang.reflect.Method;
import java.util.Locale;

import com.softonicweb.container.framework.StartUrl;
import com.softonicweb.container.framework.IPlatform;
import com.softonicweb.container.framework.ITestEnvironment;
import com.softonicweb.container.framework.TestLogger;

/**
 * Stores test-specific environment variables.
 *
 * @author ivan.solomonoff
 */
public class TestEnvironment implements ITestEnvironment {

  private final Instance instance;
  private final IPlatform platform;

  private final String startUrl;

  public TestEnvironment(Class<?> c, Method m, Instance instance, IPlatform platform) {
    this.instance = instance;
    this.platform = platform;
    startUrl = getStartUrl(c, m);
    logVariables();
  }

  private String getStartUrl(Class<?> c, Method m) {
    // Method @StartUrl overwrites class level annotation.
    if (m != null && m.isAnnotationPresent(StartUrl.class)) {
      return getInstanceUrlWithSubdomain(m.getAnnotation(StartUrl.class).subdomain())
          + m.getAnnotation(StartUrl.class).relativeUrl();
    } else {
      return c.isAnnotationPresent(StartUrl.class)
          ? getInstanceUrlWithSubdomain(c.getAnnotation(StartUrl.class).subdomain())
              + c.getAnnotation(StartUrl.class).relativeUrl()
          : getInstanceBaseUrl();
    }
  }

  private void logVariables() {
    TestLogger.getInstance().logUrl("Using instance URL:",
        (getInstanceBaseUrl() != null) ? getInstanceBaseUrl() : "None");
    TestLogger.getInstance().logUrl("Using start URL: ", (startUrl != null) ? startUrl : "None");
  }

  @Override
  public String getInstanceUrlWithSubdomain(String subdomain) {
    if (subdomain.isEmpty()) {
      return getInstanceBaseUrl();
    }
    return getInstanceBaseUrl().replaceFirst("(//(www.)?)", "//" + subdomain + ".");
  }

  @Override
  public IPlatform getPlatform() {
    return platform;
  }

  @Override
  public String getInstanceBaseUrl() {
    return getInstance().getBaseUrl();
  }

  @Override
  public String getStartUrl() {
    return startUrl;
  }

  @Override
  public String toString() {
    return "instanceUnderTest: " + getInstance().getName() + "\nbaseUrl: "
        + getInstanceBaseUrl() + "\nstartUrl: " + startUrl;
  }

  @Override
  public int getInstanceProjectId() {
    return instance.getId();
  }

  @Override
  public Locale getInstanceLocale() {
    return instance.getLocale();
  }

  @Override
  public Instance getInstance() {
    return instance;
  }
}
