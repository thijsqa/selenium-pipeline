package com.softonicweb.container.framework;

import java.io.File;
import java.util.Arrays;

import org.testng.ITestResult;

/**
 * Utility class for handling TestNG test context data.
 *
 * @author ivan.solomonoff
 */
public final class TestNgUtil {

  private TestNgUtil() {
  }

  public static boolean hasRetriesLeft(ITestResult tr) {
    RetryAnalyzer analyzer = (RetryAnalyzer) tr.getMethod().getRetryAnalyzer();
    if (analyzer != null) {
      return analyzer.getRetryCount() < RetryAnalyzer.getNumRetries();
    } else {
      return false;
    }
  }

  public static void resetRetryCount(ITestResult tr) {
    RetryAnalyzer analyzer = (RetryAnalyzer) tr.getMethod().getRetryAnalyzer();
    if (analyzer != null) {
      analyzer.resetRetryCount();
    }
  }

  public static int getTestRetryCount(ITestResult tr) {
    RetryAnalyzer analyzer = (RetryAnalyzer) tr.getMethod().getRetryAnalyzer();
    if (analyzer != null) {
      return analyzer.getRetryCount();
    } else {
      return 0;
    }
  }

  public static String getMethodLogUrl(ITestResult tr, String reportsBaseUrl) {
    String methodName = tr.getMethod().getMethodName();
    return reportsBaseUrl + FileUtil.replaceInvalidChars(methodName) + File.separator;
  }

  public static File getMethodLogDir(ISuiteEnvironment suiteEnvironment, ITestResult tr) {
    return new File(suiteEnvironment.getReportsDir()
        + FileUtil.replaceInvalidChars(tr.getMethod().getMethodName()) + File.separator);
  }

  public static boolean isSkipped(ITestResult tr) {
    return tr.getStatus() == ITestResult.SKIP;
  }

  public static String getUniqueResultInstanceName(ITestResult result) {
    // If test was called with same parameters, then is the same test instance.
    Object[] parameters = result.getParameters();
    String methodName = result.getMethod().getMethodName();
    return (result.getTestName() == null)
        ? ((parameters != null)
            ? methodName + "_" + Arrays.toString(result.getParameters())
            : methodName)
        : result.getTestName();
  }
}
