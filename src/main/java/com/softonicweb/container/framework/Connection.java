package com.softonicweb.container.framework;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.rowset.CachedRowSet;

/**
 * Database connection interface.
 *
 * @author alfredo.lopez
 */
public interface Connection {
  void close();
  Statement createStatement() throws SQLException;
  PreparedStatement prepareStatement(String sql) throws SQLException;
  CachedRowSet getCachedResults(String sql) throws SQLException;
  boolean isClosed() throws SQLException;
  Date getCurrentDate();
}
