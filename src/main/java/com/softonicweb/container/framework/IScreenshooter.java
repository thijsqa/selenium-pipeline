package com.softonicweb.container.framework;

/**
 * Interface for providing screenshooting functionality.
 *
 * @author ivan.solomonoff
 */
public interface IScreenshooter {

  String getScreenshot();

  String getAutomaticScreenshotPath();
}
