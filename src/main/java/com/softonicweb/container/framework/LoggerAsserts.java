package com.softonicweb.container.framework;

import org.testng.AssertJUnit;

/**
 * Util class that provides flexibility for logging assertions from away the test class.
 *
 * @author ivan.solomonoff
 */
public final class LoggerAsserts {

  private LoggerAsserts() {
  }

  private static TestLogger logger() {
    return TestLogger.getInstance();
  }

  /**
   * Wrapping out Junit assertions for adding a more comprehensive logging into reports.
   */
  public static void assertTrue(String message, boolean condition) {
    try {
      AssertJUnit.assertTrue(message, condition);
    } catch (AssertionError e) {
      logger().logAssertionFailure("assertTrue", message, "condition=" + condition);
      throw e;
    }
    logger().logAssertionSuccess("assertTrue", message, "condition=" + condition);
  }

  public static void assertFalse(String message, boolean condition) {
    try {
      AssertJUnit.assertFalse(message, condition);
    } catch (AssertionError e) {
      logger().logAssertionFailure("assertFalse", message, "condition=" + condition);
      throw e;
    }
    logger().logAssertionSuccess("assertFalse", message, "condition=" + condition);
  }

  public static void assertEquals(String message, Object expected, Object actual) {
    try {
      AssertJUnit.assertEquals(message, expected, actual);
    } catch (AssertionError e) {
      logger().logAssertionFailure("assertEquals", message,
          "expected=" + expected + " actual=" + actual);
      throw e;
    }
    logger().logAssertionSuccess("assertEquals", message, actual + "=" + expected);
  }

  public static void assertNotNull(String message, Object actual) {
    try {
      AssertJUnit.assertNotNull(message, actual);
    } catch (AssertionError e) {
      logger().logAssertionFailure("assertNotNull", message, "actual=" + actual);
      throw e;
    }
    logger().logAssertionSuccess("assertNotNull", message, actual + "=! null");
  }

  public static void fail(String message) {
    logger().logAssertionFailure("fail", message, "");
    AssertJUnit.fail(message);
  }
}
