package com.softonicweb.container.framework;

import java.util.ArrayList;
import java.util.List;

import net.lightbody.bmp.core.har.Har;
import net.lightbody.bmp.core.har.HarEntry;
import net.lightbody.bmp.core.har.HarNameValuePair;
import net.lightbody.bmp.proxy.ProxyServer;
import net.lightbody.bmp.proxy.http.BrowserMobHttpRequest;
import net.lightbody.bmp.proxy.http.RequestInterceptor;

import com.softonicweb.container.framework.TestLogger;

/**
 * Wrapps a ProxyServer to add extra functionality and shared use in desktop and selenium.
 *
 * @author ivan.solomonoff
 */
public class ProxyWrapper extends ProxyServer {

  private static final int LONG_QUIET_PERIOD_MS = 8000;

  public ProxyWrapper() {
    super();
  }

  public ProxyWrapper(int port) {
    super(port);
  }

  private TestLogger logger() {
    return TestLogger.getInstance();
  }

  public List<HarEntry> getProxyHarLogEntries() {
    return getHar().getLog().getEntries();
  }

  public List<HarEntry> getProxyHarLogEntries(String sessionId) {
    List<HarEntry> entries = getProxyHarLogEntries();
    List<HarEntry> results = new ArrayList<HarEntry>();
    for (HarEntry entry : entries) {
      if (entry.getPageref().equals(sessionId)) {
        results.add(entry);
      }
    }
    return results;
  }

  public List<HarEntry> filterHarEntriesByRequestUrl(String urlRegex, List<HarEntry> entries) {
    ArrayList<HarEntry> filteredEntries = new ArrayList<HarEntry>();
    for (HarEntry entry : entries) {
      if (entry.getRequest().getUrl().matches(urlRegex)) {
        filteredEntries.add(entry);
      }
    }
    return filteredEntries;
  }

  public List<HarEntry> filterHarEntriesByResponseContentType(
      String contentTypeRegex, List<HarEntry> entries) {
    ArrayList<HarEntry> filteredEntries = new ArrayList<HarEntry>();
    for (HarEntry entry : entries) {
      for (HarNameValuePair header : entry.getResponse().getHeaders()) {
        if (header.getName().equals("Content-Type")
            && header.getValue().matches(contentTypeRegex)) {
          filteredEntries.add(entry);
        }
      }
    }

    return filteredEntries;
  }

  public void waitForNetworkTrafficToStop(int quietPeriodInMs) {
    final int timeoutInMs = 30000;
    try {
      waitForNetworkTrafficToStop(quietPeriodInMs, timeoutInMs);
      logger().logComment(
          "Network traffic was stopped during " + quietPeriodInMs + "ms.");
    } catch (Exception e) {
      logger().logComment(
          "WARNING: network traffic did not stop after a " + timeoutInMs + "ms timeout.");
    }
  }

  public void logRequestUrls(String requestDescription, List<HarEntry> harEntries) {
    for (HarEntry entry : harEntries) {
      logRequestUrl(requestDescription, entry.getRequest().getUrl());
    }
  }

  public void logRequestUrl(String requestDescription, String url) {
    logger().log(requestDescription, new String[]{ "url", url},
        "OK", System.currentTimeMillis());
  }

  public void setupActionTrafficCapture(String harName) {
    waitForNetworkTrafficToStop(LONG_QUIET_PERIOD_MS);
    newHar(harName);
  }

  public void registerProxyRequestInterceptor(final String urlRegex) {
    addRequestInterceptor(new RequestInterceptor() {
      @Override
      public void process(BrowserMobHttpRequest request, Har arg1) {
        String requestUrl = request.getMethod().getURI().toString();
        if (requestUrl.matches(urlRegex)) {
          logRequestUrl("Proxy intercepted tracking request", requestUrl);
        }
      }
    });
  }
}
