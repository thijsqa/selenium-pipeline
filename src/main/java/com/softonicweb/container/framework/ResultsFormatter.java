package com.softonicweb.container.framework;

/**
 * Used to log in any format you want to.
 *
 * <p>The formatter is responsible for writing results.
 *
 * <p>Also the formatter is responsible for handling encodings right for
 * whatever output storage is used.
 *
 * @author ivan.solomonoff
 */
interface ResultsFormatter {

  /**
   * Formats a comment.
   *
   * @param logItem containing the comment to be logged.
   */
  void logCommentEvent(LogItem logItem);

  /**
   * Formats a <textarea> HTML tag.
   *
   * @param logItem containing the text to be logged.
   */
  void logTextArea(LogItem logItem);

  void logSeparator();

  /**
   * Formats a command.
   *
   * @param logItem containing all informations for logging a command
   */
  void logCommandEvent(LogItem logItem);

  /**
   * Formats a special command, which output might be handled differently.
   *
   * @param logItem containing all informations for logging a command
   */
  void logSpecialCommandEvent(LogItem logItem);

  /**
   * Formats a command which has an boolean result.
   *
   * <p>Important: At this state no information whether a false or true result
   * was or is expected by the test using this formatter. So a false result may
   * be "green" for the test. Therefore all log-events here are logged as
   * green. E.g. Selenium commands like isElementPresent
   *
   * @param logItem containing all information for logging a command
   */
  void logBooleanCommandEvent(LogItem logItem);

  /**
   * Formats a Log Event for a complete Test Method.
   *
   * <p>The Log Item may contain several child items holding the information
   * of the single commands.
   *
   * @param logItem containing all information for logging a test method and
   *                its commands
   */
  void logMethodEvent(LogItem logItem);

  /**
   * Whatever the formatter wants to do before any command will be formatted.
   *
   * @param metrics metrics gathered during test-run
   */
  void logHeaderEvent(TestMetrics metrics);

  /**
   * Like formatHeader() but here after all commands have been formatted.
   */
  void logFooterEvent();

  /**
   * Base URI to be linked to in the result.
   *
   * <p>Resources may be linked with an HTTP or similar prefix to be easier
   * accessible in whatever reporting frontend is used later
   *
   * @return the current base-uri for attached resources
   */
  String getResourceBaseUri();

  /**
   * Set a special URI for resources.
   *
   * @param resourceBaseUri the new uri for resources
   */
  void setResourceBaseUri(String resourceBaseUri);

  /**
   * Path to the (filesystem-)location where resources should be saved.
   * <p>Will be used by generateFilenameForResources()
   *
   * @return current location path
   */
  String getResourcesPath();

  /**
   * Path to the (filesystem-)location where resources should be saved.
   * <p>Will be used by generateFilenameForResources()
   *
   * @param resourcesPath location (only path) to where resources will be saved
   */
  void setResourcesPath(String resourcesPath);

}
