package com.softonicweb.container.framework;

/**
 * Contains purchase data such as credit cards, paypal account, etc. (real and fake)
 * to be used for testing.
 *
 * @author ivan.solomonoff
 */
public class PurchaseData {

  private static final String POSTAL_CODE = "08016";
  private static final String BILLING_ADDRESS = "C/ Rossello i Porcel, 21";

  /**
   * Contains various credit card Purchase data.
   */
  public enum CreditCard {
    // Valid for SADS tests
    VISA_JOAQUIN("VISA", "Joaquin", "Ruiz Negre", "4277300004592285", "102016", "405"),
    // Used in Staging. More Fake cards numbers on
    // https://wiki.redtonic/download/attachments/
    // 43757198/MCPE_Freedom_IMA_2_3.pdf?version=1&modificationDate=1337334065000&api=v2
    FAKE_CARD("VISA", "Softonic", "QA Automation", "4007000000027123", "052020", "190"),
    // Used in Prod
    TEST_CARD("VISA", "Enric", "Aldana Izquierdo", "4305043002351553", "052019", "365");

    private final String type;
    private final String ownerName;
    private final String ownerSurname;
    private final String number;
    private final String expiration;
    private final String cvv;

    CreditCard(String type, String ownerName, String ownerSurname, String number,
        String expiration, String cvv) {
      this.type = type;
      this.ownerName = ownerName;
      this.ownerSurname = ownerSurname;
      this.number = number;
      this.expiration = expiration;
      this.cvv = cvv;
    }

    public String getType() {
      return type;
    }

    public String getOwnerName() {
      return ownerName;
    }

    public String getOwnerSurname() {
      return ownerSurname;
    }

    public String getPostalCode() {
      return POSTAL_CODE;
    }

    public String getBillingAddress() {
      return BILLING_ADDRESS;
    }

    public String getNumber() {
      return number;
    }

    public String getExpirationDate() {
      return expiration;
    }

    public String getExpirationMonth() {
      return expiration.substring(0, 2);
    }

    /** @return Expiration year in YY format */
    public String getExpirationYear() {
      final int size = 4;
      return expiration.substring(size);
    }

    /** @return Expiration year in YYYY format */
    public String getExpirationYearFull() {
      return expiration.substring(2);
    }

    public String getCvv() {
      return cvv;
    }
  }
}
