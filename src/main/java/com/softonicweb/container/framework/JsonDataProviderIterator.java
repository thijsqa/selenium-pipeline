package com.softonicweb.container.framework;

import java.lang.reflect.Method;
import java.util.Iterator;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.Converter;
import org.apache.commons.lang.ArrayUtils;

import com.softonicweb.container.framework.JsonParserUtil;

/**
 * Provides data fom a given JSON for the given method.
 * <p>Fist value must always be the test index.
 *
 * @author ivan.solomonoff
 */
public class JsonDataProviderIterator implements Iterator<Object> {

  private final JSONArray testsJsonArray;
  private final int valuesCount;
  private final Class<?>[] parameterTypes;
  private final Converter[] parameterConverters;

  private int currentIndex;

  /*
   * @param commentChar If set, skip lines starting with this char. Otherwise
   * set to null.
   */
  public JsonDataProviderIterator(Method method, String jsonString) {
    testsJsonArray = ((JSONObject) new JsonParserUtil(jsonString).getJson()).getJSONArray("tests");
    currentIndex = 0;
    valuesCount = testsJsonArray.getJSONObject(currentIndex).size();

    parameterTypes = method.getParameterTypes();
    int len = parameterTypes.length;
    parameterConverters = new Converter[len];
    for (int i = 0; i < len; i++) {
      parameterConverters[i] = ConvertUtils.lookup(parameterTypes[i]);
    }
  }

  @Override
  public boolean hasNext() {
    return currentIndex < testsJsonArray.size();
  }

  @Override
  public Object[] next() {
    Object[] next = getTestValues(currentIndex);
    Object[] args = parseValues(next);
    currentIndex++;

    return args;
  }

  private Object[] getTestValues(int index) {
    Object[] values = new Object[valuesCount];
    JSONObject testObject = testsJsonArray.getJSONObject(index);
    if (testObject.size() < valuesCount) {
      throw new RuntimeException("Missing values for test object at index: " + index);
    }
    Iterator<?> keysIterator = testObject.keys();
    for (int i = 0; i < valuesCount; i++) {
      values[i] = testObject.get(keysIterator.next());
    }

    return values;
  }

  private Object[] parseValues(Object[] svals) {
    int len = svals.length;
    Object[] result = new Object[len];
    try {
      for (int i = 0; i < len; i++) {
        result[i] = parameterConverters[i].convert(parameterTypes[i], svals[i]);
      }
    } catch (RuntimeException re) {
      throw new RuntimeException("Error parsing test values: " + ArrayUtils.toString(svals), re);
    }

    return result;
  }

  @Override
  public void remove() {
    throw new UnsupportedOperationException();
  }
}
