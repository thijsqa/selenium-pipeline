package com.softonicweb.container.framework;

import java.io.IOException;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

/**
 * RabbitMQ Messenger.
 *
 * @author ivan.solomonoff
 */
public final class RabbitMqMessenger {

  private static final String HOST = "rabbit03.ofi.softonic.lan";
  private static final int RABBIT_MQ_PORT = 5672;
  private static final String TASK_QUEUE_NAME = "q.qa";

  private RabbitMqMessenger() {
  }

  public static void sendMessage(String message) {
    ConnectionFactory factory = new ConnectionFactory();
    factory.setHost(HOST);
    factory.setPort(RABBIT_MQ_PORT);
    factory.setUsername("ekpi");
    factory.setPassword("erngeuNeox");
    factory.setVirtualHost("engineering_kpi");
    try {
      Connection connection = factory.newConnection();
      Channel channel = connection.createChannel();
      boolean durable = true;
      boolean exclusive = false;
      boolean autoDelete = false;
      channel.queueDeclare(TASK_QUEUE_NAME, durable, exclusive, autoDelete, null);

      channel.basicPublish("", TASK_QUEUE_NAME, null, message.getBytes());
      System.out.println(" [x] Sent '" + message + "'");

      channel.close();
      connection.close();
    } catch (IOException e) {
      throw new RuntimeException("Unable to send message to RabbitMQ. " + e);
    }
  }
}
