package com.softonicweb.container.framework;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Contains helper methods for handling URLs.
 *
 * @author ivan.solomonoff
 */
public final class UrlsUtil {

  private UrlsUtil() {
  }

  /** Given a production URL, returns its test environment URL (if not standardized) */
  public static String convertToOldEnvironmentUrlIfNeeded(Environment environment, String url) {
    if (!environment.equals(Environment.PRODUCTION)) {
      Pattern p = Pattern.compile("http://.+(softonic.+?/|redtonic/)(.+)?");
      Matcher m = p.matcher(url);
      m.find();
      String toReplace = m.group(1);
      url = url.replace(toReplace, environment.getDomain() + "/");
    }
    return url;
  }

  /** Given a production URL, returns its test environment URL (if standardized) */
  public static String convertToEnvironmentUrlIfNeeded(Environment environment, String url) {
    if (!environment.equals(Environment.PRODUCTION)) {
      Pattern p = Pattern.compile("(https?://.+?/)(.+)?");
      Matcher m = p.matcher(url);
      m.find();
      String toReplace = m.group(1);
      url = url.replace(toReplace,
          toReplace.substring(0, toReplace.length() - 1) + "." + environment.getDomain() + "/");
    }
    return url;
  }

  /** Adds the specified subdomain to the instance's base URL */
  public static String instanceUrlWithSubdomain(String instanceUrl, String subdomain) {
    if (subdomain.isEmpty()) {
      return instanceUrl;
    }
    return instanceUrl.replaceFirst("(//(www.)?)", "//" + subdomain + ".");
  }

  public static boolean isSoftonicWebUrl(String url) {
    return url.contains(".softonic.");
  }

  public static String getWebInstanceByUrl(String softonicUrl) {
    if (softonicUrl.contains(".softonic.cn")) {
      return "CN";
    } else if (softonicUrl.contains(".softonic.jp")) {
      return "JP";
    } else if (softonicUrl.contains("de.softonic.com")) {
      return "DE";
    } else if (softonicUrl.contains("it.softonic.com")) {
      return "IT";
    } else if (softonicUrl.contains("fr.softonic.com")) {
      return "FR";
    } else if (softonicUrl.contains(".softonic.pl")) {
      return "PL";
    } else if (softonicUrl.contains(".softonic.com.br")) {
      return "BR";
    } else if (softonicUrl.contains("nl.softonic.com")) {
      return "NL";
    } else if (softonicUrl.contains("en.softonic.com")) {
      return "EN";
    } else if (softonicUrl.contains(".softonic.com")) {
      return "ES";
    } else {
      throw new IllegalArgumentException("Unable to detect Softonic domain country from URL: "
          + softonicUrl);
    }
  }
}
