package com.softonicweb.container.framework;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation to label classes containing test methods that are meant to be executed
 * together, without any other class test method interfering.
 *
 * @author alfredo.lopez
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface AtomicClassRun {
  String name() default "";
  int priority() default 0;
}
