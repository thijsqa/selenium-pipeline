package com.softonicweb.container.framework;

/**
 * Interface for a table column.
 *
 * @author alfredo.lopez
 */
public interface TableColumn {
  String getName();
  Class<?> getType();
  int index();
}
