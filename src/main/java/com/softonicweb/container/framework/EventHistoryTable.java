package com.softonicweb.container.framework;

import java.math.BigDecimal;
import java.sql.Date;

/**
 * Model for the 'event_history' table in Partners database.
 *
 * @author alfredo.lopez
 */
public final class EventHistoryTable {

  private EventHistoryTable() {
  }

  public static final String NAME = "event_history";

  /**
   * Columns in EventHistory table.
   *
   * @author alfredo.lopez
   */
  public enum Column implements TableColumn {
    ID_PARTNER_EVENT_HISTORY("id_partner_event_history", Integer.class),
    ID_EVENT("id_event",  Integer.class, 1),
    ID_PARTNER("id_partner", String.class, 2),
    INSTANCE("instance", String.class, 3),
    DATE("date", Date.class, 4),
    ID_FILE("id_file", Integer.class, 5),
    TIMES("times", Integer.class),
    SOFTONIC_REVENUE("softonic_revenue", BigDecimal.class),
    PARTNER_REVENUE("partner_revenue", BigDecimal.class),
    TOTAL_REVENUE("total_revenue", BigDecimal.class),
    COUNTRY("country", String.class, 6);

    private final String name;
    private final Class<?> type;
    private final int index;

    Column(String name, Class<?> type, int index) {
      this.name = name;
      this.type = type;
      this.index = index;
    }

    Column(String name, Class<?> type) {
      this(name, type, -1);
    }

    @Override
    public String getName() {
      return name;
    }

    @Override
    public Class<?> getType() {
      return type;
    }

    @Override
    public String toString() {
      return name;
    }

    @Override
    public int index() {
      return index;
    }
  }
}
