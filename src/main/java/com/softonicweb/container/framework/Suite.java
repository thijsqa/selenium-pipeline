package com.softonicweb.container.framework;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.softonicweb.container.framework.ISuite;
import com.softonicweb.container.framework.JsonParserUtil;

/**
 * Stores Test Suite properties.
 *
 * @author ivan.solomonoff
 */
public class Suite implements ISuite {

  private final String environmentArg;
  private final String[] instanceArg;
  private final String name;
  private final int timeout;
  private final int numThreads;
  private final int numRetries;
  private final List<Instance> instances;

  public Suite(String jsonString, String environmentArg, String instancesArg, String baseUrlArg) {
    this.environmentArg = environmentArg;
    this.instanceArg = instancesArg.split(ARRAY_SEPARATOR);
    JsonParserUtil json = new JsonParserUtil(jsonString).workWithInnerObject(SUITE_KEY);
    this.name = json.has(NAME_KEY) ? json.getString(NAME_KEY) : DEFAULT_NAME;
    this.timeout = json.has(TIMEOUT_KEY) ? json.getInt(TIMEOUT_KEY) : DEFAULT_TIMEOUT;
    this.numThreads = json.has(THREADS_KEY) ? json.getInt(THREADS_KEY) : DEFAULT_NUM_THREADS;
    this.numRetries = json.has(RETRIES_KEY) ? json.getInt(RETRIES_KEY) : DEFAULT_NUM_RETRIES;
    this.instances = new ArrayList<Instance>();
    if (json.has(ENVIRONMENTS_KEY)) {
      json.workWithInnerArray(ENVIRONMENTS_KEY);
      // If set to run against WC, fetch data from production and then replace the baseUrl
      String environmentToFetchDataFrom = isWorkingCopy(environmentArg)
          ? Environment.PRODUCTION.name().toLowerCase()
              : environmentArg;
          try {
            json.arrayToInnerObject(environmentToFetchDataFrom);
          } catch (RuntimeException e) {
            // try from INTEGRATION
            environmentToFetchDataFrom = Environment.INTEGRATION.name().toLowerCase();
            json.arrayToInnerObject(environmentToFetchDataFrom);
          }
          json.workWithInnerArray(environmentToFetchDataFrom);
          for (String instance : instanceArg) {
            int instanceIndex = instance.equals(Instance.DEFAULT_NAME)
                ? 0 // means doesn't have multiple instances for this environment.
                    : json.getArrayIndexFor(INSTANCE_KEY, instance);
            if (instanceIndex < 0) {
              throw new RuntimeException("You must set at least one test environment for "
                  + environmentArg + " in the " + CONFIG_FILE
                  + " file, if not needed, remove the 'environments' key."
                  + " If you use the instance argument,"
                  + "it must be also declared in the JSON file.");
            } else {
              String instanceName =
                  getValueOrDefault(json, instanceIndex, INSTANCE_KEY, Instance.DEFAULT_NAME);
              String id = getValueOrDefault(json, instanceIndex, PROJECT_ID_KEY,
                  String.valueOf(Instance.DEFAULT_ID));
              String baseUrl = isWorkingCopy(environmentArg)
                  ? baseUrlArg
                  : getValueOrDefault(json, instanceIndex,
                      BASE_URL_KEY, Instance.DEFAULT_BASE_URL);
              String locale = getValueOrDefault(json, instanceIndex, LOCALE_KEY,
                  Instance.DEFAULT_LOCALE.getLanguage());
              Instance singleInstance = new Instance(instanceName, id, baseUrl, locale);

              this.instances.add(singleInstance);
            }
          }
    } else {
      this.instances.add(new Instance());
    }
  }

  private boolean isWorkingCopy(String environmentArg) {
    return environmentArg.equalsIgnoreCase(Environment.WC.name());
  }

  private String getValueOrDefault(JsonParserUtil json, int instanceIndex, String key,
      String defaultValue) {
    String value = json.getValueFromArray(instanceIndex, key);
    if (value != null) {
      return value;
    } else {
      return defaultValue;
    }
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public int getTimeout() {
    return timeout;
  }

  @Override
  public int getNumThreads() {
    return numThreads;
  }

  @Override
  public int getNumRetries() {
    return numRetries;
  }

  @Override
  public Environment getEnvironment() {
    return Environment.valueOf(environmentArg.toUpperCase(Locale.ENGLISH));
  }

  @Override
  public List<Instance> getInstances() {
    return instances;
  }
}
