package com.softonicweb.container.framework;

import java.util.Collections;
import java.util.List;

/**
 * Database used for Hive exports.
 *
 * @author alfredo.lopez
 */
public class HadoopTrackingDb extends Database {

  private  static final Profile HADOOP_PROD_PROPERTIES = new Profile(
      "tracking", "172.20.1.135",
      Accounts.QA_MYSQL.getUsername(),
      Accounts.QA_MYSQL.getPassword());

  private static final Profile HADOOP_DEV_PROPERTIES = new Profile(
      "tracking_dev", "172.20.1.135",
      Accounts.QA_MYSQL.getUsername(),
      Accounts.QA_MYSQL.getPassword());

  private Profile profile;

  public HadoopTrackingDb(Environment environment) {
    super(environment);
  }

  @Override
  public Profile getProfile() {
    if (profile == null) {
      switch (environment) {
      case PRODUCTION:
        //TODO (alfredo.lopez): Hadoop Production export tables not yet implemented
        profile = HADOOP_PROD_PROPERTIES;
        break;
      case STAGING:
      case INTEGRATION:
      case DEVELOPMENT:
        profile = HADOOP_DEV_PROPERTIES;
        break;
      default:
        throw new UnsupportedOperationException();
      }
    }

    return profile;
  }

  @Override
  public List<Environment> restrictedEnvironments() {
    return Collections.emptyList();
  }
}
