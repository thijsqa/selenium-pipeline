package com.softonicweb.container.framework;

/**
 * Holds HTTP request header data.
 *
 * @author ivan.solomonoff
 */
public class HttpResource {

  private String contentType;
  private int contentLength;
  private String server;
  private String responseCode;

  public HttpResource() {
    this.contentType = "";
    this.contentLength = -1;
    this.server = "";
    this.responseCode = "";
  }

  public HttpResource(String contentType, int contentLength, String server, String responseCode) {
    this.contentType = contentType;
    this.contentLength = contentLength;
    this.server = server;
    this.responseCode = responseCode;
  }

  public String getContentType() {
    return contentType;
  }

  public void setContentType(String typeOfContent) {
    this.contentType = typeOfContent;
  }

  public int getContentLength() {
    return contentLength;
  }

  public void setContentLength(int lenghtOfContent) {
    this.contentLength = lenghtOfContent;
  }

  public String getServer() {
    return server;
  }

  public void setServer(String serverName) {
    this.server = serverName;
  }

  public String getResponseCode() {
    return this.responseCode;
  }

  public void setResponseCode(String codeInResponse) {
    this.responseCode = codeInResponse;
  }
}
