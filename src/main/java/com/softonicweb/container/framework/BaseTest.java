package com.softonicweb.container.framework;

import java.lang.reflect.Method;
import java.util.List;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;

import com.softonicweb.container.framework.WaitUtil;

/**
 * Base class for all tests.
 *
 * @author ivan.solomonoff
 */
public abstract class BaseTest {

  // Only set when using data providers for instances or devices
  protected final InheritableThreadLocal<Instance> threadLocalInstance =
      new InheritableThreadLocal<Instance>();
  protected InheritableThreadLocal<IPlatform> threadLocalPlatform =
      new InheritableThreadLocal<IPlatform>();

  private final InheritableThreadLocal<ITestEnvironment> threadLocalTestEnvironment =
      new InheritableThreadLocal<ITestEnvironment>();

  protected static final ISuiteEnvironment SUITE_ENVIRONMENT = SuiteEnvironment.getInstance();

  @BeforeMethod(alwaysRun = true)
  protected void setup(Method m, Object[] testArguments, ITestResult tr) {
    Instance instance;
    if (threadLocalInstance.get() == null) {
      // doesn't use instance data provider
      instance = getInstanceUnderTest(this.getClass(), m);
    } else {
      instance = threadLocalInstance.get();
    }

    IPlatform platform;
    if (threadLocalPlatform.get() != null) {
      // using platform data provider
      platform = threadLocalPlatform.get();
    } else {
      platform = parsePlatform(SUITE_ENVIRONMENT.getPlatforms()[0]);
    }
    setTestEnvironment(new TestEnvironment(this.getClass(), m, instance, platform));
  }

  protected abstract IPlatform parsePlatform(String platform);

  @AfterMethod(alwaysRun = true)
  protected void teardown(ITestResult tr, Method method) {
    if (threadLocalInstance.get() != null) {
      tr.setAttribute("Instance", threadLocalInstance.get().getName());
    }
  }

  protected ITestEnvironment testEnvironment() {
    return threadLocalTestEnvironment.get();
  }

  protected void setTestEnvironment(ITestEnvironment environment) {
    threadLocalTestEnvironment.set(environment);
  }

  protected static Environment getEnvironment() {
    return SUITE_ENVIRONMENT.getSuite().getEnvironment();
  }

  protected Instance instance() {
    return testEnvironment().getInstance();
  }

  protected IPlatform platform() {
    return testEnvironment().getPlatform();
  }

  protected void setInstance(Instance instance) {
    threadLocalInstance.set(instance);
  }

  protected void setPlatform(IPlatform platform) {
    threadLocalPlatform.set(platform);
  }

  protected void sleep(int milliseconds) {
    WaitUtil.pause(milliseconds);
  }

  @DataProvider(name = "instances", parallel = true)
  protected static Object[][] getInstances() {
    List<Instance> instances = SUITE_ENVIRONMENT.getInstances();
    Object[][] result = new Object[instances.size()][];
    for (int i = 0; i < instances.size(); i++) {
      result[i] = new Object[] { instances.get(i) };
    }

    return result;
  }

  private Instance getInstanceUnderTest(Class<?> c, Method m) {
    List<Instance> instanceList = SUITE_ENVIRONMENT.getInstances();
    if (instanceList.size() > 1) {
      // TODO(ivan.solomonoff): For compatibility support with old tests, return the first instance
      // in the @LimitInstances annotation.
      // Method @LimitInstances overrides class level setting.
      if (m != null && m.isAnnotationPresent(LimitInstances.class)) {
        return getMatchedInstance(m.getAnnotation(LimitInstances.class).restrictTo()[0]);
      } else if (c.isAnnotationPresent(LimitInstances.class)) {
        return getMatchedInstance(c.getAnnotation(LimitInstances.class).restrictTo()[0]);
      } else {
        throw new RuntimeException("Must use the @LimitInstances annotation for runnning a "
            + "subset of instances, otherwise leave it unset.");
      }
    } else {
      return instanceList.get(0);
    }
  }

  private Instance getMatchedInstance(String instanceStr) {
    for (Instance currentInstance : SUITE_ENVIRONMENT.getInstances()) {
      if (currentInstance.getName().equalsIgnoreCase(instanceStr)) {
        return currentInstance;
      } else {
        continue;
      }
    }
    throw new RuntimeException("Unable to find test configuration for instance: " + instanceStr);
  }
}
