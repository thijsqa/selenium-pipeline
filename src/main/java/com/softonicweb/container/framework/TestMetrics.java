package com.softonicweb.container.framework;

import org.apache.commons.lang.ArrayUtils;

/**
 * Metrics and environment information collected during test run.
 *
 * @author ivan.solomonoff
 */
class TestMetrics {

  private long startTimeStamp;
  private long endTimeStamp;

  private long failedCommands = 0;
  private String lastFailedCommandMessage;
  private String[] commandsExcludedFromLogging = {};

  private String device;
  private String testName;
  private String serverRevision;
  private String frameworkRevision;

  public long getStartTimeStamp() {
    return startTimeStamp;
  }

  public void setStartTimeStamp(long start) {
    startTimeStamp = start;
  }

  public long getEndTimeStamp() {
    return endTimeStamp;
  }

  public void setEndTimeStamp(long end) {
    endTimeStamp = end;
  }

  public long getFailedCommands() {
    return failedCommands;
  }

  public void setFailedCommands(long failed) {
    failedCommands = failed;
  }

  /**
   * Increment failedCommands by one.
   *
   * <p>Convenience method to shortcut usage like this:
   * setFailedCommands(getFailedCommands()+1)
   */
  public void incFailedCommands() {
    failedCommands++;
  }

  /**
   * Compute test duration out of startTimeStamp and endTimeStamp.
   *
   * <p>Wrong values are negative startTimeStamp or negative difference
   * between endTimeStamp and startTimeStamp
   *
   * @return duration in millis. Will be 0 in case of missing or wrong values
   */
  public long getTestDuration() {
    long testDuration = 0;
    if (startTimeStamp > 0 && endTimeStamp > startTimeStamp) {
      testDuration = endTimeStamp - startTimeStamp;
    }
    return testDuration;
  }

  public String getDevice() {
    return device;
  }

  public void setDevice(String deviceName) {
    device = deviceName;
  }

  public String getServerRevision() {
    return serverRevision;
  }

  public void setServerRevision(String revision) {
    serverRevision = revision;
  }

  public String getFrameworkRevision() {
    return frameworkRevision;
  }

  public void setFrameworkRevision(String revision) {
    frameworkRevision = revision;
  }

  public String getLastFailedCommandMessage() {
    return lastFailedCommandMessage;
  }

  public void setLastFailedCommandMessage(String message) {
    lastFailedCommandMessage = message;
  }

  public String[] getCommandsExcludedFromLogging() {
    return (String[]) ArrayUtils.clone(commandsExcludedFromLogging);
  }

  public void setCommandsExcludedFromLogging(String[] commands) {
    commandsExcludedFromLogging = (String[]) ArrayUtils.clone(commands);
  }

  public String getTestName() {
    return testName;
  }

  public void setTestName(String name) {
    testName = name;
  }

  public void resetMetrics() {
    startTimeStamp = System.currentTimeMillis();
    failedCommands = 0;
    lastFailedCommandMessage = null;
    testName = null;
    device = null;
  }
}
