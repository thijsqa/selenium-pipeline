package com.softonicweb.container.framework;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.log4j.Logger;

/**
 * Contains helper methods for running and manipulating processes.
 *
 * @author ivan.solomonoff
 */
public final class ProcessUtil {

  private static final Logger LOGGER = Logger.getLogger(ProcessUtil.class);

  private ProcessUtil() {
    // Util class
  }

  public static void runProcess(ProcessBuilder pb, boolean asynchronous) {
    pb.redirectErrorStream(true);
    try {
      final Process process = pb.start();
      if (asynchronous) {
        new Thread(new Runnable() {
          @Override
          public void run() {
            waitForProcess(process);
          }
        }).start();
      } else {
        waitForProcess(process);
      }
    } catch (IOException e) {
      LOGGER.error("Failed starting process. " + e);
    }
  }

  private static void waitForProcess(Process process) {
    try {
      BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream(),
          FileUtil.ENCODING));
      StringBuilder builder = new StringBuilder();
      String line = null;
      while ((line = br.readLine()) != null) {
        builder.append(line);
        builder.append(System.getProperty("line.separator"));
      }
      int exitStatus = process.waitFor();
      LOGGER.info(builder.toString());
      LOGGER.info("Finished process with status: " + exitStatus);
    } catch (InterruptedException | IOException e) {
      LOGGER.error("Failed waiting for process to finish. " + e);
    }
  }
}
