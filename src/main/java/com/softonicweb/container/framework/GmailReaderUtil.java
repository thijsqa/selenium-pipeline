package com.softonicweb.container.framework;

import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.search.RecipientStringTerm;
import javax.mail.search.SearchTerm;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringEscapeUtils;

import com.softonicweb.container.framework.WaitUtil.WaitCondition;

/**
 * Util class for reading emails from Gmail using the IMAP protocol.
 *
 * @author ivan.solomonoff
 */
public final class GmailReaderUtil {

  private static final String GMAIL_IMAP_ADDRESS = "imap.gmail.com";
  private static final int MAIL_POLLING_MILLIS = 5000;
  private static final int MAIL_TIMEOUT_MILLIS = 90000;

  private static Accounts gmailUser = Accounts.GMAIL_DEFAULT;

  private static Store store;

  private GmailReaderUtil() {
    // Utility class
  }

  public static synchronized void setGmailUser(Accounts user) {
    GmailReaderUtil.gmailUser = user;
  }

  public static String fetchLastEmail(String folderLabel) {
    return waitForEmailAndFetch(folderLabel, null);
  }

  /** Returns null if no email was found.*/
  public static synchronized String waitForEmailAndFetch(final String folderLabel,
      final IAccount user) {
    try {
      WaitCondition waitCondition = new WaitCondition() {
        @Override
        public boolean isTrue() {
          return isEmailFoundAfterSearch(folderLabel, user);
        }
      };
      WaitUtil.waitForMillis(waitCondition, MAIL_TIMEOUT_MILLIS, MAIL_POLLING_MILLIS);
    } catch (TimeoutException e) {
      TestLogger.getInstance().logError("Timed out waiting for email");
    }
    return getEmailMessage(folderLabel, user);
  }

  private static boolean isEmailFoundAfterSearch(String folderLabel, IAccount user) {
    try {
      if (user != null) {
        TestLogger.getInstance().logComment("Trying to fetch email to: " + user.getEmail());
      } else {
        TestLogger.getInstance().logComment("Fetching last email with label: " + folderLabel);
      }
      getEmailMessage(folderLabel, user);
    } catch (NoSuchElementException e) {
      return false;
    }
    return true;
  }

  private static Folder openStore(String folderLabel) {
    Folder folder = null;
    Properties props = System.getProperties();
    props.setProperty("mail.store.protocol", "imaps");
    Session session = Session.getDefaultInstance(props, null);
    try {
      store = session.getStore("imaps");
      store.connect(GMAIL_IMAP_ADDRESS, gmailUser.getUsername(), gmailUser.getPassword());
      folder = store.getFolder(folderLabel);
      folder.open(Folder.READ_ONLY);
    } catch (MessagingException e) {
      throw new RuntimeException(e);
    }
    return folder;
  }

  private static void closeStore() {
    try {
      store.close();
    } catch (MessagingException e) {
      e.printStackTrace();
    }
  }

  private static synchronized String getEmailMessage(String folderLabel, IAccount user) {
    String msgString = null;
    Folder folder = openStore(folderLabel);
    // If recipient User is given, filter only messages sent TO that recipient address.
    try {
      Message lastMessage;
      if (user != null) {
        SearchTerm term = new RecipientStringTerm(Message.RecipientType.TO, user.getEmail());
        Message[] messages = folder.search(term);
        int messageCount = messages.length;
        if (messageCount > 0) {
          lastMessage = messages[messageCount - 1];
        } else {
          throw new NoSuchElementException("No email found for user: " + user.getEmail()
              + ", in folder label: " + folderLabel);
        }
      } else {
        lastMessage = folder.getMessage(folder.getMessageCount());
      }
      if (lastMessage != null) {
        try {
          msgString = StringEscapeUtils.unescapeHtml(
              IOUtils.toString(lastMessage.getInputStream(), FileUtil.ENCODING));
        } catch (IOException e) {
          e.printStackTrace();
          throw new RuntimeException("IO error while fetching email from GMAIL: " + e.getMessage());
        }
      }
    } catch (MessagingException e) {
      throw new NoSuchElementException("No email found for user: " + user.getEmail()
          + ", in folder label: " + folderLabel);
    } finally {
      closeStore();
    }

    return msgString;
  }

  public static String getConfirmationUrlFromEmail(String email, String confirmationUrlPattern) {
    Pattern pattern = Pattern.compile(confirmationUrlPattern, Pattern.DOTALL);
    Matcher matcher = pattern.matcher(email);
    matcher.find();
    return matcher.group(0);
  }

  public static String findPatternInEmail(String email, String pattern) {
    return getConfirmationUrlFromEmail(email, pattern);
  }

  public static boolean isEmailPresent(String folderLabel, String pattern) {
    try {
      String email = fetchLastEmail(folderLabel);
      return !findPatternInEmail(email, pattern).equals(null);
    } catch (Exception e) {
      return false;
    }
  }
}
