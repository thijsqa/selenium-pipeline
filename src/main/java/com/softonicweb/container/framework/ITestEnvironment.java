package com.softonicweb.container.framework;

import java.util.Locale;

/**
 * Test environment interface.
 *
 * @author ivan.solomonoff
 */
public interface ITestEnvironment {

  Instance getInstance();

  String getInstanceBaseUrl();

  String getInstanceUrlWithSubdomain(String subdomain);

  String getStartUrl();

  IPlatform getPlatform();

  int getInstanceProjectId();

  Locale getInstanceLocale();
}
