package com.softonicweb.container.framework;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;

/**
 * All data of a command already processed and other informations to be
 * formatted and logged.
 *
 * @author ivan.solomonoff
 */
class LogItem {

  private String commandName = "";
  private String[] args;
  private String result = "";
  private String callingClass = "";
  private boolean commandSuccessful;
  private long cmdStartMillis;
  private long cmdEndMillis;
  private String sourceMethod;
  private boolean excludeFromLogging = false;

  private final List<LogItem> children = new ArrayList<LogItem>();

  /** Adds an LogItem child to current children. */
  public void addChild(LogItem logItem) {
    children.add(logItem);
  }

  public List<LogItem> getChildren() {
    return children;
  }

  public boolean hasChildren() {
    return getChildren().size() > 0;
  }

  public String getCommandName() {
    return commandName;
  }

  public void setCommandName(String command) {
    this.commandName = command;
  }

  public String[] getArgs() {
    return (String[]) ArrayUtils.clone(args);
  }

  public void setArgs(String[] arguments) {
    this.args = (String[]) ArrayUtils.clone(arguments);
  }

  public String getResult() {
    return result;
  }

  public void setResult(String resultToSet) {
    this.result = resultToSet;
    if (resultToSet.equals("OK") || resultToSet.equals("PASS")) {
      setCommandSuccessful(true);
    } else {
      setCommandSuccessful(false);
    }
  }

  public String getCallingClass() {
    return callingClass;
  }

  public void setCallingClass(String classToCall) {
    this.callingClass = classToCall;
  }

  public boolean isCommandSuccessful() {
    return commandSuccessful;
  }

  public void setCommandSuccessful(boolean successfulCommand) {
    this.commandSuccessful = successfulCommand;
  }

  /**
   * Do not rely on stability of this method. Subject to change in the future.
   *
   * @return commandName and its args as one String
   */
  @Override
  public String toString() {
    return "commandName=" + commandName + ", args=" + ArrayUtils.toString(args);
  }

  public long getCmdStartMillis() {
    return cmdStartMillis;
  }

  public void setCmdStartMillis(long startMillis) {
    this.cmdStartMillis = startMillis;
  }

  public long getCmdEndMillis() {
    return cmdEndMillis;
  }

  public void setCmdEndMillis(long endMillis) {
    this.cmdEndMillis = endMillis;
  }

  public String getSourceMethod() {
    return sourceMethod;
  }

  public void setSourceMethod(String source) {
    this.sourceMethod = source;
  }

  /** @return time (millis) spent for a command */
  public long getDeltaMillis() {
    return this.cmdEndMillis - this.cmdStartMillis;
  }

  public boolean isExcludeFromLogging() {
    return excludeFromLogging;
  }

  public void setExcludeFromLogging(boolean toExcludeFromLogging) {
    this.excludeFromLogging = toExcludeFromLogging;
  }
}
