package com.softonicweb.container.framework;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import net.sf.json.JSON;
import net.sf.json.JSONArray;
import net.sf.json.JSONException;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

/**
 * Class containing methods to extract data from JSON formatted responses.
 *
 * @author pablo.blazquez
 * @author ivan.solomonoff
 *
 */
public class JsonParserUtil {

  private JSON json;

  /**
   * JSON Parser constructor.
   *
   * @param [String] source - string representation of a JSON object
   */
  public JsonParserUtil(String jsonString) {
    try {
      json = returnAppropiateType(JSONSerializer.toJSON(jsonString));
    } catch (Exception e) {
      throw new RuntimeException(e + "\nJSON source:\n" + jsonString);
    }
  }

  private JSON returnAppropiateType(JSON jsonSnippet) {
    return jsonSnippet.isArray() ? (JSONArray) jsonSnippet : (JSONObject) jsonSnippet;
  }

  public JsonParserUtil(File jsonFile) {
    json = getJsonFromFile(jsonFile);
  }

  public JsonParserUtil workWithInnerObject(String key) {
    json = getJSONObject(key);
    return this;
  }

  public JsonParserUtil workWithInnerArray(String key) {
    json = ((JSONObject) json).getJSONArray(key);
    return this;
  }

  public JsonParserUtil arrayToInnerObject(String key) {
    @SuppressWarnings("unchecked")
    Iterator<JSONObject> it = ((JSONArray) json).iterator();
    boolean found = false;
    while (it.hasNext()) {
      JSONObject obj = it.next();
      if (obj.containsKey(key)) {
        json = obj;
        found = true;
      }
    }
    if (!found) {
      throw new RuntimeException("Key not found: " + key);
    } else {
      return this;
    }
  }

  private JSON getJsonFromFile(File jsonFile) {
    try {
      return returnAppropiateType(JSONSerializer.toJSON(FileUtil.getTextFromFile(jsonFile)));
    } catch (Exception e) {
      throw new RuntimeException("Failed parsing json in file " + jsonFile, e);
    }
  }

  public JSON getJson() {
    return returnAppropiateType(json);
  }

  public int getInt(String key) {
    return ((JSONObject) json).getInt(key);
  }

  public String getString(String key) {
    return ((JSONObject) json).getString(key);
  }

  public boolean has(String key) {
    return ((JSONObject) json).has(key);
  }

  public JSONObject getJSONObject(String key) {
    return ((JSONObject) json).getJSONObject(key);
  }

  public JSONObject getArrayObject(String arrayKey, int index) {
    return safelyReturnArray(arrayKey).getJSONObject(index);
  }

  public String getValueFromArray(int index, String key) {
    try {
      return ((JSONArray) json).getJSONObject(index).getString(key);
    } catch (JSONException e) {
      return null;
    }
  }

  /** Returns the first array index containing the passed <key, value> set or -1 if not found. */
  public int getArrayIndexFor(String key, String value) {
    int index;
    @SuppressWarnings("unchecked")
    ListIterator<JSONObject> it = ((JSONArray) json).listIterator();
    while (it.hasNext()) {
      index = it.nextIndex();
      if (it.next().getString(key).equals(value)) {
        return index;
      }
    }
    return -1;
  }

  /**
   * Method to return the JSONArray under the given key.
   *
   * @param [String] key - name of the field
   * @return [JSONArray]
   */
  public JSONArray safelyReturnArray(String key) {
    if (((JSONObject) json).containsKey(key)) {
      return ((JSONObject) json).getJSONArray(key);
    } else {
      return null;
    }
  }

  /**
   * Method to get all values for a given key inside a JSONArray.
   *
   * @param [String] parentKey - name of the key in JSONArray format
   * @param [String] childKey - name of the target key to match
   * @return [List<Object>] - containing the values for this key
   */
  public List<Object> getListAllValuesForKey(String parentKey, String childKey) {
    JSONArray parent = safelyReturnArray(parentKey);
    List<Object> valuesList = new ArrayList<Object>();
    for (int i = 0; i < parent.size(); i++) {
      JSONObject child = parent.getJSONObject(i);
      if (child.containsKey(childKey)) {
        valuesList.add(child.get(childKey));
      } else {
        throw new RuntimeException("\nKey [" + childKey + "] not found at JSONArray:\n");
      }
    }
    return valuesList;
  }

  public Object[] getKeys() {
    return ((JSONObject) json).keySet().toArray();
  }
}
