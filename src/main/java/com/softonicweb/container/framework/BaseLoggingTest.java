package com.softonicweb.container.framework;

import java.io.File;
import java.lang.reflect.Method;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.log4j.Logger;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import com.softonicweb.container.framework.FileUtil;
import com.softonicweb.container.framework.TestNgUtil;

/**
 * Base class for all tests with custom asserts and logging.
 *
 * @author ivan.solomonoff
 * @author alfredo.lopez
 */
public abstract class BaseLoggingTest extends BaseTest {

  private static final int CONSTANTS_LENGTH = 13;
  private static final int MAX_PATH_LENGTH = 256;

  private static final String TEST_ARGUMENTS_KEY = "test-arguments";

  private static final Logger LOGGER = Logger.getLogger(BaseLoggingTest.class);

  @Override
  @BeforeMethod(alwaysRun = true)
  protected void setup(Method method, Object[] testArguments, ITestResult tr) {
    super.setup(method, testArguments, tr);
    tr.setAttribute(TEST_ARGUMENTS_KEY,
        FileUtil.replaceInvalidChars(getLogName(testArguments, tr)));
  }

  @Override
  @AfterMethod(alwaysRun = true)
  protected void teardown(ITestResult tr, Method method) {
    handleTestLog(tr, method);
    super.teardown(tr, method);
  }

  protected String getLogName(Object[] testArguments, ITestResult tr) {
    return ArrayUtils.toString(testArguments, "").replaceAll("\\{|\\}", "");
  }

  private synchronized String closeAndSaveTestLog(ITestResult tr, Method method) {
    // Tests using dataProviders inject different arguments, share test name,
    // and must log into different files.
    String testArguments = (String) tr.getAttribute(TEST_ARGUMENTS_KEY);
    String testName = method.getName() + (!testArguments.isEmpty() ? ("_" + testArguments) : "");
    if (instance().getName() != null && !instance().getName().isEmpty()) {
      testName = testName + "_" + instance().getName();
    }
    if (tr.getAttributeNames().contains("Browser")) {
      testName = testName + "_" + tr.getAttribute("Browser").toString();
    }
    testName = method.getDeclaringClass().getSimpleName() + "." + testName;

    int retryCount = TestNgUtil.getTestRetryCount(tr);
    String result;
    switch (tr.getStatus()) {
      case ITestResult.FAILURE:
        result = "FAIL";
        break;
      case ITestResult.SKIP:
        result = "SKIP";
        break;
      case ITestResult.SUCCESS:
        result = "PASS";
        break;
      case ITestResult.STARTED:
        result = "RETRIED";
        break;
      default:
        result = "UNKNOWN";
        break;
    }
    testName = FileUtil.replaceInvalidChars(testName);
    String logFileName =
        result + "_" + testName + (tr.isSuccess() ? "" : "_" + retryCount) + ".html";
    File directory = TestNgUtil.getMethodLogDir(SUITE_ENVIRONMENT, tr);
    LOGGER.info("Storing HTML report into " + directory);
    if (!directory.exists() && !directory.mkdirs()) {
      throw new RuntimeException("Directory " + directory.getPath() + " could not be created.");
    }

    // Some versions of Windows only support a maximum of 256 characters paths
    // We need to account for ".html", result_ and retry
    File logFile = new File(directory, logFileName);
    if (logFile.getAbsolutePath().length() > MAX_PATH_LENGTH) {
      int pathSize = MAX_PATH_LENGTH - CONSTANTS_LENGTH;
      logFileName = logFileName.replace(testName,
          testName.substring(0, pathSize - logFile.getParent().length()));
      logFile = new File(directory, logFileName);
    }

    logger().finalize(logFile, testName, SUITE_ENVIRONMENT, testEnvironment());
    return logFile.toString();
  }

  protected void handleTestLog(ITestResult tr, Method method) {
    try {
      closeAndSaveTestLog(tr, method);
      tr.setAttribute("hasMethodLevelLogging", "true");
    } catch (Exception e) {
      throw new RuntimeException(e);
    } finally {
      TestLogger.cleanup();
    }
  }

  /**
   * Wrapping out Junit assertions for adding a more comprehensive logging into
   * Selenium reports.
   */
  protected void assertTrue(String message, boolean condition) {
    LoggerAsserts.assertTrue(message, condition);
  }

  protected void assertFalse(String message, boolean condition) {
    LoggerAsserts.assertFalse(message, condition);
  }

  protected void assertEquals(String message, Object expected, Object actual) {
    LoggerAsserts.assertEquals(message, expected, actual);
  }

  protected void assertNotNull(String message, Object actual) {
    LoggerAsserts.assertNotNull(message, actual);
  }

  protected void fail(String message) {
    LoggerAsserts.fail(message);
  }

  protected TestLogger logger() {
    return TestLogger.getInstance();
  }

  protected void logComment(String message) {
    logger().logComment(message);
  }

  protected void logError(String message) {
    logger().logError(message);
  }

  protected void logSeparator() {
    logger().logSeparator();
  }

  public void logResource(String resourcePath) {
    logger().logResource(resourcePath);
  }

  protected abstract IScreenshooter getScreenshooter(String screenshotName);

  protected void logScreenshot(String screenshotName) {
    logger().logScreenshot(getScreenshooter(screenshotName));
  }

  /** Captures a screenshot if an unexpected exception was thrown. */
  protected void logTestFailureWithScreenshot(String label, Throwable throwable) {
    logger().logTestFailureWithScreenshot(getScreenshooter(label), throwable);
  }
}
