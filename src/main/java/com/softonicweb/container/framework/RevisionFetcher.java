package com.softonicweb.container.framework;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

/**
 * Fetches the project tag version from EN home page.
 *
 * @author ivan.solomonoff
 * @author thijs.vandewynckel
 */
public final class RevisionFetcher {

  private static final String EN_HOME = "https://en.softonic.com";

  private RevisionFetcher() {
  }

  public static String fetchGitTagVersion() {
    String home = EN_HOME;
    URL obj = null;
    try {
      obj = new URL(home);
    } catch (MalformedURLException e) {
      return "Tag not found";
    }
    HttpsURLConnection conn = null;
    try {
      conn = (HttpsURLConnection) obj.openConnection();
    } catch (IOException e) {
      return "Tag not found";
    }
    String versionResponse = conn.getHeaderField("x-version");
    String formattedVersionResponse = " v" + versionResponse;
    return formattedVersionResponse;
  }
}
