package com.softonicweb.container.framework;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

/**
 * Contains helper methods for encrypting and decrypting data.
 *
 * @author ivan.solomonoff
 */
public final class CryptoUtil {

  private CryptoUtil() {
    // Util class
  }

  public static String computeSHAOneSignature(String signature, String signatureHashKey) {
    SecretKeySpec secretKey = new SecretKeySpec(signatureHashKey.getBytes(), "HmacSHA1");
    Mac mac;
    try {
      mac = Mac.getInstance("HmacSHA1");
    } catch (NoSuchAlgorithmException e) {
      throw new RuntimeException(e);
    }

    try {
      mac.init(secretKey);
    } catch (InvalidKeyException e) {
      throw new RuntimeException(e);
    }

    byte[] text = signature.getBytes();

    return new String(Base64.encodeBase64(mac.doFinal(text))).trim();
  }
}
