package com.softonicweb.container.framework;

import com.softonicweb.container.framework.PartnersDb;
import com.softonicweb.container.framework.Softonic2Db;
import com.softonicweb.container.framework.HadoopTrackingDb;

/**
 * Repository of available databases.
 *
 * @author alfredo.lopez
 */
public final class DatabaseRepo {

  private DatabaseRepo() {
    // Default constructor to avoid Checkstyle error
  }

  public static Database getSoftonicToDb(String instance, Environment environment) {
    return new Softonic2Db(instance, environment);
  }

  public static Database getPartnersDb(Environment environment) {
    return new PartnersDb(environment);
  }

  public static Database getHadoopTrackingDb(Environment environment) {
    return new HadoopTrackingDb(environment);
  }
}
