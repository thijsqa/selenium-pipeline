package com.softonicweb.container.framework;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.output.FileWriterWithEncoding;
import org.apache.commons.lang.StringEscapeUtils;
import org.testng.ITestContext;
import org.testng.ITestNGMethod;
import org.testng.ITestResult;
import org.testng.internal.Utils;
import org.testng.log4testng.Logger;

import com.softonicweb.container.framework.FileUtil;

/**
 * A simple reporter that collects and prints failures in HTML.
 *
 * @author ivan.solomonoff
 */
public class HtmlStackTraceReporter extends ExtendedEmailableReporter {

  public static final String DETAILED_REPORT_FILENAME = "stacktrace-report.html";

  private static final Logger LOGGER = Logger.getLogger(HtmlStackTraceReporter.class);

  private final ISuiteEnvironment suiteEnvironment = SuiteEnvironment.getInstance();
  private final String suiteName = suiteEnvironment.getSuite().getName();
  private final String reportsDir = suiteEnvironment.getReportsDir();

  private PrintWriter methodOutput;

  @Override
  public void onFinish(ITestContext context) {
    generateReport(reportsDir);
    super.onFinish(context);
  }

  private ITestNGMethod[] resultsToMethods(List<ITestResult> results) {
    ITestNGMethod[] result = new ITestNGMethod[results.size()];
    int i = 0;
    for (ITestResult tr : results) {
      result[i++] = tr.getMethod();
    }

    return result;
  }

  private Map<String, String> getAttributeMap(ITestResult tr) {
    Map<String, String> attributes = new HashMap<String, String>();
    for (String attribute : tr.getAttributeNames()) {
      attributes.put(attribute, tr.getAttribute(attribute).toString());
    }

    return attributes;
  }

  private void addConfigurationResult(List<String> testResults, Object o) {
    ITestResult tr = (ITestResult) o;
    String stackTrace = getErrorMessage(tr);

    testResults.add(getLogResult(tr.getName(),
        getTestClass(tr) + " " + Utils.detailedMethodName(tr.getMethod(), false),
        stackTrace, tr.getParameters(),
        tr.getMethod().getConstructorOrMethod().getParameterTypes(), getAttributeMap(tr),
        getStackReport(tr)));
  }

  private void addTestResult(List<String> testResults, Object o) {
    ITestResult tr = (ITestResult) o;
    String stackTrace = getErrorMessage(tr);

    testResults.add(getLogResult(tr.getName(), getTestClass(tr) + "." + tr.getName(),
        stackTrace, tr.getParameters(), tr.getMethod().getConstructorOrMethod().getParameterTypes(),
        getAttributeMap(tr), getStackReport(tr)));
  }

  private String getTestClass(ITestResult tr) {
    String testClass = tr.getMethod().getTestClass().getName();
    return testClass.substring(testClass.lastIndexOf('.') + 1);
  }

  private void logResults() {
    methodOutput.println("<table><tbody>");
    List<String> testResults = new ArrayList<String>();

    // Log Text
    for (Object o : getConfigurationFailures()) {
      addConfigurationResult(testResults, o);
    }
    sortLogAndClearResults("FAILED CONFIGURATION", testResults);

    for (Object o : getConfigurationSkips()) {
      addConfigurationResult(testResults, o);
    }
    sortLogAndClearResults("SKIPPED CONFIGURATION", testResults);

    for (Object o : getFailedTests()) {
      addTestResult(testResults, o);
    }
    sortLogAndClearResults("FAILED", testResults);

    for (Object o : getFailedButWithinSuccessPercentageTests()) {
      addTestResult(testResults, o);
    }
    sortLogAndClearResults("RETRIED", testResults);


    for (Object o : getSkippedTests()) {
      addTestResult(testResults, o);
    }
    sortLogAndClearResults("SKIPPED", testResults);

    methodOutput.println("</tbody></table>");
  }

  private String getStackReport(ITestResult tr) {
    StringBuffer sb = new StringBuffer();
    Throwable tw = tr.getThrowable();
    String fullStackTrace = "";
    String id = "stack-trace" + tr.hashCode();

    if (tw != null) {
      String[] stackTraces = Utils.stackTrace(tw, true);
      fullStackTrace = stackTraces[1];
      // JavaScript link
      sb.append("<br><a href='#' onClick='toggleBox(\"")
          .append(id)
          .append("\", this, \"click to show stack trace\", \"click to hide stack trace\"); ")
          .append("return false'>")
          .append("click to show stack trace").append("</a>\n")
          .append("<div class='stack-trace' id='" + id + "' style='display: none;'>")
          .append("<pre>" + fullStackTrace + "</pre>").append("</div>");
    }

    return sb.toString();
  }

  private void sortLogAndClearResults(String status, List<String> testResults) {
    Collections.sort(testResults, Collator.getInstance());
    for (String result : testResults) {
      logResult(status, result);
    }
    testResults.clear();
  }

  private void logResume() {
    ITestNGMethod[] ft = resultsToMethods(getFailedTests());
    ITestNGMethod[] pt = resultsToMethods(getPassedTests());
    StringBuffer logBuf = new StringBuffer("<h4>Emailable report available <a href="
        + ExtendedEmailableReporter.REPORT_FILENAME + ">here</a></h4>");
    logBuf.append("===============================================<br>");
    logBuf.append("    ").append("Suite: " + suiteName).append("<br>");
    logBuf.append("    Total Tests: ")
        .append(Utils.calculateInvokedMethodCount(getAllTestMethods())).append(", Passes: ")
        .append(Utils.calculateInvokedMethodCount(pt))
        .append(", Failures: ").append(Utils.calculateInvokedMethodCount(ft)).append(", Skips: ")
        .append(Utils.calculateInvokedMethodCount(resultsToMethods(getSkippedTests())));
    int confFailures = getConfigurationFailures().size();
    int confSkips = getConfigurationSkips().size();
    if (confFailures > 0 || confSkips > 0) {
      logBuf.append("<br>").append("    Configuration Failures: ").append(confFailures)
          .append(", Skips: ").append(confSkips);
    }
    int retried = getFailedButWithinSuccessPercentageTests().size();
    if (retried > 0) {
      logBuf.append("<br>").append("    Retried: ").append(retried);
    }
    logBuf.append("<br>===============================================<br><br>");
    logResult("", logBuf.toString());
  }

  private void logResult(String status, String message) {
    StringBuffer buf = new StringBuffer();
    if (!"".equals(status)) {
      if (status.equalsIgnoreCase("failed configuration")) {
        buf.append("<tr class=\"" + "failedlight" + "\"><td");
      } else {
        buf.append("<tr class=\"" + status.toLowerCase() + "odd" + "\"><td");
      }
      buf.append(">").append(status).append(": ");
      buf.append("</td>");
    }
    buf.append(message);
    buf.append("</td></tr>");

    methodOutput.print(buf.toString());
  }

  private String getLogResult(String anchorName, String name, String stackTrace,
      Object[] params, @SuppressWarnings("rawtypes") Class[] paramTypes,
      Map<String, String> attributes, String stackTraceReport) {
    StringBuffer msg =
        new StringBuffer("\n<td><a name=\"" + anchorName + "\"><strong>" + name + "</strong></a>");

    if ((params != null && params.length > 0) || (attributes.size() > 0)) {
      msg.append(" [");
    }
    // The error might be a data provider parameter mismatch, so make
    // a special case here
    if (params != null) {
      if (params.length != paramTypes.length) {
        msg.append("<br>" + name + ": Wrong number of arguments were passed by "
            + "the Data Provider: found " + params.length + " but " + "expected "
            + paramTypes.length + ")");
      } else {
        for (int i = 0; i < params.length; i++) {
          if (i > 0) {
            msg.append("<br>");
          }
          msg.append(Utils.toString(params[i], paramTypes[i]));
          if (i == 0) { // first parameter is the line index
            msg.append("]");
          }
        }
      }
    }

    int i = 0;
    for (String attribute : attributes.keySet()) {
      if (i > 0) {
        msg.append("<br>");
      }
      msg.append(attribute + ": " + attributes.get(attribute));
      if (i == 0) { // first parameter is the line index
        msg.append("]");
      }
      i++;
    }

    msg.append("</td></tr>");
    msg.append("<tr><td></td><td>");
    // stack trace
    msg.append("\n<tr><td></td><td>").append(stackTrace).append(stackTraceReport)
        .append("</td></tr>");

    /*
    Do not log descriptions

    if (!Utils.isStringEmpty(description)) {
      msg.append("\n");
      for (int i = 0; i < status.length() + 2; i++) {
        msg.append(" ");
      }
      msg.append(description);
    }
    */

    return msg.toString();
  }

  public void ppp(String s) {
    System.out.println("[TextReporter " + suiteName + "] " + s);
  }

  private void generateReport(String outdir) {
    try {
      methodOutput = createWriter(outdir);
    } catch (IOException e) {
      LOGGER.error("output file", e);
      return;
    }

    startHtml(methodOutput);
    logResume();
    logResults();
    endHtml(methodOutput);
    methodOutput.flush();
    methodOutput.close();

    LOGGER.info("Report stored in: " + outdir);
  }

  private PrintWriter createWriter(String outdir) throws IOException {
    new File(outdir).mkdirs();
    return new PrintWriter(new BufferedWriter(
        new FileWriterWithEncoding(new File(outdir, DETAILED_REPORT_FILENAME), FileUtil.ENCODING)));
  }

  /** Starts HTML stream. */
  private void startHtml(PrintWriter out) {
    HtmlReporterUtil.startHtml(out, suiteName, DETAILED_REPORT_FILENAME);
    addJScode(out);
  }

  private void addJScode(PrintWriter out) {
    out.println("<script type=\"text/javascript\">"
        + "function flip(e) {"
        + "current = e.style.display;"
        + "if (current == 'block') {"
        + "e.style.display = 'none';"
        + "return 0;"
        + "}"
        + "else {"
        + "e.style.display = 'block';"
        + "return 1;"
        + "}"
        + "}"
        + "function toggleBox(szDivId, elem, msg1, msg2) {"
        + "var res = -1;  if (document.getElementById) {"
        + "res = flip(document.getElementById(szDivId));" + "}"
        + "else if (document.all) {"
        // this is the way old msie versions work
        + "res = flip(document.all[szDivId]);" + "}" + "if(elem) {"
        + "if(res == 0) elem.innerHTML = msg1; else elem.innerHTML = msg2;" + "}" + "}"
        + "</script>");
  }

  /** Finishes HTML stream. */
  private void endHtml(PrintWriter out) {
    out.println("</body></html>");
  }

  private String getErrorMessage(ITestResult tr) {
    Throwable ex = tr.getThrowable();
    String error = "Unknown reason, probably due to an unsatisfied configuration";
    if (ex != null) {
      error = StringEscapeUtils.escapeHtml(ex.getMessage());
    }

    return error;
  }
}
