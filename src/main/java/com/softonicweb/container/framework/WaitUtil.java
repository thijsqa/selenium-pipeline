package com.softonicweb.container.framework;


/**
 * Utility class for waiting to conditions to happen.
 *
 * @author alfredo.lopez
 */
public final class WaitUtil {

  private static final int WAIT_TIMEOUT = 1000;
  private static final int MULTIPLIER = 1000;

  private WaitUtil() {
  }

  /**
   * Encapsulates a condition to wait for.
   */
  public interface WaitCondition {

    boolean isTrue();
  }

  /**
   * Waits in seconds until a condition is met. It will add extra time after the condition is
   * satisfied.
   *
   * @param waitCondition condition to wait for
   * @param seconds seconds before time out is reached
   * @param finalSeconds waiting time added after condition is satisfied
   * @throws TimeoutException if condition is not met after waiting time
   */
  public static void waitFor(WaitCondition waitCondition, int seconds, int finalSeconds)
      throws TimeoutException {
    for (int second = 0;; second++) {
      if (second >= seconds) {
        throw new TimeoutException("Waiting time exceeded " + seconds + "s.");
      } else if (waitCondition.isTrue()) {
        break;
      }
      pause(WAIT_TIMEOUT);
    }
    if (finalSeconds > 0) {
      pause(finalSeconds * MULTIPLIER);
    }
  }

  /**
   * Waits in seconds until a conditions is met.
   *
   * @param waitCondition condition to wait for
   * @param seconds seconds before time out is reached
   * @throws TimeoutException if condition is not met after waiting time
   */
  public static void waitFor(WaitCondition waitCondition, int seconds) throws TimeoutException {
    waitFor(waitCondition, seconds, 0);
  }

  /**
   * Waits in milliseconds until a conditions is met.
   *
   * @param waitCondition condition to wait for
   * @param timeoutmillis milliseconds before time out is reached
   * @param pollMillis milliseconds before next condition polling
   * @throws TimeoutException if condition is not met after waiting time
   */
  public static void waitForMillis(WaitCondition waitCondition, long timeoutmillis, long pollMillis)
      throws TimeoutException {
    long startTime = System.currentTimeMillis();
    boolean conditionTrue = false;
    while ((System.currentTimeMillis() - startTime) < timeoutmillis) {
      conditionTrue = waitCondition.isTrue();
      if (!conditionTrue) {
        pause(pollMillis);
      } else {
        break;
      }
    }
    if (!conditionTrue) {
      throw new TimeoutException("Timeout exceeded!");
    }
  }

  /**
   * Pauses current thread.
   *
   * @param milliseconds
   */
  public static void pause(long milliseconds) {
    try {
      Thread.sleep(milliseconds);
    } catch (InterruptedException e) {
      throw new RuntimeException("Pause failed: thread " + Thread.currentThread() + " interrupted");
    }
  }
}
