package com.softonicweb.container.framework;

import java.util.HashMap;
import java.util.List;
import java.util.Random;

import net.lightbody.bmp.core.har.Har;
import net.lightbody.bmp.core.har.HarEntry;
import net.lightbody.bmp.core.har.HarLog;

import org.apache.log4j.Logger;

/**
 * Stores and control the BrowserMob Proxy sessions.
 *
 * @author ivan.solomonoff
 * @author thijs.vandewynckel
 */
public final class ProxySession {

  private static final int MIN_PORT = 9090;
  private static final int MAX_PORT = 9103;
  private static final int PORTS_TOTAL = 14;
  public static final String SINGLE_SESSION_ID = "single_session";
  private static final Logger LOGGER = Logger.getLogger(ProxySession.class);
  private static int lastUsedPort = 0;
  // pass this param to startSession if you want to generate random ports
  // private static final int RANDOM_PROXY_PORT = 0;

  private static HashMap<String, ProxyWrapper> sessions = new HashMap<String, ProxyWrapper>() {
    private static final long serialVersionUID = 1L;
  };

  // passes random ports between predefined port range
  public static int randomPortGenerator() {
    int[] randomPorts = randomPortsWithoutRepetition(MIN_PORT, MAX_PORT, PORTS_TOTAL);
    return randomPorts[lastUsedPort++];
    }

  public static int[] randomPortsWithoutRepetition(int start, int end, int count) {
    Random rng = new Random();

    int[] result = new int[count];
    int cur = 0;
    int remaining = end - start;
    for (int i = start; i < end && count > 0; i++) {
        double probability = rng.nextDouble();
        if (probability < ((double) count) / (double) remaining) {
            count--;
            result[cur++] = i;
        }
        remaining--;
    }
    return result;
  }

  private ProxySession() {
  }

  /** Get the current Proxy session. */
  public static ProxyWrapper proxy() {
    ProxyWrapper proxy = getProxyObject(SINGLE_SESSION_ID);
    if (proxy == null) {
      return getProxyObject(getCalleeName());
    } else {
      return proxy;
    }
  }

  /** Get the intended Proxy session. */
  public static ProxyWrapper proxy(String sessionId) {
    return getProxyObject(sessionId);
  }

  private static String getCalleeName() {
    StackTraceElement[] stack = Thread.currentThread().getStackTrace();
    for (StackTraceElement item : stack) {
      String entry = item.toString();
      if (entry.matches("com\\.softonic\\.tests\\..+\\.test.+")) {
        return item.getMethodName();
      }
    }
    return null;
  }

  public static ProxyWrapper getProxyObject(String key) {
    if (sessions.containsKey(key)) {
      return sessions.get(key);
    }
    return null;
  }

  private static ProxyWrapper startSession(int port, String sessionId) {
    ProxyWrapper proxy = proxy(sessionId);

    if (proxy == null) {
      proxy = new ProxyWrapper(port);
      try {
        proxy.start();
        LOGGER.info("BrowserMob proxy server started on port: " + proxy.getPort());
      } catch (Exception e) {
        e.printStackTrace();
        throw new RuntimeException("Unable to start proxy server: " + e.getMessage());
      }
      sessions.put(sessionId, proxy);
    }

    return proxy;
  }

  public static ProxyWrapper startSession(String sessionId) {
    return startSession(randomPortGenerator(), sessionId);
  }

  public static ProxyWrapper startSession() {
    return startSession(randomPortGenerator(), SINGLE_SESSION_ID);
  }

  public static void endSession() {
    endSession(SINGLE_SESSION_ID);
  }

  /** Method to end the session, should be used after the test. */
  public static void endSession(String sessionId) {
    ProxyWrapper proxy = getProxyObject(sessionId);
    if (proxy != null) {
      try {
        proxy.stop();
        sessions.remove(sessionId);
        LOGGER.info("Proxy session closed.");
      } catch (Exception e) {
        LOGGER.error("Failed to close proxy session: " + e.getMessage());
      }
      proxy = null;
    } else {
      LOGGER.error("Proxy session does not exist.");
    }
  }

  public static int getPort() {
    return proxy().getPort();
  }

  public static List<HarEntry> getLogEntries() {
    Har har = proxy().getHar();
    HarLog harLog = har.getLog();

    return harLog.getEntries();
  }
}
