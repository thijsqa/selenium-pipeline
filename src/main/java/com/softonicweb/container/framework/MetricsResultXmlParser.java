package com.softonicweb.container.framework;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.testng.TestNGException;
import org.testng.remote.strprotocol.IRemoteSuiteListener;
import org.testng.remote.strprotocol.IRemoteTestListener;
import org.testng.xml.XMLParser;
import org.xml.sax.SAXException;

/**
 * Parses testng-result.xml.
 *
 * @see MetricsResultXmlHandler
 *
 * (uses MetricsResultXmlHandler to provide start/end metric details)
 *
 * @author Cedric Beust <cedric@beust.com>
 */
public class MetricsResultXmlParser extends XMLParser<Object> {
  private final IRemoteTestListener m_testListener;
  private final IRemoteSuiteListener m_suiteListener;

  public MetricsResultXmlParser(IRemoteSuiteListener suiteListener, IRemoteTestListener testListener) {
    m_suiteListener = suiteListener;
    m_testListener = testListener;
  }

  @Override
  public Object parse(String currentFile, InputStream inputStream, boolean loadClasses) {
    MetricsResultXmlHandler handler =
        new MetricsResultXmlHandler(m_suiteListener, m_testListener);
    try {
      m_saxParser.parse(inputStream, handler);
      return null;
    }
    catch (FileNotFoundException e) {
      throw new TestNGException(e);
    } catch (SAXException e) {
      throw new TestNGException(e);
    } catch (IOException e) {
      throw new TestNGException(e);
    }
  }
}
