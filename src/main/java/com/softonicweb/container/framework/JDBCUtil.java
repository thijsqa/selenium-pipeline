package com.softonicweb.container.framework;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

/**
 * Utility methods for handling JDBC related task.
 *
 * @author alfredo.lopez
 */
public final class JDBCUtil {

  private static final Logger LOGGER = Logger.getLogger(JDBCUtil.class);

  private JDBCUtil() {
  }

  public static void closeConnection(Connection conn) {
    try {
      conn.close();
    } catch (Exception e) {
      LOGGER.warn("Closing Connection failed", e);
    }
  }

  public static void closeStatement(Statement stmt) {
    if (stmt != null) {
      try {
        stmt.close();
      } catch (SQLException e) {
        LOGGER.warn("Closing Statement failed", e);
      }
    }
  }

  public static void closeResultSet(ResultSet rs) {
    if (rs != null) {
      try {
        rs.close();
      } catch (SQLException e) {
        LOGGER.warn("Closing ResultSet failed", e);
      }
    }
  }
}
