package com.softonicweb.container.framework;

import java.sql.Date;

/**
 * Model for the 'file_downloads' table in Softonic2 database.
 *
 * @author alfredo.lopez
 */
public final class FileDownloadsTable {

  public static final String NAME = "file_downloads";

  private FileDownloadsTable() {
  }

  /**
   * Columns in FileDownloads table.
   *
   * @author alfredo.lopez
   */
  public enum Column implements TableColumn {
    ID_DOWN("id_down", Integer.class),
    ID_FILE("id_file", Integer.class, 1),
    TYPE("type", Integer.class, 2),
    DATE("date", Date.class, 3),
    WEEK("week", String.class),
    ID_COB("id_cob", Integer.class, 4),
    N_DOWNLOADS("n_downloads", Integer.class),
    N_DOWNLOADS_GLOBAL("n_downloads_global", Integer.class),
    COUNTRY("country", String.class, 5);

    private final String name;
    private final Class<?> type;
    private final int index;

    Column(String name, Class<?> type, int index) {
      this.name = name;
      this.type = type;
      this.index = index;
    }

    Column(String name, Class<?> type) {
      this(name, type, -1);
    }

    @Override
    public String getName() {
      return name;
    }

    @Override
    public Class<?> getType() {
      return type;
    }

    @Override
    public int index() {
      return index;
    }

    @Override
    public String toString() {
      return name;
    }
  }
}
