package com.softonicweb.container.framework;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Random;

/**
 * Contains helper methods for generating test data.
 *
 * @author ivan.solomonoff
 */
public final class DataGenerator {

  private static final String TIMESTAMP_FORMAT = "MM.dd_HH.mm.ss";
  private static final String DATE_AND_TIME_FORMAT = "EEE, d MMM yyyy HH:mm:ss";

  private DataGenerator() {
    // Util class
  }

  public static String getTimestamp() {
    Calendar cal = Calendar.getInstance();
    SimpleDateFormat date = new SimpleDateFormat(TIMESTAMP_FORMAT);
    return date.format(cal.getTime());
  }

  public static String getUniqueId() {
    final int length = 8;
    return (String) String.valueOf(java.util.UUID.randomUUID()).subSequence(0, length);
  }

  public static String getDayAndTime() {
    Calendar cal = Calendar.getInstance();
    SimpleDateFormat date = new SimpleDateFormat(DATE_AND_TIME_FORMAT);
    return date.format(cal.getTime());
  }

  public static int getDayOfTheYear() {
    Calendar cal = Calendar.getInstance();
    return cal.get(Calendar.DAY_OF_YEAR);
  }

  public static int getYear() {
    Calendar cal = Calendar.getInstance();
    return cal.get(Calendar.YEAR);
  }

  /** Returns a Gmail address of the kind: qa.softonic+$timestamp@gmail.com. */
  public static String getUniqueGmailAddress() {
    return "qa.softonic+" + getTimestamp() + "@gmail.com";
  }

  /** Generates a random number. */
  public static synchronized int getRandomNumber() {
    final int range = 99999;
    return new Random().nextInt(range);
  }

  /** Generates a random number between specific limits. */
  public static synchronized int getRandomNumberLimits(int min, int max) {
    return new Random().nextInt(max - min) + min;
  }
}
