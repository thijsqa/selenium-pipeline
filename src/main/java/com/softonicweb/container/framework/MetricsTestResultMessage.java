package com.softonicweb.container.framework;

import org.testng.remote.strprotocol.TestResultMessage;

/**
 * An <code>IStringMessage</code> implementation for test results events.
 *
 * @author <a href='mailto:the_mindstorm[at]evolva[dot]ro'>Alexandru Popescu</a>
 */
public class MetricsTestResultMessage extends TestResultMessage {

  /**
   * 
   */
  private static final long serialVersionUID = -6446209110069522950L;
  private final boolean m_isConfig;

  public MetricsTestResultMessage(int resultType, String suiteName,
      String testName, String className, String methodName,
      String testDescriptor, String instanceName, String[] params,
      long startMillis, long endMillis, String stackTrace, int invocationCount,
      int currentInvocationCount, String isConfig) {
    super(resultType, suiteName, testName, className, methodName, testDescriptor,
        instanceName, params, startMillis, endMillis, stackTrace, invocationCount,
        currentInvocationCount);
    if (isConfig != null) {
      m_isConfig = true;
    } else {
      m_isConfig = false;
    }
  }

  public boolean isConfig() {
    return m_isConfig;
  }
}