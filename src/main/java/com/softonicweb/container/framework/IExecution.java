package com.softonicweb.container.framework;

/**
 * Interface representing a test execution.
 *
 * @author ivan.solomonoff
 */
public interface IExecution {

  /* Intended to be used for complete test runs to spot side-effects of implemented features. */
  String REGRESSION = "REGRESSION";
  /*
   * Intended to be used as a double-check for already tested code (eg. production), or as a
   * preliminary test (eg. to consider a build ready for deeper testing).
   */
  String SMOKE = "SMOKE";
}
