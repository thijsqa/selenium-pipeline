package com.softonicweb.container.framework;

import com.softonicweb.container.framework.IAccount;

/**
 * Contains the user account data.
 *
 * @author ivan.solomonoff
 */
public enum Accounts implements IAccount {

  QA("qa-auto@softonic.com", "1UserpaQA", "qauser"),
  QA_MYSQL("'qa'@'%'", "QAtomate2", "qa"),
  MANAGER("qa.softonic+manager@gmail.com", "QAtomate2", "QATeam"),
  SOFTONIC("qa.softonic@gmail.com", "test1234", "qa.softoe11"),
  SOFTONIC_API("qa.softonic+api@gmail.com", "test1234", "qa.softonic+api"),
  SOFTONIC_JP("josemaria.delbarco@softonic.com", "softonic", "chemaka"),
  SOFTONIC_ADS("qa.softonic+sads@gmail.com", "softonic", "qa+sads"),
  SOFTONIC_ADS_BACKEND("example@example.com", "softonic", "josemaria.delbarco"),
  CONTROLPANEL("qa.softonic+cp@gmail.com", "S0ft0n1c", "qa.softonic+cp@gmail.com"),
  FACEBOOK("qa.softonic@gmail.com", "softonic.qa", "qa.softonic"),
  FACEBOOK2("testingt123456@gmail.com", "test1234567", "testingt123456"),
  FACEBOOK_SOLUTIONS("qa.softonic@gmail.com", "softonic.qa", "filiberto de la fuen"),
  TWITTER("qa.softonic+andrea@gmail.com", "softonic.qa", "Andrea"),
  GOOGLEPLUS("qa.softonic.gooplus@gmail.com", "softonic.qa", "qa.softonic.gooplus"),
  GOOGLEPLUS2("testingt1234567@gmail.com", "t43st123456789", "testingt1234567"),
  GOOGLEPLUS_SOLUTIONS("qa.solutions.tests@gmail.com", "solutions.qa", "QA Tests Solutions"),
  SAUCE_LABS("", "de1f4f47-4916-4272-bbc7-7562782dcce9", "softonic"),
  BROWSERSTACK("", "VNr43lRiaSRoKTrQkoBC", "softonic"),
  GMAIL_DEFAULT("qa.softonic@gmail.com", "qatomate2", "qa.softonic");

  private String username;
  private String email;
  private String password;

  private Accounts(String email, String password, String username) {
    this.username = username;
    this.email = email;
    this.password = password;
  }

  @Override
  public String getUsername() {
    return username;
  }

  @Override
  public String getEmail() {
    return email;
  }

  @Override
  public String getPassword() {
    return password;
  }

  @Override
  public String getFirstname() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public String getLastname() {
    // TODO Auto-generated method stub
    return null;
  }
}
