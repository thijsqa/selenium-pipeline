package com.softonicweb.container.framework;

import com.softonicweb.container.framework.IPlatform;

/**
 * Represents a test Client.
 *
 * @author ivan.solomonoff
 */
public enum DefaultClient implements IPlatform {
  HTTP;

  @Override
  public String toString() {
    return super.toString().toLowerCase();
  }
}
