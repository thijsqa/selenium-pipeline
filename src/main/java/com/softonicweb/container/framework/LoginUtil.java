package com.softonicweb.container.framework;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;

/**
 * Contains helper methods to Login to a Softonic product via HTTP Post request.
 *
 * @author ivan.solomonoff
 */
public final class LoginUtil {

  private static final String UNIQUE_USER_URL = "http://profile.softonic.com/login";

  private LoginUtil() {
  }

  public static HttpClient login(Environment environment, String urlRedirect, Accounts user) {
    String url = UrlsUtil.convertToOldEnvironmentUrlIfNeeded(environment, UNIQUE_USER_URL);

    List<NameValuePair> loginValues = new ArrayList<NameValuePair>();
    loginValues.add(new BasicNameValuePair("url_redirect_login", urlRedirect));
    loginValues.add(new BasicNameValuePair("action", "login"));
    loginValues.add(new BasicNameValuePair("id_recommend", ""));
    loginValues.add(new BasicNameValuePair("email", user.getEmail()));
    loginValues.add(new BasicNameValuePair("password", user.getPassword()));

    HttpPost httpPost = new HttpPost(url);
    RequestConfig config = RequestConfig.custom().setCircularRedirectsAllowed(true).build();
    HttpClient httpClient = HttpClientBuilder.create()
        .setRedirectStrategy(new LaxRedirectStrategy())
        .setDefaultRequestConfig(config)
        .build();

    BasicHttpContext httpContext = new BasicHttpContext();
    try {
      httpPost.setEntity(new UrlEncodedFormEntity(loginValues));

      TestLogger.getInstance().logComment("Executing POST to: " + url);
      HttpResponse httpResponse = httpClient.execute(httpPost, httpContext);
      TestLogger.getInstance().logComment("Response:\n" + httpResponse);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }

    return httpClient;
  }
}
