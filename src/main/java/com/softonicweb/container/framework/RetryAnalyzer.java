package com.softonicweb.container.framework;

import org.apache.log4j.Logger;
import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

/**
 * Implementation of TestNG's retry analyzer.
 * <p>Stores the logic for retrying test methods.
 *
 * @author ivan.solomonoff
 */
public class RetryAnalyzer implements IRetryAnalyzer {

  private static final Logger LOGGER = Logger.getLogger(RetryAnalyzer.class);

  private static int numRetries = 0;

  private final InheritableThreadLocal<Integer> count = new InheritableThreadLocal<Integer>() {
    @Override
    protected synchronized Integer initialValue() {
      return new Integer(0);
    }
  };

  @Override
  public boolean retry(ITestResult result) {
    result.setAttribute("Retry", count.get());
    count.set(count.get() + 1);

    if (isRetryAvailable()) {
      LOGGER.info("Retrying test: " + result.getName() + ". Retry count: " + count.get());
      return true;
    } else {
      count.set(0);
      return false;
    }
  }

  public int getRetryCount() {
    return count.get();
  }

  public void resetRetryCount() {
    count.set(0);
  }

  private boolean isRetryAvailable() {
    return count.get() <= numRetries;
  }

  public static void setNumRetries(int count) {
    numRetries = count;
  }

  public static int getNumRetries() {
    return numRetries;
  }
}
