package com.softonicweb.container.framework;

import static org.apache.commons.io.FileUtils.copyURLToFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpCoreContext;

/**
 * FileDownloaderUtil class to perform file downloads in a cross platform way
 * without resorting to hacks like AutoIT.
 *
 * @author guillem.hernandez
 * @author ivan.solomonoff
 */
public class FileDownloaderUtil {

  private static final int RANDOM_FILENAME_LENGTH = 8;
  protected boolean followRedirects;
  protected int downloadHttpStatus;
  protected File downloadsFolder;
  protected File destinationFile;

  public FileDownloaderUtil() {
    followRedirects = true;
  }

  protected TestLogger logger() {
    return TestLogger.getInstance();
  }

  /**
   * Should redirects be followed before returning status code? If set to true a
   * 302 will not be returned, instead you will get the status code after the
   * redirect has been followed DEFAULT: true
   */
  public void setFollowRedirects(Boolean value) {
    followRedirects = value;
  }

  /**
   * sets whether the FileDownloader class should follow redirects when trying
   * to download a file.
   */
  public boolean getFollowRedirects() {
    return followRedirects;
  }

  /**
   * Get the local download folder.
   */
  public File getDownloadsFolder() {
    return downloadsFolder;
  }

  /**
   * Set the local download folder.
   */
  public void setDownloadsFolder(File folder) {
    downloadsFolder = folder;
  }

  /**
   * Gets the HTTP status code of the last download file attempt.
   */
  public int getLastDownloadHttpStatus() {
    return downloadHttpStatus;
  }

  public boolean downloadFromUrl(String url, String destinationDir, String byteRange) {
    HttpResponse response = downloadFromUrlResponse(url, destinationDir, byteRange);
    return response != null;
  }

  /**
   * Starts a download from the specified URL.
   *
   * @param url Url where to start the download from.
   * @param destinationDir Local destination folder.
   * @param byteRange ie. "500-999", or null for whole file.
   *        See http://greenbytes.de/tech/webdav/rfc2616.html#header.range
   *
   * @return HttpResponse object with the server response
   */
  public HttpResponse downloadFromUrlResponse(String url, String destinationDir, String byteRange) {
    URL source;
    URI sourceUri;
    try {
      source = new URL(url);
      sourceUri = source.toURI();
    } catch (MalformedURLException e) {
      throw new RuntimeException(e);
    } catch (URISyntaxException e) {
      throw new RuntimeException(e);
    }
    File parentFolder = null;
    if (destinationDir != null) {
      parentFolder = new File(destinationDir);
    } else {
      parentFolder = downloadsFolder;
    }

    if (!parentFolder.mkdirs() && !parentFolder.isDirectory()) {
      throw new RuntimeException("Directory path " + parentFolder.getPath()
          + " could not be created.");
    }

    HttpClient client = setupClient(new BasicCookieStore());
    HttpGet httpGet = new HttpGet(sourceUri);

    if (byteRange != null) {
      httpGet.addHeader("Accept-Ranges", "bytes");
      httpGet.addHeader("Range", "bytes=" + byteRange);
    }

    logger().logComment("GET request being sent to: " + httpGet.getURI());
    HttpResponse response = null;
    BasicHttpContext ctx = new BasicHttpContext();
    try {
      response = client.execute(httpGet, ctx);
      downloadHttpStatus = response.getStatusLine().getStatusCode();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    logger().logComment("GET response status: " + downloadHttpStatus);

    if ((byteRange == null) && (downloadHttpStatus != HttpStatus.SC_OK)) {
      logger().logComment("Unable to start the download, response status: " + downloadHttpStatus);
      return null;
    } else if ((byteRange != null) && (downloadHttpStatus != HttpStatus.SC_PARTIAL_CONTENT)) {
      logger().logComment(
          "Error. Response status is not PARTIAL CONTENT (206): "
          + downloadHttpStatus);
      return null;
    } else {
      destinationFile = new File(parentFolder, getServerFileNameFromURI(url, ctx));
      downloadFileFromUrl(source, destinationFile, byteRange);
      return response;
    }
  }

  /**
   * Perform the file/image download using directly the URL.
   *
   * @return true if the download has been correctly performed
   */
  public boolean downloadFromUrl(String url, String destinationDir) {
    File parentFolder;
    String realFileName = null;
    try {
      if (destinationDir != null) {
        parentFolder = new File(destinationDir);
      } else {
        parentFolder = downloadsFolder;
      }
      //try to fetch the filename
      HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
      String raw = conn.getHeaderField("Content-Disposition");
      // realFileName = "attachment; filename=abc.jpg"
      if (raw != null && raw.contains("=")) {
        realFileName = raw.split("=")[1]; // getting value after '='
        realFileName = realFileName.replace("\"", ""); // remove all " if any
      } else if (url.contains("filename=")) {
        realFileName = url.split("filename=")[1].split("&")[0]; // may have extra arguments
      } else {
        // unable to determine file name, saving as random string
        realFileName = RandomStringUtils.randomAlphanumeric(RANDOM_FILENAME_LENGTH) + ".exe";
      }
      destinationFile = new File(parentFolder, realFileName);
      logger().logComment("Downloading from URL: " + url + " to local file: " + destinationFile);
      copyURLToFile(new URL(url), destinationFile);
      return true;
    } catch (Exception e) {
      return false;
    }
  }

  protected void downloadFileFromUrl(URL source, File destination, String byteRange) {
    logger().logComment("Downloading to local file: " + destination);
    try {
      HttpURLConnection connection = (HttpURLConnection) source.openConnection();
      connection.setRequestMethod("GET");
      if (byteRange != null) {
        connection.setRequestProperty("Accept-Ranges", "bytes");
        connection.setRequestProperty("Range", "bytes=" + byteRange);
        IOUtils.copy(connection.getInputStream(), new FileOutputStream(destination));
      } else {
        copyURLToFile(source, destination);
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    logger().logComment("File downloaded successfully: " + destination);
  }

  protected static String getServerFileNameFromURI(String url, BasicHttpContext ctx) {
    String randomId = DataGenerator.getRandomNumber() + "-";
    String realFileName = null;
    if (url.contains("filename=")) {
      realFileName = url.split("filename=")[1];
      realFileName = realFileName.split("&")[0]; // may have extra arguments
    } else {
      realFileName = getServerFileName(ctx);
    }
    return randomId + realFileName;
  }

  private static String getServerFileName(BasicHttpContext ctx) {
    HttpUriRequest lastRedirectRequest =
        (HttpUriRequest) ctx.getAttribute(HttpCoreContext.HTTP_REQUEST);
    File serverFile = new File(lastRedirectRequest.getURI().getPath());
    return serverFile.getName();
  }

  public File getLastDownloadFile() {
    return destinationFile;
  }

  private static void deleteFile(File file) {
    if (file.exists()) {
      if (!file.canWrite()) {
        file.setWritable(true);
      }

      if (!file.delete()) {
        throw new RuntimeException("Unable to delete file: " + file.getAbsolutePath());
      }
    }
  }

  public void deleteLastDownloadFile() {
    if (destinationFile == null) {
      throw new RuntimeException("No download file has been stored.");
    }

    deleteFile(destinationFile);
  }

  public void deleteAllDownloadedFiles() {
    for (File file : downloadsFolder.listFiles()) {
      if (file.isFile()) {
        file.delete();
      }
    }
    destinationFile = null;
  }

  protected HttpClient setupClient(CookieStore cookieStore) {
    return HttpClientBuilder
        .create()
        .setDefaultCookieStore(cookieStore)
        .setDefaultRequestConfig(
        RequestConfig.custom().setRedirectsEnabled(getFollowRedirects()).build()).build();
  }
}
