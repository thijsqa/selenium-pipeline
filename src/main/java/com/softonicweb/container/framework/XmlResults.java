package com.softonicweb.container.framework;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URI;
import java.util.LinkedList;
import java.util.List;

import org.testng.remote.strprotocol.SuiteMessage;
import org.testng.remote.strprotocol.TestResultMessage;

/**
 * Stores testng results parsed from a testng-result.xml file.
 *
 * @author alfredo.lopez
 */
public class XmlResults {

  private final List<MetricsTestResultMessage> passedTests = new LinkedList<MetricsTestResultMessage>();
  private final List<MetricsTestResultMessage> failedTests = new LinkedList<MetricsTestResultMessage>();
  private final List<MetricsTestResultMessage> skippedTests = new LinkedList<MetricsTestResultMessage>();
  private Long suiteStartMillis;
  private Long suiteEndMillis;

  private final RemoteSuiteAdapter suiteAdapter = new RemoteSuiteAdapter() {
    @Override
    public void onStart(SuiteMessage sm) {
      suiteStartMillis = ((MetricsSuiteMessage)sm).getStartMillis();
      suiteEndMillis = ((MetricsSuiteMessage)sm).getEndMillis();
    }
  };

  private final RemoteTestAdapter testAdapter = new RemoteTestAdapter() {
    @Override
    public void onTestSuccess(TestResultMessage trm) {
      passedTests.add((MetricsTestResultMessage)trm);
    }

    @Override
    public void onTestFailure(TestResultMessage trm) {
      failedTests.add((MetricsTestResultMessage)trm);
    }

    @Override
    public void onTestSkipped(TestResultMessage trm) {
      skippedTests.add((MetricsTestResultMessage)trm);
    }
  };

  public XmlResults(String xmlUriString) {
    URI xmlUri = null;
    try {
      xmlUri = new URI(xmlUriString);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
    parseXmlStream(openXmlStream(xmlUri));
  }

  public XmlResults(URI xmlUri) {
    parseXmlStream(openXmlStream(xmlUri));
  }

  private InputStream openXmlStream(URI xmlUri) {
    InputStream xmlInputStream = null;
    try {
      if (xmlUri.getScheme().startsWith("http")) {
        xmlInputStream = xmlUri.toURL().openStream();
      } else {
        xmlInputStream = new FileInputStream(new File(xmlUri));
      }
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
    return xmlInputStream;
  }

  private void parseXmlStream(InputStream xmlInputStream) {
    MetricsResultXmlParser parser = new MetricsResultXmlParser(suiteAdapter, testAdapter);
    parser.parse("", xmlInputStream, false);
  }

  public Long getSuiteStartMillis() {
    return suiteStartMillis;
  }

  public Long getSuiteEndMillis() {
    return suiteEndMillis;
  }

  public List<MetricsTestResultMessage> getPassedTests() {
    return passedTests;
  }
}