package com.softonicweb.container.framework;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.rowset.CachedRowSet;

import com.softonicweb.container.framework.JDBCUtil;
import com.sun.rowset.CachedRowSetImpl;

/**
 * A Decorator for a java.sql.Connection object.
 *
 * @author alfredo.lopez
 */
@SuppressWarnings("restriction")
public abstract class AbstractConnection implements Connection {

  private static final String SQL_SELECT_DATE = "SELECT CURDATE() AS date";

  private java.sql.Connection connection;

  protected void setConnection(java.sql.Connection conn) {
    connection = conn;
  }

  protected java.sql.Connection connection() {
    return connection;
  }

  @Override
  public Statement createStatement() throws SQLException {
    return connection.createStatement();
  }

  @Override
  public PreparedStatement prepareStatement(String sql) throws SQLException {
    return connection.prepareStatement(sql);
  }

  @Override
  public CachedRowSet getCachedResults(String sql) throws SQLException {
    Statement stmt = null;
    ResultSet rs = null;
    CachedRowSet cachedRowSet = null;

    try {
      stmt =  createStatement();
      rs = stmt.executeQuery(sql);
      cachedRowSet = new CachedRowSetImpl();
      cachedRowSet.populate(rs);
    } catch (SQLException e) {
      throw e;
    } finally {
      JDBCUtil.closeResultSet(rs);
      JDBCUtil.closeStatement(stmt);
    }

    return cachedRowSet;
  }

  @Override
  public Date getCurrentDate() {
    Date date;
    Statement stmt = null;
    ResultSet rs = null;
    try {
      stmt = connection.createStatement();
      rs = stmt.executeQuery(SQL_SELECT_DATE);
      rs.next();
      date = rs.getDate("date");
    } catch (Exception e) {
      throw new RuntimeException(e);
    } finally {
      JDBCUtil.closeResultSet(rs);
      JDBCUtil.closeStatement(stmt);
    }
    return date;
  }

  @Override
  public boolean isClosed() throws SQLException {
    return connection.isClosed();
  }

  @Override
  public void close() {
    JDBCUtil.closeConnection(connection);
  }
}
