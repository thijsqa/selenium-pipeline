package com.softonicweb.container.framework;

import org.testng.ITestContext;
import org.testng.ITestResult;

import com.softonicweb.container.framework.HtmlStackTraceReporter;

/**
 * Extends TestNG ListenerAdapter to add custom functionality based on test execution.
 *
 * @author ivan.solomonoff
 */
public class SoftonicTestListenerAdapter extends HtmlStackTraceReporter {

  private String revision = RevisionFetcher.fetchGitTagVersion();

  /** Runs only once at test suite start. */
  @Override
  public void onStart(ITestContext tc) {
    System.out.println("Git Tag version:" + revision);
    super.onStart(tc);
  }

  @Override
  public void onTestSkipped(ITestResult tr) {
    System.out.println("Test " + tr.getName() + " -> Skipped");
    super.onTestSkipped(tr);
  }

  @Override
  public void onTestFailure(ITestResult tr) {
    System.out.println("Test " + tr.getName() + " -> Failed");
    super.onTestFailure(tr);
  }

  @Override
  public void onTestSuccess(ITestResult tr) {
    System.out.println("Test " + tr.getName() + " -> Success");
    super.onTestSuccess(tr);
  }

  @Override
  public void onFinish(ITestContext tc) {
    super.onFinish(tc);
  }

  @Override
  public void onTestFailedButWithinSuccessPercentage(ITestResult tr) {
    System.out.println("Test " + tr.getName() + " -> Failed, retrying...");
    tr.setStatus(ITestResult.STARTED);
    super.onTestFailedButWithinSuccessPercentage(tr);
  }
}
