package com.softonicweb.container.framework;

import java.util.List;

import org.apache.commons.lang.StringUtils;

/**
 * Post processes LogItem event list.
 * <p>Summarizes time spent in methods
 *
 * @author ivan.solomonoff
 */
class LogEventsPostProcessor {

  private ResultsFormatter formatter;

  /**
   * Loop through logItems and output it to the formatter.
   *
   * @param outputFormatter formatter handling formatting and storing of the
   *                        gathered log-events
   * @param loggingEventsQueue all logging events including every event inside
   *                           a wait and commands not to be logged
   * @param metrics some basic metrics and an brief abstract of the test run
   * @param doLogMethodsAsComments log method-name just entered executing
   *                               commands as a comment
   */
  public LogEventsPostProcessor(ResultsFormatter outputFormatter, List<LogItem> loggingEventsQueue,
      TestMetrics metrics) {
    formatter = outputFormatter;

    metrics.setEndTimeStamp(System.currentTimeMillis());
    formatAllLogs(metrics, loggingEventsQueue);
  }

  /** Loop through logEvents and output it to the formatter. */
  private void formatAllLogs(TestMetrics metrics, List<LogItem> logEvents) {
    getResultsFormatter().logHeaderEvent(metrics);

    String currentMethodName = "";
    LogItem rootLogItem = new LogItem();
    boolean completeResult = true;
    for (int i = 0; i < logEvents.size(); i++) {
      LogItem currentLoggingEvent = logEvents.get(i);
      if (!currentLoggingEvent.getSourceMethod().equals(currentMethodName)
          && !(currentLoggingEvent.getSourceMethod().equals("unknown"))) {
        if (StringUtils.isNotEmpty(currentMethodName)) {
          postProcessMethod(completeResult, rootLogItem, currentMethodName);
          completeResult = true;
        }

        currentMethodName = currentLoggingEvent.getSourceMethod();
        rootLogItem = new LogItem();
        rootLogItem.setCallingClass(currentLoggingEvent.getCallingClass());
        logNewMethodEntered(currentMethodName, rootLogItem);
      }
      formatAndOutputCommand(currentLoggingEvent);
      rootLogItem.addChild(currentLoggingEvent);

      completeResult = completeResult && currentLoggingEvent.isCommandSuccessful();
    }

    postProcessMethod(completeResult, rootLogItem, currentMethodName);

    getResultsFormatter().logFooterEvent();
  }

  /**
   * Calls command-type dependent method in formatter. Decides whether command
   * is a comment, has boolean result or is just a normal command.
   *
   * @param logItem containing details of the command to be formatted and
   *                logged
   */
  private void formatAndOutputCommand(LogItem logItem) {
    if (logItem.getCommandName().equals("textarea")) {
      formatter.logTextArea(logItem);
      return;
    }

    if (logItem.getCommandName().equals("comment")) {
      formatter.logCommentEvent(logItem);
      return;
    }

    if (logItem.getCommandName().equals("method")) {
      formatter.logMethodEvent(logItem);
      return;
    }

    if (logItem.getCommandName().contains("separator")) {
      formatter.logSeparator();
      return;
    }

    if (logItem.getCommandName().contains("assert")) {
      formatter.logBooleanCommandEvent(logItem);
      return;
    }

    if (logItem.getCommandName().equals("findElement")) {
      formatter.logSpecialCommandEvent(logItem);
      return;
    }

    formatter.logCommandEvent(logItem);
  }

  /**
   * Log test data after execution of each test method.
   *
   * @param completeMethodResult result for the whole test method, true if all
   *        tests succeded
   * @param currentLogItem logging bean containing the method data
   * @param currentMethod current executed method
   */
  private void postProcessMethod(boolean completeMethodResult, LogItem currentLogItem,
      String currentMethod) {
    currentLogItem.setCommandSuccessful(completeMethodResult);
    currentLogItem.setArgs(new String[] { "executing " + currentLogItem.getCallingClass() + "::"
        + currentMethod});
  }

  /**
   * Log method-name just entered executing a command as a comment.
   *
   * @param currentMethodName actual method
   * @param logItem comment logItem
   */
  private void logNewMethodEntered(final String currentMethodName, LogItem logItem) {
    logItem.setCommandName("method");
    logItem.setArgs(new String[] {"executing " + currentMethodName + "()"});
    formatAndOutputCommand(logItem);
  }

  private ResultsFormatter getResultsFormatter() {
    return formatter;
  }
}
