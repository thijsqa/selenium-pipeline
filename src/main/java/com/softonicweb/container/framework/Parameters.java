package com.softonicweb.container.framework;

import java.util.HashMap;
import java.util.Map;

import com.softonicweb.container.framework.ISuite;
import com.softonicweb.container.framework.JsonParserUtil;

/**
 * Fetches and stores as system properties the parameters provided by a json config file.
 *
 * @author ivan.solomonoff
 */
public final class Parameters {

  private static final String PARAMETERS_KEY = "parameters";
  private static final String ENVIRONMENT_KEY = "environment";
  private static final String INSTANCE_KEY = "instance";
  private static final String BASE_URL_KEY = "baseUrl";
  private static final String DEVICES_KEY = "devices";
  private static final String SELENIUM_HOST_KEY = "seleniumHost";
  private static final String SELENIUM_PORT_KEY = "seleniumPort";
  private static final String TEST_NAME_KEY = "test.name";
  private static final String CLASS_NAME_KEY = "class.name";
  private static final String EXECUTION_KEY = "execution";
  private static final String EMAIL_TO_KEY = "email.to";
  private static final String NO_EMAIL_KEY = "noEmail";
  private static final String BASE_REPORTS_DIR_KEY = "base.reports.dir";

  private final Map<String, String> parameters;

  public Parameters(String jsonString, boolean fetchFromJvmArgs) {
    parameters = new HashMap<String, String>();
    JsonParserUtil json = new JsonParserUtil(jsonString).workWithInnerObject(PARAMETERS_KEY);
    for (Object objKey : json.getKeys()) {
      String key = (String) objKey;
      String value;
      if (fetchFromJvmArgs) {
        value = System.getProperty(key, "");
      } else {
        value = json.getString(key);
        System.setProperty(key, value);
      }
      this.parameters.put(key, value);
    }
  }

  public Map<String, String> getParameters() {
    return this.parameters;
  }

  public String getParameter(String paramName, String defaultValue) {
    String value = this.parameters.get(paramName);
    return (value == null || value.isEmpty()) ? defaultValue : value;
  }

  public String getParameter(String paramName) {
    return this.parameters.get(paramName);
  }

  public String getEnvironment() {
    return getParameter(ENVIRONMENT_KEY, Environment.DEFAULT_ENVIRONMENT);
  }

  public String getInstance() {
    return getParameter(INSTANCE_KEY, Instance.DEFAULT_NAME);
  }

  public String getBaseUrl() {
    return getParameter(BASE_URL_KEY, "");
  }

  public String getDevices() {
    return getParameter(DEVICES_KEY, ISuite.DEFAULT_DEVICE.toString());
  }

  public String getSeleniumHost() {
    return getParameter(SELENIUM_HOST_KEY, RunTarget.EU_FRANKFURT.getHost());
  }

  public String getSeleniumPort() {
    return getParameter(SELENIUM_PORT_KEY, RunTarget.EU_FRANKFURT.getPort());
  }

  public String getTestName() {
    return getParameter(TEST_NAME_KEY, "");
  }

  public String getClassName() {
    return getParameter(CLASS_NAME_KEY, "");
  }

  public String getExecution() {
    return getParameter(EXECUTION_KEY, "");
  }

  public String getEmailTo() {
    return getParameter(EMAIL_TO_KEY, "");
  }

  public String getNoEmail() {
    // We will very unlikely want to set this from the JSON config files.
    return System.getProperty(NO_EMAIL_KEY);
  }

  public String getBaseReportsDir() {
    return getParameter(BASE_REPORTS_DIR_KEY, ".");
  }
}
