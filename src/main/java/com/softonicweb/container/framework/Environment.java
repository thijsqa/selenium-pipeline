package com.softonicweb.container.framework;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Enumerates the available test and production environments containing their standard
 * domain URL.
 *
 * @author ivan.solomonoff
 */
public enum Environment {
  WC, DEVELOPMENT, INTEGRATION, STAGING, PRODUCTION;

  public static final String DEFAULT_ENVIRONMENT = "production";

  private Environment() {
  }

  public String getDomain() {
    String baseUrl;

    switch (this) {
    case DEVELOPMENT:
      return "sft-dev.com";
    case INTEGRATION:
      return "noodle.softonic.one";
    case STAGING:
      return "sft-staging.com";
    case WC:
      baseUrl = SuiteEnvironment.getInstance().getSuite().getInstances().get(0).getBaseUrl();
      Pattern p = Pattern.compile("(https?://.+\\.(\\w+\\.coredev|\\w+\\.coretonic))/(.+)?");
      Matcher m = p.matcher(baseUrl);
      m.find();
      return m.group(2);
    default:
      return "";
    }
  }
}
