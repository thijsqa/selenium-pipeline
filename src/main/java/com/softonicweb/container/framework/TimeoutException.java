package com.softonicweb.container.framework;

/**
 * Replaces java.util.concurrent.TimeoutException so it's not mandatory to handle it.
 *
 * @author ivan.solomonoff
 */
public class TimeoutException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  public TimeoutException(String message) {
    super(message);
  }

  public TimeoutException(String message, Throwable cause) {
    super(message, cause);
  }

  public TimeoutException(Throwable cause) {
    super(cause);
  }
}
