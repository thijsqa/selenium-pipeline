package com.softonicweb.container.framework;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.testng.ISuite;
import org.testng.ITestClass;
import org.testng.ITestContext;
import org.testng.ITestNGMethod;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;
import org.testng.xml.XmlTest;

import com.softonicweb.container.framework.TestNgUtil;

/**
 * Implementation of EmailableReporter that distinguish flaky tests from actual failures.
 *
 * @author ivan.solomonoff
 *
 * @author Paul Mendelson
 * @since 5.2
 * @version $Revision: 961 $
 */
public class ExtendedEmailableReporter extends TestListenerAdapter {

  public static final String REPORT_FILENAME = "emailable-report.html";
  private static final Logger L = Logger.getLogger(ExtendedEmailableReporter.class);
  private static final int REPORT_COLUMNS = 4;
  /**
   * Enums styles.
   */
  private static enum Style {
    PASSED, CONF_FAILED, FAILED, SKIPPED, CONF_SKIPPED, RETRIED, PARAM;

    @Override
    public String toString() {
      switch (this) {
        case CONF_FAILED:
          return "configuration failed";
        case CONF_SKIPPED:
          return "configuration skipped";
        default:
          return super.toString().toLowerCase();
      }
    }
  }

  // ~ Instance fields ------------------------------------------------------

  private final ISuiteEnvironment suiteEnvironment = SuiteEnvironment.getInstance();
  private PrintWriter mOut;
  private int mRow;

  // ~ Methods --------------------------------------------------------------

  @Override
  public void onFinish(ITestContext context) {
    // This is called for every XmlTest that finishes.
    List<XmlTest> xmlTests = context.getSuite().getXmlSuite().getTests();
    XmlTest lastXmlTest = xmlTests.get(xmlTests.size() - 1);
    if (context.getCurrentXmlTest() == lastXmlTest) {
      // Generate only one report containing all test methods
      // coming from the different XmlTests.
      List<ISuite> suiteArray = new ArrayList<ISuite>();
      ISuite suite = context.getSuite();
      suiteArray.add(suite);

      generateReport(suiteArray, suiteEnvironment.getReportsDir());
    }
  }

  /** Creates summary of the run. */
  public void generateReport(List<ISuite> suites, String outdir) {
    try {
      mOut = createWriter(outdir);
    } catch (IOException e) {
      L.error("output file", e);
      return;
    }
    startHtml(mOut);
    generateSuiteSummaryReport(suites);
    generateMethodSummaryReport(suites);
    // We use HtmlStackTraceReporter instead which provides more accurate information.
    // generateMethodDetailReport(suites);
    endHtml(mOut);
    mOut.flush();
    mOut.close();
    L.info("Report stored in:\n" + "<a style=\"color: #3366BB\" href=\"file:///" + outdir + "\">"
        + outdir + "</a>");
  }

  private PrintWriter createWriter(String outdir) throws IOException {
    new File(outdir).mkdirs();
    return new PrintWriter(new BufferedWriter(new FileWriter(new File(outdir, REPORT_FILENAME))));
  }

  /**
   * Creates a table showing the highlights of each test method with links to
   * the method details.
   */
  private void generateMethodSummaryReport(List<ISuite> suites) {
    mOut.println("<a id=\"summary\"></a>");
    startResultSummaryTable(Style.PASSED);
    for (ISuite suite : suites) {
      if (suites.size() > 1) {
        titleRow(suite.getName(), REPORT_COLUMNS);
      }

      String suiteName = getTestContexts().get(0).getName();
      resultSummary(getConfigurationFailures(), suiteName, Style.CONF_FAILED);
      resultSummary(getFailedTests(), suiteName, Style.FAILED);
      resultSummary(getFailedButWithinSuccessPercentageTests(), suiteName, Style.RETRIED);
      resultSummary(getConfigurationSkips(), suiteName, Style.SKIPPED);
      resultSummary(getSkippedTests(), suiteName, Style.SKIPPED);
      resultSummary(getPassedTests(), suiteName, Style.PASSED);
    }
    mOut.println("</table>");
  }

  private void resultSummary(List<ITestResult> results, String suiteName, Style style) {
    if (results.size() > 0) {
      // buffer for method data
      StringBuffer buff = new StringBuffer();
      String lastClassName = "";
      int mq = 0;
      int cq = 0;

      for (ITestNGMethod method : getMethodSet(results)) {
        mRow += 1;
        ITestClass testClass = method.getTestClass();
        String className = testClass.getName();
        if (mq == 0) {
          titleRow(suiteName + " &#8212; " + style.toString(), REPORT_COLUMNS);
        }
        if (!className.equalsIgnoreCase(lastClassName)) {
          if (mq > 0) {
            cq += 1;
            mOut.println("<tr class=\"" + style.toString()
                + (cq % 2 == 0 ? "even" : "odd") + "\">" + "<td");
            if (mq > 1) {
              mOut.print(" rowspan=\"" + mq + "\"");
            }
            mOut.println(">" + lastClassName + "</td>" + buff);
          }
          mq = 0;
          buff.setLength(0);
          lastClassName = className;
        }

        long end = Long.MIN_VALUE;
        long start = Long.MAX_VALUE;
        int resultCount = 0;

        String description = method.getDescription();
        String methodLogUrl = null;
        List<String> instances = new ArrayList<String>();
        List<String> instanceCountList = new ArrayList<String>();

        for (ITestResult currentResult : results) {
          // if it's the same class and same method name
          if (currentResult.getMethod().getMethodName().equals(method.getMethodName())
              && currentResult.getMethod().getClass().getName()
                  .equals(method.getClass().getName())) {
            resultCount++;
            String instanceName = null;
            if (currentResult.getAttributeNames().contains("Instance")) {
              instanceName = currentResult.getAttribute("Instance").toString();
            }
            if (currentResult.getAttributeNames().contains("Browser")) {
              String browser = currentResult.getAttribute("Browser").toString();
              instanceName = (instanceName != null) ? instanceName + "_" + browser : browser;
            }

            if (instanceName != null) {
              instances.add(instanceName);
            }
            boolean hasMethodLevelLogging =
                currentResult.getAttributeNames().contains("hasMethodLevelLogging")
                    && currentResult.getAttribute("hasMethodLevelLogging").equals("true");
            if (hasMethodLevelLogging) {
              methodLogUrl =
                  TestNgUtil.getMethodLogUrl(currentResult, suiteEnvironment.getReportsBaseUrl());
            } else {
              methodLogUrl = HtmlStackTraceReporter.DETAILED_REPORT_FILENAME + "#"
                  + currentResult.getMethod().getMethodName();
            }
            if (currentResult.getEndMillis() > end) {
              end = currentResult.getEndMillis();
            }
            if (currentResult.getStartMillis() < start) {
              start = currentResult.getStartMillis();
            }
          }
        }
        mq += 1;
        if (mq > 1) {
          buff.append("<tr class=\"" + style.toString() + (cq % 2 == 0 ? "even" : "odd")
              + "\">");
        }

        Set<String> instanceSet = new HashSet<String>(instances);
        for (String instance : instanceSet) {
          int frequency = Collections.frequency(instances, instance);
          instanceCountList.add(instance
              + ((frequency == 1) ? "" : " x" + Collections.frequency(instances, instance)));
        }

        buff.append("<td><a href=\"" + methodLogUrl + "\">"
            + "<b>" + method.getMethodName() + "</b></a>"
            + (description != null && description.length() > 0  // if test has description...
                ? " (<a" + (description.startsWith("^https?://")  // and it's a URL...
                    ? " href=" + description  // link to that URL
                    : "")  // else show the description without link
                    + ">" + description + "</a>)"
                : "")
            + "</td>" + "<td class=\"numi\">"
            + resultCount + " "
            + ((instanceCountList.size() > 0) ? instanceCountList.toString() : "")
            + "</td><td class=\"numi\">" + formatTime(end - start)
            + "</td></tr>");
      }

      if (mq > 0) {
        cq += 1;
        mOut.println("<tr class=\"" + style.toString() + (cq % 2 == 0 ? "even" : "odd")
            + "\">" + "<td rowspan=\"" + mq + "\">" + lastClassName + buff);
      }
    }
  }

  private String formatTime(long milliseconds) {
    return String.format("%d min, %d sec",
        TimeUnit.MILLISECONDS.toMinutes(milliseconds),
        TimeUnit.MILLISECONDS.toSeconds(milliseconds)
            - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(milliseconds)));
  }

  /** Starts and defines columns result summary table. */
  private void startResultSummaryTable(Style style) {
    tableStart(style);
    mOut.println("<tr><th>Class</th>"
        + "<th>Method</th><th># of<br/>Scenarios</th><th>Time<br/>(Secs)</th></tr>");
    mRow = 0;
  }

  private Collection<ITestNGMethod> getMethodSet(List<ITestResult> results) {
    TreeSet<ITestNGMethod> r = new TreeSet<ITestNGMethod>(new TestSorter<ITestNGMethod>());

    for (int i = 0; i < results.size(); i++) {
      r.add(results.get(i).getMethod());
    }
    return r;
  }

  private void generateSuiteSummaryReport(List<ISuite> suites) {
    tableStart(Style.PARAM);
    mOut.print("<tr><th>Suite</th>");
    tableColumnStart("Methods<br/>Passed");
    tableColumnStart("Scenarios<br/>Passed");
    tableColumnStart("# Skipped");
    tableColumnStart("# Retried");
    tableColumnStart("Methods<br/>Failed");
    tableColumnStart("Scenarios<br/>Failed");
    tableColumnStart("Total Time");
    tableColumnStart("# Threads");
    tableColumnStart("# Retries");
    tableColumnStart("Execution");
    mOut.println("</tr>");
    new DecimalFormat("#,##0.0");
    int qtyPassM = getMethodSet(getPassedTests()).size();
    int qtyPassS = getPassedTests().size();
    int qtySkip = getMethodSet(getSkippedTests()).size();
    int qtyRetried = getFailedButWithinSuccessPercentageTests().size();
    int qtyFailM = getMethodSet(getFailedTests()).size();
    int qtyFailS = getFailedTests().size();
    // TODO(ivan.solomonoff): Maybe not the best solution to pass the value via
    // property set, but other solutions seem to be worse in terms of adding
    // bidirectional dependencies. This does the job for now.
    System.setProperty("failed.tests", String.valueOf(qtyFailS));
    System.setProperty("passed.tests", String.valueOf(qtyPassS));
    long timeStart = Long.MAX_VALUE;
    long timeEnd = Long.MIN_VALUE;

    if (suites.isEmpty() || getTestContexts().isEmpty()) {
      mOut.println("</tr>");
      mOut.println("</table>");
      return;
    }

    ITestContext firstTestContext = getTestContexts().get(0);
    ITestContext lastTestContext = getTestContexts().get(getTestContexts().size() - 1);

    // XmlTest with Suite name is added the last in the TestRunner's XmLTest list.
    startSummaryRow(lastTestContext.getCurrentXmlTest().getName());

    summaryCell(qtyPassM, Integer.MAX_VALUE, Style.PASSED);
    summaryCell(qtyPassS, Integer.MAX_VALUE, Style.PASSED);
    summaryCell(qtySkip, 0, Style.SKIPPED);
    summaryCell(qtyRetried, 0, Style.RETRIED);
    summaryCell(qtyFailM, 0, Style.FAILED);
    summaryCell(qtyFailS, 0, Style.FAILED);
    timeStart = Math.min(firstTestContext.getStartDate().getTime(), timeStart);
    timeEnd = Math.max(lastTestContext.getEndDate().getTime(), timeEnd);
    summaryCell(formatTime(timeEnd - timeStart), true);
    summaryCell(String.valueOf(suiteEnvironment.getSuite().getNumThreads()), true);
    summaryCell(String.valueOf(suiteEnvironment.getSuite().getNumRetries()), true);
    summaryCell(lastTestContext.getIncludedGroups());
    mOut.println("</tr>");
    mOut.println("</table>");
  }

  private void summaryCell(String[] val) {
    StringBuffer b = new StringBuffer();
    for (String v : val) {
      b.append(v + " ");
    }
    summaryCell(b.toString(), true);
  }

  private void summaryCell(String v, boolean isgood) {
    mOut.print("<td class=\"numi" + (isgood ? "" : "_attn") + "\">" + v + "</td>");
  }

  private void summaryCell(String v, boolean isgood, Style style) {
    String css = "";
    if (style.equals(Style.FAILED)) {
      css = "_attn";
    } else if (style.equals(Style.RETRIED)) {
      css = "_warn";
    } else if (style.equals(Style.SKIPPED)) {
      css = "_skip";
    } else if (style.equals(Style.PASSED)) {
      css = "_passed";
    }

    mOut.print("<td class=\"numi" + css + "\">" + v + "</td>");
  }

  private void startSummaryRow(String label) {
    mRow += 1;
    mOut.print("<tr" + (mRow % 2 == 0 ? " class=\"stripe\"" : "")
            + "><td style=\"text-align:left;padding-right:2em\">" + label
            + "</td>");
  }

  private void summaryCell(int v, int maxexpected, Style style) {
    summaryCell(String.valueOf(v), v <= maxexpected, style);
  }

  private void tableStart(Style cssclass) {
    mOut.println("<table cellspacing=0 cellpadding=0"
        + (cssclass != null ? " class=\"" + cssclass.toString() + "\""
            : " style=\"padding-bottom:2em\"") + ">");
    mRow = 0;
  }

  private void tableColumnStart(String label) {
    mOut.print("<th class=\"numi\">" + label + "</th>");
  }

  private void titleRow(String label, int cq) {
    mOut.println("<tr><th colspan=\"" + cq + "\">" + label + "</th></tr>");
    mRow = 0;
  }

  /** Starts HTML stream. */
  private void startHtml(PrintWriter out) {
    HtmlReporterUtil.startHtml(out, suiteEnvironment.getSuite().getName(), REPORT_FILENAME);
    out.println("<h4>Stack-trace report available <a href="
        + suiteEnvironment.getDetailedReportUrl() + ">here</a></h4>");
  }

  /** Finishes HTML stream. */
  private void endHtml(PrintWriter out) {
    out.println("</body></html>");
  }

  // ~ Inner Classes --------------------------------------------------------
  /** Arranges methods by classname and method name. */
  private class TestSorter<T extends ITestNGMethod> implements Comparator<Object> {
    // ~ Methods -------------------------------------------------------------

    /** Arranges methods by classname and method name. */
    @Override
    @SuppressWarnings("unchecked")
    public int compare(Object o1, Object o2) {
      int r = ((T) o1).getTestClass().getName().compareTo(((T) o2).getTestClass().getName());
      if (r == 0) {
        r = ((T) o1).getMethodName().compareTo(((T) o2).getMethodName());
      }
      return r;
    }
  }
}
