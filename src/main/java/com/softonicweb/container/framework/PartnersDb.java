package com.softonicweb.container.framework;

import java.util.Arrays;
import java.util.List;

/**
 * Partners database.
 *
 * @author alfredo.lopez
 */
public class PartnersDb extends Database {

  private static final Environment[] RESTRICTED_ENVIRONMENTS = { Environment.PRODUCTION };

  private static final Profile PRODUCTION_PROPERTIES = new Profile(
      "partners", "globalrepdb0.ext.bcn.softonic.lan", "partneruser01", "ArEdForVerAyu03");

  private static final Profile QA_PROPERTIES = new Profile(
      "partners", "globaldbw01st.ofi.softonic.lan", "partneruser01", "ArEdForVerAyu03");

  private Profile profile;

  public PartnersDb(Environment environment) {
    super(environment);
  }

  @Override
  public Profile getProfile() {
    if (profile == null) {
      switch (environment) {
        case PRODUCTION:
          profile = PRODUCTION_PROPERTIES;
          break;
        case STAGING:
          profile = QA_PROPERTIES;
          break;
      case INTEGRATION:
      case DEVELOPMENT:
        default:
          throw new UnsupportedOperationException();
      }
    }

    return profile;
  }

  @Override
  public List<Environment> restrictedEnvironments() {
    return Arrays.asList(RESTRICTED_ENVIRONMENTS);
  }
}
