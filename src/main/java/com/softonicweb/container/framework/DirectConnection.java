package com.softonicweb.container.framework;

import java.sql.DriverManager;
import java.sql.SQLException;

import com.softonicweb.container.framework.Database.Profile;

/**
 * Connection to a database server over a direct connection.
 *
 * @author alfredo.lopez
 */
public class DirectConnection extends AbstractConnection {

  public DirectConnection(Profile props) throws SQLException {
    String jdbcUrl = "jdbc:mysql://" + props.getHost() + "/" + props.getName();
    try {
      setConnection(DriverManager.getConnection(jdbcUrl, props.getUser(), props.getPassword()));
    } catch (SQLException e) {
      throw new SQLException("Connection to " + jdbcUrl + " failed.", e);
    }
  }
}
