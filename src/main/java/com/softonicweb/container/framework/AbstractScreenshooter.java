package com.softonicweb.container.framework;

import java.io.File;

import com.softonicweb.container.framework.DataGenerator;

/**
 * Base class for providing screenshooting functionality.
 *
 * @author alfredo.lopez
 */
public abstract class AbstractScreenshooter implements IScreenshooter {

  private final String label;
  private final String screenshotsDir;

  public AbstractScreenshooter(String label, String screenshotsDir) {
    this.label = label;
    this.screenshotsDir = screenshotsDir;
    new File(screenshotsDir).mkdirs();
  }

  @Override
  public abstract String getScreenshot();

  @Override
  public String getAutomaticScreenshotPath() {
    String fileName = label + "_" +  DataGenerator.getTimestamp() + ".png";
    return screenshotsDir + File.separator + fileName;
  }
}
