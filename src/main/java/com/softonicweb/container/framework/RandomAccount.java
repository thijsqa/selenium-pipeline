package com.softonicweb.container.framework;

import com.softonicweb.container.framework.IAccount;
import com.softonicweb.container.framework.DataGenerator;

/**
 * Generates a random user account.
 *
 * @author ivan.solomonoff
 */
public class RandomAccount implements IAccount {

  private String username;
  private String email;
  private String password;
  private String firstname;
  private String lastname;

  public RandomAccount(Accounts seed) {
    String timestamp = DataGenerator.getTimestamp();
    String[] emailSeed = seed.getEmail().split("@");
    String uniqueUsername = DataGenerator.getUniqueId();
    String uniqueFirstname = DataGenerator.getUniqueId();
    String uniqueLastname = DataGenerator.getUniqueId();
    String uniqueEmail =
        emailSeed[0] + "+" + timestamp + "_" + DataGenerator.getRandomNumber() + "@" + emailSeed[1];

    this.username = uniqueUsername;
    this.firstname = uniqueFirstname;
    this.lastname = uniqueLastname;
    this.email = uniqueEmail;
    this.password = seed.getPassword();
  }

  public RandomAccount() {
    this(Accounts.SOFTONIC);
  }

  @Override
  public String getUsername() {
    return username;
  }

  public void setUsername(String user) {
    this.username = user;
  }

  @Override
  public String getFirstname() {
    return firstname;
  }

  public void setFirstname(String user) {
    this.firstname = user;
  }

  @Override
  public String getLastname() {
    return lastname;
  }

  public void setLastname(String user) {
    this.lastname = user;
  }

  @Override
  public String getEmail() {
    return email;
  }

  public void setEmail(String mail) {
    this.email = mail;
  }

  @Override
  public String getPassword() {
    return password;
  }

  public void setPassword(String pw) {
    this.password = pw;
  }
}
