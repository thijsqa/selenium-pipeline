package com.softonicweb.container.framework;

import org.testng.remote.strprotocol.GenericMessage;
import org.testng.remote.strprotocol.IRemoteSuiteListener;
import org.testng.remote.strprotocol.SuiteMessage;

/**
 * Adapter class for IRemoteSuiteListener.
 *
 * @author alfredo.lopez
 */
public class RemoteSuiteAdapter implements IRemoteSuiteListener {

  @Override
  public void onInitialization(GenericMessage gm) {
  }

  @Override
  public void onStart(SuiteMessage sm) {
  }

  @Override
  public void onFinish(SuiteMessage sm) {
  }
}
