package com.softonicweb.container.framework;

import java.io.IOException;
import java.io.Reader;
import java.lang.reflect.Method;
import java.util.Iterator;

import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.Converter;
import org.apache.commons.lang.ArrayUtils;
import org.testng.SkipException;

import com.csvreader.CsvReader;

/**
 * Provides data fom a given .csv for the given method.
 * <p>Fist column will be always the line index.
 *
 * @author ivan.solomonoff
 */
public class CsvDataProviderIterator implements Iterator<Object> {

  private final CsvReader csvReader;
  private final int columnCount;
  private String[] last = null;
  private final Class<?>[] parameterTypes;
  private final Converter[] parameterConverters;

  /*
   * @param commentChar If set, skip lines starting with this char. Otherwise
   * set to null.
   */
  public CsvDataProviderIterator(Method method, Reader csvDataReader, char delimiter,
      String commentChar) {
    csvReader = new CsvReader(csvDataReader);
    if (commentChar != null) {
      csvReader.setUseComments(true);
      csvReader.setComment(commentChar.toCharArray()[0]);
    }
    csvReader.setDelimiter(delimiter);

    try {
      csvReader.readHeaders();
      columnCount = csvReader.getHeaderCount() + 1; // extra column for line index
    } catch (IOException ioe) {
      throw new RuntimeException(ioe);
    }
    parameterTypes = method.getParameterTypes();
    int len = parameterTypes.length;
    parameterConverters = new Converter[len];
    for (int i = 0; i < len; i++) {
      parameterConverters[i] = ConvertUtils.lookup(parameterTypes[i]);
    }
  }

  @Override
  public boolean hasNext() {
    boolean hasNext;
    try {
      hasNext = csvReader.readRecord();
      if (!hasNext) {
        last = null;
      }
    } catch (IOException ioe) {
      throw new RuntimeException(ioe);
    }

    return hasNext;
  }

  @Override
  public Object[] next() {
    String[] next;
    if (last != null) {
      next = last;
    } else {
      next = getNextLine();
    }
    last = null;
    Object[] args = parseLine(next);

    return args;
  }

  private String[] getNextLine() {
    if (last == null) {
      try {
        last = new String[columnCount + 1];
        last =
            (String[]) ArrayUtils.addAll(new String[] {
                String.valueOf(csvReader.getCurrentRecord() + 2) }, csvReader.getValues());
        // last = csvReader.getValues();
      } catch (IOException ioe) {
        throw new RuntimeException(ioe);
      }
    }

    return last;
  }

  private Object[] parseLine(String[] svals) {
    int len = svals.length;
    Object[] result = new Object[len];
    // avoid parsing CSS styles from Google sheets
    if (ArrayUtils.toString(svals).contains("style>")) {
      throw new SkipException("Skipped because CSS styling code was found in data input");
    }
    try {
      for (int i = 0; i < len; i++) {
        result[i] = parameterConverters[i].convert(parameterTypes[i], svals[i]);
      }
    } catch (RuntimeException re) {
      throw new RuntimeException("Error parsing line values: " + ArrayUtils.toString(svals), re);
      }
    return result;
  }

  @Override
  public void remove() {
    throw new UnsupportedOperationException();
  }
}
