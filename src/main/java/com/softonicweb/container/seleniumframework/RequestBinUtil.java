package com.softonicweb.container.seleniumframework;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;

/**
 * Contains helper methods to interact with http://requestb.in/.
 *
 * @author ivan.solomonoff
 */
public class RequestBinUtil {

  private static final String REQUEST_BIN_WEBSITE = "http://requestb.in/";
  private static final String INSPECT_ARGUMENT = "?inspect";

  private static final By CREATE_BIN_BUTTON_CSS_LOCATOR = By.cssSelector("a.btn-success");
  private static final By BIN_URL_CSS_LOCATOR = By.cssSelector("#content input.xxlarge");
  private static final String REQUEST_BY_INDEX_LOCATOR =
      "//div[contains(@id, 'message-wrapper')][%s]";
  private static final String REQUEST_METHOD_LOCATOR_BY_INDEX = REQUEST_BY_INDEX_LOCATOR
      + "//span[@class='method']";
  private static final String ABSOLUTE_PATH_LOCATOR_BY_INDEX = REQUEST_BY_INDEX_LOCATOR
      + "//span[@class='absolute-path']";
  private static final String QUERY_STRING_LOCATOR_BY_INDEX = REQUEST_BY_INDEX_LOCATOR
      + "//span[@class='querystring']";
  private static final String REQUEST_CONTENT_BY_INDEX_LOCATOR =
      "//div[contains(@id, 'message-wrapper-')][%s]";
  private static final String QUERY_STRING_LOCATOR_BY_INDEX_AND_FIELD =
      REQUEST_CONTENT_BY_INDEX_LOCATOR + "//div[contains(@id, 'detail-')]"
      + "//div[contains(@id, 'request-detail-')]"
      + "/div[1]//p[@class='keypair']/strong[text()='%s']/..";

  private final WebDriver driver;
  private final String requestBinUrl;

  public RequestBinUtil(WebDriver driver) {
    this.driver = driver;
    requestBinUrl = createRequestBin();
  }

  public String getRequestBinUrl() {
    return requestBinUrl;
  }

  private String createRequestBin() {
    navigateToRequestBin();
    clickCreateRequestBin();
    return fetchRequestBinUrl();
  }

  public String getRequestMethod(int callIndex) {
    navigateToInspectPage();
    return driver.findElement(
        By.xpath(String.format(REQUEST_METHOD_LOCATOR_BY_INDEX, callIndex + 1))).getText();
  }

  public String getAbsolutePath(int callIndex) {
    navigateToInspectPage();
    return driver.findElement(
        By.xpath(String.format(ABSOLUTE_PATH_LOCATOR_BY_INDEX, callIndex + 1))).getText();
  }

  public String getQueryString(int callIndex) {
    navigateToInspectPage();
    return driver.findElement(
        By.xpath(String.format(QUERY_STRING_LOCATOR_BY_INDEX, callIndex + 1))).getText();
  }

  public String getQueryStringField(int callIndex, String field) {
    // eg. FieldName: xxxx
    field += ":";
    navigateToInspectPage();
    String text = null;
    try {
      text = driver.findElement(
          By.xpath(String.format(QUERY_STRING_LOCATOR_BY_INDEX_AND_FIELD, callIndex + 1, field)))
          .getText();
    } catch (NoSuchElementException e) {
      throw new RuntimeException("Expected callback not found.");
    }
    // from "FieldName: xxxx" we extract "xxxx" only.
    text = text.replaceFirst(field + " ", "");
    return text;
  }

  private void navigateToRequestBin() {
    driver.get(REQUEST_BIN_WEBSITE);
  }

  private void navigateToInspectPage() {
    driver.get(requestBinUrl + INSPECT_ARGUMENT);
  }

  private void clickCreateRequestBin() {
    driver.findElement(CREATE_BIN_BUTTON_CSS_LOCATOR).click();
  }

  private String fetchRequestBinUrl() {
    return driver.findElement(BIN_URL_CSS_LOCATOR).getAttribute("value");
  }
}
