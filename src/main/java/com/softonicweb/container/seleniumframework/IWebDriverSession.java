package com.softonicweb.container.seleniumframework;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

/**
 * Common WebDriver session interface for both local and multi-threaded contexts
 *
 * @author ivan.solomonoff
 */
public interface IWebDriverSession {

  WebDriver driver();

  WebDriver startSession(Browser browser, DesiredCapabilities capabilities);

  void endSession();
}
