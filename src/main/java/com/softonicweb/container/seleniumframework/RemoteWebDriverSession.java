package com.softonicweb.container.seleniumframework;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.softonicweb.container.framework.ProxySession;
import com.softonicweb.container.framework.Accounts;
import com.softonicweb.container.framework.ProcessUtil;
import com.softonicweb.container.framework.RunTarget;
import com.softonicweb.container.framework.SuiteEnvironment;

/**
 * Holds the WebDriver session in a thread safe way.
 *
 * @author ivan.solomonoff
 */
public class RemoteWebDriverSession extends AbstractWebDriverSession {

  private static final String BROWSERSTACK = "/qa-builds/vm-farm/jenkins/browserstack.sh";
  public static final Logger LOGGER = Logger.getLogger(RemoteWebDriverSession.class);

  private static HashMap<String, WebDriver> sessions = new HashMap<String, WebDriver>() {
    private static final long serialVersionUID = 1L;
  };

  private final InheritableThreadLocal<String> sessionId = new InheritableThreadLocal<String>();

  public RemoteWebDriverSession(String sessionId, boolean useProxy) {
    this.sessionId.set(sessionId);
    setUseProxy(useProxy);
  }

  protected String sessionId() {
    return sessionId.get();
  }

  /** Get the intended WebDriver session. */
  @Override
  public WebDriver driver() {
    return getDriverObject(sessionId());
  }

  private WebDriver getDriverObject(String key) {
    if (sessions.containsKey(key)) {
      return sessions.get(key);
    }
    return null;
  }

  /** Method to end the session, should use after the test. */
  @Override
  public void endSession() {
    WebDriver driver = getDriverObject(sessionId());
    if (driver != null) {
      try {
        driver.quit();
        sessions.remove(sessionId());
        LOGGER.info("Closed WebDriver session: " + sessionId());
      } catch (Exception e) {
        LOGGER.error("Failed to close webdriver: " + e.getMessage());
      }
      if (useProxy()) {
        ProxySession.endSession(sessionId());
      }
    } else {
      LOGGER.warn("WebDriver session: " + sessionId() + " does not exist.");
    }
  }

  @Override
  public WebDriver startSession(Browser browser, DesiredCapabilities capabilities) {
    if (useProxy()) {
      setupProxy(capabilities, sessionId(), browser);
    }
    WebDriver remoteDriver = getRemoteWebDriver(browser, capabilities);
    WebDriver augmentedDriver = getEventFiringDriver(remoteDriver);
    String browserName = capabilities.getBrowserName();
    sessions.put(sessionId(), augmentedDriver);

    logger().logComment("Started browser: " + browserName);
    LOGGER.info("Started WebDriver: " + augmentedDriver.toString() + " browser: " + browserName
        + " session: " + sessionId());

    setImplicitWait(augmentedDriver);

    return augmentedDriver;
  }

  private WebDriver getRemoteWebDriver(Browser browser, DesiredCapabilities capability) {
    String serverHost = SuiteEnvironment.getInstance().getParameters().getSeleniumHost();
    String serverPort = SuiteEnvironment.getInstance().getParameters().getSeleniumPort();

    if (serverHost.equals("saucelabs")) {
      serverHost =  Accounts.SAUCE_LABS.getUsername() + ":" + Accounts.SAUCE_LABS.getPassword()
          + "@ondemand.saucelabs.com";
      serverPort = "80";
    } else if (serverHost.equals("browserstack")) {
      serverHost =
          Accounts.BROWSERSTACK.getUsername() + ":" + Accounts.BROWSERSTACK.getPassword()
              + "@hub.browserstack.com";
      capability.setCapability("browserstack.local", "true");
      serverPort = "80";
      bsScript();
    } else if (serverHost.equals("EU_Frankfurt")) {
      serverHost = RunTarget.EU_FRANKFURT.getHost();
      serverPort = RunTarget.EU_FRANKFURT.getPort();
    } else if (serverHost.equals("US_West_Oregon")) {
      serverHost = RunTarget.US_WEST_OREGON.getHost();
      serverPort = RunTarget.US_WEST_OREGON.getPort();
    } else if (serverHost.equals("Asia_Southeast_Singapore")) {
      serverHost = RunTarget.ASIA_SOUTHEAST_SINGAPORE.getHost();
      serverPort = RunTarget.ASIA_SOUTHEAST_SINGAPORE.getPort();
    }

    setExtraCapabilities(browser, capability);

    try {
      String hubUrl = "http://" + serverHost + ":" + serverPort + "/wd/hub";
      LOGGER.info("Requesting " + capability.getBrowserName() + " session to: " + hubUrl
          + ". Session ID: " + sessionId());
      return new RemoteWebDriver(new URL(hubUrl), capability);
    } catch (MalformedURLException e) {
      throw new RuntimeException("Malformed HUB URL passed to SoftonicRemoteWebDriver.", e);
    }
  }

  private static void bsScript() {
    LOGGER.info("Starting Browserstack local testing script..");
    ProcessBuilder pb = new ProcessBuilder(BROWSERSTACK);
    ProcessUtil.runProcess(pb, false);
  }
}
