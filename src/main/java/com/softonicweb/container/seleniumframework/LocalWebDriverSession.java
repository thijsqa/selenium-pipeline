package com.softonicweb.container.seleniumframework;

import org.apache.log4j.Logger;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;

import com.softonicweb.container.framework.ProxySession;

/**
 * Holds a single local WebDriver session.
 *
 * @author alfredo.lopez
 * @author ivan.solomonoff
 */
public class LocalWebDriverSession extends AbstractWebDriverSession {

  private static final Logger LOGGER = Logger.getLogger(LocalWebDriverSession.class);

  private static WebDriver driverSession = null;

  public LocalWebDriverSession(boolean useProxy) {
    setUseProxy(useProxy);
  }

  /** Gets the current local WebDriver session. */
  @Override
  public WebDriver driver() {
    return driverSession;
  }

  @Override
  public void endSession() {
    if (driverSession != null) {
      try {
        driverSession.quit();
        driverSession = null;
        LOGGER.info("Local WebDriver session closed.");
      } catch (Exception e) {
        LOGGER.error("Failed to close local webdriver: " + e.getMessage());
      }
    } else {
      LOGGER.error("Local WebDriver session does not exist.");
    }
    if (useProxy()) {
      ProxySession.endSession();
    }
  }

  @Override
  public WebDriver startSession(Browser supportedBrowser, DesiredCapabilities capabilities) {
    if (useProxy()) {
      setupProxy(capabilities, ProxySession.SINGLE_SESSION_ID, supportedBrowser);
    }
    String browser = capabilities.getBrowserName();
    WebDriver driver = getLocalWebDriver(supportedBrowser, capabilities);
    driverSession = getEventFiringDriver(driver);

    logger().logComment("Started browser: " + browser);
    LOGGER.info("Started local WebDriver: " + driverSession.toString() + " browser: " + browser);

    setImplicitWait(driverSession);

    return driverSession;
  }

  private WebDriver getLocalWebDriver(Browser browser, DesiredCapabilities capability) {
    setExtraCapabilities(browser, capability);
    LOGGER.info("Starting local WebDriver on browser '" + capability.getBrowserName() + "'");
    capability.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR,
        UnexpectedAlertBehaviour.DISMISS);
    switch (browser) {
      case FIREFOX:
        FirefoxOptions firefoxOptions = new FirefoxOptions();
        return new FirefoxDriver(firefoxOptions);
      case CHROME:
        ChromeOptions chromeOptions = new ChromeOptions();
        return new ChromeDriver(chromeOptions);
      case PHANTOMJS:
        capability.setCapability(CapabilityType.TAKES_SCREENSHOT, true);
        return new PhantomJSDriver(capability);
      case IE8:
      case IE9:
        return new InternetExplorerDriver();
      case SAFARI:
        return new SafariDriver();
      case HTMLUNIT:
        return new HtmlUnitDriver(capability);
      default:
        throw new UnsupportedOperationException("Browser used not supported.");
    }
  }
}
