package com.softonicweb.container.seleniumframework;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.events.WebDriverEventListener;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.softonicweb.container.framework.ProxySession;
import com.softonicweb.container.framework.ProxyWrapper;
import com.softonicweb.container.seleniumframework.SoftonicWebDriverEventListener;
import com.softonicweb.container.framework.ISuiteEnvironment;
import com.softonicweb.container.framework.SuiteEnvironment;
import com.softonicweb.container.framework.TestLogger;

/**
 * Base WebDriver session class that provides common functionalities.
 *
 * @author alfredo.lopez
 * @author ivan.solomonoff
 * @author thijs.vandewynckel
 */
public abstract class AbstractWebDriverSession implements IWebDriverSession {

  public static final Logger LOGGER = Logger.getLogger(AbstractWebDriverSession.class);

  private static final ISuiteEnvironment SUITE_ENVIRONMENT = SuiteEnvironment.getInstance();

  private static final Map<String, String[]> SUITE_MAPPING;
  static {
    SUITE_MAPPING = new HashMap<String, String[]>();
    SUITE_MAPPING.put("SADS_TRACKER_SERVICE", new String[] { "172.31.12.81" });
    SUITE_MAPPING.put("WEB_TRACKING", new String[] { "172.31.12.81" });
    SUITE_MAPPING.put("SOFTONICMOBILE_TRACKING", new String[] { "172.31.9.93" });
    SUITE_MAPPING.put("WEB", new String[] { "172.31.12.81" });
    SUITE_MAPPING.put("QA_GRID", new String[] { "172.31.14.223" });
    SUITE_MAPPING.put("APPVISOR_TRACKING", new String[] { "172.31.10.191" });
    SUITE_MAPPING.put("XROBOTS", new String[] { "172.31.10.191" });
  }

  private boolean useProxy;

  protected static TestLogger logger() {
    return TestLogger.getInstance();
  }

  public static void acceptAlertIfPresent(WebDriver driver) {
    try {
      WebDriverWait wait = new WebDriverWait(driver, 1);
      wait.until(ExpectedConditions.alertIsPresent());
      driver.switchTo().alert().accept();
      logger().logComment("Browser alert detected and accepted");
    } catch (Exception e) {
      logger().logComment("Exception found while waiting for browser alert: " + e.getMessage());
    }
  }

  protected static void setExtraCapabilities(Browser browser, DesiredCapabilities capability) {
    capability.setJavascriptEnabled(true);
    capability.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR,
        UnexpectedAlertBehaviour.DISMISS);
    switch (browser) {
      case FIREFOX:
        capability.setCapability(CapabilityType.TAKES_SCREENSHOT, true);
        break;
      case CHROME:
        capability.setCapability(CapabilityType.TAKES_SCREENSHOT, true);
        break;
      case PHANTOMJS:
        capability.setJavascriptEnabled(true);
        capability.setCapability(CapabilityType.TAKES_SCREENSHOT, true);
        // PhantomJS.exe must be somewhere in your PATH! Otherwise set:
        // capability.setCapability("phantomjs.binary.path", fullPathToPhanTomJsExeFile);
        break;
      case IE8:
      case IE9:
      case IE11:
        String version = browser.toString().split("ie")[1];
        capability.setVersion(version);
        // IE keeps sessions between tests, to maintain consistent behavior.
        // between all browsers we need to clear IE context and cookies.
        capability.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);
        capability.setCapability("ignoreZoomSetting", true);
        capability.setCapability(CapabilityType.TAKES_SCREENSHOT, true);
        break;
      case SAFARI:
        capability.setCapability(CapabilityType.TAKES_SCREENSHOT, true);
        break;
      case HTMLUNIT:
        capability.setJavascriptEnabled(true);
        capability.setCapability(CapabilityType.TAKES_SCREENSHOT, false);
        break;
      default:
        throw new UnsupportedOperationException("Browser used not supported.");
    }
  }

  protected static WebDriver getEventFiringDriver(WebDriver baseDriver) {
    // Wrap driver to add logging functionality through a listener
    WebDriverEventListener eventListener = new SoftonicWebDriverEventListener();
    WebDriver eventFiringDriver = new EventFiringWebDriver(baseDriver).register(eventListener);

    return eventFiringDriver;
  }

  /** Sets the implicit wait time for any wd "find" action */
  protected static void setImplicitWait(WebDriver driver) {
    final int waitAmount = 10;
    driver.manage().timeouts().implicitlyWait(waitAmount, TimeUnit.SECONDS);
  }

  protected boolean useProxy() {
    return useProxy;
  }

  protected void setUseProxy(boolean usesProxy) {
    useProxy = usesProxy;
  }

  protected DesiredCapabilities setupProxy(DesiredCapabilities capabilities, String sessionId,
      Browser browser) {
    String jenkinsPrivateIp = getJenkinsPrivateIp();
    ProxyWrapper server = ProxySession.startSession(sessionId);

    switch (browser) {
      case FIREFOX:
        Proxy mobProxy = server.seleniumProxy();

        LOGGER.info("ProxyServer started at: " + mobProxy.getHttpProxy());
        try {
          LOGGER.info("ProxyServer Canonical Host Name: "
              + InetAddress.getLocalHost().getCanonicalHostName());
          LOGGER.info("ProxyServer Host Name: " + InetAddress.getLocalHost().getHostName());
        } catch (UnknownHostException e) {
          e.printStackTrace();
        }

        // Overriding default and setting the proxy to be IP address of the machine, to avoid DNS
        // issues
        try {
        String uri = jenkinsPrivateIp + ":" + server.getPort();
          mobProxy.setSslProxy(uri);
          mobProxy.setHttpProxy(uri);
        } finally {
          // ... cleanup that will execute whether or not an error occurred ...
        }

        capabilities.setCapability(CapabilityType.PROXY, mobProxy);
        return capabilities;
      case CHROME:
      try {
      Proxy proxy = server.seleniumProxy();
        LOGGER.info("ProxyServer started at: " + proxy.getHttpProxy());
        String uri = jenkinsPrivateIp + ":" + server.getPort();
      proxy.setSslProxy("trustAllSSLCertificates");
      proxy.setSslProxy(uri);
      proxy.setHttpProxy(uri);
      capabilities.setCapability(CapabilityType.PROXY, proxy);
      capabilities.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);
        try {
          LOGGER.info("ProxyServer Canonical Host Name: "
              + InetAddress.getLocalHost().getCanonicalHostName());
          LOGGER.info("ProxyServer Host Name: " + InetAddress.getLocalHost().getHostName());
        } catch (UnknownHostException e) {
          e.printStackTrace();
        }
      } finally {
        // ... cleanup that will execute whether or not an error occurred ...
      }
        return capabilities;
      default:
        throw new UnsupportedOperationException("Setting proxy to browser: "
             + browser.name() + " is not supported.");
    }
  }

  private static String getJenkinsPrivateIp() {
    String suiteName = String.valueOf(SUITE_ENVIRONMENT.getSuite().getName());
    String[] jenkinsUrl = SUITE_MAPPING.get(suiteName);
    if (jenkinsUrl == null) {
      throw new RuntimeException("No data for instance: " + suiteName);
    }
    String privateip = jenkinsUrl[0];
    if (jenkinsUrl.length > 1) {
      privateip += jenkinsUrl[1];
    }
    if (SUITE_ENVIRONMENT.getParameters().getExecution().equals("DISPLAY")) {
      privateip = "jenkins-ads.softonic.one";
    }
    return privateip;
  }

  protected ProxyWrapper proxy() {
    if (useProxy) {
      return ProxySession.proxy();
    } else {
      throw new RuntimeException("Proxy not initalized, this test is not using a proxy.");
    }
  }

  protected ProxyWrapper proxy(String sessionId) {
    if (useProxy) {
      return ProxySession.proxy(sessionId);
    } else {
      throw new RuntimeException("Proxy not initalized, this test is not using a proxy.");
    }
  }
}
