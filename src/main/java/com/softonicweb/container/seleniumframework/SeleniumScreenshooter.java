package com.softonicweb.container.seleniumframework;

import static org.openqa.selenium.OutputType.BASE64;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.ScreenshotException;

import com.softonicweb.container.framework.AbstractScreenshooter;
import com.softonicweb.container.framework.FileUtil;
import com.softonicweb.container.framework.IScreenshooter;

/**
 * Provides screenshooting functionality to a WebDriver instance.
 *
 * @author ivan.solomonoff
 */
public class SeleniumScreenshooter extends AbstractScreenshooter implements IScreenshooter {

  private final WebDriver driver;

  public SeleniumScreenshooter(WebDriver driver, String label, String screenshotsDir) {
    super(label, screenshotsDir);
    this.driver = driver;
  }

  private <X> X getScreenshotAs(OutputType<X> target) {
    if (driver instanceof TakesScreenshot) {
      return  ((TakesScreenshot) driver).getScreenshotAs(target);
    } else {
      return ((TakesScreenshot) new Augmenter().augment(driver)).getScreenshotAs(target);
    }
  }

  /**
   * Captures a remote screenshot as Base64 string, decodes it and writes it into a local file.
   *
   * @return String The screenshot file full path or error log message
   */
  @Override
  public String getScreenshot() {
    String screenshotString;
    try {
      screenshotString = getScreenshotAs(BASE64);
    } catch (UnsupportedOperationException e) {
      return "Current driver does not support taking screenshots";
    } catch (WebDriverException wde) {
      return "Failed to take screenshot";
    } catch (ClassCastException e) {
      return "Current driver does not support taking screenshots";
    }

    return saveScreenshot(screenshotString);
  }

  /**
   * Writes screenshot into a local file.
   *
   * @return String The screenshot file full path or error log message
   */
  public String saveScreenshot(String screenshotString) {
    String screenshotPath = getAutomaticScreenshotPath();
    FileUtil.decodeAndWriteImage(screenshotString, screenshotPath);
    return screenshotPath;
  }

  public String extractScreenShot(WebDriverException e) {
    Throwable cause = e.getCause();
    if (cause instanceof ScreenshotException) {
      return ((ScreenshotException) cause).getBase64EncodedScreenshot();
    }
    return null;
  }
}
