package com.softonicweb.container.seleniumframework;

import com.softonicweb.container.framework.IPlatform;

/**
 * Enumerates the supported browsers in the Softonic Selenium farm.
 *
 * @author ivan.solomonoff
 */
public enum Browser implements IPlatform {
  FIREFOX, IE8, IE9, IE11, SAFARI, CHROME, HTMLUNIT, PHANTOMJS;

  @Override
  public String toString() {
    return super.toString().toLowerCase();
  }
}
