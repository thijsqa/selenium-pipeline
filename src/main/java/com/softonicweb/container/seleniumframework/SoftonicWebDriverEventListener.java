package com.softonicweb.container.seleniumframework;

import org.openqa.selenium.By;
import org.openqa.selenium.InvalidSelectorException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.WebDriverEventListener;

import com.softonicweb.container.framework.TestLogger;

/**
 * Implementation of WebDriver's event listener.
 *
 * @author ivan.solomonoff
 */
public class SoftonicWebDriverEventListener implements WebDriverEventListener {

  private By lastFindBy;
  private String originalValue;
  private long cmdStartMillis = 0;

  public SoftonicWebDriverEventListener() {
  }

  private TestLogger logger() {
    return TestLogger.getInstance();
  }

  @Override
  public void beforeNavigateTo(String url, WebDriver driver) {
    cmdStartMillis = System.currentTimeMillis();
  }

  @Override
  public void beforeChangeValueOf(WebElement element, WebDriver driver, CharSequence[] keysToSend) {
    originalValue = element.getAttribute("value");
    cmdStartMillis = System.currentTimeMillis();
  }

  @Override
  public void afterChangeValueOf(WebElement element, WebDriver driver, CharSequence[] keysToSend) {
    String newValue;
    // Avoid StaleElementRefereceException when the element goes away after changing its value.
    try {
      newValue = "new: '" + element.getAttribute("value") + "'";
    } catch (StaleElementReferenceException e) {
      newValue = "";
    }
    logger().log("Changed value of '" + lastFindBy, new String[] { "old: '" + originalValue + "'",
        newValue }, "OK", cmdStartMillis);
  }

  @Override
  public void beforeFindBy(By by, WebElement element, WebDriver driver) {
    lastFindBy = by;
    cmdStartMillis = System.currentTimeMillis();
  }

  @Override
  public void onException(Throwable error, WebDriver driver) {
    Throwable cause = error.getCause();
    if (error.getClass().equals(NoSuchElementException.class)) {
      logger().logException("Element not found. ",
          new String[] { error.getMessage().split("Command duration")[0],
              error.getClass().getName() }, null, System.currentTimeMillis());
    } else if (error.getClass().equals(InvalidSelectorException.class)) {
      logger().logException("Invalid Selector",
          new String[] {  error.getMessage(), error.getClass().getName() }, null,
          System.currentTimeMillis());
    } else {
      String name;
      try {
        name = error.getClass().getName();
      } catch (NullPointerException e) {
        name = "Unable to determine error class name";
      }
      logger().logException("Exception Error", new String[] { name }, cause,
          System.currentTimeMillis());
    }
  }

  @Override
  public void afterClickOn(WebElement element, WebDriver driver) {
    logger().log("Clicked at", new String[] { lastFindBy.toString() }, "OK", cmdStartMillis);
  }

  @Override
  public void afterFindBy(By by, WebElement element, WebDriver driver) {
    // Do not log findElements when waiting for page to load.
    if (!lastFindBy.toString().equals("By.xpath: //*")) {
      logger().log("findElement", new String[] { lastFindBy.toString() }, "OK", cmdStartMillis);
    }
  }

  @Override
  public void afterNavigateBack(WebDriver driver) {
    logger().log("Navigated back", new String[] { "" }, "OK", cmdStartMillis);
  }

  @Override
  public void afterNavigateForward(WebDriver driver) {
    logger().log("Navigated forward", new String[] { "" }, "OK", cmdStartMillis);
  }

  @Override
  public void afterNavigateTo(String url, WebDriver driver) {
    logger().log("Navigated to", new String[] { url }, "OK", cmdStartMillis);
  }

  @Override
  public void afterScript(String script, WebDriver driver) {
  }

  @Override
  public void beforeClickOn(WebElement element, WebDriver driver) {
    cmdStartMillis = System.currentTimeMillis();
  }

  @Override
  public void beforeNavigateBack(WebDriver driver) {
    cmdStartMillis = System.currentTimeMillis();
  }

  @Override
  public void beforeNavigateForward(WebDriver driver) {
    cmdStartMillis = System.currentTimeMillis();
  }

  @Override
  public void beforeScript(String script, WebDriver driver) {
  }

  @Override
  public void afterNavigateRefresh(WebDriver driver) {
  }

  @Override
  public void beforeNavigateRefresh(WebDriver driver) {
  }
}
