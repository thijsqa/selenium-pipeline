package com.softonicweb.container.seleniumframework;

import org.openqa.selenium.WebDriver;

/* Represents a generic web page to facilitate instantiation.
 * <p>Creates a page object from an instance of Selenium or a page model.
 *
 * @author ivan.solomonoff
 */
public class GenericPage extends AbstractWebPage {

  public GenericPage(final WebDriver driver) {
    super(driver);
  }

  public GenericPage(final WebDriver driver, final int timeout) {
    super(driver, timeout);
  }
}
