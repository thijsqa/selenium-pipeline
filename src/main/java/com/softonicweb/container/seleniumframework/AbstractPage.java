package com.softonicweb.container.seleniumframework;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocator;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocator;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.base.Function;

/**
 * Abstract class for all page models.
 * <p>Creates a page object from an instance of Selenium or a page model.
 *
 * @author alfredo.lopez
 */
public abstract class AbstractPage {

  protected final WebDriver driver;
  protected final SoftonicWebDriverWait wait;

  public AbstractPage(final WebDriver driver) {
    this(driver, SoftonicWebDriverWait.SHORT_TIMEOUT_IN_SECS);
  }

  public AbstractPage(final WebDriver driver, final int timeout) {
    this.driver = driver;
    wait = new SoftonicWebDriverWait(driver, timeout);

    ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, timeout) {

      @Override
      public ElementLocator createLocator(final Field field) {
        return new AjaxElementLocator(driver, field, timeout) {
        };
      };
    };

    PageFactory.initElements(finder, this);
  }

  protected WebElement findElementByXpathFormat(String xpathFormat, String argument) {
    String xpathLocator = String.format(xpathFormat, argument);
    return driver.findElement(By.xpath(xpathLocator));
  }

  protected WebElement findElementByNameFormat(String nameFormat, String argument) {
    String nameLocator = String.format(nameFormat, argument);
    return driver.findElement(By.name(nameLocator));
  }

  protected void selectComboBoxByValue(WebElement element, String option) {
    Select comboBox = new Select(element);
    List<WebElement> options = comboBox.getOptions();
    for (WebElement we : options) {
      if (we.getText().contains(option)) {
        we.click();
        break;
      }
    }
  }

  protected void selectComboBoxByIndex(WebElement element, int index) {
    Select comboBox = new Select(element);
    List<WebElement> options = comboBox.getOptions();
    for (WebElement we : options) {
      we.click();
      if (index == 0) {
        break;
      }
      index--;
    }
  }

  public boolean isElementDisplayed(WebElement element) {
    boolean isVisible;
    try {
      wait.disableImplicitWait();
      isVisible = element.isDisplayed();
    } catch (NoSuchElementException e) {
      isVisible = false;
    } finally {
      wait.enableImplicitWait();
    }
    return isVisible;
  }

  public boolean isElementDisplayed(By by) {
    boolean isVisible;
    try {
      wait.disableImplicitWait();
      isVisible = driver.findElement(by).isDisplayed();
    } catch (NoSuchElementException e) {
      isVisible = false;
    } finally {
      wait.enableImplicitWait();
    }
    return isVisible;
  }

  public boolean areAllElementsPresent(WebElement... elementsList) {
    return ExpectedConditions.visibilityOfAllElements(Arrays.asList(elementsList)) != null;
  }

  public SoftonicWebDriverWait getWait() {
    return wait;
  }

  // We need to wrap sendKeys calls as sendKeys is not thread safe
  public void sendKeysThreadSafe(final WebElement webelement, final String inputStr) {
    WebDriverWait keyWait = new WebDriverWait(driver, SoftonicWebDriverWait.SHORT_TIMEOUT_IN_SECS);
    keyWait.until(new Function<WebDriver, Boolean>() {
      @Override
      public Boolean apply(WebDriver input) {
        webelement.clear();
        webelement.sendKeys(inputStr);
        return webelement.getAttribute("value").equals(inputStr);
      }
    });
  }

  protected void switchToNewWindow() {
    Set<String> winHandles = driver.getWindowHandles();
    driver.switchTo().window((String) winHandles.toArray()[winHandles.size() - 1]);
  }

}
