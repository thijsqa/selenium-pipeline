package com.softonicweb.container.seleniumframework;

/**
 * Abstract class for all widget models.
 * <p>Creates a widget object from an instance of a page model.
 *
 * @param <T> parent page
 *
 * @author ivan.solomonoff
 */
public abstract class AbstractWidget<T extends AbstractWebPage> extends AbstractWebPage {

  protected static final int POPUP_TIMEOUT = 10000;
  protected static final int HOVER_TIMEOUT = 1000;

  protected T parentPage;

  public AbstractWidget(T parentPage) {
    super(parentPage.driver);
    this.parentPage = parentPage;
  }

  public T getParentPage() {
    return parentPage;
  }
}
