package com.softonicweb.container.seleniumframework;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;

import com.softonicweb.container.framework.BaseLoggingTest;
import com.softonicweb.container.framework.DataGenerator;
import com.softonicweb.container.framework.IPlatform;
import com.softonicweb.container.framework.IScreenshooter;
import com.softonicweb.container.framework.ProxySession;
import com.softonicweb.container.framework.ProxyWrapper;

/**
 * Base class for all WebDriver tests.
 *
 * @author ivan.solomonoff
 */
public abstract class BaseWebDriverTest extends BaseLoggingTest {

  private static final Logger LOGGER = Logger.getLogger(BaseWebDriverTest.class);

  private final InheritableThreadLocal<IWebDriverSession> wdSession =
      new InheritableThreadLocal<IWebDriverSession>();

  private final InheritableThreadLocal<String> wdSessionId = new InheritableThreadLocal<String>();

  protected SoftonicWebDriverWait wait;

  private final int startWebDriverWaitAfterFailure = 5000;
  private final int firefoxBrowserProfileWait = 60;
  private final long teardownTimeoutInMillis = 240000; // 4 minutes

  protected final String[] whitelist = new String[] { ".*?facebook.+", ".*?rubiconproject.+",
      ".*?gstatic.+", ".*?google(?!ads).+", ".*?sft.+", ".*?softonic.+", ".*?google.+" };

  private IWebDriverSession wdSession() {
    return wdSession.get();
  }

  protected void setWebDriverSession(IWebDriverSession session) {
    wdSession.set(session);
  }

  protected void setWebDriverSessionId(String sessionId) {
    wdSessionId.set(sessionId);
  }

  protected ProxyWrapper proxy() {
    if (wdSession() instanceof LocalWebDriverSession) {
      return ProxySession.proxy();
    } else {
      return ProxySession.proxy(wdSessionId.get());
    }
  }

  // We use the RemoteWebDriver with no proxy by default
  protected IWebDriverSession getWebDriverSession(String sessionId) {
    return new RemoteWebDriverSession(sessionId, false);
  }

  protected void setProxy() {
    proxy().setCaptureHeaders(true);
    proxy().setCaptureContent(false);
    proxy().setCaptureBinaryContent(false);
    Pattern p = Pattern.compile("(https?://(www.)?(.+)/(.+)?)");
    Matcher m = p.matcher(testEnvironment().getInstanceBaseUrl());
    m.find();
    final int domainGroup = 3;
    String baseUrlRegex = ".*?" + m.group(domainGroup) + ".+";
    proxy().whitelistRequests(ArrayUtils.addAll(whitelist, new String[] { baseUrlRegex }),
        HttpStatus.SC_OK);
  }

  @Override
  @BeforeMethod(alwaysRun = true)
  protected void setup(Method method, Object[] testArguments, ITestResult tr) {
    super.setup(method, testArguments, tr);

    String sessionId = method.getName() + testArguments.hashCode();
    startWebDriver(sessionId, getDesiredCapabilitiesPerBrowser());
  }

  @Override
  @AfterMethod(alwaysRun = true, timeOut = teardownTimeoutInMillis)
  protected void teardown(ITestResult tr, Method method) {
    if (threadLocalPlatform.get() != null) {
      tr.setAttribute("Browser", platform().toString());
    }
    if ((logger() != null) && (tr.getStatus() == ITestResult.FAILURE)
        || (tr.getStatus() == ITestResult.SUCCESS_PERCENTAGE_FAILURE)) {
      logTestFailureWithScreenshot(method.getName() + "_" + DataGenerator.getUniqueId(),
          tr.getThrowable());
    }

    closeWebDriverSession();
    super.teardown(tr, method);
  }

  protected WebDriver driver() {
    return wdSession().driver();
  }

  @Override
  protected IPlatform parsePlatform(String platform) {
    return Browser.valueOf(platform.toUpperCase(Locale.ENGLISH));
  }

  @Override
  protected Browser platform() {
    return (Browser) super.platform();
  }

  @Override
  protected IScreenshooter getScreenshooter(String screenshotName) {
    return new SeleniumScreenshooter(driver(), screenshotName,
        SUITE_ENVIRONMENT.getScreenshotsDir());
  }

  protected WebDriver startWebDriver(String sessionId, DesiredCapabilities capablities) {
    setWebDriverSessionId(sessionId);
    IWebDriverSession driverSession = getWebDriverSession(sessionId);
    setWebDriverSession(driverSession);
    WebDriver driver = null;
    final int maxRetries = 2;
    for (int i = 0; i <= maxRetries; i++) { // try 3 times
      try {
        driver = startWebDriverSession(capablities, sessionId);
        wait = new SoftonicWebDriverWait(driver);
        // apply temporary workaround for chromedriver bug
        // https://bugs.chromium.org/p/chromedriver/issues/detail?id=1901
        // maximizeBrowser(driver);
        driver().manage().deleteAllCookies();
        break;
      } catch (RuntimeException e) {
        String error = "Failed to open new WebDriver session. " + e.getMessage();
        LOGGER.error(error);
        closeWebDriverSession();
        if (i == maxRetries) {
          throw new RuntimeException(e);
        } else {
          sleep(startWebDriverWaitAfterFailure); // wait 5 seconds.
        }
      }
    }
    return driver;
  }

  /** apply temporary workaround for chromedriver bug
   https://bugs.chromium.org/p/chromedriver/issues/detail?id=1901
    protected void maximizeBrowser(WebDriver driver) {
      driver.manage().window().maximize();
    }**/

  protected WebDriver startWebDriverSession(DesiredCapabilities capability, String sessionId) {
    return wdSession().startSession(platform(), capability);
  }

  protected void openStartUrl() {
    String startUrl = getStartUrl();
    driver().get(startUrl);
    if (testEnvironment().getPlatform().toString().startsWith("IE")) {
      // TODO(carlos.silva): IE has issues cleaning cookies, needs to get URL twice, see:
      // https://code.google.com/p/selenium/issues/detail?id=5101
      // http://www.coderanch.com/t/570015/tools/Internet-cookies-deleted-selenium-webdriver
      driver().manage().deleteAllCookies();
      logger().logComment("Deleted all IE cookies. Reloading page.");
      driver().get(startUrl);
    }
  }

  protected String getStartUrl() {
    return testEnvironment().getStartUrl();
  }

  protected void closeWebDriverSession() {
    if (wdSession() != null) {
      wdSession().endSession();
    } else {
      LOGGER.info("No WD session found to close");
    }
  }

  protected DesiredCapabilities getDesiredCapabilitiesPerBrowser() {
    DesiredCapabilities caps;
    Proxy proxy = new Proxy();
    proxy.setProxyType(org.openqa.selenium.Proxy.ProxyType.SYSTEM);
    switch (platform()) {
      case IE8:
      case IE9:
      case IE11:
        caps = DesiredCapabilities.internetExplorer();
        break;
      case CHROME:
        caps = DesiredCapabilities.chrome();
        caps.setCapability("chrome.prefs", getChromePrefs());
        caps.setCapability(ChromeOptions.CAPABILITY, getChromeOptions());
        break;
      case SAFARI:
        return DesiredCapabilities.safari();
      case HTMLUNIT:
        return DesiredCapabilities.htmlUnit();
      case PHANTOMJS:
        return DesiredCapabilities.phantomjs();
      case FIREFOX:
        FirefoxOptions options = new FirefoxOptions();
        caps = DesiredCapabilities.firefox();
        caps.setCapability("moz:firefoxOptions", options);
        caps.setCapability(FirefoxDriver.PROFILE, getFirefoxProfile());
        break;
      default:
        caps = DesiredCapabilities.firefox();
        caps.setCapability(FirefoxDriver.PROFILE, getFirefoxProfile());
    }
    caps.setCapability(CapabilityType.PROXY, proxy);
    return caps;
  }

  private String getDeviceLanguageForCurrentLocale() {
    return instance().getLocale().getLanguage();
  }

  protected FirefoxProfile getFirefoxProfile() {
    FirefoxProfile firefoxProfile = new FirefoxProfile();
    firefoxProfile.setPreference("intl.accept_languages", getDeviceLanguageForCurrentLocale());
    // Browser Timeouts to handle async scripts.
    firefoxProfile.setPreference("dom.max_chrome_script_run_time", firefoxBrowserProfileWait);
    firefoxProfile.setPreference("setTimeoutInSeconds", firefoxBrowserProfileWait);
    firefoxProfile.setPreference("dom.max_script_run_time", firefoxBrowserProfileWait);
    firefoxProfile.setPreference("browser.xul.error_pages.enabled", false);
    firefoxProfile.setPreference("general.useragent.extra.firefox", "Firefox");
    firefoxProfile.setPreference("app.update.enabled", false);
    firefoxProfile.setPreference("dom.disable_open_during_load", false);
    firefoxProfile.setPreference("browser.frames.enabled", true);

    // Add pop up handling functionality.
    firefoxProfile.setPreference("dom.popup_allowed_events",
        "change click dblclick mouseup reset submit");
    firefoxProfile.setPreference("dom.popup_maximum", 0);
    firefoxProfile.setPreference("browser.popups.showPopupBlocker", true);
    firefoxProfile.setPreference("privacy.popups.showBrowserMessage", false);
    firefoxProfile.setPreference("privacy.popups.usecustom", true);
    firefoxProfile.setPreference("privacy.popups.firstTime", true);
    firefoxProfile.setPreference("privacy.popup.policy", 1);
    firefoxProfile.setPreference("privacy.popups.disable_from_plugins", 2);

    // Preferences to allow direct downloading of executable files, with no dialog
    firefoxProfile.setPreference("browser.helperApps.neverAsk.saveToDisk",
        "application/octet-stream");
    firefoxProfile.setPreference("browser.download.dir", SUITE_ENVIRONMENT.getDownloadsFolder());
    firefoxProfile.setPreference("browser.download.folderList", 2);

    // Preferences to allow JS execution between different origins, issue in Adsense suite..
    // http://code.google.com/p/selenium/issues/detail?id=2863
    firefoxProfile.setPreference("capability.policy.default.Window.QueryInterface", "allAccess");
    firefoxProfile.setPreference("capability.policy.default.Window.frameElement.get", "allAccess");

    firefoxProfile.setPreference("network.proxy.type", 0);
    firefoxProfile.setAcceptUntrustedCertificates(true);
    firefoxProfile.setAssumeUntrustedCertificateIssuer(true);
    return firefoxProfile;
  }

  protected Map<String, String> getChromePrefs() {
    Map<String, String> prefs = new HashMap<String, String>();
    prefs.put("download.prompt_for_download", "false");
    prefs.put("download.default_directory",
        FilenameUtils.normalize(SUITE_ENVIRONMENT.getDownloadsFolder()));
    return prefs;
  }

  protected ChromeOptions getChromeOptions() {
    ChromeOptions ops = new ChromeOptions();
    ops.addArguments("start-maximized");
    ops.addArguments("disable-infobars");
    ops.addArguments("disable-notifications");
    setChromeLanguageOptions(ops, getDeviceLanguageForCurrentLocale());
    return ops;
  }

  private ChromeOptions setChromeLanguageOptions(ChromeOptions options,
      String browserLanguage) {
    if (browserLanguage != null) {
      options.addArguments("lang=" + browserLanguage);
    } else {
      options.addArguments("lang=es");
    }
    return options;
  }

  protected void openNewTab() {
    driver().findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL + "t");
  }

  protected void closeCurrentTab() {
    driver().findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL + "w");
  }

  protected void switchToNewWindow() {
    Set<String> winHandles = driver().getWindowHandles();
    driver().switchTo().window((String) winHandles.toArray()[winHandles.size() - 1]);
  }

  @DataProvider(name = "devices", parallel = true)
  protected static Object[][] getPlatforms() {
    String[] platforms = SUITE_ENVIRONMENT.getPlatforms();
    Object[][] result = new Object[platforms.length][];

    for (int i = 0; i < platforms.length; i++) {
      result[i] = new Object[] { Browser.valueOf(platforms[i].toUpperCase(Locale.ENGLISH)) };
    }

    return result;
  }

  @DataProvider(name = "instancesAndDevices", parallel = true)
  protected static Object[][] getInstancesAndPlatforms() {
    Object[][] instances = getInstances();
    Object[][] platforms = getPlatforms();

    int resultsSize = instances.length * platforms.length;
    Object[][] result = new Object[resultsSize][];

    int counter = 0;
    for (Object[] instance : instances) {
      for (Object[] platform : platforms) {
        result[counter] = new Object[] { instance[0], platform[0] };
        counter++;
      }
    }

    return result;
  }
}
