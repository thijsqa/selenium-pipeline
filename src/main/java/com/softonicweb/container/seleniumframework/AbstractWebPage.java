package com.softonicweb.container.seleniumframework;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Abstract class for all web page models.
 * <p>Creates a page object from an instance of Selenium or a page model.
 *
 * @author ivan.solomonoff
 * @author thijs.vandewynckel
 */
public abstract class AbstractWebPage extends AbstractPage {

  @FindBy(xpath = "//a[@class='ugc-modal__close js-login-modal-close']")
  private WebElement solutionsExitIntentClose;

  private static final String RELATEDAPPS = "[data-auto=app-related-apps-title]";

  public AbstractWebPage(final WebDriver driver) {
    super(driver);
    wait.forPageAndAjaxToLoad();
  }

  public AbstractWebPage(final WebDriver driver, final int timeout) {
    super(driver, timeout);
    wait.forPageAndAjaxToLoad();
  }

  protected WebElement findElementByCssFormat(String cssFormat, String argument) {
    String cssLocator = String.format(cssFormat, argument);
    return driver.findElement(By.cssSelector(cssLocator));
  }

  public String url() {
    return driver.getCurrentUrl();
  }

  public void mouseHover(WebElement toElem) {
    scrollIntoView(toElem);
  }

  public AbstractWebPage scrollDown() {
    driver.findElement(By.cssSelector("body")).sendKeys(Keys.PAGE_DOWN);
    return this;
  }

  public AbstractWebPage scrollToRelatedApps() {
    WebElement element = driver.findElement(By.cssSelector(RELATEDAPPS));
    JavascriptExecutor jse = (JavascriptExecutor) driver;
    jse.executeScript("arguments[0].scrollIntoView()", element);
    return this;
  }

  public String executeJavascriptCode(String jsCode) {
    JavascriptExecutor js = (JavascriptExecutor) driver;
    return String.valueOf(js.executeScript(jsCode));
  }

  public void scrollIntoView(WebElement element) {
    ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
  }

  public String getInnerHTML(WebElement element) {
    return (String) ((JavascriptExecutor) driver).executeScript("return arguments[0].innerHTML;",
        element);
  }

  public boolean isSolutionsExitIntentPresent() {
    return isElementDisplayed(solutionsExitIntentClose);
  }

  public AbstractWebPage dismissSolutionsExitIntent() {
    solutionsExitIntentClose.click();
    return this;
  }
}
