package com.softonicweb.container.seleniumframework;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Set;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.protocol.BasicHttpContext;
import org.openqa.selenium.Cookie;

import com.softonicweb.container.framework.FileDownloaderUtil;

/**
 * Class for downloading files using Selenium Cookie class.
 * @author carlos.silva
 *
 */
public class SeleniumFileDownloaderUtil extends FileDownloaderUtil {
  private final Set<Cookie> cookies;

  public SeleniumFileDownloaderUtil(Set<Cookie> cookies) {
    super();
    this.cookies = cookies;
  }

  /**
   * Starts a download from the specified URL.
   *
   * @param url Url where to start the download from.
   * @param destinationDir Local destination folder.
   * @param byteRange ie. "500-999", or null for whole file.
   *        See http://greenbytes.de/tech/webdav/rfc2616.html#header.range
   *
   * @return true if the download has been correctly performed
   */
  @Override
  public boolean downloadFromUrl(String url, String destinationDir, String byteRange) {
    URL source;
    URI sourceUri;
    try {
      source = new URL(url);
      sourceUri = source.toURI();
    } catch (MalformedURLException e) {
      throw new RuntimeException(e);
    } catch (URISyntaxException e) {
      throw new RuntimeException(e);
    }
    File parentFolder = null;
    if (destinationDir != null) {
      parentFolder = new File(destinationDir);
    } else {
      parentFolder = downloadsFolder;
    }

    if (!parentFolder.mkdirs() && !parentFolder.isDirectory()) {
      throw new RuntimeException("Directory path " + parentFolder.getPath()
          + " could not be created.");
    }

    HttpClient client = setupClient(loadCookieState(cookies));
    BasicHttpContext ctx = new BasicHttpContext();
    HttpGet httpGet = new HttpGet(sourceUri);

    if (byteRange != null) {
      httpGet.addHeader("Accept-Ranges", "bytes");
      httpGet.addHeader("Range", "bytes=" + byteRange);
    }

    logger().logComment("GET request being sent to: " + httpGet.getURI());
    try {
      HttpResponse response = client.execute(httpGet, ctx);
      downloadHttpStatus = response.getStatusLine().getStatusCode();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    logger().logComment("GET response status: " + downloadHttpStatus);

    if ((byteRange == null) && (downloadHttpStatus != HttpStatus.SC_OK)) {
      logger().logComment("Unable to start the download, response status: " + downloadHttpStatus);
      return false;
    } else if ((byteRange != null) && (downloadHttpStatus != HttpStatus.SC_PARTIAL_CONTENT)) {
      logger().logComment(
          "Error. Response status is not PARTIAL CONTENT (206): "
          + downloadHttpStatus);
      return false;
    } else {
      destinationFile = new File(parentFolder, getServerFileNameFromURI(url, ctx));
      downloadFileFromUrl(source, destinationFile, byteRange);
      return true;
    }
  }

  /**
   * Load in all the cookies WebDriver currently knows about so that we can
   * mimic the browser cookie state.
   */
  private BasicCookieStore loadCookieState(Set<Cookie> seleniumCookieSet) {
    BasicCookieStore webDriverCookieStore = new BasicCookieStore();
    for (Cookie seleniumCookie : seleniumCookieSet) {
      BasicClientCookie duplicateCookie =
          new BasicClientCookie(seleniumCookie.getName(), seleniumCookie.getValue());
      duplicateCookie.setDomain(seleniumCookie.getDomain());
      duplicateCookie.setSecure(seleniumCookie.isSecure());
      duplicateCookie.setExpiryDate(seleniumCookie.getExpiry());
      duplicateCookie.setPath(seleniumCookie.getPath());
      webDriverCookieStore.addCookie(duplicateCookie);
    }
    return webDriverCookieStore;
  }
}
