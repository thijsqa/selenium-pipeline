package com.softonicweb.container.seleniumframework;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Duration;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Sleeper;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.softonicweb.container.framework.GmailReaderUtil;
import com.softonicweb.container.framework.TestLogger;

/**
 * Wraps the selenium object to be used in a more convenient way and adds functionality.
 *
 * @author ivan.solomonoff
 */
public class SoftonicWebDriverWait extends WebDriverWait {

  public static final int SHORT_TIMEOUT_IN_SECS = 15; // 15 seconds
  public static final long FADE_IN_WAIT = 3000;
  public static final long SHORT_PAUSE = 500;

  protected static final long PAGE_LOAD_TIMEOUT = 10000; // ms

  private static final long DEFAULT_WAIT_TIMEOUT_IN_SECS = 10;
  private static final TestLogger LOGGER = TestLogger.getInstance();

  private final WebDriver driver;

  public SoftonicWebDriverWait(WebDriver driver, long timeOutInSeconds) {
    super(driver, timeOutInSeconds);
    this.driver = driver;
  }

  public SoftonicWebDriverWait(WebDriver driver) {
    this(driver, DEFAULT_WAIT_TIMEOUT_IN_SECS);
  }

  public void forElementPresent(WebElement element) {
    until(isElementPresent(element));
  }

  public void forElementPresent(By locator) {
    until(ExpectedConditions.presenceOfElementLocated(locator));
  }

  public void forTitle(String title) {
    until(ExpectedConditions.titleIs(title));
  }

  public void forTitleContaining(String titleSubstr) {
    until(ExpectedConditions.titleContains(titleSubstr));
  }

  public void forElementNotPresent(WebElement element) {
    until(not(isElementPresent(element)));
  }

  public void forAllElementsPresent(WebElement... elementsList) {
    until(areAllElementsPresent(elementsList));
  }

  public void forElementToBeSelected(WebElement element) {
    until(ExpectedConditions.elementToBeSelected(element));
  }

  public void forElementDisabled(WebElement element) {
    until(ExpectedConditions.stalenessOf(element));
  }

  public void forElementVisible(By locator) {
    forElementPresent(locator);
    until(ExpectedConditions.visibilityOfElementLocated(locator));
  }

  public void forElementVisible(WebElement element) {
    forElementPresent(element);
    until(ExpectedConditions.visibilityOf(element));
  }

  public void forElementNotVisible(By locator) {
    until(ExpectedConditions.invisibilityOfElementLocated(locator));
  }

  public void forElementToBeClickable(By locator) {
    until(ExpectedConditions.elementToBeClickable(locator));
  }

  public void forElementToBeClickable(WebElement element) {
    until(ExpectedConditions.elementToBeClickable(element));
  }

  public void forElementToBeGone(WebElement element) {
    until(ExpectedConditions.stalenessOf(element));
  }

  public void forTextPresent(By locator, String text) {
    until(ExpectedConditions.textToBePresentInElementLocated(locator, text));
  }

  public void forTextPresentInElementValue(By locator, String text) {
    until(ExpectedConditions.textToBePresentInElementValue(locator, text));
  }

  public void forFrameAvailableAndSwitchToIt(String frameLocator) {
    until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(frameLocator));
  }

  public void forEmailPresent(String folderLabel, String pattern) {
    until(isEmailPresent(folderLabel, pattern));
  }

  public Boolean forAsyncAttribute(WebElement e, String textValue, long milliseconds,
      int retries) {
    // return true when the element is present
    // Up to n retries times
    for (int i = 0; i < retries; i++) {
      // Check whether our element is there yet
      if (e.getAttribute(textValue) != null) {
        return Boolean.valueOf(e.getAttribute(textValue));
      }
      pause(milliseconds);
    }
    return null;
  }

  public void forPageAndAjaxToLoad() {
    try {
      until(isAjaxLoaded());
    } catch (TimeoutException e) {
      // If reached timeout, try to go ahead if at least the status is 'interactive'.
      until(isAjaxInteractive());
    }
  }

  private ExpectedCondition<Boolean> isElementPresent(final WebElement element) {
    ExpectedCondition<Boolean> elementPresent = new ExpectedCondition<Boolean>() {
      @Override
      public Boolean apply(WebDriver localDriver) {
        return ExpectedConditions.visibilityOf(element) != null;
      }
    };
    return elementPresent;
  }

  private ExpectedCondition<Boolean> areAllElementsPresent(final WebElement... elementsList) {
    ExpectedCondition<Boolean> elementsPresent = new ExpectedCondition<Boolean>() {
      @Override
      public Boolean apply(WebDriver localDriver) {
        return ExpectedConditions.visibilityOfAllElements(Arrays.asList(elementsList)) != null;
      }
    };
    return elementsPresent;
  }

  private ExpectedCondition<Boolean> not(final ExpectedCondition<?> toInvert) {
    return new ExpectedCondition<Boolean>() {
      @Override
      public Boolean apply(WebDriver localDriver) {
        try {
          Object result = toInvert.apply(driver);
          return (result == null || result == Boolean.FALSE);
        } catch (Exception e) {
          e.printStackTrace();
          return true;
        }
      }
    };
  }

  private ExpectedCondition<Boolean> isEmailPresent(final String folderLabel, final String regex) {
    ExpectedCondition<Boolean> emailPresent = new ExpectedCondition<Boolean>() {
      @Override
      public Boolean apply(WebDriver localDriver) {
        return GmailReaderUtil.isEmailPresent(folderLabel, regex);
      }
    };
    return emailPresent;
  }

  private ExpectedCondition<Boolean> isAjaxLoaded() {
    ExpectedCondition<Boolean> pageLoaded = new ExpectedCondition<Boolean>() {
      @Override
      public Boolean apply(WebDriver localDriver) {
        // Checks that Ajax events are processed and we can start testing
        String script = "return document.readyState";
        // some old browsers might report as 'loaded'
        return ((JavascriptExecutor) localDriver).executeScript(script).equals("complete")
            || ((JavascriptExecutor) localDriver).executeScript(script).equals("loaded");
        }
    };
    return pageLoaded;
  }

  private ExpectedCondition<Boolean> isAjaxInteractive() {
    ExpectedCondition<Boolean> pageLoaded = new ExpectedCondition<Boolean>() {
      @Override
      public Boolean apply(WebDriver localDriver) {
        String script = "return document.readyState";
        return ((JavascriptExecutor) localDriver).executeScript("return document.readyState")
            .equals("interactive")
            || ((JavascriptExecutor) localDriver).executeScript(script).equals("complete")
            || ((JavascriptExecutor) localDriver).executeScript(script).equals("loaded");
        }
    };
    return pageLoaded;
  }

  public void pause(long milliseconds) {
    try {
      LOGGER.logComment("Sleeping for " + milliseconds + " milliseconds");
      Sleeper.SYSTEM_SLEEPER.sleep(new Duration(milliseconds, MILLISECONDS));
    } catch (InterruptedException ie) {
      LOGGER.logComment("Wait interrupted.");
    }
  }

  // Leaving this here just in case we want to add extra actions in the future.
  @Override
  protected RuntimeException timeoutException(String message, Throwable lastException) {
    return super.timeoutException(message, lastException);
  }

  public void disableImplicitWait() {
    driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
  }

  public void enableImplicitWait() {
    driver.manage().timeouts().implicitlyWait(SHORT_TIMEOUT_IN_SECS, TimeUnit.SECONDS);
  }
}
