package com.softonicweb.container.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.softonicweb.container.seleniumframework.AbstractWebPage;
import com.softonicweb.container.seleniumframework.AbstractWidget;

/**
 * A reusable element to provide standardized Twitter Social button manipulation.
 *
 * @param <T> parent page
 *
 * @author guillem.hernandez
 */
public class TwitterWidget<T extends AbstractWebPage> extends AbstractWidget<T> {

  @FindBy(id = "twitter_button")
  private WebElement tweetFrame;

  @FindBy(id = "count")
  private WebElement tweetsCounter;

  public TwitterWidget(T page) {
    super(page);
  }

  public void clickTweetButton() {
    driver.switchTo().frame(tweetFrame);
    WebElement b = driver.findElement(By.id("b"));
    b.click();
    driver.switchTo().defaultContent();
    wait.pause(POPUP_TIMEOUT);
  }

  public boolean isTweetButtonPresent() {
    wait.pause(POPUP_TIMEOUT);
    return isElementDisplayed(tweetFrame);
  }

  public int getTweetsCounter() {
    driver.switchTo().frame(tweetFrame);
    int tweets = Integer.parseInt(tweetsCounter.getText());
    driver.switchTo().defaultContent();
    return tweets;
  }

}
