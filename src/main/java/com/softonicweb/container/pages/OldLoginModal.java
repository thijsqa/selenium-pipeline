package com.softonicweb.container.pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Contains methods to interact with the modal login window in Softonic.
 *
 * @author ivan.solomonoff
 * @author guillem.hernandez
 */
public class OldLoginModal extends AbstractSoftonicWebPage {

  @FindBy(xpath = "//div[contains(@class,'with-site-login')]/div[4]/input")
  private WebElement submit;
  @FindBy(xpath = "//input[contains(@name,'username')]")
  private WebElement emailTextbox;
  @FindBy(css = "input[type='password']")
  private WebElement passwordTextbox;
  @FindBy(xpath = "//a[contains(@class,'gigya-forgotPassword')]")
  private WebElement forgotPasswordLink;
  @FindBy(xpath = "id('gigya-modal-plugin-container-showScreenSet_social_0_uiContainer')"
      + "//span/button[@title='Facebook']")
  private WebElement fbConnectButton;
  @FindBy(xpath = "id('gigya-modal-plugin-container-showScreenSet_social_0_uiContainer')"
      + "//span[@title='Twitter']")
  private WebElement twitterSignInButton;

  protected static final long TAB_WAIT = 2000; // ms

  public OldLoginModal(WebDriver driver) {
    super(driver);
  }

  public OldLoginModal enterEmail(String email) {
    wait.forElementPresent(emailTextbox);
    sendKeysThreadSafe(emailTextbox, email);
    wait.pause(TAB_WAIT);
    emailTextbox.sendKeys(Keys.TAB);
    return this;
  }

  public OldLoginModal enterPassword(String password) {
    sendKeysThreadSafe(passwordTextbox, password);
    return this;
  }

  public DownloadPage clickLoginButton() {
    submit.click();
    return new DownloadPage(driver);
  }

  public String getForgotPasswordLink() {
    return (isElementDisplayed(forgotPasswordLink)) ? forgotPasswordLink.getAttribute("href") : "";
  }

  public boolean isLoginModalPresent() {
    return isElementDisplayed(submit);
  }

  public RecoverPasswordModal clickOnForgotPasswordLink() {
    forgotPasswordLink.click();
    return new RecoverPasswordModal(driver);
  }

  public SocialMediaPopUpExternalModal clickConnectWithFacebook() {
    fbConnectButton.click();
    return new SocialMediaPopUpExternalModal(driver);
  }

  public SocialMediaPopUpExternalModal clickConnectWithTwitter() {
    twitterSignInButton.click();
    return new SocialMediaPopUpExternalModal(driver);
  }
}
