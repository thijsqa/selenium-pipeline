package com.softonicweb.container.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.softonicweb.container.framework.Localization.Country;

/**
 * Contains methods to interact with the user modal window.
 *
 * @author guillem.hernandez
 */
public class NewUserModal extends AbstractSoftonicWebPage {

  @FindBy(id = "closeBut")
  private WebElement closeButton;

  @FindBy(xpath = ".//*[@class='btn toggle_form']")
  private WebElement newAccountButton;

  @FindBy(id = "signup_email")
  private WebElement emailInput;

  @FindBy(id = "signup_password")
  private WebElement passwordInput;

  @FindBy(id = "nickname")
  private WebElement usernameInput;

  @FindBy(id = "bday")
  private WebElement birthdateDaySelect;

  @FindBy(id = "bmonth")
  private WebElement birthdateMonthSelect;

  @FindBy(id = "byear")
  private WebElement birthdateYearSelect;

  @FindBy(id = "country")
  private WebElement countrySelect;

  @FindBy(xpath = "id('already_registered')/a")
  private WebElement signInHereButton;

  @FindBy(xpath = "id('register_form')//button")
  private WebElement submitButton;

  @FindBy(id = "accept-checkbox")
  private WebElement acceptCheckbox;

  public NewUserModal(WebDriver driver) {
    super(driver);
  }

  public NewUserModal enterEmail(String email) {
    sendKeysThreadSafe(emailInput, email);
    return this;
  }

  public NewUserModal enterPassword(String password) {
    sendKeysThreadSafe(passwordInput, password);
    return this;
  }

  public NewUserModal enterUsername(String username) {
    sendKeysThreadSafe(usernameInput, username);
    return this;
  }

  // Enters birth date in the format DDMMYYYY.
  public NewUserModal enterDay(String day) {
    birthdateDaySelect.sendKeys(day);
    return this;
  }

  // Enters birth date in the format DDMMYYYY.
  public NewUserModal enterMonth(int month) {
    selectComboBoxByIndex(birthdateMonthSelect, month);
    return this;
  }

  // Enters birth date in the format DDMMYYYY.
  public NewUserModal enterYear(String year) {
    birthdateYearSelect.sendKeys(year);
    return this;
  }

  public NewUserModal selectCountryOfResidense(String value) {
    countrySelect.sendKeys(value);
    return this;
  }

  public NewUserModal submitRegistration() {
    submitButton.click();
    return this;
  }

  public NewUserModal quickRegisterUser(String email, String password, String username, String day,
      String month, String year, Country country) {
    enterEmail(email).enterPassword(password).enterUsername(username);
    enterDay(day).enterMonth(Integer.parseInt(month)).enterYear(year);
    if (country.equals(Country.ES) || country.equals(Country.EN)) {
      selectCountryOfResidense("EN");
    }
    checkAcceptCheckbox();
    return submitRegistration();
  }

  public void checkAcceptCheckbox() {
    if (!acceptCheckbox.isSelected()) {
      acceptCheckbox.click();
    }
  }
}
