package com.softonicweb.container.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Contains methods to interact with the Softonic Downloads search page in any Softonic
 * plaftorm.
 * <p>
 * eg. http://www.softonic.com/s/vlc
 *
 * @author james.oram
 * @author guillem.hernandez
 */
public class DownloadSearchResultsPage extends DownloadSearchPage {

  private static final String AD_CPD_BY_NAME =
      "//div[@class='sponsored']/../h5/a[contains(., '%s')]";

  private static final String AD_CPC_BY_NAME = "id('results')/ul/li/h5/a[contains(., '%s')]";

  @FindBy(css = "#ad_cpd/td/a")
  private WebElement adCpd;

  @FindBy(id = "program_list")
  private WebElement searchResultsDiv;

  @FindBy(xpath = "//article[contains(@class, 'app-list-item')]")
  private WebElement firstResultProgramLink;

  @FindBy(xpath = "id('program_list')/tbody/tr[1]/td[4]/ul/li/a")
  private WebElement downloadButtonFromSearchResults;

  @FindBy(id = "ads-container-top")
  private WebElement csaWidget;

  @FindBy(xpath = "//em[@class='title-page-details']")
  private WebElement numberResults;

  @FindBy(xpath = "id('program_list')/li[@data-ua-common='l=pg1-pos1']/div[4]/ul/li/a/strong")
  private WebElement catalogWarningDLButton;

  @FindBy(xpath = "//*[contains(@class,'popup-catalog-warn')]")
  private WebElement searchCatalogWarning;

  @FindBy(xpath = "//*[contains(@class,'popup-catalog-warn')]/button/i")
  private WebElement searchCatalogWarningCloseButton;

  @FindBy(xpath = "id('program_list')/li[@data-ua-common='l=pg1-pos1']/div[1]/h5/a/strong")
  private WebElement searchCatalogWarningTitle;

  @FindBy(xpath = "//*[contains(@class,'popup-catalog-warn-ctd-btn')]")
  private WebElement searchCatalogWarningContinueDLButton;

  public DownloadSearchResultsPage(WebDriver driver) {
    super(driver);
  }

  public String getSearchResults() {
    return searchResultsDiv.getText();
  }

  public boolean isfirstResultProgramLinkPresent() {
    return isElementDisplayed(firstResultProgramLink);
  }

  public DownloadSearchResultsPage clickFirstResult() {
    firstResultProgramLink.click();
    return this;
  }

  public boolean isCatalogWarningDLButtonPresent() {
    return isElementDisplayed(catalogWarningDLButton);
  }

  public boolean isSearchCatalogWarningPresent() {
    return isElementDisplayed(searchCatalogWarning);
  }

  public void clickToDownload() {
    downloadButtonFromSearchResults.click();
  }

  public DownloadSearchResultsPage clickCatalogWarningDLButton() {
    catalogWarningDLButton.click();
    return this;
  }

  public DownloadSearchResultsPage clickSearchCatalogWarningCloseButton() {
    searchCatalogWarningCloseButton.click();
    return this;
  }

  public ProgramPage clickSearchCatalogWarningTitle() {
    searchCatalogWarningTitle.click();
    return new ProgramPage(driver);
  }

  public DownloadPage clickSearchCatalogWarningContinueDLButton() {
    searchCatalogWarningContinueDLButton.click();
    return new DownloadPage(driver);
  }

  public String getAdCpdTitle() {
    return adCpd.getAttribute("title");
  }

  public boolean isCpdAdPresent(String productName) {
    return isElementDisplayed(driver.findElement(
        By.xpath(String.format(AD_CPD_BY_NAME, productName))));
  }

  public boolean isCpcAdPresent(String productName) {
    return isElementDisplayed(driver.findElement(
        By.xpath(String.format(AD_CPC_BY_NAME, productName))));
  }

  public int getNumberResults() {
    try {
      int results =
          driver.findElements(By.xpath("//*[@id='wideSfbl']/div"))
          .size();
      return results;
    } catch (NoSuchElementException e) {
      // No results found, but perfectly normal...
      e.printStackTrace();
      return 0;
    }
  }
}
