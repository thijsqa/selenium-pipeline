package com.softonicweb.container.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Contains methods to interact with the recover password modal.
 *
 * @author ivan.solomonoff
 * @author guillem.hernandez
 */
public class RecoverPasswordEmailSentModal extends AbstractSoftonicWebPage {

  @FindBy(xpath = "//div[@class='msg_ok']")
  private WebElement successfulMessage;
  @FindBy(xpath = "//div[@class='msg_ok']/p/strong")
  private WebElement emailAddress;

  public RecoverPasswordEmailSentModal(WebDriver driver) {
    super(driver);
  }

  public boolean isSuccessfulMessagePresent() {
    return isElementDisplayed(successfulMessage);
  }

  public String getUserEmail() {
    return emailAddress.getText();
  }
}
