package com.softonicweb.container.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.softonicweb.container.seleniumframework.AbstractWebPage;
import com.softonicweb.container.seleniumframework.AbstractWidget;

/**
 * Contains methods to interact with the left menu of the user profile page.
 *
 * @param <T> parent page
 *
 * @author josemaria.delbarco
 */
public class ProfileMenuWidget<T extends AbstractWebPage> extends AbstractWidget<T> {

  // Personal Details
  @FindBy(xpath = "id('userprofile_box')/dd[contains(@class,'edit_details')]/a")
  private WebElement editMyDetailsLink;
  @FindBy(xpath = "id('userprofile_box')//dd[contains(@class,'suscriptions')]/a")
  private WebElement mySubscriptionsLink;
  // Softonic Activity
  @FindBy(xpath = "id('userprofile_box')/li[contains(@class,'recent_activity')]/a")
  private WebElement recentActivityLink;
  @FindBy(xpath = "id('userprofile_box')/li[contains(@class,'comments')]/a")
  private WebElement reviewsLink;
  @FindBy(xpath = "id('userprofile_box')/li[contains(@class,'favorites')]/a")
  private WebElement favoriteSoftwareLink;
  @FindBy(xpath = "id('userprofile_box')/li[contains(@class,'friends')]/a")
  private WebElement friendsLink;

  public ProfileMenuWidget(T page) {
    super(page);
  }

  public MyDetailsPage clickEditMyDetails() {
    editMyDetailsLink.click();
    return new MyDetailsPage(driver);
  }

  public ProfileMenuWidget<T> clickRecentActivity() {
    recentActivityLink.click();
    return this;
  }

  public ProfileMenuWidget<T> clickUserReviews() {
    reviewsLink.click();
    return this;
  }

  public ProfileMenuWidget<T> clickFavoriteSoftware() {
    favoriteSoftwareLink.click();
    return this;
  }

  public ProfileMenuWidget<T> clickFriends() {
    friendsLink.click();
    return this;
  }

  public void clickMySubscriptions() {
    mySubscriptionsLink.click();
  }
}
