package com.softonicweb.container.pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.softonicweb.container.pages.AfsWidget;

/**
 * Contains methods to interact with the Softonic Download search no results page
 * <p>eg. http://www.softonic.com/s/vlc
 *
 * @author guillem.hernandez
 *
 */
public class DownloadSearchNoResultsPage extends DownloadSearchPage {

  @FindBy(id = "title_button_0")
  private WebElement firstResultTitle;

  public DownloadSearchNoResultsPage(WebDriver driver) {
    super(driver);
  }

  public String getFirstResultName() {
    return firstResultTitle.getText();
  }

  @Override
  public AfsWidget<DownloadSearchPage> getCsaWidget() {
    return new AfsWidget<DownloadSearchPage>(this);
  }
}
