package com.softonicweb.container.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Contains methods to interact with the recover password modal window on user profile.
 *
 * @author ivan.solomonoff
 * @author guillem.hernandez
 */
public class RecoverPasswordModal extends AbstractSoftonicWebPage {

  @FindBy(xpath = "//input[contains(@name,'username')]")
  private WebElement emailTextbox;
  @FindBy(xpath = "//input[contains(@class,'gigya-input-submit')]")
  private WebElement sendButton;

  public RecoverPasswordModal(WebDriver driver) {
    super(driver);
  }

  public RecoverPasswordModal enterEmail(String email) {
    sendKeysThreadSafe(emailTextbox, email);
    return this;
  }

  public RecoverPasswordEmailSentModal clickSend() {
    sendButton.click();
    return new RecoverPasswordEmailSentModal(driver);
  }
}
