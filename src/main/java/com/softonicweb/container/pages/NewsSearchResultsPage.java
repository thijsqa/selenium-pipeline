package com.softonicweb.container.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Contains methods to interact with the Softonic news search page in any Softonic
 * plaftorm.
 *
 * @author guillem.hernandez
 */
public class NewsSearchResultsPage extends AbstractSoftonicWebPage {

  @FindBy(xpath = "id('content')/div/section/div/article[1]")
  private WebElement firstNewsResultArticleLink;

  @FindBy(className = "title-page")
  private WebElement title;

  public NewsSearchResultsPage(WebDriver driver) {
    super(driver);
  }

  public boolean isfirstResultNewsArticleLinkPresent() {
    return isElementDisplayed(firstNewsResultArticleLink);
  }

  public boolean isTitlePresent() {
    return isElementDisplayed(title);
  }

  public String getTitleText() {
    return title.getText().trim();
  }
}
