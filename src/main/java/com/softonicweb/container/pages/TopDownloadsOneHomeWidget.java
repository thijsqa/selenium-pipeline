package com.softonicweb.container.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.softonicweb.container.seleniumframework.AbstractWebPage;
import com.softonicweb.container.seleniumframework.AbstractWidget;

/**
 * Methods to interact with softonic Onehome page.
 *
 * @param <T> parent page
 *
 * @author guillem.hernandez
 * @author antonio.martin
 */
public class TopDownloadsOneHomeWidget<T extends AbstractWebPage> extends AbstractWidget<T> {

  private static final String DOWNLOAD_BUTTON =
      "id('categories_front_page')/ul/li[%s]/dl[@class='program_data']/dd/a";

  @FindBy(id = "categories_front_page")
  private WebElement topDowloadTitle;

  @FindBy(id = "categories_front_page")
  private List<WebElement> topDowloadButtons;

  public TopDownloadsOneHomeWidget(T page) {
    super(page);
  }

  public boolean isTopDownloadsButtonPresent(int index) {
    return isElementDisplayed(driver.findElement(By.xpath(String.format(DOWNLOAD_BUTTON, index))));
  }

  public boolean isAutomaticTopDownloadsPresent() {
    return isElementDisplayed(topDowloadTitle);
  }

  public boolean areTopDownloadButtonsPresent() {
    boolean result = topDowloadButtons.size() > 0;
    for (WebElement button : topDowloadButtons) {
      result = result && button.isDisplayed();
    }
    return result;
  }
}
