package com.softonicweb.container.pages;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Locale;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.softonicweb.container.framework.TestLogger;
import com.softonicweb.container.framework.UrlsUtil;
import com.softonicweb.container.pages.SocialMediaPopUpExternalModal;
import com.softonicweb.container.pages.ProgramPage;

/**
 * Contains methods to interact with Softonic Download Program Page.
 * <p>
 * eg. http://google-chrome.en.softonic.com/mac/download
 *
 * @author josemaria.delbarco
 * @author james.oram
 * @author guillem.hernandez
 * @author thijs.vandewynckel
 */
public class DownloadPage extends AbstractSoftonicWebPage {

  private static final String SCREENSHOT_ITEMS =
      "//ul/li/div[@class='app-download-promoted-gallery__image']/img";

  // Non-Sd Download Module (non SD)
  @FindBy(xpath = ".//*[contains(@class,'box-download')]")
  private WebElement downloadModule;

  @FindBy(css = "[data-auto=download-button]")
  private WebElement freeDownloadButton;

  @FindBy(xpath = "//a[contains(@class,'button-main-download-xl')]")
  private WebElement downloadMainButton;

  @FindBy(xpath = "//a[contains(@class,'btn--free-download')]")
  private WebElement noodleFreeDownloadMainButton;

  @FindBy(xpath = "//div[contains(@class,'app-download-promoted-btn')]/a")
  private WebElement nativeDownloadButton;

  @FindBy(xpath = "//div[contains(@class,'app-download-info__container')]/a")
  private WebElement noodleDownloadMainButton;

  @FindBy(id = "download-button-sd")
  private WebElement downloadSecurityAlertButton;

  // SD Download Module
  @FindBy(id = "ud_download_box")
  private WebElement sdDownloadModule;

  @FindBy(id = "download-button-alternative")
  private WebElement alternativeDownloadLink;

  @FindBy(xpath = "//p[contains(@class,'app-download-info__alt-message')]/a")
  private WebElement noodleAlternativeDownloadLink;

  @FindBy(xpath = "//a[contains(@class,'btn--alternative')]")
  private WebElement noodleSocialAlternativeDownloadLink;

  @FindBy(id = "social-download-button-alternative")
  private WebElement alternativeDownloadButton;

  // Download failure module
  @FindBy(id = "download_message_failure")
  private WebElement downloadFailureMessageModule;

  @FindBy(xpath = "//div[@class='file_security_download_alternative']")
  private WebElement alternativeSecureProgramsDownloadModule;

  // Current Version of the program. This is needed for Control Panel Update
  // test
  @FindBy(xpath = "id('program_info')/dl/dd[4]")
  private WebElement programVersion;

  @FindBy(xpath = "//a[contains(@class,'button-main-buy-xl')]")
  private WebElement buyButton;

  @FindBy(xpath = "//*[@class='warning-external-download']")
  private WebElement warningMessage;

  @FindBy(xpath = "//section[@class='app-download-instructions']/ul/li[1]")
  private WebElement noodleWarningMessage;

  @FindBy(xpath = "id('box-download')/div/p")
  private WebElement alternativeDownloadMessage;

  @FindBy(xpath = "//*[@class='app-download-info__alt-message']")
  private WebElement alternativeNoodleDownloadMessage;

  @FindBy(xpath = "//div[@class='modal-warning__content']")
  private WebElement catalogWarning;

  @FindBy(xpath = "//h3[contains(@class,'card-app-alternative__title')]/a")
  private WebElement catalogWarningAlternativeTitle;

  @FindBy(xpath = "//a[contains(@class,'js-modal-window-close')]")
  private WebElement noodleCatalogWarningCloseButton;

  @FindBy(xpath = "//div[@class='modal-warning__content']/a")
  private WebElement noodleCatalogWarningContinueButton;

  @FindBy(id = "facebook-login")
  private WebElement fbLoginButton;

  @FindBy(id = "google-login")
  private WebElement googlePlusLoginButton;

  @FindBy(xpath = "//ul[contains(@class,'modal-social-login__buttons')]/li[2]/a")
  private WebElement popupGooglePlusLoginButton;

  @FindBy(id = "gigya-complete-registration-screen")
  private WebElement confirmRegisterModal;

  @FindBy(id = "gigya-textbox-email")
  private WebElement emailInputField;

  @FindBy(xpath = "//*[@id='gigya-profile-form']/div[3]/a")
  private WebElement cancelRegisterButton;

  @FindBy(xpath = "//*[@id='gigya-profile-form']/div[2]/div[1]/input")
  private WebElement confirmRegisterButton;

  @FindBy(xpath = "//input[contains(@id,'gigya-multiChoice-1')]")
  private WebElement newsletterOptOutCheckbox;

  @FindBy(xpath = "//ul/li[contains(@class,'list-tags__item')]/a")
  private WebElement platformLInks;

  @FindBy(xpath = "//section[contains(@id,'new-apps')]")
  private WebElement newAppsModule;

  @FindBy(css = "[data-auto=search-input]")
  private WebElement searchInput;

  @FindBy(xpath = "//div[@class='card-app-alternative__more-info']/a")
  private WebElement catalogWarningAlternativeDLButton;

  private static final TestLogger LOGGER = TestLogger.getInstance();

  public DownloadPage(WebDriver driver) {
    super(driver);
  }

  public boolean isSearchBarPresent() {
    return isElementDisplayed(searchInput);
  }

  public DownloadPage enterQuery(String query) {
    sendKeysThreadSafe(searchInput, query);
    searchInput.submit();
    return new DownloadPage(driver);
  }

  public boolean isDownloadPage() {
    return isElementDisplayed(downloadModule);
  }

  public boolean isSocialDownload() {
    try {
      LOGGER.logComment("Looking for Alternative download button in case there is Social Login");
      driver.findElement(By.xpath("//a[contains(@class,'btn--alternative')]"));
      return true;
    } catch (NoSuchElementException e) {
      LOGGER.logComment("Alternative download button not found");
      return false;
    }
  }

  public String getFreeDownloadUrl() {
    return freeDownloadButton.getAttribute("href");
  }

  public PostDownloadPage clickFreeDownload() {
    scrollIntoView(freeDownloadButton);
    freeDownloadButton.click();
    return new PostDownloadPage(driver);
  }

  public PostDownloadPage clickFreeDownloadButton() {
    freeDownloadButton.click();
    return new PostDownloadPage(driver);
  }

  public boolean isFreeDowloadButtonPresent() {
    return isElementDisplayed(freeDownloadButton);
  }

  public PostDownloadPage clickSecurityAlertDownload() {
    scrollIntoView(downloadSecurityAlertButton);
    downloadSecurityAlertButton.click();
    return new PostDownloadPage(driver);
  }

  public String getSecurityAlertUrl() {
    return downloadSecurityAlertButton.getAttribute("href");
  }

  public String getAlternativeDownloadUrl() {
    return alternativeDownloadLink.getAttribute("href");
  }

  public String getNoodleAlternativeDownloadUrl() {
    String downloadUrl = noodleAlternativeDownloadLink.getAttribute("data-download");
    String keyword = "downloadUrl\":\"";
    String[] strArray = downloadUrl.split(keyword);
    String trimmedDownloadUrl = strArray[1];
    String endPart = "\"";
    String[] downloadUrlArray = trimmedDownloadUrl.split(endPart);
    String fullDownloadUrl = downloadUrlArray[0];
    return fullDownloadUrl.trim();
  }

  public WebElement getAlternativeSocialDownloadLink() {
    return alternativeDownloadButton;
  }

  public PostDownloadPage clickAlternativeDownload() {
    scrollIntoView(alternativeDownloadLink);
    alternativeDownloadLink.click();
    return new PostDownloadPage(driver);
  }

  public PostDownloadPage clickNoodleAlternativeDownload() {
    scrollIntoView(noodleAlternativeDownloadLink);
    noodleAlternativeDownloadLink.click();
    return new PostDownloadPage(driver);
  }

  public PostDownloadPage clickNoodleSocialAlternativeDownload() {
    scrollIntoView(noodleSocialAlternativeDownloadLink);
    noodleSocialAlternativeDownloadLink.click();
    return new PostDownloadPage(driver);
  }

  public String getSocialAlternativeDownloadUrl() {
    return noodleSocialAlternativeDownloadLink.getAttribute("href");
  }

  public void clickAuthorDownload() {
    scrollIntoView(downloadMainButton);
    downloadMainButton.click();
  }

  public void clickNoodleAuthorDownload() {
    scrollIntoView(noodleDownloadMainButton);
    noodleDownloadMainButton.click();
  }

  public void clickFreeNoodleDownload() {
    scrollIntoView(noodleFreeDownloadMainButton);
    noodleFreeDownloadMainButton.click();
  }

  public PostDownloadPage clickDownload() {
    clickAuthorDownload();
    return new PostDownloadPage(driver);
  }

  public PostDownloadPage clickNoodleDownload() {
    clickNoodleAuthorDownload();
    return new PostDownloadPage(driver);
  }

  public PostDownloadPage clickNoodleFreeDownload() {
    clickFreeNoodleDownload();
    return new PostDownloadPage(driver);
  }

  public PostDownloadPage clickNativeDownload() {
    nativeDownloadButton.click();
    return new PostDownloadPage(driver);
  }

  public String getDownloadUrl() {
    return downloadMainButton.getAttribute("href");
  }

  public String getNoodleDownloadUrl() {
    String downloadUrl = noodleDownloadMainButton.getAttribute("data-download");
    String keyword = "downloadUrl\":\"";
    String[] strArray = downloadUrl.split(keyword);
    String trimmedDownloadUrl = strArray[1];
    String endPart = "\"";
    String[] downloadUrlArray = trimmedDownloadUrl.split(endPart);
    String fullDownloadUrl = downloadUrlArray[0];
    return fullDownloadUrl.trim();
  }

  public String getDownloadType() {
    String downloadUrl = noodleDownloadMainButton.getAttribute("data-download");
    String token = "token=";
    String[] strArray = downloadUrl.split(token);
    String trimmedTokenValue = strArray[1];
    String endPart = "\"";
    String[] tokenArray = trimmedTokenValue.split(endPart);
    String fullToken = tokenArray[0];
    String[] splitToken = fullToken.split("\\.");
    String base64EncodedBody = splitToken[1];
    String body =
        new String(Base64.getMimeDecoder().decode(base64EncodedBody), StandardCharsets.UTF_8);
    String key = "downloadType\":\"";
    String[] stringArray = body.split(key);
    String trimmedDownloadType = stringArray[1];
    String endChar = "\"";
    String[] downloadTypeArray = trimmedDownloadType.split(endChar);
    String downloadType = downloadTypeArray[0];
    return downloadType.trim();
  }

  public PostDownloadPage clickDownloadFormerVersionButton() {
    // downloadMainButton.click();
    // As this link opens a new window and IE webdriver on grid crashes
    // sometimes we do this...
    driver.get(downloadMainButton.getAttribute("href"));
    return new PostDownloadPage(driver);
  }

  public boolean hasPassedSecuritySealVirusCheck() {
    return sdDownloadModule.getAttribute("class").contains("virus_free");
  }

  public boolean isCatalogWarningAlternativeDLButtonPresent() {
    return isElementDisplayed(catalogWarningAlternativeDLButton);
  }

  public DownloadPage clickCatalogWarningAlternativeDLButton() {
    catalogWarningAlternativeDLButton.click();
    return this;
  }

  public boolean isFreeDownloadPage() {
    return isElementDisplayed(freeDownloadButton);
  }

  public boolean isNonSdExternalDownloadPage() {
    return isFreeDownloadPage() && UrlsUtil.isSoftonicWebUrl(getFreeDownloadUrl());
  }

  public boolean isNonSdInternalDownloadPage() {
    return isFreeDownloadPage() && UrlsUtil.isSoftonicWebUrl(getFreeDownloadUrl());
  }

  public boolean isSdDownloadPage() {
    return isElementDisplayed(sdDownloadModule);
  }

  public boolean isSsWarningDownloadPage() {
    return isElementDisplayed(alternativeSecureProgramsDownloadModule);
  }

  public boolean isMobileNameInDownloadText(String mobileName) {
    return isElementDisplayed(driver.findElement(By
        .xpath("id('content_2col_right_thin')//*[contains(text(),'" + mobileName + "')]")));
  }

  public boolean isFeedbackMessagePresentOnDownloadFailure() {
    return isElementDisplayed(downloadFailureMessageModule);
  }

  public boolean isAlternativeLinkPresent() {
    return isElementDisplayed(alternativeDownloadLink);
  }

  public boolean isNativeDownloadButtonPresent() {
    return isElementDisplayed(nativeDownloadButton);
  }

  public boolean isNoodleAlternativeLinkPresent() {
    return isElementDisplayed(noodleAlternativeDownloadLink);
  }

  public boolean isNoodleSocialAlternativeLinkPresent() {
    return isElementDisplayed(noodleSocialAlternativeDownloadLink);
  }

  public boolean isAlternativeDownloadButtonPresent() {
    return isElementDisplayed(alternativeDownloadButton);
  }

  public boolean isSecureAlternativeProgramsModulePresent() {
    return isElementDisplayed(alternativeSecureProgramsDownloadModule);
  }

  public String getProgramVersionString() {
    return programVersion.getText();
  }

  public ArrayList<String> getAfcAds() {
    ArrayList<String> results = new ArrayList<String>();
    List<WebElement> ads = driver.findElements(
        By.xpath(".//div[contains(@class,'google_ad_content afc')]"));
    for (WebElement ad : ads) {
      // It's not possible to interact with hidden elements
      String resultAd = new String();
      resultAd = getInnerHTML(ad);
      results.add(resultAd);
    }
    return results;
  }

  public boolean isBuyButtonPresent() {
    return isElementDisplayed(buyButton);
  }

  public boolean isWarningMessagePresent() {
    return isElementDisplayed(warningMessage);
  }

  public boolean isNoodleWarningMessagePresent() {
    return isElementDisplayed(noodleWarningMessage);
  }

  public boolean isDownloadButtonPresent() {
    return isElementDisplayed(downloadMainButton);
  }

  public boolean isNoodleDownloadButtonPresent() {
    return isElementDisplayed(noodleDownloadMainButton);
  }

  public boolean isNoodleFreeDownloadButtonPresent() {
    return isElementDisplayed(noodleFreeDownloadMainButton);
  }

  public boolean isDPCatalogWarningPresent() {
    return isElementDisplayed(catalogWarning);
  }

  public boolean isCatalogWarningAlternativeTitlePresent() {
    return isElementDisplayed(catalogWarningAlternativeTitle);
  }

  public boolean isAltLinkPresent() {
    return isElementDisplayed(alternativeDownloadLink);
  }

  public ProgramPage clickCatalogWarningAlternativeTitle() {
    catalogWarningAlternativeTitle.click();
    return new ProgramPage(driver);
  }

  public DownloadPage clickNoodleCatalogWarningCloseButton() {
    noodleCatalogWarningCloseButton.click();
    return this;
  }

  public DownloadPage clickNoodleCatalogWarningContinueDownload() {
    noodleCatalogWarningContinueButton.click();
    return this;
  }

  public boolean isFBLoginButtonPresent() {
    return isElementDisplayed(fbLoginButton);
  }

  public boolean arePlatformLinksPresent() {
    return isElementDisplayed(platformLInks);
  }

  public SocialMediaPopUpExternalModal clickFBLoginButton() {
    fbLoginButton.click();
    return new SocialMediaPopUpExternalModal(driver);
  }

  public boolean isGooglePlusLoginButtonPresent() {
    return isElementDisplayed(googlePlusLoginButton);
  }

  public SocialMediaPopUpExternalModal clickGooglePlusLoginButton() {
    wait.until(ExpectedConditions.elementToBeClickable(googlePlusLoginButton));
    googlePlusLoginButton.click();
    return new SocialMediaPopUpExternalModal(driver);
  }

  public SocialMediaPopUpExternalModal clickPopupGooglePlusLoginButton() {
    wait.until(ExpectedConditions.elementToBeClickable(popupGooglePlusLoginButton));
    popupGooglePlusLoginButton.click();
    return new SocialMediaPopUpExternalModal(driver);
  }

  public boolean isConfirmRegisterModalPresent() {
    return isElementDisplayed(confirmRegisterModal);
  }

  public boolean isEmailInputFieldPresent() {
    wait.until(ExpectedConditions.elementToBeClickable(emailInputField));
    return isElementDisplayed(emailInputField);
  }

  public boolean isSocialDownloadModalPresent() {
    try {
      driver.findElement(By.id("social-down-flow"));
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  @Override
  public boolean isNewAppsModulePresent() {
    return isElementDisplayed(newAppsModule);
  }

  public boolean isNoodleSocialDownloadModalPresent() {
    try {
      driver.findElement(By.xpath("//div[contains(@class,'app-download-social-login')]"));
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  public PostDownloadPage clickCancelRegister() {
    wait.until(ExpectedConditions.elementToBeClickable(cancelRegisterButton));
    cancelRegisterButton.click();
    return new PostDownloadPage(driver);
  }

  public PostDownloadPage clickConfirmRegister() {
    confirmRegisterButton.click();
    return new PostDownloadPage(driver);
  }

  public DownloadPage enterEmail(String email) {
    wait.forElementPresent(emailInputField);
    sendKeysThreadSafe(emailInputField, email);
    return this;
  }

  @Override
  public ProgramPage checkOptOutNewsletter() {
    newsletterOptOutCheckbox.click();
    return new ProgramPage(driver);
  }

  public int getScreenshotCount() {
    return driver.findElements(By.xpath(SCREENSHOT_ITEMS)).size();
  }

  public boolean isWarningMessageCorrect(String instance) {
    String expectedMessage;
    switch (instance.toUpperCase(Locale.ENGLISH)) {
    case "ES":
      expectedMessage = "Te redirigiremos a un sitio web externo para completar la descarga.";
      break;
    case "EN":
      expectedMessage = "You will be redirected to an external website to complete the download.";
      break;
    case "FR":
      expectedMessage =
          "Vous allez \u00eatre redirig\u00e9 vers un site Web externe pour terminer votre "
              + "t\u00e9l\u00e9chargement.";
      break;
    case "DE":
      expectedMessage =
          "Sie werden auf eine externe Website weitergeleitet, um den Download abzuschlie\u00dfen.";
      break;
    case "IT":
      expectedMessage = "Sarai reindirizzato a un sito esterno per completare il download.";
      break;
    case "BR":
      expectedMessage =
          "Voc\u00ea ser\u00e1 redirecionado para um site externo para concluir o download.";
      break;
    case "PL":
      expectedMessage =
          "W celu uko\u0144czenia pobierania nast\u0105pi przekierowanie do zewn\u0119trznej "
              + "witryny.";
      break;
    case "NL":
      expectedMessage =
          "Je wordt doorgestuurd naar een externe website om de download te voltooien.";
      break;
    case "JP":
      expectedMessage =
          "\u30c0\u30a6\u30f3\u30ed\u30fc\u30c9\u306e\u305f\u3081\u3001\u5916\u90e8Web\u30b5"
              + "\u30a4\u30c8\u3078\u79fb\u52d5\u3057\u307e\u3059\u3002";
      break;
    default:
      throw new RuntimeException("Instance " + instance.toString() + " not accepted");
    }
    String actualMessage = warningMessage.getText().trim();
    return actualMessage.equalsIgnoreCase(expectedMessage);
  }

  public boolean isNoodleWarningMessageCorrect(String instance) {
    String expectedMessage;
    switch (instance.toUpperCase(Locale.ENGLISH)) {
    case "ES":
      expectedMessage = "La descarga se realizará desde un sitio web externo.";
      break;
    case "EN":
      expectedMessage = "You will be redirected to an external website to complete the download.";
      break;
    case "FR":
      expectedMessage =
          "Vous allez être redirigé vers un site Web externe pour compléter le téléchargement.";
      break;
    case "DE":
      expectedMessage = "Sie werden für den Download zu einer externen Website weitergeleitet.";
      break;
    case "IT":
      expectedMessage = "Verrai reindirizzato a un sito esterno per completare il download.";
      break;
    case "BR":
      expectedMessage = "Você será redirecionado para um site externo para concluir o download.";
      break;
    case "PL":
      expectedMessage = "W celu ukończenia pobierania przekierujemy Cię do witryny zewnętrznej.";
      break;
    case "NL":
      expectedMessage =
          "Je wordt doorgestuurd naar een externe website om de download te voltooien.";
      break;
    case "JP":
      expectedMessage = "ダウンロードを完了するため、外部Webサイトへリダイレクトされます.";
      break;
    default:
      throw new RuntimeException("Instance " + instance.toString() + " not accepted");
    }
    String actualMessage = noodleWarningMessage.getText().trim();
    LOGGER.logComment("Actual message is: " + actualMessage);
    return actualMessage.equalsIgnoreCase(expectedMessage);
  }

  public boolean isAlternativeDownloadTextCorrect(String instance) {
    String regexMessage = addExpectedMessage(instance);
    String actualMessage = alternativeDownloadMessage.getText().trim();
    LOGGER.logComment("Actual message is: " + actualMessage);
    return actualMessage.matches(regexMessage);
  }

  public boolean isAlternativeNoodleDownloadTextCorrect(String instance) {
    String regexMessage = addExpectedMessage(instance);
    String actualMessage = alternativeNoodleDownloadMessage.getText().trim();
    LOGGER.logComment("Actual message is: " + actualMessage);
    return actualMessage.matches(regexMessage);
  }

  public String addExpectedMessage(String instance) {
    switch (instance.toUpperCase(Locale.ENGLISH)) {
      case "ES":
        return "Descarga alternativa de .+ desde un servidor externo";
      case "EN":
        return "Alternative .+ download from external server \\(availability not guaranteed\\)";
      case "DE":
        return ".+ - Alternativer Download von einem externen "
            + "Server \\(Erreichbarkeit nicht garantiert\\)";
      case "FR":
        return ".+: T\u00e9l\u00e9chargement alternatif depuis un serveur externe "
            + "\\(disponibilit\u00e9 non garantie\\)";
      case "IT":
        return "Download alternativo di .+ da server esterni "
            + "\\(disponibilit\u00e0 non garantita\\)";
      case "BR":
        return "Download alternativo do .+ atrav\u00e9s de um servidor externo "
            + "\\(disponibilidade n\u00e3o garantida\\)";
      case "PL":
        return "Alternatywne pobieranie .+ z serwera zewn\u0119trznego "
            + "\\(bez gwarancji dost\u0119pno\u015bci\\)";
      case "NL":
        return ".+ alternatieve download via een externe server "
            + "\\(beschikbaarheid niet gegarandeerd\\)";
      case "JP":
        return ".+\u306e\u4f5c\u8005\u30b5\u30a4\u30c8\u304b\u3089\u30c0\u30a6"
            + "\u30f3\u30ed\u30fc\u30c9 \u30bd\u30d5\u30c8\u30cb\u30c3\u30af\u30c0"
            + "\u30a6\u30f3\u30ed\u30fc\u30c0\u30fc\u3092\u4f7f\u7528\u3057\u307e"
            + "\u305b\u3093\u3002\u30bd\u30d5\u30c8\u30cb\u30c3\u30af\u306f\u30a6"
            + "\u30a4\u30eb\u30b9\u5bfe\u7b56\u3068\u30ea\u30f3\u30af\u306e\u6709"
            + "\u52b9\u6027\u306e\u4fdd\u8a3c\u3092\u884c\u3046\u3053\u3068\u304c"
            + "\u3067\u304d\u307e\u305b\u3093\u3002";
      default:
        throw new RuntimeException("No data for instance: " + instance.toString());
    }
  }
}
