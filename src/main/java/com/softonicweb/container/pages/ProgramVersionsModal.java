package com.softonicweb.container.pages;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Contains methods to interact with the different program versions modal.
 *
 * @author guillem.hernandez
 */
public class ProgramVersionsModal extends AbstractSoftonicWebPage {

  static final String VERSION = "Version";
  static final String LICENSE = "License";
  static final String LANGUAGE = "Language";
  static final String OS = "OS";
  static final String DOWNLOAD = "Download";

  @FindBy(xpath = ".//*[@id='all_versions_window']/table/tbody/tr[2]/td[5]/a")
  private WebElement secondDownloadButton;
  @FindBy(xpath = "id('all_versions_window')/table/tbody/tr")
  private List<WebElement> versionsTableRows;
  @FindBy(xpath = "id('all_versions_window')/table/thead/tr")
  private WebElement versionsTableHeader;
  @FindBy(id = "all_versions_window")
  private WebElement modalWindowContainer;

  public ProgramVersionsModal(WebDriver driver) {
    super(driver);
  }

  public String getDownloadFormerVersionUrl() {
    return secondDownloadButton.getAttribute("href").toString();
  }

  public DownloadPage clickPreviousVersionDownloadButton() {
    scrollIntoView(secondDownloadButton);
    secondDownloadButton.click();
    return new DownloadPage(driver);
  }

  public List<Map<String, String>> getTableInfo() {

    List<Map<String, String>> table = new ArrayList<Map<String, String>>();
    Map<String, String> headerMap = new LinkedHashMap<String, String>();
    headerMap.put(VERSION, versionsTableHeader
        .findElement(By.xpath(".//th[@class='name first']"))
        .getText());
    headerMap.put(LICENSE, versionsTableHeader.findElement(By.className("license")).getText());
    headerMap.put(LANGUAGE, versionsTableHeader.findElement(By.className("language")).getText());
    headerMap.put(OS, versionsTableHeader.findElement(By.className("os")).getText());
    headerMap.put(DOWNLOAD, versionsTableHeader.findElement(By.className("download")).getText());
    table.add(headerMap);
    // After consulted with PO, Language column is not mandatory so we don't
    // check it.
    for (WebElement row : versionsTableRows) {
      Map<String, String> rowMap = new LinkedHashMap<String, String>();
      rowMap.put(VERSION, row.findElement(By.xpath(".//td[@class='name first']")).getText());
      rowMap.put(LICENSE, row.findElement(By.className("license")).getText());
      rowMap.put(OS, row.findElement(By.className("os")).getText());
      table.add(rowMap);
    }
    return table;
  }

  public boolean isPresent() {
    return isElementDisplayed(modalWindowContainer);
  }
}
