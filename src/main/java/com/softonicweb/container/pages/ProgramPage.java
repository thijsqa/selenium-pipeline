package com.softonicweb.container.pages;

import java.util.List;
import java.util.UUID;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Contains methods to interact with the Softonic Program Page Method from old
 * and new program page.
 *
 * @author guillem.hernandez
 * @author antonio.martin
 */
public class ProgramPage extends AbstractSoftonicWebPage {

  // Octopus
  private static final String ALTERNATIVE_BY_INDEX = "id('alternative_%s')/a[2]";

  private static final String COMPLEMENT_BY_INDEX = "id('complement_%s')/a[2]";

  private static final String OCTOPUS_ALTERNATIVE_MORE_INFO_FORMAT =
      "id('alternative_%s')/a[contains(@class,'octopus-more-info')]";

  private static final String OCTOPUS_ALTERNATIVE_DOWNLOAD_BUTTON_FORMAT =
      "id('extra_info_alternative_%s')/a";

  private static final String OCTOPUS_COMPLEMENTS_MORE_INFO_FORMAT =
      "id('complement_%s')/a[contains(@class,'octopus-more-info')]";

  private static final String OCTOPUS_COMPLEMENT_DOWNLOAD_BUTTON_FORMAT =
      "id('extra_info_complement_%s')/a";

  // Bottom Media Gallery
  private static final String BOTTOM_MEDIA_GALLERY_IMG_FORMAT =
      "id('multimedia_gallery')/ul/li[%s]/a/img";
  private static final String BOTTOM_MEDIA_GALLERY_LINK_FORMAT =
      "id('multimedia_gallery')/ul/li[%s]/a";

  private static final long REDIRECTION_WAIT = 5000; // ms

  private static final int NEWSLETTER_TIMEOUT = 2000;

  @FindBy(id = "show_banner_leaderboard")
  private WebElement topLeaderBoardBanner;

  @FindBy(id = "show_banner_bottom_leaderboard")
  private WebElement bottomLeaderBoardBanner;

  @FindBy(id = "pathbar")
  private WebElement breadCumb;

  @FindBy(xpath = "//section[contains(@id,'app-softonic-review')]")
  private WebElement companyReview;

  @FindBy(xpath = "//div[contains(@class,'app-specs')]")
  private WebElement programSpecs;

  @FindBy(xpath = "//div/h4[@class='title-module-answers']")
  private WebElement questions;

  @FindBy(id = "octopus")
  private WebElement octopus;

  @FindBy(css = "[data-auto=app-cover] a")
  private WebElement mediaGallery;

  @FindBy(id = "content-tag-cloud")
  private WebElement fileCloud;

  @FindBy(id = "languages_reviews")
  private WebElement languagesReviews;

  @FindBy(id = "read_more")
  private WebElement readMore;

  @FindBy(xpath = "//[@id='file_title']/a/strong")
  private WebElement appTitleAnchor;

  @FindBy(xpath = "id('program_info')/dl/dd[4]")
  private WebElement programVersion;

  @FindBy(xpath = "//a[contains(@class,'button-main-download-xl')]")
  private WebElement downloadButton;

  @FindBy(css = "[data-auto=download-button]")
  private WebElement noodleDownloadButton;

  @FindBy(xpath = "//a[contains(@class,'button-main-web-xl')]")
  private WebElement accessButton;

  @FindBy(css = "[data-auto=download-button]")
  private WebElement accessNoodleButton;

  @FindBy(xpath = "id('author_review')/div[1]/h2")
  private WebElement shortText;

  @FindBy(xpath = "id('review_picture_0')/a/img")
  private WebElement screenshot;

  @FindBy(xpath = "id('author_review')/div[2]/p[1]")
  private WebElement descriptionText;

  @FindBy(xpath = "id('sun')/div/a")
  private WebElement centerDownloadButton;

  @FindBy(xpath = "id('gallery_nav')/a[1]")
  private WebElement bottomGalleryLeftArrow;

  @FindBy(xpath = "id('gallery_nav')/a[2]")
  private WebElement bottomGalleryRightArrow;

  @FindBy(xpath = "//a[contains(@class,'button-main-buy-xl')]")
  private WebElement buyButton;

  @FindBy(xpath = "id('review_nav')/ul/li[2]/a")
  private WebElement opinionButtonProgramPage;

  @FindBy(xpath = "id('review_nav')/ul/li[2]/a")
  private WebElement moreOpinions;

  @FindBy(xpath = "//div[@class='send-compare']/a/strong")
  private WebElement clickOnComparisonButton;

  @FindBy(id = "g_triblock_ads_container")
  private WebElement googleTriblockAds;

  @FindBy(id = "opinion-body-review")
  private WebElement opinionBodyReview;

  @FindBy(xpath = "id('users_opinion')/div/form/button")
  private WebElement opinionButtonOnTab;

  @FindBy(id = "closeBut")
  private WebElement dismissPopup;

  @FindBy(xpath = "id('suggest_instance')/a")
  private WebElement instanceSuggestClose;

  @FindBy(xpath = "//h1[@id='file_title']/img")
  private WebElement appLogo;

  @FindBy(id = "last_version_link")
  private WebElement latestVersionMoreInfobutton;

  @FindBy(xpath = "id('alternatives')/form/ul/li")
  private List<WebElement> octopusAlternatives;

  @FindBy(xpath = "id('complements')/ul/li")
  private List<WebElement> octopusComplements;

  @FindBy(id = "follow-download")
  private WebElement scrollHeader;

  @FindBy(css = "[data-auto=app-user-opinions]")
  private WebElement usersOpinion;

  @FindBy(xpath = "//*[contains(@class,'popup-catalog-warn')]")
  private WebElement catalogWarning;

  @FindBy(xpath = "//*[contains(@class,'warning-modal')]")
  private WebElement noodleCatalogWarning;

  @FindBy(xpath = "//*[contains(@class,'popup-catalog-warn')]/button/i")
  private WebElement catalogWarningCloseButton;

  @FindBy(xpath = "//*[contains(@class,'popup-catalog-warn')]/ul/li[2]/a[2]/strong")
  private WebElement catalogWarningAlternativeDLButton;

  @FindBy(xpath = "id('box-catalog-alternatives')/div[2]/div/a/strong")
  private WebElement noDLButtonAlternativeDLButton;

  @FindBy(xpath = "//*[contains(@class,'card-app-alternative__more-info')]/a")
  private WebElement noDLButtonNoodleAlternativeDLButton;

  @FindBy(xpath = "id('content')/div[2]/div[2]/p")
  private WebElement noDLButtonWarning;

  @FindBy(xpath = "//*[contains(@class, 'app-alternative__message')]")
  private WebElement noDLButtonNoodleWarning;

  @FindBy(xpath = "id('content')/div[2]/div[2]/p[2]")
  private WebElement noDLButtonAlternativesWarning;

  @FindBy(css = "[data-auto=no-download-button] p")
  private WebElement noDLButtonNoodleAlternativesWarning;

  @FindBy(id = "content_info_review")
  private WebElement reviewContent;

  @FindBy(xpath = "//*[@id='Noodle-LiteRegistration-container_content']/div/form/div[1]"
      + "/div[7]/div[1]/input")
  private WebElement newsletterMailTextbox;

  @FindBy(xpath = "//*[@id='Noodle-LiteRegistration-container_content']/div/form/div[1]"
      + "/div[7]/div[2]/input")
  private WebElement newsletterSubscribeButton;

  @FindBy(xpath = "//div[contains(@class,'app-rating__score')]")
  private WebElement fileRanking;

  @FindBy(xpath = "//*[@id='Noodle-LiteRegistration-container_content']/div/div[1]"
      + "/div[@class='subscribe-thank-you']")
  private WebElement confirmationMessage;

  public ProgramPage(WebDriver driver) {
    super(driver);
  }

  public boolean isAffiliationPriceModulePresent() {
    try {
      driver.findElement(By.xpath("//div[contains(@class,'app-offer-details')]"));
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  public boolean isTopLeaderBoardBannerPresent() {
    return isElementDisplayed(topLeaderBoardBanner);
  }

  public boolean isCompanyReviewPresent() {
    return isElementDisplayed(companyReview);
  }

  public boolean isProgramSpecificationPresent() {
    return isElementDisplayed(programSpecs);
  }

  public boolean areQuestionsPresent() {
    return isElementDisplayed(questions);
  }

  public boolean isOctopusPresent() {
    scrollIntoView(octopus);
    return isElementDisplayed(octopus);
  }

  public boolean isMediaGalleryPresent() {
    return isElementDisplayed(mediaGallery);
  }

  public boolean isFileRankingPresent() {
    return isElementDisplayed(fileRanking);
  }

  public boolean isFileCloudPresent() {
    return isElementDisplayed(fileCloud);
  }

  public boolean areLanguageReviewsPresent() {
    return isElementDisplayed(languagesReviews);
  }

  public boolean isCenterDownloadButtonPresent() {
    return isElementDisplayed(centerDownloadButton);
  }

  public boolean isInstanceSuggestPresent() {
    List<WebElement> elements = driver.findElements(By.id("suggest_instance"));
    if (elements.isEmpty()) {
      return false;
    }
    return true;
  }

  public String getCenterDownloadButtonUrl() {
    return centerDownloadButton.getAttribute("href");
  }

  public ProgramPage clickOnMoreOpinionsTab() {
    moreOpinions.click();
    return new ProgramPage(driver);
  }

  public ProgramPage clickOnPlusAlternativeByIndex(int index) {
    driver.findElement(By.xpath(String.format(ALTERNATIVE_BY_INDEX, index))).click();
    return new ProgramPage(driver);
  }

  public ProgramPage clickOnPlusComplementByIndex(int index) {
    driver.findElement(By.xpath(String.format(COMPLEMENT_BY_INDEX, index))).click();
    return new ProgramPage(driver);
  }

  public int getOctopusAlternativesCount() {
    return octopusAlternatives.size();
  }

  public int getOctopusComplementsCount() {
    return octopusComplements.size();
  }

  public int getMediaGalleryElementsCount() {
    return mediaGallery.findElements(By.xpath(".//ul/li")).size();
  }

  public boolean isBottomMediaGalleryItemByIndexVisible(int index) {
    return isElementDisplayed(driver.findElement(By.xpath(String.format(
        BOTTOM_MEDIA_GALLERY_IMG_FORMAT, index))));
  }

  public ProgramPage clickOnBottomMediaGalleryRightArrow() {
    bottomGalleryRightArrow.click();
    return new ProgramPage(driver);
  }

  public boolean isBottomMediaGalleryRightArrowPresent() {
    return isElementDisplayed(bottomGalleryRightArrow);
  }

  public ProgramPage clickOnBottomMediaGalleryLeftArrow() {
    bottomGalleryLeftArrow.click();
    return new ProgramPage(driver);
  }

  public ProgramPage clickOnReadMore() {
    readMore.click();
    return new ProgramPage(driver);
  }

  public boolean isBottomMediaGalleryLeftArrowPresent() {
    return isElementDisplayed(bottomGalleryLeftArrow);
  }

  public boolean isBuyButtonPresent() {
    return isElementDisplayed(buyButton);
  }

  public boolean isDownloadButtonPresent() {
    return isElementDisplayed(downloadButton);
  }

  public boolean isNoodleDownloadButtonPresent() {
    return isElementDisplayed(noodleDownloadButton);
  }

  public boolean isCatalogWarningPresent() {
    return isElementDisplayed(catalogWarning);
  }

  public boolean isNoodleCatalogWarningPresent() {
    return isElementDisplayed(noodleCatalogWarning);
  }

  public ProgramPage clickCatalogWarningCloseButton() {
    catalogWarningCloseButton.click();
    return this;
  }

  public boolean isNoDLButtonAlternativeDLButtonPresent() {
    return isElementDisplayed(noDLButtonAlternativeDLButton);
  }

  public boolean isNoodleNoDLButtonAlternativeDLButtonPresent() {
    return isElementDisplayed(noDLButtonNoodleAlternativeDLButton);
  }

  public boolean isNoDLButtonAlternativesBoxPresent() {
    List<WebElement> elements = driver.findElements(By.id("box-catalog-alternatives"));
    if (elements.isEmpty()) {
      return false;
    }
    return true;
  }

  public boolean isNoodleNoDLButtonAlternativesBoxPresent() {
    List<WebElement> elements =
        driver.findElements(By.xpath("//ul[contains(@class,'app-alternatives__container')]"));
    if (elements.isEmpty()) {
      return false;
    }
    return true;
  }

  public boolean isProgramDownloadButtonPresent() {
    List<WebElement> elements =
        driver.findElements(By.xpath("//a[contains(@class,'button-main-download-xl')]"));
    if (elements.isEmpty()) {
      return false;
    }
    return true;
  }

  public boolean isNoodleProgramDownloadButtonPresent() {
    List<WebElement> elements =
        driver.findElements(By.xpath("//div[contains(@class,'app-download-btn')]"));
    if (elements.isEmpty()) {
      return false;
    }
    return true;
  }

  public boolean isNoDLButtonWarningPresent() {
    return isElementDisplayed(noDLButtonWarning);
  }

  public boolean isNoodleNoDLButtonWarningPresent() {
    return isElementDisplayed(noDLButtonNoodleWarning);
  }

  public boolean isNoDLButtonAlternativesWarningPresent() {
    return isElementDisplayed(noDLButtonAlternativesWarning);
  }

  public boolean isNoodleNoDLButtonAlternativesWarningPresent() {
    return isElementDisplayed(noDLButtonNoodleAlternativesWarning);
  }

  public String getDownloadButtonUrl() {
    return downloadButton.getAttribute("href");
  }

  public DownloadPage clickDownloadButton() {
    downloadButton.click();
    return new DownloadPage(driver);
  }

  public ProgramPage clickNoodleDownloadButton() {
    noodleDownloadButton.click();
    return this;
  }

  public DownloadPage clickCatalogWarningAlternativeDLButton() {
    catalogWarningAlternativeDLButton.click();
    return new DownloadPage(driver);
  }

  public DownloadPage clickNoDLButtonAlternativeDLButton() {
    scrollIntoView(reviewContent);
    noDLButtonAlternativeDLButton.click();
    return new DownloadPage(driver);
  }

  public DownloadPage clickNoodleNoDLButtonAlternativeDLButton() {
    noDLButtonNoodleAlternativeDLButton.click();
    return new DownloadPage(driver);
  }

  public String getShortText() {
    return shortText.getText();
  }

  public int getScreenshotHeight() {
    // When there are no images this will throw a NPE (and screenshot is not
    // null)
    try {
      return screenshot.getSize().getHeight();
    } catch (NullPointerException npe) {
      return 0;
    }
  }

  public final String getDescriptionText() {
    if (!isElementDisplayed(descriptionText)) {
      return "";
    }
    return descriptionText.getText();
  }

  public String getProgramVersionString() {
    return programVersion.getText().substring(0, 1);
  }

  public int getScreenshotWidth() {
    return screenshot.getSize().getWidth();
  }

  public String getAppTitle() {
    return appTitleAnchor.getText();
  }

  public String getOpinionUrl() {
    return opinionButtonProgramPage.getAttribute("href").toString();
  }

  public boolean isComparisonButtonPresent() {
    return isElementDisplayed(clickOnComparisonButton);
  }

  public AlternativesPage clickOnComparisonButton() {
    // Click on the app logo to set the focus on the page. Chrome seems to have problems sometimes
    appLogo.click();

    scrollIntoView(clickOnComparisonButton);
    scrollIntoView(clickOnComparisonButton);
    clickOnComparisonButton.click();
    return new AlternativesPage(driver);
  }

  public boolean isMoreOpinionsPresent() {
    return isElementDisplayed(moreOpinions);
  }

  public UserOpinionPage clickOnMoreOpinions() {
    moreOpinions.click();
    return new UserOpinionPage(driver);
  }

  public ProgramVersionsModal clickOnMoreInfoProgramVersion() {
    programVersion.click();
    return new ProgramVersionsModal(driver);
  }

  public boolean isGoogleTriblockAdsPresent() {
    return isElementDisplayed(googleTriblockAds);
  }

  public void fillOpinionOnProgramPage() {
    sendKeysThreadSafe(opinionBodyReview, "QA Test comment " + UUID.randomUUID().toString()
        + " do not accept this comment, " + "It is just a test that has to be performed,"
        + UUID.randomUUID().toString() + " and it has be performed on each regression");
  }

  public OpinionFirstFormPage clickOpinionButtonOnTab() {
    opinionButtonOnTab.submit();
    return new OpinionFirstFormPage(driver);
  }

  public ProgramPage dismissPopup() {
    dismissPopup.click();
    return this;
  }

  public ProgramPage dismissInstanceSuggest() {
    instanceSuggestClose.click();
    return this;
  }

  public ProgramVersionsModal clickLatestVersionMoreInfoButton() {
    latestVersionMoreInfobutton.click();
    return new ProgramVersionsModal(driver);
  }

  public String getBottomMediaGalleryLinkByIndex(int index) {
    return driver.findElement(By.xpath(String.format(BOTTOM_MEDIA_GALLERY_LINK_FORMAT, index)))
        .getAttribute("href");
  }

  public boolean areOctopusAlternativesPresent() {
    return areElementsPresent(octopusAlternatives);
  }

  public boolean areOctopusComplementsPresent() {
    return areElementsPresent(octopusComplements);
  }

  public ProgramPage clickOnOctopusAlternativeMoreInfoByIndex(int index) {
    driver.findElement(By.xpath(String.format(OCTOPUS_ALTERNATIVE_MORE_INFO_FORMAT, index)))
    .click();
    return this;
  }

  public boolean isOctopusAlternativeDownloadButtonByIndexPresent(int index) {
    return isElementDisplayed(driver.findElement(By.xpath(String.format(
        OCTOPUS_ALTERNATIVE_DOWNLOAD_BUTTON_FORMAT, index))));
  }

  public String getOctopusAlternativeDownloadButtonUrlByIndex(int index) {
    return driver.findElement(
        By.xpath(String.format(OCTOPUS_ALTERNATIVE_DOWNLOAD_BUTTON_FORMAT, index)))
        .getAttribute("href");
  }

  public ProgramPage clickOnOctopusComplementsMoreInfoByIndex(int index) {
    driver.findElement(By.xpath(String.format(OCTOPUS_COMPLEMENTS_MORE_INFO_FORMAT, index)))
    .click();
    return this;
  }

  public boolean isOctopusComplementsDownloadButtonByIndexPresent(int index) {
    return isElementDisplayed(driver.findElement(By.xpath(String.format(
        OCTOPUS_COMPLEMENT_DOWNLOAD_BUTTON_FORMAT, index))));
  }

  public String getOctopusComplementsDownloadButtonUrlByIndex(int index) {
    return driver.findElement(
        By.xpath(String.format(OCTOPUS_COMPLEMENT_DOWNLOAD_BUTTON_FORMAT, index)))
        .getAttribute("href");
  }

  public boolean isScrollHeaderPresent() {
    scrollIntoView(scrollHeader);
    return isElementDisplayed(scrollHeader);
  }

  public boolean isUsersOpinionPresent() {
    return isElementDisplayed(usersOpinion);
  }

  public boolean isBottomLeaderBoardBannerPresent() {
    return isElementDisplayed(bottomLeaderBoardBanner);
  }

  public String getDownloadButtonText() {
    return downloadButton.getText();
  }

  public boolean isAccessButtonPresent() {
    return isElementDisplayed(accessButton);
  }

  public String getAccessButtonUrl() {
    return accessButton.getAttribute("href");
  }

  public ProgramPage clickAccessButton() {
    accessButton.click();
    wait.pause(REDIRECTION_WAIT);
    return this;
  }

  public ProgramPage clickNoodleAccessButton() {
    accessNoodleButton.click();
    wait.pause(REDIRECTION_WAIT);
    return this;
  }

  public String getAccessButtonText() {
    return accessButton.getText();
  }

  public String getNoodleAccessButtonText() {
    return accessNoodleButton.getText();
  }

  public PlatformHomePage subscribeNewsletter(String mail) {
    wait.pause(NEWSLETTER_TIMEOUT);
    sendKeysThreadSafe(newsletterMailTextbox, mail);
    wait.pause(NEWSLETTER_TIMEOUT);
    newsletterSubscribeButton.click();
    return new PlatformHomePage(driver);
  }

  public boolean isNewsletterConfirmationMessagePresent() {
    return isElementDisplayed(confirmationMessage);
  }
}
