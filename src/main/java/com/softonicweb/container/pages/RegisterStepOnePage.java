package com.softonicweb.container.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.softonicweb.container.framework.Localization.Country;

/**
 * Contains methods to interact with the Softonic register page.
 *
 * @author ivan.solomonoff
 * @author guillem.hernandez
 */
public class RegisterStepOnePage extends AbstractSoftonicWebPage {

  @FindBy(xpath = "//input[@name='email']")
  private WebElement emailInput;
  @FindBy(xpath = "//input[@name='profile.firstName']")
  private WebElement firstnameInput;
  @FindBy(xpath = "//input[@name='profile.lastName']")
  private WebElement lastnameInput;
  @FindBy(xpath = "//input[@name='password']")
  private WebElement passwordInput;
  @FindBy(xpath = "//input[@name='passwordRetype']")
  private WebElement passwordRepeatInput;
  @FindBy(id = "nickname")
  private WebElement usernameInput;
  @FindBy(id = "country")
  private WebElement countrySelect;
  @FindBy(xpath = "//input[contains(@class,'gigya-input-submit')]")
  private WebElement createMyAccountButton;
  @FindBy(xpath = "id('already_registered')/a")
  private WebElement signInHereButton;

  public RegisterStepOnePage(WebDriver driver) {
    super(driver);
  }

  public RegisterStepOnePage enterEmail(String email) {
    wait.forElementPresent(emailInput);
    sendKeysThreadSafe(emailInput, email);
    return this;
  }

  public RegisterStepOnePage enterFirstName(String firstname) {
    wait.forElementPresent(firstnameInput);
    sendKeysThreadSafe(firstnameInput, firstname);
    return this;
  }

  public RegisterStepOnePage enterLastName(String lastname) {
    wait.forElementPresent(lastnameInput);
    sendKeysThreadSafe(lastnameInput, lastname);
    return this;
  }

  public RegisterStepOnePage enterPassword(String password) {
    wait.forElementPresent(passwordInput);
    sendKeysThreadSafe(passwordInput, password);
    return this;
  }

  public RegisterStepOnePage enterRepeatPassword(String repeatpassword) {
    wait.forElementPresent(passwordRepeatInput);
    sendKeysThreadSafe(passwordRepeatInput, repeatpassword);
    return this;
  }

  public RegisterStepOnePage enterUsername(String username) {
    wait.forElementPresent(usernameInput);
    sendKeysThreadSafe(usernameInput, username);
    return this;
  }

  public RegisterStepOnePage selectCountryOfResidence() {
    final int countrySelection = 8;
    selectComboBoxByIndex(countrySelect, countrySelection);
    return this;
  }

  public RegisterStepTwoPage submitRegistration(Country country) {
    final int waitTimeForJsButton = 1000;
    wait.forElementPresent(createMyAccountButton);
    // JP and CN are very slow and are not using Unique User WebService.
    // Verify hash function is slower than the other instances.
    if (country.equals(Country.JP)) {
      wait.pause(waitTimeForJsButton);
    }
    createMyAccountButton.click();
    return new RegisterStepTwoPage(driver);
  }

  public RegisterStepTwoPage
      registerUser(String email, String firstname, String lastname, String password,
          String repeatpassword, String day, String month, String year, Country country) {
    enterEmail(email).enterFirstName(firstname).enterLastName(lastname).enterPassword(password)
        .enterRepeatPassword(repeatpassword).enterDay("2").enterMonth("5").enterYear("2002")
        .checkOptInNewsletter();
    return submitRegistration(country);
  }
}
