package com.softonicweb.container.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Contains methods to interact with the Softonic Category Page
 *
 * @author thijs.vandewynckel
 */
public class CategoryPage extends AbstractSoftonicWebPage {

  @FindBy(xpath = "//h1[@class='page-title']")
  private WebElement categoryTitle;
  @FindBy(xpath = "//section[@class='wrapper-essentials']/ul/li")
  private WebElement essentialApps;
  @FindBy(xpath = "//label[@for='filter-license']")
  private WebElement licenseFilter;
  @FindBy(xpath = "//label[@for='filter-subcategory']")
  private WebElement subcategoryFilter;
  @FindBy(xpath = "//label[@for='filter-platform']")
  private WebElement platformFilter;

  public CategoryPage(WebDriver driver) {
    super(driver);
  }

  public boolean isCategoryTitlePresent() {
    return isElementDisplayed(categoryTitle);
  }

  public boolean isEssentialAppsPresent() {
    return isElementDisplayed(essentialApps);
  }

  public boolean isLicenseFilterPresent() {
    return isElementDisplayed(licenseFilter);
  }

  public boolean isSubcategoryFilterPresent() {
    return isElementDisplayed(subcategoryFilter);
  }

  public boolean isPlatformFilterPresent() {
    return isElementDisplayed(platformFilter);
  }
}
