package com.softonicweb.container.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

/**
 * Contains methods to interact with the "Recent activity" page in the user profile.
 *
 * @author ivan.solomonoff
 * @author thijs.vandewynckel
 */
public class ProfileRecentActivityPage extends AbstractSoftonicWebPage {

  @FindBy(id = "my-account_content")
  private WebElement detailsPane;
  @FindBy(xpath = "id('userprofile_box')/dl/dd[1]/a")
  private WebElement editMyDetailsLink;
  @FindBy(css = ".tabs__label[for='tab-subscriptions']")
  private WebElement editMySubscriptionsLink;
  @FindBy(css = ".tabs__label[for='tab-privacy']")
  private WebElement privacySettingsLink;
  @FindBy(xpath = "id('popover-newsletter-cancel')")
  private WebElement facebookPopupCancel;

  public ProfileRecentActivityPage(WebDriver driver) {
    super(driver);
  }

  public boolean isDetailsPanePresent() {
    return isElementDisplayed(detailsPane);
  }

  public MyDetailsPage clickEditMyDetails() {
    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("id('userprofile_box')"
        + "/dl/dd[1]/a")));
    editMyDetailsLink.click();
    return new MyDetailsPage(driver);
  }

  public MyDetailsPage clickPrivacySettings() {
    wait.until(ExpectedConditions.elementToBeClickable(By
        .cssSelector(".tabs__label[for='tab-privacy']")));
    privacySettingsLink.click();
    return new MyDetailsPage(driver);
  }

  public NewsletterSubscriptionPage clickEditMysubscriptions() {
    wait.until(ExpectedConditions.elementToBeClickable(By
        .cssSelector(".tabs__label[for='tab-subscriptions']")));
    editMySubscriptionsLink.click();
    return new NewsletterSubscriptionPage(driver);
  }

  public boolean isFBPopupPresent() {
    List<WebElement> elements = driver.findElements(By.id("popover-newsletter"));
    if (elements.isEmpty()) {
      return false;
    }
    return true;
  }

  public ProfileRecentActivityPage dismissFBPopup() {
    facebookPopupCancel.click();
    return this;
  }
}
