package com.softonicweb.container.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Page Object Model class for the my details page.
 *
 * @author carlos.silva
 * @author thijs.vandewynckel
 */
public class MyDetailsPage extends AbstractMyAccountPage {

  @FindBy(id = "name")
  private WebElement name;
  @FindBy(id = "surname")
  private WebElement surname;
  @FindBy(id = "sex-undefined")
  private WebElement sexUndefined;
  @FindBy(id = "sex-male")
  private WebElement sexMale;
  @FindBy(id = "sex-female")
  private WebElement sexFemale;
  @FindBy(id = "bday")
  private WebElement bday;
  @FindBy(id = "bmonth")
  private WebElement bmonth;
  @FindBy(id = "byear")
  private WebElement byear;
  @FindBy(id = "country")
  private WebElement country;
  @FindBy(id = "province")
  private WebElement province;
  @FindBy(id = "city")
  private WebElement city;
  @FindBy(id = "address")
  private WebElement address;
  @FindBy(id = "postalcode")
  private WebElement postalcode;
  @FindBy(id = "new_email")
  private WebElement newEmail;
  @FindBy(id = "email_confirmation")
  private WebElement emailConfirmation;
  @FindBy(id = "new_username")
  private WebElement newUsername;
  @FindBy(id = "current_password")
  private WebElement currentPassword;
  @FindBy(id = "new_password")
  private WebElement newPassword;
  @FindBy(id = "passwordConfirmation")
  private WebElement passwordConfirmation;
  @FindBy(xpath = "//button[contains(.,'Guardar')]")
  private WebElement submitButton;
  @FindBy(xpath = "id('registration-details')/ol/li[1]/dl/dd/a")
  private WebElement editButton;
  @FindBy(xpath = "id('content_1col')/ul/li[5]/a")
  private WebElement deleteSection;
  @FindBy(xpath = "id('content_1col')/div[1]/div/ul/li[2]/a")
  private WebElement newsletterSection;
  @FindBy(xpath = "id('change-email-form')/button[1]")
  private WebElement confirmButton;
  @FindBy(xpath = "id('errorMessages')")
  private WebElement phpWarning;

  public MyDetailsPage(WebDriver driver) {
    super(driver);
  }

  public MyDetailsPage submit() {
    submitButton.click();
    return this;
  }

  public String getName() {
    return name.getAttribute("value");
  }

  public String getSurname() {
    return surname.getAttribute("value");
  }

  public String getCity() {
    return city.getAttribute("value");
  }

  public String getAddress() {
    return address.getAttribute("value");
  }

  public String getPostalCode() {
    return postalcode.getAttribute("value");
  }

  public MyDetailsPage enterName(String nameStr) {
    name.clear();
    name.sendKeys(nameStr);
    return this;
  }

  public MyDetailsPage enterSurname(String surnameStr) {
    surname.clear();
    surname.sendKeys(surnameStr);
    return this;
  }

  public MyDetailsPage enterCity(String cityStr) {
    city.clear();
    city.sendKeys(cityStr);
    return this;
  }

  public MyDetailsPage enterAddress(String addressStr) {
    address.clear();
    address.sendKeys(addressStr);
    return this;
  }

  public MyDetailsPage enterPostalCode(String postalStr) {
    postalcode.clear();
    postalcode.sendKeys(postalStr);
    return this;
  }

  public MyDetailsPage enterNewEmail(String email) {
    wait.forElementPresent(newEmail);
    sendKeysThreadSafe(newEmail, email);
    return this;
  }

  public MyDetailsPage enterNewEmailConfirmation(String email) {
    wait.forElementPresent(emailConfirmation);
    sendKeysThreadSafe(emailConfirmation, email);
    return this;
  }

  public MyDetailsPage changeEmail(String email) {
    enterNewEmail(email).enterNewEmailConfirmation(email);
    clickConfirmButton();
    return this;
  }

  public MyDetailsPage clickEditButton() {
    editButton.click();
    return new MyDetailsPage(driver);
  }

  public DeleteMyAccountPage clickDeleteSection() {
    deleteSection.click();
    return new DeleteMyAccountPage(driver);
  }

  public MyDetailsPage clickNewsletterSection() {
    newsletterSection.click();
    return new MyDetailsPage(driver);
  }

  public MyDetailsPage clickConfirmButton() {
    confirmButton.click();
    return new MyDetailsPage(driver);
  }

  public MyDetailsPage enterNewUsername(String usernameStr) {
    newUsername.clear();
    newUsername.sendKeys(usernameStr);
    return this;
  }

  public MyDetailsPage enterCurrentPassword(String currentPassStr) {
    currentPassword.clear();
    currentPassword.sendKeys(currentPassStr);
    return this;
  }

  public MyDetailsPage enterNewPassword(String newPassStr) {
    newPassword.clear();
    newPassword.sendKeys(newPassStr);
    return this;
  }

  public MyDetailsPage enterNewPasswordConfirmation(String newPassStr) {
    passwordConfirmation.clear();
    passwordConfirmation.sendKeys(newPassStr);
    return this;
  }

  public MyDetailsPage showHiddenFields() {
    List<WebElement> listElements =
        driver.findElements(By.xpath("//a[@class='js-form-collapsible-toggle ml-m']"));

    for (WebElement element : listElements) {
      element.click();
    }
    return this;
  }

  public boolean isPHPWarningPresent() {
    List<WebElement> elements = driver.findElements(By.id("errorMessages"));
    if (elements.isEmpty()) {
      return false;
    }
    return true;
  }

  public MyDetailsPage dismissPHPWarning() {
    phpWarning.click();
    return this;
  }
}
