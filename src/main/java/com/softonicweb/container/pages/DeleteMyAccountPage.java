package com.softonicweb.container.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Contains methods to interact with the user deletion page model.
 *
 * @author guillem.hernandez
 */
public class DeleteMyAccountPage extends AbstractSoftonicWebPage {

  @FindBy(xpath = "id('user_cancel_registry')/fieldset/ul/li[5]/input[2]")
  private WebElement deleteButton;

  public DeleteMyAccountPage(WebDriver driver) {
    super(driver);
  }

  public CancelRegistrationConfirmPage clickDeleteButton() {
    deleteButton.click();
    return new CancelRegistrationConfirmPage(driver);
  }
}
