package com.softonicweb.container.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Contains methods to interact with the Softonic Newsletter Subscription page.
 *
 * @author guillem.hernandez
 * @author thijs.vandewynckel
 */
public class NewsletterSubscriptionPage extends AbstractSoftonicWebPage {

  @FindBy(xpath = "id('name')")
  private WebElement newsletterFormName;

  @FindBy(xpath = "id('gender')")
  private WebElement newsletterFormGender;

  @FindBy(xpath = "id('sday')")
  private WebElement newsletterFormDay;

  @FindBy(xpath = "id('smonth')")
  private WebElement newsletterFormMonth;

  @FindBy(xpath = "id('syear')")
  private WebElement newsletterFormYear;

  @FindBy(xpath = "id('country')")
  private WebElement newsletterFormCountry;

  @FindBy(xpath = "id('send_info')")
  private WebElement newsletterFormSendButton;

  public NewsletterSubscriptionPage(WebDriver driver) {
    super(driver);
  }

  private NewsletterSubscriptionPage enterName(String name) {
    sendKeysThreadSafe(newsletterFormName, name);
    return this;
  }

  private NewsletterSubscriptionPage selectGenderByValue(String gender) {
    newsletterFormGender.sendKeys(gender);
    return this;
  }

  private NewsletterSubscriptionPage selectDayByValue(String day) {
    newsletterFormDay.sendKeys(day);
    return this;
  }

  private NewsletterSubscriptionPage selectMonthByValue(String month) {
    newsletterFormMonth.sendKeys(month);
    return this;
  }

  private NewsletterSubscriptionPage selectYearByValue(String year) {
    newsletterFormYear.sendKeys(year);
    return this;
  }

  private NewsletterSubscriptionPage selectCountryByValue(String country) {
    newsletterFormCountry.sendKeys(country);
    return this;
  }

  private NewsletterSubscriptionPage clickSubscriptionButton() {
    newsletterFormSendButton.click();
    return new NewsletterSubscriptionPage(driver);
  }

  public NewsletterSubscriptionPage fillSubscriptionForm(String name, String gender, String day,
      String month, String year, String country) {
    return enterName(name).selectGenderByValue(gender).selectDayByValue(day)
        .selectMonthByValue(month).selectYearByValue(year).selectCountryByValue(country)
        .clickSubscriptionButton();
  }

  public boolean isStandardNewsletterChecked() {
    List<WebElement> elements =
        driver.findElements(By.cssSelector("#gigya-checkbox-subscription:checked"));
    if (elements.isEmpty()) {
      return false;
    }
    return true;
  }

  public boolean isPromoNewsletterChecked() {
    List<WebElement> elements =
        driver.findElements(By.cssSelector("#gigya-checkbox-116497590074796830:checked"));
    if (elements.isEmpty()) {
      return false;
    }
    return true;
  }
}
