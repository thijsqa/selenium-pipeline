package com.softonicweb.container.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Contains methods to interact with the Softonic popular searches page in any Softonic plaftorm.
 *
 * @author thijs.vandewynckel
 */
public class HotSearchesPage extends AbstractSoftonicWebPage {

  @FindBy(xpath = "id('content_1col')/ul/li[1]/h1/a")
  private WebElement tagCloud;

  public HotSearchesPage(WebDriver driver) {
    super(driver);
  }

  public boolean isTagCloudPresent() {
    return isElementDisplayed(tagCloud);
  }
}
