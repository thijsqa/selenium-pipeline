package com.softonicweb.container.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.softonicweb.container.pages.LoginModal;

/**
 * Webdriver model for the New Home Page.
 *
 * @author antonio.martin
 * @author thijs.vandewynckel
 */
public class HomePage extends AbstractSoftonicWebPage {

  private static final String HOME_CARD_ITEMS = "//div[contains(@class,'cu-card-image')]";
  private static final int WAIT = 2000; // ms

  @FindBy(id = "header-login-link")
  private WebElement loginButton;

  @FindBy(className = "ico-remove")
  private WebElement logoutButton;

  @FindBy(xpath = "id('header')/div/div[2]/a")
  private WebElement myNoodleProfileButton;

  @FindBy(xpath = "id('suggest_instance')/a")
  private WebElement instanceSuggestClose;

  @FindBy(xpath = "id('home')/div[1]/div")
  private WebElement oneHome;

  @FindBy(xpath = "//*[@id='one-home']/ul/li[1]/article")
  private WebElement heroModule;

  @FindBy(xpath = "//section[@class='featured-app']/div")
  private WebElement featuredApp;

  @FindBy(xpath = "//*[@id='trending-categories-apps']/ul/li[1]/div")
  private WebElement trendingCategories;

  @FindBy(id = "top-downloads")
  private WebElement homeTopDownloads;

  @FindBy(xpath = "//*[@id='top-downloads']/div/section[1]/div/h2/label"
      + "[contains(text(), 'Windows')]")
  private WebElement windowsTab;

  @FindBy(xpath = "//*[@id='top-downloads']/div/section[2]/div/h2/label"
      + "[contains(text(), 'Mac')]")
  private WebElement macTab;

  @FindBy(xpath = "//*[@id='top-downloads']/div/section[3]/div/h2/label"
      + "[contains(text(), 'Android')]")
  private WebElement androidTab;

  @FindBy(xpath = "//*[@id='top-downloads']/div/section[4]/div/h2/label"
      + "[contains(text(), 'iPhone')]")
  private WebElement iphoneTab;

  @FindBy(xpath = "//div[@class='top-downloads-tab__button']/a")
  private WebElement fullListButton;

  @FindBy(xpath = "//*[@id='latest-articles']/ul/li[1]/article")
  private WebElement latestArticles;

  @FindBy(xpath = "//div[contains(@class,'widget-facebook-timeline')]")
  private WebElement facebookFeed;

  @FindBy(xpath = "//div[contains(@class,'widget-twitter-timeline')]")
  private WebElement twitterFeed;

  public HomePage(WebDriver driver) {
    super(driver);
  }

  public LoginModal<HomePage> clickOnLogin() {
    loginButton.click();
    return new LoginModal<HomePage>(this);
  }

  @Override
  public boolean isLoggedIn() {
    wait.until(ExpectedConditions.elementToBeClickable(By.className("ico-remove")));
    return isElementDisplayed(logoutButton);
  }

  @Override
  public String getLoggedNoodleUserPartialName() {
    return myNoodleProfileButton.getText().replace("...", "").trim().toLowerCase();
  }

  private static final String SEOMODULES =
      "//article[@id='top-published-last-month-apps-windows' or"
          + "@id='top-published-last-month-apps-mac' or @id='top-published-last-month-apps-android'"
          + "or @id='top-published-last-month-apps-iphone']";

  public boolean isInstanceSuggestPresent() {
    List<WebElement> elements = driver.findElements(By.id("suggest_instance"));
    if (elements.isEmpty()) {
      return false;
    }
    return true;
  }

  public HomePage dismissInstanceSuggest() {
    instanceSuggestClose.click();
    return this;
  }

  public boolean isOneHomePresent() {
    return isElementDisplayed(oneHome);
  }

  public boolean isHeroModulePresent() {
    wait.pause(WAIT);
    wait.until(ExpectedConditions.visibilityOfElementLocated(By
        .xpath("//*[@id='one-home']/ul/li[1]/article")));
    return isElementDisplayed(heroModule);
  }

  public boolean isFeaturedAppModulePresent() {
    return isElementDisplayed(featuredApp);
  }

  public boolean isTrendingCategoriesPresent() {
    return isElementDisplayed(trendingCategories);
  }

  public boolean isHomeTopDownloadsPresent() {
    return isElementDisplayed(homeTopDownloads);
  }

  public boolean isWindowsTabPresent() {
    return isElementDisplayed(windowsTab);
  }

  public boolean isMacTabPresent() {
    return isElementDisplayed(macTab);
  }

  public boolean isAndroidTabPresent() {
    return isElementDisplayed(androidTab);
  }

  public boolean isIphoneTabPresent() {
    return isElementDisplayed(iphoneTab);
  }

  public boolean isFullListButtonPresent() {
    return isElementDisplayed(fullListButton);
  }

  public boolean isLatestArticlesPresent() {
    return isElementDisplayed(latestArticles);
  }

  public boolean isFacebookFeedPresent() {
    return isElementDisplayed(facebookFeed);
  }

  public boolean isTwitterFeedPresent() {
    return isElementDisplayed(twitterFeed);
  }

  public boolean areSeoModulesPresent() {
    return isElementDisplayed(By.xpath(SEOMODULES));
  }

  public int getHomeCardsCount() {
    return driver.findElements(By.xpath(HOME_CARD_ITEMS)).size();
  }
}
