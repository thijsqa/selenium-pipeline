package com.softonicweb.container.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.softonicweb.container.seleniumframework.AbstractWebPage;
import com.softonicweb.container.seleniumframework.AbstractWidget;

/**
 * A reusable element to provide standardized Facebook Social button
 * manipulation.
 *
 * @param <T> parent page
 *
 * @author guillem.hernandez
 */
public class FacebookWidget<T extends AbstractWebPage> extends AbstractWidget<T> {

  @FindBy(xpath = "//iframe[contains(@src, 'http://www.facebook.com/plugins')]")
  private WebElement facebookFrame;

  @FindBy(id = "facebook_connect")
  private WebElement facebookConnectButton;

  @FindBy(xpath = "//div[@class='pluginCountBox']/div/span/span")
  private List<WebElement> likesCounterList;

  public FacebookWidget(T page) {
    super(page);
  }

  public void clickFacebookLikeButton() {
    driver.switchTo().frame(facebookFrame);
    WebElement b = driver.findElement(By.xpath("//button[@type='submit']"));
    Actions builder = new Actions(driver);
    builder.moveToElement(b);
    wait.pause(HOVER_TIMEOUT);
    builder.click();
    Action c = builder.build();
    c.perform();
    driver.switchTo().defaultContent();
    wait.pause(POPUP_TIMEOUT);
  }

  public boolean isFacebookLikeButtonPresent() {
    wait.pause(POPUP_TIMEOUT);
    return isElementDisplayed(facebookFrame);
  }

  public boolean isFacebookConnectButtonPresent() {
    wait.until(ExpectedConditions.visibilityOf(facebookConnectButton));
    return isElementDisplayed(facebookConnectButton);
  }

  public int getLikesCounter() {
    driver.switchTo().frame(facebookFrame);
    int likes;
    if (isElementDisplayed(likesCounterList.get(0))) {
      likes = Integer.parseInt(likesCounterList.get(0).getText());
    } else {
      likes = Integer.parseInt(likesCounterList.get(1).getText());
    }
    driver.switchTo().defaultContent();
    return likes;
  }
}
