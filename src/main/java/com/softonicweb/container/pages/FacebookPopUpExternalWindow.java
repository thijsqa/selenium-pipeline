package com.softonicweb.container.pages;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.softonicweb.container.framework.Accounts;

/**
 * Contains methods to interact with the quick register facebook connect options
 *
 * @author guillem.hernandez
 */
public class FacebookPopUpExternalWindow extends AbstractSoftonicWebPage {

  private final String emailTextArea = "email";
  private final String passwordTextArea = "pass";
  private final String submitButton = "u_0_1";

  public FacebookPopUpExternalWindow(WebDriver driver) {
    super(driver);
  }

  public void loginOnFacebookConnect() {
    String parentWindowHandle = driver.getWindowHandle();
    WebDriver popup = null;
    Set<String> windowIterator = driver.getWindowHandles();
    Iterator<String> windowIt = windowIterator.iterator();
    while (windowIt.hasNext()) {
      String windowHandle = windowIt.next().toString();
      popup = driver.switchTo().window(windowHandle);
      if (popup.getTitle().contains("Facebook")) {
        popup.findElement(By.id(emailTextArea)).isDisplayed();
        sendKeysThreadSafe(popup.findElement(By.id(emailTextArea)), Accounts.FACEBOOK.getEmail());
        popup.findElement(By.id(passwordTextArea)).isDisplayed();
        sendKeysThreadSafe(popup.findElement(By.id(passwordTextArea)),
            Accounts.FACEBOOK.getPassword());
        popup.findElement(By.id(submitButton)).isDisplayed();
        popup.findElement(By.id(submitButton)).click();
      }
    }
    driver.switchTo().window(parentWindowHandle);
  }

  public OpinionThanksPage loginOnFacebookPopUpExternalWindowFromOpinionForm() {
    loginOnFacebookConnect();
    return new OpinionThanksPage(driver);
  }
}
