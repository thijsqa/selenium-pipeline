package com.softonicweb.container.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Contains methods to interact with the Article Page
 *
 * @author thijs.vandewynckel
 */

public class ArticlePage extends AbstractSoftonicWebPage {

  @FindBy(css = "[data-auto=article-name]")
  private WebElement articleTitle;
  @FindBy(css = "[data-auto=share-fb-button] a")
  private WebElement fbButton;
  @FindBy(css = "[data-auto=share-google-button] a")
  private WebElement gplusButton;
  @FindBy(css = "[data-auto=share-twitter-button] a")
  private WebElement twitterButton;
  @FindBy(css = "[data-auto=article-body]")
  private WebElement articleBody;

  public ArticlePage(WebDriver driver) {
    super(driver);
  }

  public boolean isArticleTitlePresent() {
    return isElementDisplayed(articleTitle);
  }

  public boolean isFbButtonPresent() {
    return isElementDisplayed(fbButton);
  }

  public boolean isGplusButtonPresent() {
    return isElementDisplayed(gplusButton);
  }

  public boolean isTwitterButtonPresent() {
    return isElementDisplayed(twitterButton);
  }

  public boolean isArticleBodyPresent() {
    return isElementDisplayed(articleBody);
  }

  public ArticlePage clickTwitterButton() {
    twitterButton.click();
    return this;
  }
}
