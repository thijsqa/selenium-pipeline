package com.softonicweb.container.pages;

import java.util.ArrayList;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Contains methods which are useful to interact with the Videos home page and
 * video-related parts of the main and download pages. Reference URL:
 * http://videos.softonic.com/
 *
 * @author james.oram
 * @author guillem.hernandez
 */
public class VideosHomePage extends AbstractSoftonicWebPage {

  private static final String VIDEO_LIST = "//ul[@class='list-video']/li";

  private static final String CLICK_RANDOM_VIDEO = "//ul[@class='list-video']/li[%s]/div/a/span";

  private static final String VIDEO_HREF = "id('content')/div/ul/li[%s]/div/a";

  public VideosHomePage(WebDriver driver) {
    super(driver);
  }

  public VideoPage clickRandomVideo() {
    Random random = new Random();
    int videoId = random.nextInt(getVideoListSize() - 1) + 1;
    WebElement videoLink = driver.findElement(By.xpath(String.format(CLICK_RANDOM_VIDEO, videoId)));
    videoLink.click();
    return new VideoPage(driver);
  }

  public int getVideoListSize() {
    return driver.findElements(By.xpath(VIDEO_LIST)).size();
  }

  public ArrayList<String> getVideoHrefList() {
    ArrayList<String> videoHrefList = new ArrayList<String>();
    for (int i = 1; i <= getVideoListSize(); i++) {
      WebElement videoLink = driver.findElement(By.xpath(String.format(VIDEO_HREF, i)));
      videoHrefList.add(videoLink.getAttribute("href"));
    }
    return videoHrefList;
  }

}
