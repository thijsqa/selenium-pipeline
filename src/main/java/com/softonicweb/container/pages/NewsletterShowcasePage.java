package com.softonicweb.container.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Contains methods to interact with the Softonic Newsletter Showcase page.
 *
 * @author thijs.vandewynckel
 */
public class NewsletterShowcasePage extends AbstractSoftonicWebPage {

  @FindBy(xpath = "//div/h1[@class='header__title']")
  private WebElement title;

  @FindBy(xpath = "//div/p[@class='section-description']")
  private WebElement description;

  @FindBy(xpath = "//div[contains(@class,'nl-box')]")
  private WebElement newsletterBox;

  @FindBy(xpath = "//div/div/span[contains(@class,'nl-box__timing')]")
  private WebElement timingBox;

  @FindBy(xpath = "//div/div/div/img[contains(@class,'nl-box__img')]")
  private WebElement newsletterIcon;

  @FindBy(xpath = "//div/div/h3[@class='nl-box__title']")
  private WebElement newsletterTitle;

  @FindBy(xpath = "//div/div/p[@class='nl-box__description']")
  private WebElement newsletterDescription;

  @FindBy(xpath = "//div/div/button[@class='nl-box__subscribe']")
  private WebElement subscribeButton;

  @FindBy(css = "[data-category=VoteComingNewsletters]")
  private WebElement cookingSection;

  public NewsletterShowcasePage(WebDriver driver) {
    super(driver);
  }

  public boolean isTitlePresent() {
    return isElementDisplayed(title);
  }

  public boolean isDescriptionPresent() {
    return isElementDisplayed(description);
  }

  public boolean isNewsletterboxPresent() {
    return isElementDisplayed(newsletterBox);
  }

  public boolean isTimingboxPresent() {
    return isElementDisplayed(timingBox);
  }

  public boolean isNewsletterIconPresent() {
    return isElementDisplayed(newsletterIcon);
  }

  public boolean isNewsletterTitlePresent() {
    return isElementDisplayed(newsletterTitle);
  }

  public boolean isNewsletterDescriptionPresent() {
    return isElementDisplayed(newsletterDescription);
  }

  public boolean isSubscribeButtonPresent() {
    return isElementDisplayed(subscribeButton);
  }

  public boolean isCookingSectionPresent() {
    return isElementDisplayed(cookingSection);
  }
}
