package com.softonicweb.container.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.softonicweb.container.framework.Localization.Country;

/**
 * Contains methods to interact with the user registration modal window.
 *
 * @author guillem.hernandez
 */
public class NewUserModalWindow extends AbstractSoftonicWebPage {

  @FindBy(id = "closeBut")
  private WebElement closeButton;

  @FindBy(xpath = ".//*[@class='btn toggle_form']")
  private WebElement newAccountButton;

  @FindBy(id = "signup_email")
  private WebElement emailInput;

  @FindBy(id = "signup_password")
  private WebElement passwordInput;

  @FindBy(id = "nickname")
  private WebElement usernameInput;

  @FindBy(id = "bday")
  private WebElement birthdateDaySelect;

  @FindBy(xpath = "id('bmonth')/option[10]")
  private WebElement birthdateMonthSelect;

  @FindBy(id = "byear")
  private WebElement birthdateYearSelect;

  @FindBy(id = "country")
  private WebElement countrySelect;

  @FindBy(xpath = "id('already_registered')/a")
  private WebElement signInHereButton;

  @FindBy(xpath = "id('register_form')//button")
  private WebElement submitButton;

  public NewUserModalWindow(WebDriver driver) {
    super(driver);
  }

  public NewUserModalWindow enterEmail(String email) {
    sendKeysThreadSafe(emailInput, email);
    return this;
  }

  public NewUserModalWindow enterPassword(String password) {
    sendKeysThreadSafe(passwordInput, password);
    return this;
  }

  public NewUserModalWindow enterUsername(String username) {
    sendKeysThreadSafe(usernameInput, username);
    return this;
  }

  // Enters birth date in the format DDMMYYYY.
  public NewUserModalWindow enterDay(String day) {
    sendKeysThreadSafe(birthdateDaySelect, day);
    return this;
  }

  // Enters birth date in the format DDMMYYYY.
  public NewUserModalWindow enterMonth(String month) {
    sendKeysThreadSafe(birthdateMonthSelect, month);
    birthdateMonthSelect.click();
    return this;
  }

  // Enters birth date in the format DDMMYYYY.
  public NewUserModalWindow enterYear(String year) {
    sendKeysThreadSafe(birthdateYearSelect, year);
    return this;
  }

  public NewUserModalWindow selectCountryOfResidense(String value) {
    sendKeysThreadSafe(countrySelect, value);
    return this;
  }

  public NewUserModalWindow submitRegistration() {
    submitButton.click();
    return this;
  }

  public NewUserModalWindow quickRegisterUser(String email, String password, String username,
      String day, String month, String year, Country country) {
    enterEmail(email).enterPassword(password).enterUsername(username).enterDay(day)
        .enterMonth(month).enterYear(year);
    if (country.equals(Country.ES) || country.equals(Country.EN)) {
      selectCountryOfResidense("EN");
    }
    return submitRegistration();
  }
}
