package com.softonicweb.container.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.softonicweb.container.pages.OpinionThanksPage;

/**
 * Contains methods to interact with the quick user register modal window.
 *
 * @author guillem.hernandez
 */
public class QuickRegisterLoginModal extends AbstractSoftonicWebPage {

  @FindBy(id = "login_email")
  private WebElement loginEmail;

  @FindBy(id = "login_password")
  private WebElement loginPassword;

  @FindBy(xpath = "id('login_form')/button")
  private WebElement loginButton;

  public QuickRegisterLoginModal(WebDriver driver) {
    super(driver);
  }

  public QuickRegisterLoginModal typeEmail(String email) {
    sendKeysThreadSafe(loginEmail, email);
    return this;
  }

  public QuickRegisterLoginModal typePassword(String password) {
    sendKeysThreadSafe(loginPassword, password);
    return this;
  }

  public OpinionThanksPage clickLoginButtonFromAnOpinion() {
    loginButton.submit();
    return new OpinionThanksPage(driver);
  }
}
