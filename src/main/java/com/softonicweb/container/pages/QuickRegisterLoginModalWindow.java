package com.softonicweb.container.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Contains methods to interact with the quick user registration modal window.
 *
 * @author guillem.hernandez
 */
public class QuickRegisterLoginModalWindow extends AbstractSoftonicWebPage {

  @FindBy(id = "login_email")
  private WebElement loginEmail;

  @FindBy(id = "login_password")
  private WebElement loginPassword;

  @FindBy(xpath = "id('login_form')/ul/li[3]/button")
  private WebElement loginButton;

  public QuickRegisterLoginModalWindow(WebDriver driver) {
    super(driver);
  }

  public QuickRegisterLoginModalWindow typeEmail(String email) {
    sendKeysThreadSafe(loginEmail, email);
    return this;
  }

  public QuickRegisterLoginModalWindow typePassword(String password) {
    sendKeysThreadSafe(loginPassword, password);
    return this;
  }

  public OpinionThanksPage clickLoginButtonFromAnOpinion() {
    loginButton.click();
    return new OpinionThanksPage(driver);
  }
}
