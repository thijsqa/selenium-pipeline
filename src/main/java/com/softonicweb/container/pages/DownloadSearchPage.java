package com.softonicweb.container.pages;

import org.openqa.selenium.WebDriver;

import com.softonicweb.container.pages.AfsWidget;

/**
 * Common methods for downloads search pages.
 *
 * @author carlos.silva
 *
 */
public class DownloadSearchPage extends AbstractSoftonicWebPage {

  public DownloadSearchPage(WebDriver driver) {
    super(driver);
  }

  public AfsWidget<DownloadSearchPage> getCsaWidget() {
    return new AfsWidget<DownloadSearchPage>(this);
  }
}
