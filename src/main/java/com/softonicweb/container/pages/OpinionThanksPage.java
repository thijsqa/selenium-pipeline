package com.softonicweb.container.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

/**
 * Contains methods to interact with the Softonic Opinion Thanks Page.
 *
 * @author guillem.hernandez
 */
public class OpinionThanksPage extends AbstractSoftonicWebPage {

  @FindBy(xpath = "id('users_opinion')/div[1]")
  private WebElement thanksFrame;

  @FindBy(id = "user_data_logged_username")
  private WebElement usernameText;

  @FindBy(id = "title_preview")
  private WebElement opinionTitle;

  @FindBy(id = "body_preview")
  private WebElement opinionBody;

  public OpinionThanksPage(WebDriver driver) {
    super(driver);
  }

  public boolean isThanksFramePresent() {
    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("id('users_opinion')/div[1]")));
    return isElementDisplayed(thanksFrame);
  }

  public boolean isUsernameTextPresent() {
    wait.until(ExpectedConditions.elementToBeClickable(By.id("user_data_logged_username")));
    return isElementDisplayed(usernameText);
  }

  public boolean isOpinionTitlePresent() {
    wait.until(ExpectedConditions.elementToBeClickable(By.id("title_preview")));
    return isElementDisplayed(opinionTitle);
  }

  public boolean isOpinionBodyPresent() {
    wait.until(ExpectedConditions.elementToBeClickable(By.id("body_preview")));
    return isElementDisplayed(opinionBody);
  }
}
