package com.softonicweb.container.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.softonicweb.container.seleniumframework.AbstractWebPage;
import com.softonicweb.container.seleniumframework.AbstractWidget;

/**
 * A reusable element to provide standardized CSA element support.
 *
 * @param <T> parent page
 *
 * @author carlos.silva
 */
public class AfsWidget<T extends AbstractWebPage> extends AbstractWidget<T> {

  private static final String TARGET_LOCATION = "test_titleLink";

  private static final String ADS_CONTAINER_TOP_IFRAME = "master-1";

  private static final String ADS_CONTAINER_BOTTOM_IFRAME = "master-2";

  private static final String AD_LOCATION = "sfblAd";

  @FindBy(css = "[data-auto=top-contextual]")
  private WebElement adsContainerTop;

  @FindBy(id = "afs-container-top")
  private WebElement oldAdsContainerTop;

  @FindBy(id = "bottom-contextual-list-desktop")
  private WebElement adsContainerBottom;

  public AfsWidget(T page) {
    super(page);
  }

  public boolean hasCorrectOldAdsContainerTop() {
    return isOldContainerTopPresent() ? hasCorrectAdsContainer(ADS_CONTAINER_TOP_IFRAME) : false;
  }

  public boolean hasCorrectAdsContainerTop() {
    return isContainerTopPresent() ? hasCorrectAdsContainer(ADS_CONTAINER_TOP_IFRAME) : false;
  }

  public boolean hasCorrectAdsContainerBottom() {
    return isContainerBottomPresent() ? hasCorrectAdsContainer(ADS_CONTAINER_BOTTOM_IFRAME) : false;
  }

  public boolean hasCorrectNoResultAdsContainerBottom() {
    return isContainerBottomPresent() ? hasCorrectAdsContainer(ADS_CONTAINER_TOP_IFRAME) : false;
  }

  public boolean hasCorrectAdsContainer(String iframe) {
    return hasAdTargetsIncorrect(iframe);
  }

  public boolean isContainerTopPresent() {
    return adsContainerTop.isDisplayed();
  }

  public boolean isOldContainerTopPresent() {
    return oldAdsContainerTop.isDisplayed();
  }

  public boolean isContainerBottomPresent() {
    return adsContainerBottom.isDisplayed();
  }

  public boolean isAdsContainerTopPresent() {
    try {
      driver.switchTo().frame(ADS_CONTAINER_TOP_IFRAME);
      driver.findElement(By.xpath("//div[contains(@class,'adD')]"));
      driver.switchTo().defaultContent();
      return true;
    } catch (NoSuchElementException e) {
      driver.switchTo().defaultContent();
      return false;
    } catch (NoSuchFrameException ex) {
      driver.switchTo().defaultContent();
      return false;
    }
  }

  public boolean isAdsContainerBottomPresent() {
    try {
      driver.switchTo().frame(ADS_CONTAINER_BOTTOM_IFRAME);
      driver.findElement(By.xpath("//div[contains(@class,'adD')]"));
      driver.switchTo().defaultContent();
      return true;
    } catch (NoSuchElementException e) {
      driver.switchTo().defaultContent();
      return false;
    } catch (NoSuchFrameException ex) {
      driver.switchTo().defaultContent();
      return false;
    }
  }

  public boolean hasAdTargetsIncorrect(String iframe) {
    boolean adsCorrect = true;
    driver.switchTo().frame(driver.findElement(By.id(iframe)));
    List<WebElement> ads = driver.findElements(By.className(AD_LOCATION));
    for (WebElement ad : ads) {
      if (!isAdTargetCorrect(ad)) {
        adsCorrect = false;
        break;
      }
    }
    driver.switchTo().defaultContent();
    return adsCorrect;
  }

  public boolean isAdTargetCorrect(WebElement ad) {
    WebElement target = ad.findElement(By.className(TARGET_LOCATION));
    String a = target.getAttribute("target");
    return a.equals("_blank");
  }

  public int getCsaCount(String iframe) {
    int value;
    driver.switchTo().frame(iframe);
    value = driver.findElements(By.className(AD_LOCATION)).size();
    driver.switchTo().defaultContent();
    return value;
  }

  public int getTopCsaCount() {
    return getCsaCount(ADS_CONTAINER_TOP_IFRAME);
  }

  public int getBottomCsaCount() {
    return getCsaCount(ADS_CONTAINER_BOTTOM_IFRAME);
  }
}
