package com.softonicweb.container.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Contains methods to interact with the Softonic Topics Page
 *
 * @author thijs.vandewynckel
 */
public class TopicsPage extends AbstractSoftonicWebPage {

  @FindBy(xpath = "//h1[contains(@class,'topic-page__title-section')]")
  private WebElement topicsTitle;
  @FindBy(xpath = "//div[contains(@class,'app-rank-table')]/table")
  private WebElement bestRatedTable;
  @FindBy(xpath = "//div[contains(@class,'media__image media-app__image')]/img")
  private WebElement programListIcon;
  @FindBy(xpath = "//ul[contains(@class,'list-apps__pros-cons list-apps__pros-cons--h')]/li")
  private WebElement prosCons;
  @FindBy(css = ".button-dropdown__label svg")
  private WebElement dropdownDlButton;
  @FindBy(xpath = "//ul[contains(@class,'button-dropdown__menu')]")
  private WebElement otherPlatformDownloads;

  public TopicsPage(WebDriver driver) {
    super(driver);
  }

  public boolean isTopicsTitlePresent() {
    return isElementDisplayed(topicsTitle);
  }

  public boolean isBestRatedTablePresent() {
    return isElementDisplayed(bestRatedTable);
  }

  public boolean isProgramListIconPresent() {
    return isElementDisplayed(programListIcon);
  }

  public boolean areProsAndConsPresent() {
    return isElementDisplayed(prosCons);
  }

  public boolean isDropdownDownloadButtonPresent() {
    return isElementDisplayed(dropdownDlButton);
  }

  public TopicsPage clickDropdownDlButton() {
    dropdownDlButton.click();
    return this;
  }

  public boolean areOtherPlatformDownloadsPresent() {
    return isElementDisplayed(otherPlatformDownloads);
  }
}
