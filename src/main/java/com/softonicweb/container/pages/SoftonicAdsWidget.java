package com.softonicweb.container.pages;

import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.softonicweb.container.seleniumframework.AbstractWebPage;
import com.softonicweb.container.seleniumframework.AbstractWidget;
import com.softonicweb.container.framework.HttpUtil;

/**
 * Widget class for handling Softonic Ads.
 * This module, if present, shows a maximum of four ads inside the module.
 *
 * @param <T> parent page
 *
 * @author josemaria.delbarco
 * @author guillem.hernandez (WebDriver migration and json checks)
 */
public class SoftonicAdsWidget<T extends AbstractWebPage> extends AbstractWidget<T> {

  private static final String SADS_FORMAT = "id('result')/ul/li[%s]/h5/a";
  private static final String SADS_QUANTITY = "//div[@id='result']/ul/li";

  @FindBy(id = "ads_cpd_pdp")
  private WebElement softonicAdsContainer;

  @FindBy(id = "ads_section_textlinks")
  private WebElement softonicPlatformAdsContainer;

  public SoftonicAdsWidget(T page) {
    super(page);
  }

  public boolean isSoftonicAdsJsonRequestNotProvidingResults(String requestUrl) {
    HttpUtil httpConnection = new HttpUtil(requestUrl);
    return !httpConnection.getSourceAsString().contains("NO RESULTS");
  }

  public boolean isSoftonicAdsReachingTheTracker(String requestUrl) {
    HttpUtil httpConnection = new HttpUtil(requestUrl);
    return httpConnection.getResponseCode().equals("200");
  }

  public boolean isSoftonicPdpAdsWidgetPresent() {
    return isElementDisplayed(softonicAdsContainer);
  }

  public boolean isSoftonicAdsWidgetPresent() {
    return isElementDisplayed(softonicPlatformAdsContainer);
  }

  public int getSadsCount() {
    return driver.findElements(By.xpath((SADS_QUANTITY))).size();
  }

  public AbstractSoftonicWebPage clickRandomSad() {
    int adNumber = new Random().nextInt(getSadsCount()) + 1;
    WebElement element = driver.findElement(By.xpath(String.format(SADS_FORMAT, adNumber)));
    wait.forElementPresent(element);
    element.click();
    return new AbstractSoftonicWebPage(driver);
  }
}
