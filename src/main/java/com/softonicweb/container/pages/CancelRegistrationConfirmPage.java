package com.softonicweb.container.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Contains methods to interact with the user account cancellation page.
 *
 * @author guillem.hernandez
 */
public class CancelRegistrationConfirmPage extends AbstractSoftonicWebPage {

  @FindBy(id = "message_box")
  private WebElement messageBox;
  @FindBy(xpath = "id('content')/h1")
  private WebElement titleBox;

  public CancelRegistrationConfirmPage(WebDriver driver) {
    super(driver);
  }

  public boolean isConfirmationOk() {
    return areAllElementsPresent(messageBox, titleBox);
  }
}
