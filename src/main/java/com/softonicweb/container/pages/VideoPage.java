package com.softonicweb.container.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Model for the videos page.
 *
 * @author james.oram
 */
public class VideoPage extends AbstractSoftonicWebPage {

  @FindBy(xpath = ".//*[@class='photo-video-big']")
  private WebElement video;
  @FindBy(xpath = "id('socialmedia_options')/ul/li")
  private WebElement socialMedia;
  @FindBy(xpath = ".//*[@class='program_buttons']//a")
  private WebElement downloadButton;
  @FindBy(id = "video-like-input")
  private WebElement likeButton;
  @FindBy(id = "video-dislike-input")
  private WebElement dislikeButton;

  public VideoPage(WebDriver driver) {
    super(driver);
  }

  public WebElement getVideo() {
    return video;
  }

  public WebElement getDownloadButton() {
    return downloadButton;
  }

  public WebElement getSocialMedia() {
    return socialMedia;
  }

  public DownloadPage clickDownload() {
    wait.forElementPresent(downloadButton);
    downloadButton.click();
    return new DownloadPage(driver);
  }

  public boolean isLikeButtonPresent() {
    return isElementDisplayed(likeButton);
  }

  public boolean isDislikeButtonPresent() {
    return isElementDisplayed(dislikeButton);
  }

  public boolean isDownloadButtonPresent() {
    return isElementDisplayed(downloadButton);
  }

  public String getDownloadUrl() {
    return downloadButton.getAttribute("href");
  }
}
