package com.softonicweb.container.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import com.softonicweb.container.framework.Localization.Country;
import com.softonicweb.container.pages.AfsWidget;

/**
 * Contains methods to interact with the Softonic normal postdownload page.
 *
 * @author josemaria.delbarco
 * @author guillem.hernandez
 */
public class PostDownloadPage extends AbstractSoftonicWebPage {

  private static final String RELATED_VIDEOS = "//div[@id='program_videos_related']/ul/li";

  @FindBy(id = "postdownload")
  private WebElement normalPostdownloadModule;

  @FindBy(id = "download_starting")
  private WebElement downloadStartingBarModule;

  @FindBy(id = "help-download-link")
  private WebElement sdHelpPostdownloadModule;

  @FindBy(id = "similar")
  private WebElement similarProgramsModule;

  @FindBy(xpath = "id('sponsored_links')/h4")
  private WebElement sponsoredLinksModule;

  @FindBy(id = "g_ads")
  private WebElement gadsModule;

  @FindBy(xpath = "//div[@class = 'show_banner_down_leaderboard banner-center']")
  private WebElement topBanner;

  @FindBy(xpath = "//div[@class = 'show_banner_down_bottom_leaderboard banner-center']")
  private WebElement bottomBanner;

  @FindBy(xpath = "//div[@class = 'show_banner_down_medium_rectangle banner']")
  private WebElement rightBanner;

  @FindBy(id = "download_again")
  private WebElement downloadAgain;

  @FindBy(id = "download-button-sd")
  private WebElement downloadButtonSd;

  @FindBy(id = "download-button")
  private WebElement downloadButtonNonSd;

  @FindBy(xpath = "//div[contains(@class,'gigya-screen-dialog-close')]/a")
  private WebElement gigyaCloseButton;

  @FindBy(id = "installation-steps")
  private WebElement installationStepsSection;

  @FindBy(className = "header-fdh-img")
  private WebElement fdhAppLogo;

  @FindBy(xpath = "id('download_starting')//input")
  private WebElement downloadUrlInput;

  @FindBy(id = "social-download-button-alternative")
  private WebElement alternativeDownloadButton;

  @FindBy(id = "download-button-alternative")
  private WebElement alternativeDownloadLink;

  @FindBy(id = "box-video")
  private WebElement videoPlayer;

  @FindBy(xpath = "//div[contains(@id,'zeus')]")
  private WebElement promotedVideo;

  @FindBy(id = "message-app-support")
  private WebElement supportLink;

  @FindBy(xpath = "//div[contains(@class,'message-download__body')]/p/a")
  private WebElement noodleExternalDownloadAgainLink;

  @FindBy(xpath = "//h2[contains(@class,'app-download-promoted-cover__downloading')]")
  private WebElement thanksMessage;

  @FindBy(id = "exit-intent-app-post-download-page-desktop")
  private WebElement exitIntent;

  public PostDownloadPage(WebDriver driver) {
    super(driver);
  }

  public boolean isExitIntentPresent() {
    wait.forElementPresent(exitIntent);
    return isElementDisplayed(exitIntent);
  }

  public boolean isPostdownloadBoxPresent() {
    wait.forElementPresent(normalPostdownloadModule);
    return isElementDisplayed(normalPostdownloadModule);
  }

  public boolean isRightBannerPresent() {
    wait.forElementPresent(rightBanner);
    return isElementDisplayed(rightBanner)
        && verifyImgLink(rightBanner);
  }

  public boolean isDownloadStartingBarPresent() {
    wait.forElementPresent(downloadStartingBarModule);
    return isElementDisplayed(downloadStartingBarModule);
  }

  public boolean isSdHelpPostdownloadPresent() {
    wait.forElementPresent(sdHelpPostdownloadModule);
    return isElementDisplayed(sdHelpPostdownloadModule);
  }

  public boolean isSimilarProgramsPresent() {
    wait.forElementPresent(similarProgramsModule);
    return isElementDisplayed(similarProgramsModule);
  }

  public boolean isSponsoredLinksPresent() {
    wait.forElementPresent(sponsoredLinksModule);
    return isElementDisplayed(sponsoredLinksModule);
  }

  public boolean isGoogleAfsPresent() {
    wait.forElementPresent(gadsModule);
    return isElementDisplayed(gadsModule);
  }

  public boolean isThanksMessagePresent() {
    wait.forElementPresent(thanksMessage);
    return isElementDisplayed(thanksMessage);
  }

  public boolean isTopBottomBannerPresent() {
    wait.forElementPresent(topBanner);
    wait.forElementPresent(bottomBanner);
    return isElementDisplayed(topBanner) && isElementDisplayed(bottomBanner)
        && verifyImgLink(topBanner)
        && verifyImgLink(bottomBanner);
  }

  private boolean verifyImgLink(WebElement iframe) {
    driver.switchTo().frame(iframe.findElement(By.xpath("//iframe")));
    driver.findElement(By.xpath("//html/body"));
    driver.switchTo().defaultContent();
    return true;
  }

  public boolean isDownloadAgainLinkPresent() {
    // Download Link is asyncronously loaded.
    wait.forElementVisible(downloadAgain);
    return isElementDisplayed(downloadAgain);
  }

  public boolean isSupportLinkPresent() {
    // Download Link is asyncronously loaded.
    wait.forElementVisible(supportLink);
    return isElementDisplayed(supportLink);
  }

  public boolean isNoodleExternalDownloadAgainLinkPresent() {
    // Download Link is asyncronously loaded.
    wait.forElementVisible(noodleExternalDownloadAgainLink);
    return isElementDisplayed(noodleExternalDownloadAgainLink);
  }

  public int getRelatedVideosCount() {
    return driver.findElements(By.xpath(RELATED_VIDEOS)).size();
  }

  public AfsWidget<PostDownloadPage> getCsaWidget() {
    return new AfsWidget<PostDownloadPage>(this);
  }

  public PostDownloadPage clickDownloadButtonSd() {
    downloadButtonSd.click();
    return this;
  }

  public PostDownloadPage clickDownloadButtonNonSd() {
    downloadButtonNonSd.click();
    return this;
  }

  public PostDownloadPage clickGigyaCloseButton() {
    gigyaCloseButton.click();
    return this;
  }

  public boolean hasBannerAds() {
    return isTopBottomBannerPresent() && isRightBannerPresent() && isGoogleAfsPresent();
  }

  /**
   * Common postdownload page layout checks.
   */
  public boolean hasAllCommonModulesLoaded() {
    return isDownloadStartingBarPresent() && isSimilarProgramsPresent()
        && isDownloadAgainLinkPresent();
  }

  public String getActualRetryDownloadText() {
    return downloadAgain.getText().trim();
  }

  public String getExpectedRetryDownloadText(Country country) {
    String expectedMessage;
    switch (country) {
    case ES:
      expectedMessage =
          "Si la descarga no se inicia automáticamente,"
          + " puedes volver a intentarlo";
      break;
    case EN:
      expectedMessage = "If the download doesn't start automatically, click here";
      break;
    case FR:
      expectedMessage = "Si le t\u00e9l\u00e9chargement ne d\u00e9marre pas automatiquement,"
          + " cliquez ici";
      break;
    case DE:
      expectedMessage = "Sollte der Download nicht automatisch starten, klicken Sie hier";
      break;
    case IT:
      expectedMessage = "Se il download non inizia automaticamente, clicca qui";
      break;
    case BR:
      expectedMessage = "Se o download n\u00e3o se inicia automaticamente, clique aqui";
      break;
    case PL:
      expectedMessage = "Je\u015bli pobieranie nie rozpocznie si\u0119 automatycznie,"
          + " mo\u017cesz je uruchomi\u0107 klikaj\u0105c, tutaj";
      break;
    case NL:
      expectedMessage = "Als de download niet automatisch start, klik dan hier";
      break;
    case JP:
      expectedMessage = "\u30c0\u30a6\u30f3\u30ed\u30fc\u30c9\u753b\u9762\u304c\u8868\u793a"
          + "\u3055\u308c\u306a\u3044\u5834\u5408\u306f \u3053\u3053\u3092\u30af\u30ea\u30c3"
          + "\u30af\u3057\u3066\u304f\u3060\u3055\u3044\u3002";
      break;
    default:
      throw new IllegalArgumentException("Invalid country: " + country.toString());
    }

    return expectedMessage;
  }

  public boolean areInstallationStepsPresent() {
    return isElementDisplayed(installationStepsSection);
  }

  public boolean isFdhAppLogoPresent() {
    return isElementDisplayed(fdhAppLogo);
  }

  public boolean isVideoPlayerPresent() {
    return isElementDisplayed(videoPlayer);
  }

  public boolean isPromotedVideoPresent() {
    return isElementDisplayed(promotedVideo);
  }

  public String getFdhAppLogoAlt() {
    return fdhAppLogo.getAttribute("alt");
  }

  public String getDownloadUrl() {
    return downloadUrlInput.getAttribute("value");
  }

  public PostDownloadPage clickAlternativeSocialDownload() {
    // workaround for IE9+ browsers
    Actions actions = new Actions(driver);
    actions.moveToElement(alternativeDownloadButton).click().perform();
    return this;
  }

  public PostDownloadPage clickAlternativeDownload() {
    // workaround for IE9+ browsers
    Actions actions = new Actions(driver);
    actions.moveToElement(alternativeDownloadLink).click().perform();
    return this;
  }
}
