package com.softonicweb.container.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Contains methods to interact with the Softonic articles search page in any Softonic
 * plaftorm.
 *
 * @author guillem.hernandez
 */
public class ArticlesSearchResultsPage extends AbstractSoftonicWebPage {

  @FindBy(xpath = "id('content')/div/section/div/article[1]/h3/a")
  private WebElement firstResultArticleLink;

  @FindBy(className = "title-page")
  private WebElement titlte;

  public ArticlesSearchResultsPage(WebDriver driver) {
    super(driver);
  }

  public boolean isfirstResultArticleLinkPresent() {
    return isElementDisplayed(firstResultArticleLink);
  }

  public boolean isTitlePresent() {
    return isElementDisplayed(titlte);
  }

  public String getTitleText() {
    return titlte.getText().trim();
  }
}
