package com.softonicweb.container.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Webdriver model for the profile menu page.
 * @author james.oram
 */
public class ProfileMenuPage extends AbstractSoftonicWebPage {

  @FindBy(xpath = "id('userprofile_box')/dl/dd/a")
  private WebElement editDetailsLink;

  public ProfileMenuPage(WebDriver driver) {
    super(driver);
  }

  public MyDetailsPage clickEditDetails() {
    wait.forElementPresent(editDetailsLink);
    editDetailsLink.click();
    return new MyDetailsPage(driver);
  }
}
