package com.softonicweb.container.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

/**
 * Contains methods to interact with the Softonic Comparison Page
 *
 * @author thijs.vandewynckel
 */
public class AlternativesPage extends AbstractSoftonicWebPage {

  @FindBy(xpath = "//div[@class='media__body']/h2/span")
  private WebElement programLicense;
  @FindBy(xpath = "//ul/li[contains(@class,'list-tags__item')]/a")
  private WebElement programTag;
  @FindBy(css = "[data-auto=app-summary]")
  private WebElement programDescription;
  @FindBy(xpath = "//div[@class='app-info__description']/p[2]")
  private WebElement programLead;
  @FindBy(xpath = "//ul[contains(@class,'list-apps')]/li[1]/article/div/div/div[1]/img")
  private WebElement programListIcon;
  @FindBy(xpath = "//div[@class='alternative-vote__answers']")
  private WebElement programListVoting;
  @FindBy(xpath = "//a[contains(@class,'btn--download')]")
  private WebElement programListDownloadButton;
  @FindBy(xpath = "//a[@class='list-apps__link']")
  private WebElement programListAlternativesLink;
  @FindBy(xpath = "//ul[contains(@class,'filter-dropdown__menu')]/li/a")
  private WebElement platformDropdownFirstItem;
  @FindBy(xpath = "//ul[contains(@class,'filter-dropdown__menu')]/li[2]/a")
  private WebElement platformDropdownSecondItem;
  @FindBy(css = "[data-auto=app-user-votes]")
  private WebElement fileRanking;

  public AlternativesPage(WebDriver driver) {
    super(driver);
  }

  public boolean isFileRankingPresent() {
    return isElementDisplayed(fileRanking);
  }

  public boolean isProgramLicensePresent() {
    return isElementDisplayed(programLicense);
  }

  public boolean areProgramTagsPresent() {
    return isElementDisplayed(programTag);
  }

  public boolean isProgramDescriptionPresent() {
    return isElementDisplayed(programDescription);
  }

  public boolean isProgramLeadPresent() {
    return isElementDisplayed(programLead);
  }

  public boolean isProgramListIconPresent() {
    return isElementDisplayed(programListIcon);
  }

  public boolean isProgramListVotingPresent() {
    return isElementDisplayed(programListVoting);
  }

  public boolean isProgramListDownloadButtonPresent() {
    return isElementDisplayed(programListDownloadButton);
  }

  public boolean isProgramListAlternativesLinkPresent() {
    return isElementDisplayed(programListAlternativesLink);
  }

  public String getPlatformFirstItem() {
    return platformDropdownFirstItem.getText();
  }

  public AlternativesPage clickPlatformSecondItem() {
    wait.until(ExpectedConditions.visibilityOfElementLocated(By
        .xpath("//ul[contains(@class,'filter-dropdown__menu')]/li[2]/a")));
    platformDropdownSecondItem.click();
    return this;
  }
}
