package com.softonicweb.container.pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.softonicweb.container.pages.PopularSearchesPage;
import com.softonicweb.container.pages.VideosHomePage;

/**
 * Webdriver model for the Abstract Platform Home Page.
 *
 * @author james.oram
 * @author guillem.hernandez
 */
public class PlatformHomePage extends AbstractSoftonicWebPage {

  private static final String OS_CONTAINER = "id('platforms-selector')/ul/li";
  private static final String OS_CONTAINER_LINK = OS_CONTAINER + "[%s]/a";
  // Download buttons on the newest software
  private static final String NEWEST_SOFTWARE_XPATH = "id('hot_software')";
  private static final String DOWNLOAD_HOT_SOFTWARE_BUTTON_FORMAT = NEWEST_SOFTWARE_XPATH
      + "/li[%s]/dl/dd[1]/a";
  private static final String DOWNLOAD_LAST_NEWS_BUTTON_FORMAT =
      "id('last_news')/li[%s]/dl/dd[1]/a";
  private static final String DOWNLOAD_LAST_UPDATES_BUTTON_FORMAT =
      "id('last_updates')/li[%s]/dl/dd[1]/a";
  private static final String DOWNLOAD_BUTTON_TOP_FORMAT =
      ".//*[@id='categories_front_page']/ul/li[%s]/dl/dd[1]/a";
  private static final String SEARCH_BOX_OPTION_FORMAT = "//input[@value='%s']";
  // HotSoftware, New Software, Latest updates
  private static final String HOT_SOFTWARE = "id('newest')/div/div/ul/li";
  private static final String LATEST_SOFTWARE_REVIEWS = "id('newest')/div/div/ul/li[%s]";
  private static final String TAB_ROWS_FORMAT = "id('newest')/ul[%s]/li";
  private static final String TAB_ROW_FORMAT = "id('newest')/ul[%s]/li[%s]";
  private static final String BUTTONS = ".//a[contains(@class,'button')]";
  // First Category
  private static final String SEARCH_LIST_COMBO = "//ul[@class='list_combo']//li";

  private static final String CATEGORIES = "id('categories_nav')/div/ul";
  private static final String SUBCATEGORIES_FORMAT = CATEGORIES + "[%s]/li/ul/li";

  private static final String TAB_BUTTONS_FORMAT =
      "id('newest')/ul[%s]/li/dl/dd/a[contains(@class,'button')]";
  private static final String ONE_HOME_ID = "onehome";
  private static final String TOP_DOWNLOADS_ONE_HOME_ID = "categories_front_page";

  @FindBy(xpath = "id('container')/footer/div[1]/ul/li[3]/a[9]")
  private WebElement allVideosLink;
  @FindBy(id = "home_set")
  private WebElement homeSet;
  @FindBy(xpath = "id('carrousel_item_0')/h2/a")
  private WebElement firstCarrouselItem;
  @FindBy(xpath = "id('carrousel_item_0')/a/img")
  private WebElement carrouselImage;
  // Local Top Download Tab
  @FindBy(id = "local_tab")
  private WebElement localTab;
  // Global Top Download Tab
  @FindBy(xpath = "id('section_tops')/div[1]/div/ul/li[2]/a")
  private WebElement globalTab;
  @FindBy(id = TOP_DOWNLOADS_ONE_HOME_ID)
  private WebElement topDownloadsOneHome;
  // One Home
  @FindBy(id = ONE_HOME_ID)
  private WebElement oneHomeCarrousel;
  @FindBy(xpath = "id('carrousel_item_0')/a/img")
  private WebElement carrouselImageOnHomePage;
  @FindBy(xpath = "//div[@class='wrapper_combo']")
  private WebElement searchCombo;
  @FindBy(xpath = "//div[contains(@class,'list_container')]")
  private WebElement searchListCombo;
  @FindBy(xpath = "//li[@class='multiplatform']//input")
  private WebElement searchAllSoftwareCheckbox;
  @FindBy(xpath = "//a[contains(@class,'track-action-SfWindows')]")
  private WebElement softonicForWindowsLink;
  @FindBy(xpath = "id('categories_nav')/div/ul/li")
  private List<WebElement> categories;
  @FindBy(xpath = "id('categories_nav')/h4")
  private WebElement categoriesTitle;
  @FindBy(xpath =
      "id('categories_front_page')/ul/li/dl/dd/a[contains(@class,'button-main-download-s')]")
  private List<WebElement> downloadButtonsTopCategory;

  @FindBy(xpath = "//*[contains(@class,'mb-l')]/ul[1]/li[1]/h1/a")
  private WebElement tagCloud;

  @FindBy(xpath = "//*[contains(@class,'mb-l')]/ul[2]/li[1]/a")
  private WebElement popularSearchesLink;

  @FindBy(xpath = "id('header')/a")
  private WebElement softonicLogo;

  @FindBy(xpath = "//*[@id='platforms-selector']/a")
  private WebElement platformSelector;

  @FindBy(xpath = "//*[@id='platforms-selector']/div/ul/li[1]/a")
  private WebElement windowsPlatform;

  @FindBy(xpath = "//*[@id='categories-top-apps']/ul/li/article")
  private WebElement categoryModule;

  public PlatformHomePage(WebDriver driver) {
    super(driver);
  }

  public PlatformHomePage clickPlatformSelector() {
    platformSelector.click();
    return this;
  }

  public PlatformHomePage clickWindowsPlatform() {
    windowsPlatform.click();
    return this;
  }

  public boolean isOnPlatformPage() {
    return isElementDisplayed(homeSet);
  }

  public boolean isCategoryModulePresent() {
    return isElementDisplayed(categoryModule);
  }

  public int getOsCount() {
    return driver.findElements(By.xpath(OS_CONTAINER)).size();
  }

  public String getOsHref(int index) {
    return driver.findElement(By.xpath(String.format(OS_CONTAINER_LINK, index))).getAttribute(
        "href");
  }

  public int getHotSoftwareTabsCount() {
    return driver.findElements(By.xpath(HOT_SOFTWARE)).size();
  }

  public boolean isLocalDownloadTabPresent() {
    return (isElementDisplayed(localTab) && isElementDisplayed(globalTab));
  }

  public PlatformHomePage clickOnCategory(int categoryIndex) {
    categories.get(categoryIndex).findElement(By.tagName("a")).click();
    return new PlatformHomePage(driver);
  }

  public PlatformHomePage clickOnTab(int index) {
    driver.findElement(By.xpath(String.format(LATEST_SOFTWARE_REVIEWS, index))).click();
    return new PlatformHomePage(driver);
  }

  public boolean isDownloadButtonsOnHotSoftware(int index) {
    WebElement downloadButton =
        driver.findElement(By.xpath(String.format(DOWNLOAD_HOT_SOFTWARE_BUTTON_FORMAT, index)));
    return isElementDisplayed(downloadButton);
  }

  public boolean isDownloadButtonsOnLastNews(int index) {
    WebElement downloadButton =
        driver.findElement(By.xpath(String.format(DOWNLOAD_LAST_NEWS_BUTTON_FORMAT, index)));
    return isElementDisplayed(downloadButton);
  }

  public boolean isDownloadButtonsOnLastUpdates(int index) {
    WebElement downloadButton =
        driver.findElement(By.xpath(String.format(DOWNLOAD_LAST_UPDATES_BUTTON_FORMAT, index)));
    return isElementDisplayed(downloadButton);
  }

  public boolean isDownloadButtonTopCategoryPresent(int index) {
    return isElementDisplayed(driver.findElement(By.xpath(String.format(DOWNLOAD_BUTTON_TOP_FORMAT,
        index))));
  }

  public boolean isTagCloudPresent() {
    return isElementDisplayed(tagCloud);
  }

  public PopularSearchesPage clickPopularSearchesLink() {
    popularSearchesLink.click();
    return new PopularSearchesPage(driver);
  }

  @Override
  public HomePage clickSoftonicLogo() {
    softonicLogo.click();
    return new HomePage(driver);
  }

  public VideosHomePage clickAllVideos() {
    driver.get(allVideosLink.getAttribute("href"));
    return new VideosHomePage(driver);
  }

  public List<WebElement> getNewestSoftwareList() {
    return driver.findElements(By.xpath(NEWEST_SOFTWARE_XPATH));
  }

  public String getFirstCarrouselItemText() {
    return firstCarrouselItem.getText();
  }

  public int getCarrouselImageHeight() {
    return carrouselImage.getSize().getHeight();
  }

  public int getCarrouselImageWidth() {
    return carrouselImage.getSize().getWidth();
  }

  public boolean isOneHomePresent() {
    return isElementDisplayed(oneHomeCarrousel);
  }

  public boolean isTopDownloadsOneHomePresent() {
    return isElementDisplayed(topDownloadsOneHome);
  }

  public String getImageCarrouselUrlOnHomepage() {
    return carrouselImageOnHomePage.getAttribute("src");
  }

  public PlatformHomePage hoverOnSearchCombo() {
    scrollIntoView(searchCombo);
    return new PlatformHomePage(driver);
  }

  public PlatformHomePage clickOnSearchCombo() {
    searchCombo.click();
    return new PlatformHomePage(driver);
  }

  public int getNumberOfOSonSearchCombo() {
    return driver.findElements(By.xpath(SEARCH_LIST_COMBO)).size();
  }

  public PlatformHomePage hoverOnSearchList() {
    scrollIntoView(searchListCombo);
    return new PlatformHomePage(driver);
  }

  public boolean isListSearchComboVisible() {
    return isElementDisplayed(searchListCombo);
  }

  public boolean isListSearchAllSoftwareCheckboxPresent() {
    return isElementDisplayed(searchAllSoftwareCheckbox);
  }

  public PlatformHomePage clickOnAllSoftwareSearch() {
    searchAllSoftwareCheckbox.click();
    return new PlatformHomePage(driver);
  }

  public String getSoftonicForWindowsLink() {
    return softonicForWindowsLink.getAttribute("href");
  }

  public PlatformHomePage displaySubcategories(String categoryName) {
    scrollIntoView(getCategory(categoryName));
    return this;
  }

  private WebElement getCategory(String categoryName) {
    int i = 0;
    boolean categoryMatch = getCategoryName(i).equals(categoryName);
    while (i < categories.size() && !categoryMatch) {
      i++;
      categoryMatch = getCategoryName(i).equals(categoryName);
    }
    if (!categoryMatch) {
      throw new NoSuchElementException("Not found category '" + categoryName + "'");
    }
    return categories.get(i);
  }

  public String getCategoriesTitle() {
    return categoriesTitle.getText();
  }

  public boolean areSubcategoriesDisplayed(String categoryName) {
    return getCategory(categoryName).findElement(By.tagName("ul")).isDisplayed();
  }

  public boolean hasSubcategories(String categoryName) {
    List<WebElement> subcategories = getCategory(categoryName).findElements(By.xpath(".//ul/li"));
    boolean result = subcategories.size() > 0;
    for (WebElement subcategory : subcategories) {
      result = result && !subcategory.getText().isEmpty();
    }
    return result;
  }

  public List<String> getCategoryNames() {
    List<String> names = new ArrayList<String>();
    for (int i = 0; i < categories.size(); i++) {
      names.add(getCategoryName(i));
    }
    return names;
  }

  public int getCategoriesCount() {
    return categories.size();
  }

  public String getCategoryName(int i) {
    return getCategory(i).findElement(By.tagName("strong")).getText();
  }

  public WebElement getCategory(int i) {
    return categories.get(i);
  }

  public PlatformHomePage displaySubcategories(int i) {
    scrollIntoView(getCategory(i));
    return this;
  }
  public boolean areSubcategoriesDisplayed(int i) {
    return getCategory(i).findElement(By.tagName("ul")).isDisplayed();
  }

  public boolean hasCategoryIndexSubcategories(int i) {
    List<WebElement> subcategories = driver.findElements(
        By.xpath(String.format(SUBCATEGORIES_FORMAT, i + 1))); //i+1 because xpaths index start at 1
    boolean result = subcategories.size() > 0;
    for (WebElement subcategory : subcategories) {
      result = result && !subcategory.getText().isEmpty();
    }
    return result;
  }

  public int tabIndexButtonsCount(int tabIndex) {
    return driver.findElements(By.xpath(String.format(TAB_BUTTONS_FORMAT, tabIndex)))
        .size();
  }

  public boolean areTabIndexButtonsDisplayed(int tabIndex) {
    List<WebElement> buttons = driver.findElements(
        By.xpath(String.format(TAB_BUTTONS_FORMAT, tabIndex)));
    return areElementsPresent(buttons);
  }

  public int oneHomeCount() {
    return driver.findElements(By.id(ONE_HOME_ID)).size();
  }

  public int topDownloadsOneHomeCount() {
    return driver.findElements(By.id(TOP_DOWNLOADS_ONE_HOME_ID)).size();
  }

  public void checkBoxSearch(String value) {
    WebElement checkBox =
        driver.findElement(By.xpath(String.format(SEARCH_BOX_OPTION_FORMAT, value)));
    if (!checkBox.isSelected()) {
      checkBox.click();
    }
  }

  public String getSearchFilterText() {
    return searchCombo.getText().trim();
  }

  public Object downloadButtonsTopCategoryCount() {
    return downloadButtonsTopCategory.size();
  }

  public boolean areDownloadButtonsTopCategoryPresent() {
    return areElementsPresent(downloadButtonsTopCategory);
  }

  public List<String> getTabIndexButtonsLinks(int tabIndex) {
    return getLinks(driver.findElements(By.xpath(String.format(TAB_BUTTONS_FORMAT,
        tabIndex))));
  }

  public List<String> getDownloadButtonsTopCategoryLinks() {
    return getLinks(downloadButtonsTopCategory);
  }

  private List<String> getLinks(List<WebElement> list) {
    List<String> links = new ArrayList<String>();
    for (WebElement element : list) {
      links.add(element.getAttribute("href"));
    }
    return links;
  }

  public int tabIndexRowsCount(int tabIndex) {
    return driver.findElements(By.xpath(String.format(TAB_ROWS_FORMAT, tabIndex))).size();
  }

  public int tabIndexRowIndexButtonsCount(int tabIndex, int rowIndex) {
    return driver.findElement(By.xpath(String.format(TAB_ROW_FORMAT, tabIndex, rowIndex)))
        .findElements(By.xpath(BUTTONS)).size();
  }
}
