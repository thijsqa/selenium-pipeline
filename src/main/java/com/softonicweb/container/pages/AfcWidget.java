package com.softonicweb.container.pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.softonicweb.container.seleniumframework.AbstractWebPage;
import com.softonicweb.container.seleniumframework.AbstractWidget;

/**
 * A reusable element to provide standardized Google Adsense for Content
 * manipulation.
 *
 * @param <T> parent page
 *
 * @author guillem.hernandez
 */
public class AfcWidget<T extends AbstractWebPage> extends AbstractWidget<T> {

  private static final String AFC_CONTAINER_IFRAME = "//iframe[contains(@id,'Programpage_0')]";

  private static final String PDPAFC_CONTAINER_IFRAME =
      "//iframe[contains(@id,'Postdownload/ATF_MPU_First_0')]";

  private static final String AD_LOCATION = "grid-link-container";

  private static final String TARGET_LOCATION = "grid-link";

  @FindBy(xpath = ".//*[contains(@class,'triblock')]")
  private WebElement triblockModule;

  @FindBy(xpath = ".//*[contains(@class,'afc-download-block')]")
  private WebElement afcBlock;

  @FindBy(css = "[data-auto=contextual-advertisement]")
  private WebElement contextualAdsContainer;

  @FindBy(xpath = "id('g_ads')/div/span/a")
  private List<WebElement> googleAdsList;

  public AfcWidget(T page) {
    super(page);
  }

  public boolean isTriblockPresent() {
    wait.forElementVisible(triblockModule);
    return isElementDisplayed(triblockModule);
  }

  public boolean isWidgetPresent() {
    return contextualAdsContainer.isDisplayed();
  }

  public int getAfcAdCount() {
    return googleAdsList.size();
  }

  public ProgramPage clickAdByIndex(int index) {
    googleAdsList.get(index).click();
    return new ProgramPage(driver);
  }

  public String getReceivedAfcGoogleAds() {
    return executeJavascriptCode("return Namespace.globals.afc.aBlocks[0].aGoogleAds.length;");
  }

  public String getDisplayedAfcGoogleAds() {
    return executeJavascriptCode("return "
      + "document.getElementById(Namespace.globals.afc.aBlocks[0].sOutput)."
      + "querySelectorAll('.google_ad_content').length;");
  }

  public boolean hasCorrectAfcAds() {
    return getDisplayedAfcGoogleAds().equals(getReceivedAfcGoogleAds());
  }

  public boolean hasCorrectAfcContainer() {
    return isAfcContainerPresent() ? hasCorrectAfcIframe(AFC_CONTAINER_IFRAME) : false;
  }

  public boolean hasCorrectPdpAfcContainer() {
    return isAfcContainerPresent() ? hasCorrectPdpAfcIframe(PDPAFC_CONTAINER_IFRAME) : false;
  }

  public boolean isAfcContainerPresent() {
    return contextualAdsContainer.isDisplayed();
  }

  public boolean hasCorrectPdpAfcIframe(String iframe) {
    return hasPdpAfcAdTargetsIncorrect(iframe);
  }

  public boolean hasCorrectAfcIframe(String iframe) {
    return hasAfcAdTargetsIncorrect(iframe);
  }

  public boolean hasAfcAdTargetsIncorrect(String iframe) {
    boolean afcAdsCorrect = true;
    scrollDown();
    try {
      driver.switchTo().frame(driver.findElement(By.xpath(iframe)));
    } catch (StaleElementReferenceException e) {
      wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By
          .xpath("//iframe[contains(@id,'Programpage_0')]")));
      driver.switchTo().frame(driver.findElement(By.xpath(iframe)));
    }
    List<WebElement> ads = driver.findElements(By.className(AD_LOCATION));
    for (WebElement ad : ads) {
      if (!isAfcAdTargetCorrect(ad)) {
        afcAdsCorrect = false;
        break;
      }
    }
    driver.switchTo().defaultContent();
    return afcAdsCorrect;
  }

  public boolean hasPdpAfcAdTargetsIncorrect(String iframe) {
    boolean afcAdsCorrect = true;
    driver.switchTo().frame(driver.findElement(By.xpath(iframe)));
    List<WebElement> ads = driver.findElements(By.className(AD_LOCATION));
    for (WebElement ad : ads) {
      if (!isAfcAdTargetCorrect(ad)) {
        afcAdsCorrect = false;
        break;
      }
    }
    driver.switchTo().defaultContent();
    return afcAdsCorrect;
  }

  public boolean isAfcAdTargetCorrect(WebElement ad) {
    WebElement target = ad.findElement(By.className(TARGET_LOCATION));
    String a = target.getAttribute("target");
    return a.equals("_top");
  }

  public boolean verifyAdsOpenNewTab() {
    if (afcBlock.isDisplayed()) {
      List<WebElement> adBlocks = afcBlock.findElements(By.xpath("div/div"));
      for (WebElement ad : adBlocks) {
        if (!ad.findElement(By.xpath("a")).getAttribute("target").equals("_blank")) {
          return false;
        }
      }
    } else {
      return false;
    }
    return true;
  }

  public boolean verifyAfcAds(ArrayList<String> expectedAfcAds) {
    List<WebElement> foundAfcAds = contextualAdsContainer.findElements(By.xpath("div"));
    @SuppressWarnings("unchecked")
    ArrayList<String> markingAfc = (ArrayList<String>) expectedAfcAds.clone();
    boolean flag;
    for (String expectedAfcAd : expectedAfcAds) {
      flag = false;
      for (WebElement foundAfcAd : foundAfcAds) {
        if (getInnerHTML(foundAfcAd).equals(expectedAfcAd)) {
          flag = true;
          markingAfc.remove(expectedAfcAd);
          break;
        }
      }
      if (!flag) {
        return false;
      }
    }
    return (markingAfc.isEmpty());
  }

  public String getAdURL(int i) {
    return googleAdsList.get(i).getAttribute("href");
  }

  public boolean isAdPresent(int index) {
    return googleAdsList.get(index).isDisplayed();
  }

  public boolean isAdOpenInNewWindow(int index) {
    return googleAdsList.get(index).getAttribute("target").equals("_blank");
  }

  public String getAdLinkTitle(int index) {
    return googleAdsList.get(index).getAttribute("title");
  }

  /* Returns the company name of an ad.
   * Examples:
   * http://www.google.com/test --> google
   * www.something.com --> something
   * http://test.com --> test
   */
  public String getAdCompanyName(int index) {
    String title = getAdLinkTitle(index);
    title = title.replace("http://", "");
    title = title.replace("www.", "");
    if (title.contains("/")) {
      title = title.substring(0, title.indexOf("/"));
    }
    title = title.substring(0, title.lastIndexOf("."));
    return title;
  }
}
