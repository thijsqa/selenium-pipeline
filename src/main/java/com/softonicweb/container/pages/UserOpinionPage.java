package com.softonicweb.container.pages;

import org.openqa.selenium.WebDriver;

/**
 * Contains methods to interact with the Softonic User Opinion Page Method
 *
 * @author guillem.hernandez
 */
public class UserOpinionPage extends AbstractSoftonicWebPage {

  public UserOpinionPage(WebDriver driver) {
    super(driver);
  }
}
