package com.softonicweb.container.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Contains methods to interact with the Softonic Articles page.
 *
 * @author guillem.hernandez
 */
public class ContentPage extends AbstractSoftonicWebPage {

  private static final String ARTICLES_ON_COLUMN = "//div[@id='cols-container']/div[%s]/article";

  private static final String ARTICLE_TITLE_LINK = "[%s]/h3/a";

  private static final String ARTICLE_COMMENT_LINK = "[%s]/footer/a";

  private static final String TWO_MAIN_HIGHLIGHTS = "//article[@class='post-simple-highlights']";

  private static final String NUMBER_OF_COLUMNS = "//div[@id='cols-container']/div";

  private static final String TAG_INDEX = "//ul[contains(@class,'tag-cloud')]/li[%s]//a";

  @FindBy(xpath = "//div[@class='mainbar big-onehome']")
  private WebElement oneBigHome;

  @FindBy(xpath = "//div[@class='mainbar']")
  private WebElement searchTagCloud;

  @FindBy(xpath = "//div[@class='firstbar']")
  private WebElement facebookWidget;

  @FindBy(id = "section_tops")
  private WebElement tops;

  @FindBy(xpath = "//div[@id='cols-container']/div/article")
  private List<WebElement> articles;

  @FindBy(xpath = "//ul[contains(@class,'tag-cloud')]/li")
  private List<WebElement> tags;




  public ContentPage(WebDriver driver) {
    super(driver);
  }

  public boolean areTwoMainHighlightsPresent() {
    if (driver.findElements(By.xpath(TWO_MAIN_HIGHLIGHTS)).size() == 2) {
      return true;
    }
    return false;
  }

  public int getNumberOfTags() {
    return tags.size();
  }

  public int numberOfColumns() {
    return driver.findElements(By.xpath(NUMBER_OF_COLUMNS)).size();
  }

  public int numberOfArticlesPerColumn(int index) {
    String articlesColumn = String.format(ARTICLES_ON_COLUMN, index);
    return driver.findElements(By.xpath(articlesColumn)).size();
  }

  public String getTagUrl(int index) {
    WebElement element = driver.findElement(By.xpath(String.format(TAG_INDEX, index)));
    return element.getAttribute("href").toString();
  }

  public String getArticleTitleUrl(int index1, int index2) {
    String articlesColumn = String.format(ARTICLES_ON_COLUMN, index1) + ARTICLE_TITLE_LINK;
    WebElement articleTitle = driver.findElement(By.xpath(String.format(articlesColumn, index2)));
    return articleTitle.getAttribute("href").toString();
  }

  public String getArticleCommentUrl(int index1, int index2) {
    String articlesColumn = String.format(ARTICLES_ON_COLUMN, index1) + ARTICLE_COMMENT_LINK;
    WebElement articleComment = driver.findElement(By.xpath(String.format(articlesColumn, index2)));
    return articleComment.getAttribute("href").toString();
  }

  public boolean isOneBigHomePresent() {
    wait.forElementPresent(oneBigHome);
    return isElementDisplayed(oneBigHome);
  }

  public boolean isSearchTagPresent() {
    wait.forElementPresent(searchTagCloud);
    return isElementDisplayed(searchTagCloud);
  }

  public boolean isFacebookWidgetPresent() {
    wait.forElementPresent(facebookWidget);
    return isElementDisplayed(facebookWidget);
  }

  public boolean isTopDownloadsPresent() {
    wait.forElementPresent(tops);
    return isElementDisplayed(tops);
  }

  public int getNumberArticles() {
    return articles.size();
  }

  public boolean areArticlesPresent() {
    return areElementsPresent(articles);
  }

  public boolean areTagsPresent() {
    return areElementsPresent(tags);
  }

  public String getArticleTitleUrl(int articleIndex) {
    return articles.get(articleIndex).findElement(By.xpath("./h3/a"))
        .getAttribute("href").toString();
  }

  public String getArticleCommentUrl(int articleIndex) {
    return articles.get(articleIndex).findElement(By.xpath("./footer/a"))
        .getAttribute("href").toString();
  }

}
