package com.softonicweb.container.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Contains methods to interact with the Softonic Support Page
 *
 * @author thijs.vandewynckel
 */
public class SupportPage extends AbstractSoftonicWebPage {

  private static final String VOTING_ICONS = "//div[@class='js-survey-pre']";

  @FindBy(xpath = "//article/img[@class='support-app__icon']")
  private WebElement appIcon;
  @FindBy(xpath = "//p[@class='support-app__name']")
  private WebElement supportAppName;
  @FindBy(xpath = "//p[@class='support-app__version']")
  private WebElement appVersion;
  @FindBy(xpath = "//h1[@class='page-title']")
  private WebElement supportTitle;
  @FindBy(id = "how-to-install")
  private WebElement video;
  @FindBy(xpath = "//nav[@class='support-nav']")
  private WebElement navigationMenu;
  @FindBy(css = "[data-auto=relaunch-download-button]")
  private WebElement relaunchDownload;
  @FindBy(id = "where-is-my-download")
  private WebElement whereIsDownload;
  @FindBy(id = "is-this-a-free-program")
  private WebElement freeDownload;
  @FindBy(id = "long-time-download")
  private WebElement longDownload;
  @FindBy(id = "crosspromotion-solutions-problems")
  private WebElement solutionsModule;

  public SupportPage(WebDriver driver) {
    super(driver);
  }

  public boolean isAppIconPresent() {
    return isElementDisplayed(appIcon);
  }

  public boolean isSupportAppNamePresent() {
    return isElementDisplayed(supportAppName);
  }

  public boolean isAppVersionPresent() {
    return isElementDisplayed(appVersion);
  }

  public boolean isSupportTitlePresent() {
    return isElementDisplayed(supportTitle);
  }

  public boolean isVideoPresent() {
    return isElementDisplayed(video);
  }

  public boolean isNavigationMenuPresent() {
    return isElementDisplayed(navigationMenu);
  }

  public boolean isRelaunchDownloadPresent() {
    return isElementDisplayed(relaunchDownload);
  }

  public int getVotingIconsCount() {
    return driver.findElements(By.xpath(VOTING_ICONS)).size();
  }

  public boolean isWhereIsDownloadPresent() {
    return isElementDisplayed(whereIsDownload);
  }

  public boolean isFreeProgramPresent() {
    return isElementDisplayed(freeDownload);
  }

  public boolean isLongDownloadPresent() {
    return isElementDisplayed(longDownload);
  }

  public boolean isSolutionsModulePresent() {
    return isElementDisplayed(solutionsModule);
  }
}
