package com.softonicweb.container.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.softonicweb.container.seleniumframework.AbstractWebPage;
import com.softonicweb.container.seleniumframework.AbstractWidget;

/**
 * A reusable element to provide standardized Google Plus Social button
 * manipulation.
 *
 * @param <T> parent page
 *
 * @author guillem.hernandez
 */
public class GooglePlusWidget<T extends AbstractWebPage> extends AbstractWidget<T> {

  @FindBy(xpath =
      ".//li[@class='google-option']/span/iframe[contains(@src,'https://apis.google.com/')]")
  private WebElement googlePlusFrame;

  @FindBy(id = "aggregateCount")
  private WebElement plusOneCounter;

  public GooglePlusWidget(T page) {
    super(page);
  }

  public void clickGooglePlusButton() {
    driver.switchTo().frame(googlePlusFrame);
    WebElement b = driver.findElement(By.xpath(".//*[@id='button']"));
    b.click();
    driver.switchTo().defaultContent();
    wait.pause(POPUP_TIMEOUT);
  }

  public boolean isGooglePlusButtonPresent() {
    wait.pause(POPUP_TIMEOUT);
    return isElementDisplayed(googlePlusFrame);
  }

  public int getPlusOneCounter() {
    driver.switchTo().frame(googlePlusFrame);
    int counter = Integer.parseInt(plusOneCounter.getText());
    driver.switchTo().defaultContent();
    return counter;
  }
}
