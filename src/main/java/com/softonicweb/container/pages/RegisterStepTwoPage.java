package com.softonicweb.container.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Contains methods to interact with the Softonic registration confirmation page. <p>eg.
 * http://www.softonic.com/registro-2
 *
 * @author josemaria.delbarco
 * @author guillem.hernandez
 */
public class RegisterStepTwoPage extends AbstractSoftonicWebPage {

  @FindBy(xpath = "id('content')/div[2]/p[1]/strong")
  private WebElement emailAlertDiv;

  public RegisterStepTwoPage(WebDriver driver) {
    super(driver);
  }

  public boolean assertOnPage() {
    return isElementDisplayed(emailAlertDiv);
  }
}
