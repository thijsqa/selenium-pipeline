package com.softonicweb.container.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Contains methods to interact with the starting step on the user registration modal window.
 *
 * @author guillem.hernandez
 */
public class StartQuickRegisterModalWindow extends AbstractSoftonicWebPage {

  @FindBy(id = "closeBut")
  private WebElement closeButton;

  @FindBy(xpath = "id('start_process')/p/a/strong")
  private WebElement loginButton;

  @FindBy(xpath = "id('start_process')/ul/li[2]/a")
  private WebElement newAccountButton;

  @FindBy(xpath = "id('start_process')/ul/li[1]/button")
  private WebElement facebookConnectButton;

  public StartQuickRegisterModalWindow(WebDriver driver) {
    super(driver);
  }

  public NewUserModalWindow clickNewAccount() {
    wait.forElementPresent(closeButton);
    newAccountButton.click();
    return new NewUserModalWindow(driver);
  }

  public QuickRegisterLoginModalWindow clickLoginQuickRegister() {
    wait.forElementPresent(closeButton);
    loginButton.click();
    return new QuickRegisterLoginModalWindow(driver);
  }

  public NewUserModalWindow clickFacebookConnect() {
    wait.forElementPresent(closeButton);
    facebookConnectButton.click();
    return new NewUserModalWindow(driver);
  }

  public OpinionThanksPage waitingForFacebookLoginRefresh() {
    wait.forElementNotPresent(closeButton);
    return new OpinionThanksPage(driver);
  }
}
