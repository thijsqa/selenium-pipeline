package com.softonicweb.container.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.softonicweb.container.pages.OpinionThanksPage;

/**
 * Contains methods to interact with the initial Quick Register Modal window.
 *
 * @author guillem.hernandez
 */
public class StartQuickRegisterModal extends AbstractSoftonicWebPage {

  @FindBy(xpath = "id('start_process')/div/a")
  private WebElement newAccountButton;

  @FindBy(xpath = "id('start_process')/p/a/strong")
  private WebElement loginButton;

  @FindBy(xpath = "id('start_process')/div/button")
  private WebElement facebookConnectButton;

  public StartQuickRegisterModal(WebDriver driver) {
    super(driver);
  }

  public NewUserModal clickNewAccount() {
    newAccountButton.click();
    return new NewUserModal(driver);
  }

  public QuickRegisterLoginModal clickLoginQuickRegister() {
    loginButton.click();
    return new QuickRegisterLoginModal(driver);
  }

  public SocialMediaPopUpExternalModal clickFacebookConnect() {
    facebookConnectButton.click();
    return new SocialMediaPopUpExternalModal(driver);
  }

  public OpinionThanksPage waitingForFacebookLoginRefresh() {
    return new OpinionThanksPage(driver);
  }
}
