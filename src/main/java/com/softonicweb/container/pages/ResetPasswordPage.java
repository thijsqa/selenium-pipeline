package com.softonicweb.container.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Contains methods to interact with the Softonic reset password page.
 *
 * @author ivan.solomonoff
 */
public class ResetPasswordPage extends AbstractSoftonicWebPage {

  @FindBy(id = "password")
  private WebElement passwordTextbox;
  @FindBy(id = "passwordRetype")
  private WebElement retypePasswordTextbox;
  @FindBy(xpath = "//input[@value = 'Submit']")
  private WebElement saveButton;
  @FindBy(xpath = "//label[@id = 'lblSuccess']")
  private WebElement confirmationMessage;

  public ResetPasswordPage(WebDriver driver) {
    super(driver);
  }

  public ResetPasswordPage enterPassword(String password) {
    sendKeysThreadSafe(passwordTextbox, password);
    return this;
  }

  public ResetPasswordPage enterRetypedPassword(String password) {
    sendKeysThreadSafe(retypePasswordTextbox, password);
    return this;
  }

  public ResetPasswordPage clickSavePassword() {
    saveButton.click();
    return this;
  }

  public boolean isConfirmationMessageShown() {
    return isElementDisplayed(confirmationMessage);
  }
}
