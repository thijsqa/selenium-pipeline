package com.softonicweb.container.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Contains methods to interact with the Articles Home Page
 *
 * @author thijs.vandewynckel
 */

public class ArticlesHomePage extends AbstractSoftonicWebPage {

  private static final String CARD_ITEMS = "//article[contains(@class,'card')]";

  @FindBy(id = "onehome")
  private WebElement oneHome;
  @FindBy(css = "[data-auto=share-twitter-button] a")
  private WebElement twitterButton;

  public ArticlesHomePage(WebDriver driver) {
    super(driver);
  }

  public boolean isOneHomePresent() {
    return isElementDisplayed(oneHome);
  }

  public int getCardsCount() {
    return driver.findElements(By.xpath(CARD_ITEMS)).size();
  }

  public ArticlesHomePage clickTwitterButton() {
    twitterButton.click();
    return this;
  }
}
