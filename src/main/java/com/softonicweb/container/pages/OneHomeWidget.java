package com.softonicweb.container.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.softonicweb.container.seleniumframework.AbstractWebPage;
import com.softonicweb.container.seleniumframework.AbstractWidget;

/**
 * Methods to interact with softonic Onehome page.
 *
 * @param <T> parent page
 *
 * @author guillem.hernandez
 * @author antonio.martin
 */
public class OneHomeWidget<T extends AbstractWebPage> extends AbstractWidget<T> {

  private static final String ONEHOME_CARROUSEL_IMAGES_LIST = "id('carrousel-list')/li";

  @FindBy(xpath = "//li[@class='next']")
  private WebElement carrouselNext;

  @FindBy(xpath = "//li[@class='previous ']")
  private WebElement carrouselPrev;

  @FindBy(xpath = "id('carrousel-list')/li")
  private List<WebElement> slides;

  private static final String CARROUSEL_IMAGE_FORMAT = "id('carrousel_item_%s')/a/img";
  private static final int TRANSITION_WAIT = 1000; // Transition time between slides in milliseconds

  public OneHomeWidget(T page) {
    super(page);
  }

  public boolean isCarrouselImagePresent(int index) {
    return driver.findElement(By.xpath(String.format(CARROUSEL_IMAGE_FORMAT, index))).isDisplayed();
  }

  public boolean isOneHomeOnTheFirstImage() {
    return !isElementDisplayed(carrouselPrev);
  }

  public boolean isOneHomeOnTheLastImage() {
    return !isElementDisplayed(carrouselNext);
  }

  public void clickOnNext() {
    carrouselNext.click();
    wait.pause(TRANSITION_WAIT);
  }

  public void clickOnPrev() {
    carrouselPrev.click();
    wait.pause(TRANSITION_WAIT);
  }

  public int getNumberImagesCount() {
    return driver.findElements(By.xpath(ONEHOME_CARROUSEL_IMAGES_LIST)).size();
  }

  public boolean isLeftArrowPresent() {
    return isElementDisplayed(carrouselPrev);
  }

  public boolean isRightArrowPresent() {
    return isElementDisplayed(carrouselNext);
  }

  public void moveToFirstSlide() {
    while (isLeftArrowPresent()) {
      clickOnPrev();
    }
  }

  public void moveToLastSlide() {
    while (isRightArrowPresent()) {
      clickOnNext();
    }
  }

  public List<WebElement> getSlides() {
    return slides;
  }

  public boolean isSlideImagePresent(WebElement slide) {
    return isElementDisplayed(slide.findElement(By.tagName("img")));
  }
}
