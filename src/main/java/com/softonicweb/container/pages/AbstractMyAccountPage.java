package com.softonicweb.container.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Contains methods to interact with the my details page model on the user profile.
 *
 * @author guillem.hernandez
 */
public abstract class AbstractMyAccountPage extends AbstractSoftonicWebPage {

  @FindBy(xpath = "//section[@class='user-profile__section-container']/ul/li[2]/a")
  private WebElement deleteAccountLink;
  @FindBy(xpath = "//div[@class='modal-window__content']/a")
  private WebElement deleteAccountButton;

  public AbstractMyAccountPage(WebDriver driver) {
    super(driver);
  }

  public DeleteMyAccountPage clickDeleteAccount() {
    scrollIntoView(deleteAccountLink);
    deleteAccountLink.click();
    return new DeleteMyAccountPage(driver);
  }

  public DeleteMyAccountPage clickDeleteButton() {
    scrollIntoView(deleteAccountButton);
    deleteAccountButton.click();
    return new DeleteMyAccountPage(driver);
  }
}
