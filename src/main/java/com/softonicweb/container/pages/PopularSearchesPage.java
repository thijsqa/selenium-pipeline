package com.softonicweb.container.pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Contains methods to interact with the Softonic popular search page in any Softonic plaftorm.
 *
 * @author antonio.martin
 */
public class PopularSearchesPage extends AbstractSoftonicWebPage {

  @FindBy(xpath = "//ul[contains(@class,'tag-cloud')]/li")
  private List<WebElement> tagCloudList;

  public PopularSearchesPage(WebDriver driver) {
    super(driver);
  }

  public boolean isTagCloudPresent() {
    return areElementsPresent(tagCloudList);
  }
}
