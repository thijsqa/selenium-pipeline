package com.softonicweb.container.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.softonicweb.container.framework.Accounts;
import com.softonicweb.container.framework.RandomAccount;
import com.softonicweb.container.seleniumframework.AbstractWidget;

/**
 * Contains methods to interact with the Softonic user login modal.
 *
 * @author ivan.solomonoff
 */
public class LoginModal<T extends AbstractSoftonicWebPage> extends AbstractWidget<T> {

  @FindBy(xpath = "id('gigya-login-screen')/form/div[1]/div[3]/div[1]/input")
  private WebElement email;
  @FindBy(xpath = "id('gigya-login-screen')/form/div[1]/div[3]/div[2]/input")
  private WebElement password;
  @FindBy(xpath = "id('gigya-login-screen')/form/div[1]/div[3]/div[5]/input")
  private WebElement submitButton;
  @FindBy(xpath = "id('gigya-modal-plugin-container-showScreenSet_social_0_uiContainer')"
      + "//span[@title='Facebook']")
  private WebElement fbConnectButton;
  @FindBy(id = "google-signin")
  private WebElement googleSignInButton;

  public LoginModal(T page) {
    super(page);
  }

  public LoginModal<T> enterEmail(String emailStr) {
    email.sendKeys(emailStr);
    return this;
  }

  public LoginModal<T> enterPassword(String passStr) {
    password.sendKeys(passStr);
    return this;
  }

  public T login(Accounts user) {
    return enterEmail(user.getEmail()).enterPassword(user.getPassword()).submit();
  }

  public T loginUser(RandomAccount user) {
    return enterEmail(user.getEmail()).enterPassword(user.getPassword()).submit();
  }

  public T submit() {
    submitButton.click();
    wait.forPageAndAjaxToLoad();
    return getParentPage();
  }

  public SocialMediaPopUpExternalModal clickConnectWithFacebook() {
    fbConnectButton.click();
    return new SocialMediaPopUpExternalModal(driver);
  }

  public SocialMediaPopUpExternalModal clickConnectWithGoogle() {
    googleSignInButton.click();
    return new SocialMediaPopUpExternalModal(driver);
  }
}
