package com.softonicweb.container.pages;

import java.math.BigInteger;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.softonicweb.container.framework.Accounts;
import com.softonicweb.container.pages.OpinionThanksPage;

/**
 * Contains methods to interact with a generic Social Media Pop up window.
 *
 * @author guillem.hernandez
 */
public class SocialMediaPopUpExternalModal extends AbstractSoftonicWebPage {

  protected static final long CONFIRM_WAIT = 3650; // ms

  private static final String EMAIL_TWITTER = "username_or_email";
  private static final String PASSWORD_TWITTER = "password";
  private static final String SUBMIT_TWITTER = "allow";
  private static final String TWEET_TEXT_TWITTER = "status";

  private static final String EMAIL_FACEBOOK = "email";
  private static final String PASSWORD_FACEBOOK = "pass";
  private static final String SUBMIT_FACEBOOK = "//*[@id='u_0_0']";

  private static final String EMAIL_GPLUS = "identifierId";
  private static final String NEXT_GPLUS = "id('identifierNext')";
  private static final String PASSWORD_GPLUS = "[name=password]";
  private static final String SUBMIT_GPLUS = "id('passwordNext')";

  private final String mainWindowHandle;

  /**
   * Enum for the titles of the external login modal window.
   * Used to manager the window change.
   *
   * @author guillem.hernandez
   */
  private enum ModalWindowTitle {
    FACEBOOK("Facebook"), TWITTER("Twitter"), GOOGLEPLUS("Google+"), GOOGLE("Google");

    private final String title;

    private ModalWindowTitle(String title) {
      this.title = title;
    }

    private String getTitle() {
      return title;
    }
  }

  public SocialMediaPopUpExternalModal(WebDriver driver) {
    super(driver);
    mainWindowHandle = driver.getWindowHandle();
    wait.forPageAndAjaxToLoad();
  }

  private void loginFacebook(Accounts user, String email, String password, String submit,
      String title) {
    try {
      switchToNewWindow();
      wait.forElementPresent(By.id(email));
      // sendKeysThreadSafe(driver.findElement(By.id(email)), user.getEmail());
      driver.findElement(By.id(email)).sendKeys(user.getEmail());
      wait.forElementPresent(By.id(password));
      // sendKeysThreadSafe(driver.findElement(By.id(password)),
      // user.getPassword());
      driver.findElement(By.id(password)).sendKeys(user.getPassword());
      // submit login
      wait.forElementPresent(By.xpath(submit));
      driver.findElement(By.xpath(submit)).click();
    } catch (UnhandledAlertException uae) {
      driver.switchTo().alert().dismiss();
    }
    Set<String> winHandles = driver.getWindowHandles();
    driver.switchTo().window((String) winHandles.toArray()[0]);
  }

  private void loginTwitter(Accounts user, String password, String allow, String title) {
    try {
      switchToNewWindow();
      wait.forElementPresent(By.id(EMAIL_TWITTER));
      // sendKeysThreadSafe(driver.findElement(By.id(email)), user.getEmail());
      driver.findElement(By.id(EMAIL_TWITTER)).sendKeys(user.getEmail());
      wait.forElementPresent(By.id(password));
      // sendKeysThreadSafe(driver.findElement(By.id(password)),
      // user.getPassword());
      driver.findElement(By.id(password)).sendKeys(user.getPassword());
      wait.forElementPresent(By.id(allow));
      driver.findElement(By.id(allow)).click();
    } catch (UnhandledAlertException uae) {
      driver.switchTo().alert().dismiss();
    }
    Set<String> winHandles = driver.getWindowHandles();
    driver.switchTo().window((String) winHandles.toArray()[0]);
  }

  public void loginOnTwitter() {
    loginTwitter(Accounts.TWITTER, PASSWORD_TWITTER, SUBMIT_TWITTER,
        ModalWindowTitle.TWITTER.getTitle());
  }

  public void loginOnFacebook() {
    loginFacebook(Accounts.FACEBOOK, EMAIL_FACEBOOK, PASSWORD_FACEBOOK, SUBMIT_FACEBOOK,
        ModalWindowTitle.FACEBOOK.getTitle());
  }

  public void loginOnFacebookDF() {
    loginFacebook(Accounts.FACEBOOK2, EMAIL_FACEBOOK, PASSWORD_FACEBOOK, SUBMIT_FACEBOOK,
        ModalWindowTitle.FACEBOOK.getTitle());
  }

  public void loginOnGoogleSignIn() {
    try {
      switchToNewWindow();
      wait.forElementPresent(By.id(EMAIL_GPLUS));
      // sendKeysThreadSafe(driver.findElement(By.id(EMAIL_GPLUS)),
      // Accounts.GOOGLEPLUS.getEmail());
      driver.findElement(By.id(EMAIL_GPLUS)).sendKeys(Accounts.GOOGLEPLUS.getEmail());
      wait.forElementPresent(By.xpath(NEXT_GPLUS));
      driver.findElement(By.xpath(NEXT_GPLUS)).click();
      wait.forElementPresent(By.cssSelector(PASSWORD_GPLUS));
      // sendKeysThreadSafe(driver.findElement(By.id(PASSWORD_GPLUS)),
      driver.findElement(By.cssSelector(PASSWORD_GPLUS))
          .sendKeys(Accounts.GOOGLEPLUS.getPassword());
      wait.forElementToBeClickable(By.xpath(SUBMIT_GPLUS));
      wait.pause(CONFIRM_WAIT);
      driver.findElement(By.xpath(SUBMIT_GPLUS)).click();
    } catch (UnhandledAlertException uae) {
      driver.switchTo().alert().dismiss();
    }
    Set<String> winHandles = driver.getWindowHandles();
    driver.switchTo().window((String) winHandles.toArray()[0]);
  }

  public void loginOnGoogleSolutionsSignIn() {
    try {
      switchToNewWindow();
      wait.forElementPresent(By.id(EMAIL_GPLUS));
      // sendKeysThreadSafe(driver.findElement(By.id(EMAIL_GPLUS)),
      // Accounts.GOOGLEPLUS.getEmail());
      driver.findElement(By.id(EMAIL_GPLUS)).sendKeys(Accounts.GOOGLEPLUS_SOLUTIONS.getEmail());
      wait.forElementPresent(By.xpath(NEXT_GPLUS));
      driver.findElement(By.xpath(NEXT_GPLUS)).click();
      wait.forElementPresent(By.cssSelector(PASSWORD_GPLUS));
      // sendKeysThreadSafe(driver.findElement(By.id(PASSWORD_GPLUS)),
      driver.findElement(By.cssSelector(PASSWORD_GPLUS)).sendKeys(
          Accounts.GOOGLEPLUS_SOLUTIONS.getPassword());
      wait.forElementToBeClickable(By.xpath(SUBMIT_GPLUS));
      wait.pause(CONFIRM_WAIT);
      driver.findElement(By.xpath(SUBMIT_GPLUS)).click();
    } catch (UnhandledAlertException uae) {
      driver.switchTo().alert().dismiss();
    }
    Set<String> winHandles = driver.getWindowHandles();
    driver.switchTo().window((String) winHandles.toArray()[0]);
  }

  public void loginOnGoogleDFSignIn() {
    try {
      switchToNewWindow();
      wait.forElementPresent(By.id(EMAIL_GPLUS));
      // sendKeysThreadSafe(driver.findElement(By.id(EMAIL_GPLUS)),
      // Accounts.GOOGLEPLUS.getEmail());
      driver.findElement(By.id(EMAIL_GPLUS)).sendKeys(Accounts.GOOGLEPLUS2.getEmail());
      wait.forElementPresent(By.xpath(NEXT_GPLUS));
      driver.findElement(By.xpath(NEXT_GPLUS)).click();
      wait.forElementPresent(By.cssSelector(PASSWORD_GPLUS));
      // sendKeysThreadSafe(driver.findElement(By.id(PASSWORD_GPLUS)),
      driver.findElement(By.cssSelector(PASSWORD_GPLUS)).sendKeys(
          Accounts.GOOGLEPLUS2.getPassword());
      wait.forElementToBeClickable(By.xpath(SUBMIT_GPLUS));
      wait.pause(CONFIRM_WAIT);
      driver.findElement(By.xpath(SUBMIT_GPLUS)).click();
    } catch (UnhandledAlertException uae) {
      driver.switchTo().alert().dismiss();
    }
    Set<String> winHandles = driver.getWindowHandles();
    driver.switchTo().window((String) winHandles.toArray()[0]);
  }

  public boolean isSignInModalWindowPresent(String title) {
    try {
      WebDriver popup = null;
      Set<String> windowIterator = driver.getWindowHandles();
      Iterator<String> windowIt = windowIterator.iterator();
      while (windowIt.hasNext()) {
        String windowHandle = windowIt.next();
        popup = driver.switchTo().window(windowHandle);
        if (popup.getTitle().contains(title)) {
          driver.switchTo().window(mainWindowHandle);
          return true;
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    driver.switchTo().defaultContent();
    return false;
  }

  public boolean isGoogleSignInModalWindowPresent() {
    return isSignInModalWindowPresent(ModalWindowTitle.GOOGLE.getTitle());
  }

  public boolean isFBSignInModalWindowPresent() {
    return isSignInModalWindowPresent(ModalWindowTitle.FACEBOOK.getTitle());
  }

  public boolean isTwitterSignInModalWindowPresent() {
    return isSignInModalWindowPresent(ModalWindowTitle.TWITTER.getTitle());
  }

  public OpinionThanksPage loginOnFacebookPopUpExternalWindowFromOpinionForm() {
    loginOnFacebook();
    return new OpinionThanksPage(driver);
  }

  public void fillTweet() {
    final int stringLength = 32;
    final int numBits = 100;
    try {
      WebDriver popup = null;
      Set<String> windowIterator = driver.getWindowHandles();
      Iterator<String> windowIt = windowIterator.iterator();
      while (windowIt.hasNext()) {
        String windowHandle = windowIt.next();
        popup = driver.switchTo().window(windowHandle);
        if (popup.getTitle().contains(ModalWindowTitle.TWITTER.getTitle())) {
          Random random = new Random();
          WebElement textArea = driver.findElement(By.id(TWEET_TEXT_TWITTER));
          sendKeysThreadSafe(textArea,
              "Random stuff " + new BigInteger(numBits, random).toString(stringLength));
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    driver.switchTo().defaultContent();
  }

}
