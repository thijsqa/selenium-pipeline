package com.softonicweb.container.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Contains methods to interact with the Softonic Opinion 1st form.
 *
 * @author guillem.hernandez
 */
public class OpinionFirstFormPage extends AbstractSoftonicWebPage {

  @FindBy(id = "body")
  private WebElement opininonTextArea;

  @FindBy(id = "title")
  private WebElement opinionTitle;

  @FindBy(id = "quick_register_email")
  private WebElement opinionsEmails;

  @FindBy(xpath = "//*[contains(@class,'finish_button')]")
  private WebElement finishOpinionButton;

  public OpinionFirstFormPage(WebDriver driver) {
    super(driver);
  }

  public OpinionFirstFormPage typeOpinion(String opinion) {
    sendKeysThreadSafe(opininonTextArea, opinion);
    return this;
  }

  public OpinionFirstFormPage typeTitle(String title) {
    sendKeysThreadSafe(opinionTitle, title);
    return this;
  }

  public OpinionFirstFormPage typeEmail(String email) {
    sendKeysThreadSafe(opinionsEmails, email);
    return this;
  }

  public StartQuickRegisterModal clickEndOpinion() {
    finishOpinionButton.click();
    return new StartQuickRegisterModal(driver);
  }
}
