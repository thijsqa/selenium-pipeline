package com.softonicweb.container.pages;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Duration;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Sleeper;

import com.softonicweb.container.framework.Accounts;
import com.softonicweb.container.framework.IAccount;
import com.softonicweb.container.framework.TestLogger;
import com.softonicweb.container.seleniumframework.AbstractWebPage;
import com.softonicweb.container.pages.HomePage;
import com.softonicweb.container.pages.PlatformHomePage;
import com.softonicweb.container.pages.OldLoginModal;
import com.softonicweb.container.pages.SocialMediaPopUpExternalModal;
import com.softonicweb.container.pages.ProfileRecentActivityPage;
import com.softonicweb.container.pages.ArticlesSearchResultsPage;
import com.softonicweb.container.pages.DownloadSearchResultsPage;
import com.softonicweb.container.pages.NewsSearchResultsPage;
import com.softonicweb.container.pages.AfcWidget;

/**
 * Contains methods to interact with elements common to all Softonic Web pages.
 *
 * @author josemaria.delbarco
 * @author guillem.hernandez
 * @author thijs.vandewynckel
 */
public class AbstractSoftonicWebPage extends AbstractWebPage {

  @FindBy(id = "header-login-link")
  private WebElement homeLoginLink;
  @FindBy(id = "user-logout")
  private WebElement logoutLink;
  @FindBy(xpath = "//a[contains(@class,'js-logout-button')]")
  private WebElement noodleLogoutLink;
  @FindBy(xpath = "//*[@id='header-site-user']/a[2]")
  private WebElement homeLogoutLink;
  @FindBy(xpath = "//input[contains(@name,'username')]")
  private WebElement loginEmailBox;
  @FindBy(css = "input[type='password']")
  private WebElement loginPasswordBox;
  @FindBy(xpath = "//div[contains(@class,'with-site-login')]/div[4]/input")
  private WebElement loginSubmit;
  @FindBy(xpath = "//input[contains(@class,'gigya-input-submit')]")
  private WebElement submitReconsent;
  @FindBy(id = "search_input")
  private WebElement searchBox;
  @FindBy(css = "[data-auto=search-input]")
  private WebElement noodleSearchBox;
  @FindBy(id = "user_login")
  private WebElement userProfileLink;
  @FindBy(xpath = "//div[@class='header__button header__button--username']/a")
  private WebElement noodleUserProfileLink;
  @FindBy(xpath = "//*[@id='header-site-user']/a")
  private WebElement homeUserProfileLink;
  @FindBy(id = "gigya-login-screen")
  private WebElement loginBox;
  @FindBy(xpath = "id('header-site-user')/div")
  private WebElement homeLoginBox;
  @FindBy(id = "fb_connect")
  private WebElement facebookConnectButton;
  @FindBy(xpath = "id('gigya-modal-plugin-container-showScreenSet_social_0_uiContainer')"
      + "//span/button[@title='Google+']")
  private WebElement googlePlusSignInButton;
  @FindBy(xpath = "id('platforms-selector')//a")
  private List<WebElement> platforms;
  @FindBy(className = "list-pulldown-platforms")
  private WebElement platformPulldown;
  @FindBy(id = "platforms-selector-more")
  private WebElement platformPulldownMore;
  @FindBy(id = "img-social-buttons")
  private WebElement socialMediaFakeImage;
  @FindBy(css = "[data-auto=share-google-button]")
  private WebElement googlePlusSocialMediaButton;
  @FindBy(css = "[data-auto=share-twitter-button]")
  private WebElement twitterSocialMediaButton;
  @FindBy(css = "[data-auto=share-fb-button]")
  private WebElement facebookSocialMediaButton;
  @FindBy(xpath = "//ol[@class='pagination']/li")
  private List<WebElement> paginationList;
  @FindBy(xpath = "//*[contains(@class,'msg_ok')]")
  private WebElement unsubscribedMessage;
  @FindBy(xpath = "//a[contains(@class,'optanon-allow-all')]")
  private WebElement noodleCookieWarning;
  @FindBy(xpath = "//div[@class='optanon-alert-box-body']")
  private WebElement cookieWarningBody;
  @FindBy(xpath = "//a[@class='ad-slider-content__close']")
  private WebElement slideIn;
  @FindBy(xpath = "id('popup-newsletter-software')/div[1]/a")
  private WebElement closeNewsletterPopup;
  @FindBy(xpath = "id('cookie-policies')/p/i")
  private WebElement cookieWarningClose;
  @FindBy(xpath = "id('gigya-login-form')/div[3]/div/a")
  private WebElement signupLink;
  @FindBy(xpath = "id('header')/a")
  private WebElement softonicLogo;
  @FindBy(css = "[data-auto=btn-report-software]")
  private WebElement reportSoftware;
  @FindBy(css = "[data-auto=related-articles]")
  private WebElement articlesSection;
  @FindBy(css = "[data-auto=app-related-searches]")
  private WebElement relatedSearches;
  @FindBy(xpath = "//section[@id='related-articles']/div[2]/a")
  private WebElement readMoreButton;
  @FindBy(css = "[data-auto=app-icon]")
  private WebElement programIcon;
  @FindBy(xpath = "//h1[contains(@class,'app-download-promoted-cover__name')]")
  private WebElement pageTitle;
  @FindBy(xpath = "//div/p[contains(@class,'app-download-promoted-cover__claim')]")
  private WebElement downloadInfo;
  @FindBy(xpath = "//*[@class='attribution' or @id='sponsorTitle']")
  private WebElement sadsSponsoredMsg;
  @FindBy(xpath = "//a[@class='logo-link' or @id='sadsicon']")
  private WebElement sadsProgramIcon;
  @FindBy(xpath = "//div[@class='logo']/a")
  private WebElement sadsPpProgramIcon;
  @FindBy(xpath = "//*[@class='advertiser' or @class='spontitle leftContent']")
  private WebElement sadsProgramTitle;
  @FindBy(xpath = "//div[@class='cu-app__content']//h3[contains(@class,'media-app__title')]")
  private WebElement sadsPpProgramTitle;
  @FindBy(xpath = "//span[@id='license']")
  private WebElement sadsProgramLicense;
  @FindBy(xpath = "//span[@id='programLanguage']")
  private WebElement sadsProgramLanguage;
  @FindBy(xpath = "//*[@class='body-link' or @class='spondesc leftContent']")
  private WebElement sadsProgramDescription;
  @FindBy(xpath = "//div[@class='cu-app__content']//h4[contains(@class,'media-app__summary')]")
  private WebElement sadsPpProgramDescription;
  @FindBy(xpath = "//a[@class='call-to-action-link' or @id='ctaButtonLink']")
  private WebElement sadsDownloadButton;
  @FindBy(xpath = "//a[@class='cta-button']")
  private WebElement sadsPpDownloadButton;
  @FindBy(css = "[data-auto=app-name]")
  private WebElement programTitle;
  @FindBy(xpath = "//ul[contains(@class,'app-download-info__list--horizontal')]")
  private WebElement programInfo;
  @FindBy(css = "[data-auto=app-info]")
  private WebElement programDownloadInfo;
  @FindBy(css = "[data-auto=top-downloads]")
  private WebElement topDownloads;
  @FindBy(css = "[data-auto=btn-alternative-apps]")
  private WebElement altAppsButton;
  @FindBy(css = "[data-auto=related-topics] li a")
  private WebElement relatedTopic;
  @FindBy(id = "recommended-articles")
  private WebElement recommendedArticles;
  @FindBy(xpath = "id('header')/div/div[3]/a")
  private WebElement myNoodleProfileButton;
  @FindBy(xpath = "//div[contains(@class,'app-download-promoted-section-top__image')]/img")
  private WebElement nativeProgramIcon;
  @FindBy(xpath = "//h1[contains(@class,'app-download-promoted-section-top__name')]")
  private WebElement nativeProgramTitle;
  @FindBy(xpath = "//div[contains(@class,'app-download-promoted-section-top__summary')]/p")
  private WebElement nativeProgramDescription;
  // @FindBy(xpath = "//div[@class='filter-dropdown__btn-expand']/a")
  @FindBy(css = ".filter-dropdown__btn-expand[for='license'] a")
  private WebElement freeFilter;
  @FindBy(css = ".filter-dropdown__btn-expand[for='platform'] svg")
  private WebElement platformDropdown;
  @FindBy(css = "[data-auto=list-apps]")
  private WebElement programsListing;
  @FindBy(xpath = "//h2[contains(@class,'media-app__title')]/a")
  private WebElement programListName;
  @FindBy(xpath = "//dd[contains(@class,'media-app__license')]")
  private WebElement programListLicense;
  @FindBy(xpath = "//dd[@class='media-app__platform']/a")
  private WebElement programListPlatform;
  @FindBy(xpath = "//p[@class='media-app__summary']")
  private WebElement programListDescription;
  @FindBy(xpath = "//div[@class='list-apps__rating']")
  private WebElement programListRating;
  @FindBy(xpath = "//*[@id='trending-apps']/ul/li[1]/article")
  private WebElement trendingAppsModule;
  @FindBy(id = "last-published-apps")
  private WebElement latestApps;
  @FindBy(xpath = "//*[@id='new-apps']/ul/li[1]/article")
  private WebElement newAppsModule;
  @FindBy(id = "last-updated-apps")
  private WebElement updatedApps;
  @FindBy(xpath = "//a[@class='header__sign-in js-login']")
  private WebElement solutionsSignInButton;
  @FindBy(xpath = "//a[@class='button button--large button--facebook social-buttons__button "
      + "js-social-network']")
  private WebElement fbLoginButton;
  @FindBy(className = "avatar__outline")
  private WebElement userAvatar;
  @FindBy(xpath = "//span[@class='header__user-name js-user-name']")
  private WebElement myProfileButton;
  @FindBy(xpath = "//a[@class='button button--large button--google-plus "
      + "social-buttons__button js-social-network']")
  private WebElement googleLoginButton;
  @FindBy(xpath = "//select[@name='profile.birthDay']")
  private WebElement birthdateDaySelection;
  @FindBy(xpath = "//select[@name='profile.birthMonth']")
  private WebElement birthdateMonthSelection;
  @FindBy(xpath = "//select[@name='profile.birthYear']")
  private WebElement birthdateYearSelection;
  @FindBy(xpath = "//input[contains(@id,'gigya-multiChoice-0')]")
  private WebElement newsletterOptInCheckbox;
  @FindBy(xpath = "//input[contains(@id,'gigya-multiChoice-1')]")
  private WebElement newsletterOptOutCheckbox;
  @FindBy(xpath = "//input[@name='preferences.other_profiling_on_social_data.isConsentGranted'"
      + "and contains(@id,'gigya-multiChoice-0')]")
  private WebElement likesOptInCheckbox;
  @FindBy(id = "top-notification-bar-wrapper")
  private WebElement topNotificationBar;
  @FindBy(css = "[data-auto=page]")
  private WebElement page;
  @FindBy(xpath = "//div[@class='footer__row']")
  private WebElement footer;
  @FindBy(xpath = "//ol[@class='breadcrumb-list']")
  private WebElement breadcrumbs;

  private static final String PP_NATIVE_ADS_IFRAME =
      "//iframe[contains(@id,'BTF_Relatedapp_First_0')]";

  private static final String NATIVE_ADS_IFRAME =
      "//iframe[contains(@id,'ATF_Search_first_0') or contains(@id,'BTF_Search_first_0')"
          + " or contains(@id,'BTF_Relatedapp_First_0')]";

  private static final String SADS_POSITION = "//div[@id='adunit' or @id='sads-results']";

  private static final String SADS_BUTTON =
      "//a[@class='call-to-action-link' or @id='ctaButtonLink']";

  private static final String ADS_REGEX = "\\.setTargeting\\('type'\\, '([^\\(]+)'\\)";

  private static final String NOODLE_ADS_REGEX = "\\.display\\('([^\\(]+)'\\)";

  protected static final long PAGE_LOAD_TIMEOUT = 10000; // ms

  private static final TestLogger LOGGER = TestLogger.getInstance();

  /**
   * TODO (antonio.martin): Due to the current change to "new home" some pages
   * still have the old style. Remove once new style is applied to all pages.
   */
  /* Old style */

  @FindBy(id = "login-link")
  private WebElement oldLoginLink;
  @FindBy(xpath = "//a[@class='js-login-button']")
  private WebElement noodleLoginLink;
  @FindBy(id = "user-logout")
  private WebElement oldLogoutLink;
  @FindBy(id = "message_box")
  private WebElement deletedMessage;

  /* End of old style */

  protected List<String> getAdsIdsUsingPatternGroup(Pattern pattern, int groupNumber) {
    String pageSource = driver.getPageSource();
    List<String> adsIds = new ArrayList<String>();
    Matcher matcher = pattern.matcher(pageSource);
    while (matcher.find()) {
      adsIds.add("show_banner_" + matcher.group(groupNumber));
    }

    return adsIds;
  }

  protected List<String> getNoodleAdsIdsUsingPatternGroup(Pattern pattern, int groupNumber) {
    String pageSource = driver.getPageSource();
    List<String> adsIds = new ArrayList<String>();
    Matcher matcher = pattern.matcher(pageSource);
    while (matcher.find()) {
      adsIds.add(matcher.group(groupNumber));
    }

    return adsIds;
  }

  private boolean isPlatformSelected(String platform) {
    return platforms.get(0).getText().equalsIgnoreCase(platform);
  }

  public AbstractSoftonicWebPage(WebDriver driver) {
    super(driver);
    // Checks that there are no more display ads to load
    waitForPageElementsLoaded(PAGE_LOAD_TIMEOUT);
  }

  // Checks page every few ms to see if all display ads loaded, if not page
  // should be loaded
  private void waitForPageElementsLoaded(long timeoutMs) {
    LOGGER.logComment("Waiting for page to load.");
    final long timeSlice = 1000; // check every second
    int previous;
    int current = 0;
    do {
      previous = current;
      pause(timeSlice);
      timeoutMs -= timeSlice;
      current = driver.findElements(By.xpath("//*[contains(@id,'header')]")).size();
    } while (current != previous && timeoutMs > 0);
    if (timeoutMs <= 500) {
      LOGGER.logComment("Timeout exceeded waiting for page to load, proceeding anyway. "
          + "Test might fail due to page not being completely loaded.");
    }
  }

  public void pause(long milliseconds) {
    try {
      LOGGER.logComment("Sleeping for " + milliseconds + " milliseconds");
      Sleeper.SYSTEM_SLEEPER.sleep(new Duration(milliseconds, MILLISECONDS));
    } catch (InterruptedException ie) {
      LOGGER.logComment("Wait interrupted.");
    }
  }

  /**
   * Do home page login.
   *
   * @param user
   *            User which we will use to login.
   * @return this page so we can chain methods.
   */
  public AbstractSoftonicWebPage login(IAccount user) {
    loginCredentials(user);
    return new PlatformHomePage(driver);
  }

  public AbstractSoftonicWebPage login() {
    return login(Accounts.SOFTONIC);
  }

  public void loginCredentials(IAccount user) {
    // type credentials and click submit
    wait.forElementPresent(loginEmailBox);
    wait.forElementPresent(loginPasswordBox);
    wait.forElementPresent(loginSubmit);
    sendKeysThreadSafe(loginEmailBox, user.getEmail());
    sendKeysThreadSafe(loginPasswordBox, user.getPassword());
    loginSubmit.click();
  }

  /**
   * find out if the current user is already logged in.
   *
   * @return true of the user is logged in, false in the opposite case
   */
  public boolean isNoodleLoggedIn() {
    return isElementDisplayed(noodleLogoutLink);
  }

  public boolean isLoggedIn() {
    return isElementDisplayed(logoutLink);
  }

  public boolean isHomeLoggedIn() {
    return isElementDisplayed(homeLogoutLink);
  }

  public boolean isLoggedOut() {
    return isElementDisplayed(noodleLoginLink);
  }

  public boolean isDeleteMsgShown() {
    return isElementDisplayed(deletedMessage);
  }

  public boolean isLoginBoxPresent() {
    return isElementDisplayed(loginBox);
  }

  public boolean isHomeLoginBoxPresent() {
    return isElementDisplayed(homeLoginBox);
  }

  public boolean areBreadcrumbsPresent() {
    return isElementDisplayed(breadcrumbs);
  }

  public boolean isFacebookConnectPresent() {
    return isElementDisplayed(facebookConnectButton);
  }

  public boolean isGooglePlusSignInPresent() {
    return isElementDisplayed(googlePlusSignInButton);
  }

  public boolean isFreeFilterPresent() {
    return isElementDisplayed(freeFilter);
  }

  public AbstractSoftonicWebPage clickFreeFilterButton() {
    wait.until(ExpectedConditions.visibilityOfElementLocated(By
        .cssSelector(".filter-dropdown__btn-expand[for='license'] a")));
    wait.until(ExpectedConditions.elementToBeClickable(By
        .cssSelector(".filter-dropdown__btn-expand[for='license'] a")));
    freeFilter.click();
    return this;
  }

  public AbstractSoftonicWebPage scrollToFooter() {
    scrollIntoView(footer);
    return this;
  }

  public boolean isPlatformDropdownPresent() {
    return isElementDisplayed(platformDropdown);
  }

  public AbstractSoftonicWebPage clickPlatformDropdownButton() {
    wait.until(ExpectedConditions.visibilityOfElementLocated(By
        .cssSelector(".filter-dropdown__btn-expand[for='platform'] svg")));
    wait.until(ExpectedConditions.elementToBeClickable(By
        .cssSelector(".filter-dropdown__btn-expand[for='platform'] svg")));
    platformDropdown.click();
    return this;
  }

  protected void disableTopNotificationBar() {
    if (isTopNotificationBarPresent()) {
      removeTopNotificationBar();
      LOGGER.logComment("Removed top notification bar");
    } else {
      LOGGER.logComment("No top notification bar displayed");
    }
  }

  public boolean isTopNotificationBarPresent() {
    try {
      driver.findElement(By.xpath("//div[@id='top-notification-bar-wrapper']"));
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  public void removeTopNotificationBar() {
    ((JavascriptExecutor) driver).executeScript(
        "arguments[0].parentNode.removeChild(arguments[0])", topNotificationBar);
    }

  public boolean isProgramsListingPresent() {
    return isElementDisplayed(programsListing);
  }

  public boolean isProgramListNamePresent() {
    return isElementDisplayed(programListName);
  }

  public boolean isProgramListLicensePresent() {
    return isElementDisplayed(programListLicense);
  }

  public boolean areProgramListPlatformsPresent() {
    return isElementDisplayed(programListPlatform);
  }

  public boolean isProgramListDescriptionPresent() {
    return isElementDisplayed(programListDescription);
  }

  public boolean isProgramListRatingPresent() {
    return isElementDisplayed(programListRating);
  }

  public boolean areBlogArticlesPresent() {
    return isElementDisplayed(articlesSection);
  }

  public boolean isReadMorePresent() {
    return isElementDisplayed(readMoreButton);
  }

  public boolean isReportSoftwarePresent() {
    return isElementDisplayed(reportSoftware);
  }

  public boolean areRelatedSearchesPresent() {
    return isElementDisplayed(relatedSearches);
  }

  public boolean isProgramIconPresent() {
    return isElementDisplayed(programIcon);
  }

  public boolean isPageTitlePresent() {
    return isElementDisplayed(pageTitle);
  }

  public boolean isDownloadInfoPresent() {
    return isElementDisplayed(downloadInfo);
  }

  public boolean isProgramTitlePresent() {
    return isElementDisplayed(programTitle);
  }

  public boolean isProgramInfoPresent() {
    return isElementDisplayed(programInfo);
  }

  public boolean isProgramDownloadInfoPresent() {
    return isElementDisplayed(programDownloadInfo);
  }

  public boolean isTopDownloadsPresent() {
    return isElementDisplayed(topDownloads);
  }

  public boolean isSadsProgramIconPresent() {
    return isElementDisplayed(sadsProgramIcon);
  }

  public boolean isSadsPpProgramIconPresent() {
    return isElementDisplayed(sadsPpProgramIcon);
  }

  public boolean isSadsSponsoredMsgPresent() {
    return isElementDisplayed(sadsSponsoredMsg);
  }

  public boolean isSadsProgramTitlePresent() {
    return isElementDisplayed(sadsProgramTitle);
  }

  public boolean isSadsPpProgramTitlePresent() {
    return isElementDisplayed(sadsPpProgramTitle);
  }

  public boolean isSadsProgramLicensePresent() {
    return isElementDisplayed(sadsProgramLicense);
  }

  public boolean isSadsProgramLanguagePresent() {
    return isElementDisplayed(sadsProgramLanguage);
  }

  public boolean isSadsProgramDescriptionPresent() {
    return isElementDisplayed(sadsProgramDescription);
  }

  public boolean isSadsPpProgramDescriptionPresent() {
    return isElementDisplayed(sadsPpProgramDescription);
  }

  public boolean isSadsDownloadButtonPresent() {
    return isElementDisplayed(sadsDownloadButton);
  }

  public boolean isSadsPpDownloadButtonPresent() {
    return isElementDisplayed(sadsPpDownloadButton);
  }

  public boolean isSadsButtonEmpty() {
    return !sadsDownloadButton.getText().isEmpty();
  }

  public boolean isSadsPpButtonEmpty() {
    return !sadsPpDownloadButton.getText().isEmpty();
  }

  public boolean isNativeProgramIconPresent() {
    return isElementDisplayed(nativeProgramIcon);
  }

  public boolean isNativeProgramTitlenPresent() {
    return isElementDisplayed(nativeProgramTitle);
  }

  public boolean isNativeProgramDescriptionPresent() {
    return isElementDisplayed(nativeProgramDescription);
  }

  public boolean isTrendingAppsModulePresent() {
    return isElementDisplayed(trendingAppsModule);
  }

  public boolean areLatestAppsPresent() {
    return isElementDisplayed(latestApps);
  }

  public boolean isNewAppsModulePresent() {
    return isElementDisplayed(newAppsModule);
  }

  public boolean areUpdatedAppsPresent() {
    return isElementDisplayed(updatedApps);
  }

  public boolean hasCorrectNativeAdsContainer() {
    return isContainerPresent(NATIVE_ADS_IFRAME) ? hasCorrectContainer() : false;
  }

  public boolean hasCorrectPpNativeAdsContainer() {
    return isPpContainerPresent(PP_NATIVE_ADS_IFRAME) ? hasCorrectContainer() : false;
  }

  public boolean isContainerPresent(String iframe) {
    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(iframe)));
    wait.until(ExpectedConditions.elementToBeClickable(By.xpath(iframe)));
    driver.switchTo().frame(driver.findElement(By.xpath(iframe)));
    return isNativeAdsContainerPresent();
  }

  public boolean isPpContainerPresent(String iframe) {
    wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By
        .xpath("//iframe[contains(@id,'BTF_Relatedapp_First_0')]")));
    return isNativeAdsContainerPresent();
  }

  public boolean isNativeAdsContainerPresent() {
    try {
      driver.findElement(By.xpath("//div[@id='adunit' or @id='sads-results']"));
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  public boolean hasCorrectContainer() {
    return hasAdTargetsIncorrect();
  }

  public boolean hasAdTargetsIncorrect() {
    boolean adsCorrect = true;
    List<WebElement> ads = driver.findElements(By.xpath(SADS_POSITION));
    for (WebElement ad : ads) {
      if (!isAdTargetCorrect(ad)) {
        adsCorrect = false;
        break;
      }
    }
    return adsCorrect;
  }

  public boolean isAdTargetCorrect(WebElement ad) {
    WebElement target = ad.findElement(By.xpath(SADS_BUTTON));
    String a = target.getAttribute("target");
    return a.equals("_blank");
  }

  public Boolean waitForGooglePlusSignInOperative(int timeout, int retries) {
    return wait.forAsyncAttribute(googlePlusSignInButton, "data-gapiattached", timeout, retries);
  }

  /**
   * click on the logout link.
   *
   * @return current page.
   */
  public AbstractSoftonicWebPage logout() {
    wait.until(ExpectedConditions.elementToBeClickable(By.id("user-logout")));
    logoutLink.click();
    return new PlatformHomePage(driver);
  }

  public AbstractSoftonicWebPage submit() {
    wait.until(ExpectedConditions.elementToBeClickable(By
        .xpath("//input[contains(@class,'gigya-input-submit')]")));
    submitReconsent.click();
    return new PlatformHomePage(driver);
  }

  public boolean isReconsentPresent() {
    List<WebElement> elements =
        driver.findElements(By.xpath("//input[contains(@id,'gigya-multiChoice-0')]"));
    if (elements.isEmpty()) {
      return false;
    }
    return true;
  }

  public DownloadSearchResultsPage enterNoodleSearch(String terms) {
    wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("[data-auto=search-input]")));
    sendKeysThreadSafe(noodleSearchBox, terms);
    noodleSearchBox.submit();
    return new DownloadSearchResultsPage(driver);
  }

  public DownloadSearchResultsPage enterSearch(String terms) {
    wait.until(ExpectedConditions.elementToBeClickable(By.id("search_input")));
    sendKeysThreadSafe(searchBox, terms);
    searchBox.submit();
    return new DownloadSearchResultsPage(driver);
  }

  public ArticlesSearchResultsPage enterArticleSearch(String terms) {
    wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("[data-auto=search-input]")));
    sendKeysThreadSafe(searchBox, terms);
    searchBox.submit();
    return new ArticlesSearchResultsPage(driver);
  }

  public NewsSearchResultsPage enterNewsSearch(String terms) {
    wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("[data-auto=search-input]")));
    sendKeysThreadSafe(searchBox, terms);
    searchBox.submit();
    return new NewsSearchResultsPage(driver);
  }

  public String getLoggedUser() {
    return userProfileLink.getText().replaceAll("\\d*$", "").trim().toLowerCase();
  }

  public String getLoggedNoodleUser() {
    return noodleUserProfileLink.getText().replaceAll("\\d*$", "").trim().toLowerCase();
  }

  public String getLoggedUserPartialProfileName() {
    return homeUserProfileLink.getText().replaceAll("\\d*$", "").trim().toLowerCase();
  }

  public String getLoginLink() {
    wait.until(ExpectedConditions.elementToBeClickable(By.id("login-link")));
    return oldLoginLink.getAttribute("href");
  }

  public String getHomeLoginLink() {
    wait.until(ExpectedConditions.elementToBeClickable(By.id("header-login-link")));
    return homeLoginLink.getAttribute("href");
  }

  public ProfileRecentActivityPage clickProfile() {
    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("user_login")));
    userProfileLink.click();
    return new ProfileRecentActivityPage(driver);
  }

  public SocialMediaPopUpExternalModal clickFacebookConnectOnModal() {
    wait.until(ExpectedConditions.elementToBeClickable(By.id("fb_connect")));
    facebookConnectButton.click();
    return new SocialMediaPopUpExternalModal(driver);
  }

  public SocialMediaPopUpExternalModal clickGooglePlusSignInOnModal() {
    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("id('gigya-modal-plugin-container-"
        + "showScreenSet_social_0_uiContainer')//span/button[@title='Google+']")));
    // Waiting for Google Plus Sign In Library to load.
    googlePlusSignInButton.click();
    return new SocialMediaPopUpExternalModal(driver);
  }

  public AbstractSoftonicWebPage clickSignUpLink() {
    signupLink.click();
    return this;
  }

  public ProfileRecentActivityPage clickOnMyProfile() {
    noodleUserProfileLink.click();
    return new ProfileRecentActivityPage(driver);
  }

  public AfcWidget<AbstractSoftonicWebPage> getAfcWidget() {
    return new AfcWidget<AbstractSoftonicWebPage>(this);
  }

  public String getPlatformName() {
    return platforms.get(0).getText();
  }

  public List<String> getPlatformsNames() {
    List<String> names = new ArrayList<String>();
    unfoldPlatformList();
    for (WebElement platform : platforms) {
      names.add(platform.getText().toLowerCase());
    }
    return names;
  }

  private void unfoldPlatformList() {
    if (!platformPulldown.isDisplayed()) {
      platforms.get(0).click();
      if (platformPulldownMore.isDisplayed()) {
        platformPulldownMore.click();
      }
    }
  }

  public void closeNoodleCookieWarningIfPresent() {
    if (isNoodleCookieWarningPresent()) {
      dismissNoodleCookieWarning();
      LOGGER.logComment("Closed cookie warning modal");
    } else {
      LOGGER.logComment("No cookie warning modal displayed");
    }
  }

  public AbstractSoftonicWebPage selectPlatform(String platform) {
    if (!isPlatformSelected(platform)) {
      unfoldPlatformList();
      int i = 0;
      boolean found = false;
      while (i < platforms.size() && !found) {
        if (platforms.get(i).getText().equalsIgnoreCase(platform)) {
          platforms.get(i).click();
          found = true;
        }
        i++;
      }
      if (!found) {
        throw new NoSuchElementException("Platform not found ('" + platform + "')");
      }
    }
    wait.forPageAndAjaxToLoad();
    return this;
  }

  protected boolean areElementsPresent(List<WebElement> list) {
    boolean result = list.size() > 0;
    for (WebElement element : list) {
      result = result && element.isDisplayed();
    }
    return result;
  }

  public boolean isSocialMediaButtonsFakeImagePresent() {
    return isElementDisplayed(socialMediaFakeImage);
  }

  public AbstractSoftonicWebPage hoverOnSocialMediaButtons() {
    scrollIntoView(socialMediaFakeImage);
    return this;
  }

  public boolean isFacebookSocialMediaButtonPresent() {
    return isElementDisplayed(facebookSocialMediaButton);
  }

  public boolean isGooglePlusSocialMediaButtonPresent() {
    return isElementDisplayed(googlePlusSocialMediaButton);
  }

  public boolean isTwitterSocialMediaButtonPresent() {
    return isElementDisplayed(twitterSocialMediaButton);
  }

  public boolean isAltAppsButtonPresent() {
    return isElementDisplayed(altAppsButton);
  }

  public boolean areRelatedTopicsPresent() {
    return isElementDisplayed(relatedTopic);
  }

  public boolean isRecommendedArticlesPresent() {
    return isElementDisplayed(recommendedArticles);
  }

  public String getRelatedTopicLink() {
    return relatedTopic.getAttribute("href");
  }

  public boolean isUnsubscribedMessagePresent() {
    return isElementDisplayed(unsubscribedMessage);
  }

  public boolean isPaginationPresent() {
    return areElementsPresent(paginationList);
  }

  public int getPaginationCount() {
    return paginationList.size();
  }

  public String getHrefFromPaginationElement(int index) {
    return paginationList.get(index).findElement(By.tagName("a")).getAttribute("href");
  }

  public String getTitleFromPaginationElement(int index) {
    return paginationList.get(index).findElement(By.tagName("a")).getAttribute("title");
  }

  public String getValueFromPaginationElement(int index) {
    return paginationList.get(index).getText();
  }

  public List<String> getAdsIds() {
    Pattern pattern = Pattern.compile(ADS_REGEX);
    int group = 1;
    return getAdsIdsUsingPatternGroup(pattern, group);
  }

  public List<String> getNoodleAdsIds() {
    Pattern pattern = Pattern.compile(NOODLE_ADS_REGEX);
    int group = 1;
    return getNoodleAdsIdsUsingPatternGroup(pattern, group);
  }

  public int getContentDimensions() {
    int contentWidth = page.getSize().getWidth();
    LOGGER.logComment("Page width is: " + contentWidth + " px");
    int contentHeight = page.getSize().getHeight();
    LOGGER.logComment("Page height is: " + contentHeight + " px");
    int dimensions = contentWidth * contentHeight;
    return dimensions;
  }

  public boolean isAdPresent(String id) {
    return isElementDisplayed(By.id(id));
  }

  public boolean isNoodleAdPresent(String id) {
    try {
      driver.findElement(By.xpath("//div[@id='" + id + "']"));
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  public boolean isNoodleAdDisplayed(String id) {
    try {
      driver.findElement(By.xpath("//div[@id='" + id + "-wrapper"
          + "'][contains(@class,'ad--displayed')]"));
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  public boolean isAdEmpty(String id) {
    try {
      driver.findElement(By.xpath("//div[@id='" + id + "-wrapper"
          + "'][contains(@class,'ad--empty')]"));
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  public int getAdDimension(String id) {
    WebElement adPosition = driver.findElement(By.xpath("//div[@id='" + id + "']"));
    int adWidth = adPosition.getSize().getWidth();
    LOGGER.logComment("Ad width is: " + adWidth + " px");
    int adHeight = adPosition.getSize().getHeight();
    LOGGER.logComment("Ad height is: " + adHeight + " px");
    int dimensions = adWidth * adHeight;
    return dimensions;
  }

  public boolean isNonFixedAd(String id) {
    return id.equals("show_banner_interstitial") || id.equals("show_banner_wallpaper_background")
        || id.equals("show_banner_softonic_presents") || id.contains("interstitial")
        || id.contains("wallpaper") || id.contains("promoted-app") || id.contains("contextual")
        || id.contains("sads") || id.contains("slide") || id.contains("exit")
        || id.contains("see-through") || id.contains("related-apps-app")
        || id.contains("top-notification-bar") || id.contains("bottom-notification-bar")
        || id.equals("top-leaderboard-home-page-desktop") || id.contains("branded")
        || id.contains("review-app") || id.contains("you-may-also-like-app")
        || id.contains("promoted-topic") || id.contains("new-apps-category")
        || id.contains("new-apps-app-download-page-desktop") || id.contains("app-region-c");
  }

  public boolean isNonFixedAdSource(String id) {
    return id.equals("top-notification-bar") || id.contains("bottom-notification-bar");
  }

  public boolean isFixedPpAdSource(String id) {
    return id.equals("top-leaderboard-app-page-desktop") || id.equals("mpu-app-page-desktop")
        || id.equals("bottom-mpu-app-page-desktop")
        || id.equals("bottom-contextual-app-page-desktop")
        || id.equals("interstitial-app-page-desktop") || id.equals("related-apps-app-page-desktop")
        || id.equals("bottom-leaderboard-app-page-desktop")
        || id.equals("see-through-app-page-desktop") || id.equals("slide-in-app-page-desktop");
  }

  public boolean isFixedDpAdSource(String id) {
    return id.equals("aside-top-app-download-page-desktop")
        || id.equals("top-leaderboard-app-download-page-desktop")
        || id.equals("bottom-leaderboard-app-download-page-desktop")
        || id.equals("related-apps-app-download-page-desktop");
  }

  public boolean isFixedPdpAdSource(String id) {
    return id.equals("top-leaderboard-app-post-download-page-desktop")
        || id.equals("top-mpu-app-post-download-page-desktop")
        || id.equals("bottom-mpu-app-post-download-page-desktop")
        || id.equals("bottom-leaderboard-app-post-download-page-desktop")
        || id.equals("top-contextual-app-post-download-page-desktop")
        || id.equals("right-contextual-app-post-download-page-desktop")
        || id.equals("exit-intent-app-post-download-page-desktop")
        || id.equals("first-promoted-app-post-download-page-desktop");
  }

  public boolean isCookieWarningPresent() {
    try {
      driver.findElement(By.xpath("//a[@class='optanon-alert-box-close']"));
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  public boolean isNoodleCookieWarningPresent() {
    try {
      driver.findElement(By.xpath("//a[contains(@class,'optanon-allow-all')]"));
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  public boolean isSlideInPresent() {
    try {
      driver.findElement(By.xpath("//a[@class='ad-slider-content__close']"));
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  public boolean isNewsletterPopupPresent() {
    try {
      driver.findElement(By.id("popup-newsletter-software"));
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  public AbstractSoftonicWebPage dismissCookieWarning() {
    cookieWarningClose.click();
    return this;
  }

  public AbstractSoftonicWebPage dismissNoodleCookieWarning() {
    // scrollIntoView(cookieWarningBody);
    wait.until(ExpectedConditions.elementToBeClickable(noodleCookieWarning));
    noodleCookieWarning.click();
    return this;
  }

  public AbstractSoftonicWebPage dismissSlideIn() {
    try {
    slideIn.click();
    return this;
  } catch (ElementNotVisibleException e) {
      return this;
    }
  }

  public AbstractSoftonicWebPage dismissNewsletterPopup() {
    closeNewsletterPopup.click();
    return this;
  }

  // Enters birth date in the format DDMMYYYY
  public AbstractSoftonicWebPage enterDay(String day) {
    selectComboBoxByValue(birthdateDaySelection, day);
    return this;
  }

  // Enters birth date in the format DDMMYYYY
  public AbstractSoftonicWebPage enterMonth(String month) {
    selectComboBoxByIndex(birthdateMonthSelection, Integer.parseInt(month));
    return this;
  }

  // Enters birth date in the format DDMMYYYY
  public AbstractSoftonicWebPage enterYear(String year) {
    selectComboBoxByValue(birthdateYearSelection, year);
    return this;
  }

  public AbstractSoftonicWebPage checkOptInNewsletter() {
    newsletterOptInCheckbox.click();
    return this;
  }

  public AbstractSoftonicWebPage checkOptInLikes() {
    likesOptInCheckbox.click();
    return this;
  }

  public AbstractSoftonicWebPage checkOptOutNewsletter() {
    newsletterOptOutCheckbox.click();
    return this;
  }

  /**
   * TODO (antonio.martin): Due to the current change to "new home" some pages
   * still have the old style, because of that some overriding is needed in
   * order to deal with old pages. Remove them once new style is applied.
   */
  /* Old style */
  public OldLoginModal clickOldLogin() {
    wait.until(ExpectedConditions.elementToBeClickable(oldLoginLink));
    oldLoginLink.click();
    return new OldLoginModal(driver);
  }

  public OldLoginModal clickNoodleLogin() {
    wait.until(ExpectedConditions.elementToBeClickable(noodleLoginLink));
    noodleLoginLink.click();
    return new OldLoginModal(driver);
  }

  public boolean isLoggedInOld() {
    return isElementDisplayed(oldLogoutLink);
  }

  public boolean isLoggedOutOld() {
    return isElementDisplayed(oldLoginLink);
  }

  public AbstractSoftonicWebPage oldLogout() {
    oldLogoutLink.click();
    return new PlatformHomePage(driver);
  }

  /* End of old style */

  public HomePage clickSoftonicLogo() {
    softonicLogo.click();
    return new HomePage(driver);
  }

  public ProfileRecentActivityPage clickOnMyNoodleProfile() {
    myNoodleProfileButton.click();
    return new ProfileRecentActivityPage(driver);
  }

  public String getLoggedNoodleUserPartialName() {
    return myNoodleProfileButton.getText().replace("...", "").trim().toLowerCase(Locale.ENGLISH);
  }

  public AbstractSoftonicWebPage clickSignInButton() {
    solutionsSignInButton.click();
    return this;
  }

  public AbstractSoftonicWebPage clickOnFbLogin() {
    fbLoginButton.click();
    return this;
  }

  public boolean isLoggedInSolutions() {
    wait.until(ExpectedConditions.elementToBeClickable(By.className("avatar__outline")));
    return isElementDisplayed(userAvatar);
  }

  public String getLoggedUserPartialName() {
    return myProfileButton.getText().replace("...", "").trim().toLowerCase(Locale.ENGLISH);
  }

  public AbstractSoftonicWebPage clickOnGoogleLogin() {
    googleLoginButton.click();
    return this;
  }
}
