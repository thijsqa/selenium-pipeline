package com.softonicweb.container.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Contains methods to interact with the Softonic user login page.
 *
 * @author carlos.silva
 */
public class LoginPage extends AbstractSoftonicWebPage {

  @FindBy(id = "email")
  private WebElement email;
  @FindBy(id = "password")
  private WebElement password;
  @FindBy(id = "form-submit")
  private WebElement submitButton;

  public LoginPage(WebDriver driver) {
    super(driver);
  }

  public boolean isEmailPresent() {
    return isElementDisplayed(email);
  }

  public LoginPage enterEmail(String emailStr) {
    email.sendKeys(emailStr);
    return this;
  }

  public LoginPage enterPassword(String passStr) {
    password.sendKeys(passStr);
    return this;
  }

  public HomePage submit() {
    submitButton.click();
    return new HomePage(driver);
  }
}
