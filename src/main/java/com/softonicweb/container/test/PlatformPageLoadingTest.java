package com.softonicweb.container.test;

import org.testng.annotations.Test;

import com.softonicweb.container.framework.LimitInstances;
import com.softonicweb.container.framework.StartUrl;
import com.softonicweb.container.pages.PlatformHomePage;

/**
 * Platform Page Test on WebDriver.
 *
 * @author thijs.vandewynckel
 */
public class PlatformPageLoadingTest extends BaseWebTest {

  private static final String TEST_DESCRIPTION =
      "https://softonic-development.atlassian.net/browse/QA-139";

  @Test(description = TEST_DESCRIPTION, groups = { "24x7", "REGRESSION", "SMOKE", "PLATFORM",
      "VOLATILE" })
  @LimitInstances(restrictTo = { "ES" })
  @StartUrl(relativeUrl = "mac")
  public void testPlatformPage_ES() {
    verifyPlatformPage();
  }

  @Test(description = TEST_DESCRIPTION, groups = { "24x7", "REGRESSION", "SMOKE", "PLATFORM",
      "VOLATILE" })
  @LimitInstances(restrictTo = { "EN" })
  @StartUrl(relativeUrl = "windows")
  public void testPlatformPage_EN() {
    verifyPlatformPage();
  }

  @Test(description = TEST_DESCRIPTION, groups = { "24x7", "REGRESSION", "SMOKE", "PLATFORM",
      "VOLATILE" })
  @LimitInstances(restrictTo = { "FR" })
  @StartUrl(relativeUrl = "windows")
  public void testPlatformPage_FR() {
    verifyPlatformPage();
  }

  @Test(description = TEST_DESCRIPTION, groups = { "24x7", "REGRESSION", "SMOKE", "PLATFORM",
      "VOLATILE" })
  @LimitInstances(restrictTo = { "DE" })
  @StartUrl(relativeUrl = "web-apps")
  public void testPlatformPage_DE() {
    verifyPlatformPage();
  }

  @Test(description = TEST_DESCRIPTION, groups = { "24x7", "REGRESSION", "SMOKE", "PLATFORM",
      "VOLATILE" })
  @LimitInstances(restrictTo = { "IT" })
  @StartUrl(relativeUrl = "windows")
  public void testPlatformPage_IT() {
    verifyPlatformPage();
  }

  @Test(description = TEST_DESCRIPTION, groups = { "24x7", "REGRESSION", "SMOKE", "PLATFORM",
      "VOLATILE" })
  @LimitInstances(restrictTo = { "BR" })
  @StartUrl(relativeUrl = "windows")
  public void testPlatformPage_BR() {
    verifyPlatformPage();
  }

  @Test(description = TEST_DESCRIPTION, groups = { "24x7", "REGRESSION", "SMOKE", "PLATFORM",
      "VOLATILE" })
  @LimitInstances(restrictTo = { "NL" })
  @StartUrl(relativeUrl = "android")
  public void testPlatformPage_NL() {
    verifyPlatformPage();
  }

  @Test(description = TEST_DESCRIPTION, groups = { "24x7", "REGRESSION", "SMOKE", "PLATFORM",
      "VOLATILE" })
  @LimitInstances(restrictTo = { "JP" })
  @StartUrl(relativeUrl = "windows")
  public void testPlatformPage_JP() {
    verifyPlatformPage();
  }

  @Test(description = TEST_DESCRIPTION, groups = { "24x7", "REGRESSION", "SMOKE", "PLATFORM",
      "VOLATILE" })
  @LimitInstances(restrictTo = { "PL" })
  @StartUrl(relativeUrl = "windows")
  public void testPlatformPage_PL() {
    verifyPlatformPage();
  }

  @Test(description = TEST_DESCRIPTION, groups = { "24x7", "REGRESSION", "SMOKE", "PLATFORM",
      "VOLATILE", "MINIS" })
  @LimitInstances(restrictTo = { "RU" })
  @StartUrl(relativeUrl = "windows")
  public void testPlatformPage_RU() {
    verifyPlatformPage();
  }

  @Test(description = TEST_DESCRIPTION, groups = { "24x7", "REGRESSION", "SMOKE", "PLATFORM",
      "VOLATILE", "MINIS" })
  @LimitInstances(restrictTo = { "TR" })
  @StartUrl(relativeUrl = "windows")
  public void testPlatformPage_TR() {
    verifyPlatformPage();
  }

  @Test(description = TEST_DESCRIPTION, groups = { "24x7", "REGRESSION", "SMOKE", "PLATFORM",
      "VOLATILE", "MINIS" })
  @LimitInstances(restrictTo = { "KO" })
  @StartUrl(relativeUrl = "windows")
  public void testPlatformPage_KO() {
    verifyPlatformPage();
  }

  @Test(description = TEST_DESCRIPTION, groups = { "24x7", "REGRESSION", "SMOKE", "PLATFORM",
      "VOLATILE", "MINIS" })
  @LimitInstances(restrictTo = { "AR" })
  @StartUrl(relativeUrl = "windows")
  public void testPlatformPage_AR() {
    verifyPlatformPage();
  }

  @Test(description = TEST_DESCRIPTION, groups = { "24x7", "REGRESSION", "SMOKE", "PLATFORM",
      "VOLATILE", "MINIS" })
  @LimitInstances(restrictTo = { "ID" })
  @StartUrl(relativeUrl = "windows")
  public void testPlatformPage_ID() {
    verifyPlatformPage();
  }

  @Test(description = TEST_DESCRIPTION, groups = { "24x7", "REGRESSION", "SMOKE", "PLATFORM",
      "VOLATILE", "MINIS" })
  @LimitInstances(restrictTo = { "VI" })
  @StartUrl(relativeUrl = "windows")
  public void testPlatformPage_VI() {
    verifyPlatformPage();
  }

  @Test(description = TEST_DESCRIPTION, groups = { "24x7", "REGRESSION", "SMOKE", "PLATFORM",
      "VOLATILE", "MINIS" })
  @LimitInstances(restrictTo = { "TH" })
  @StartUrl(relativeUrl = "windows")
  public void testPlatformPage_TH() {
    verifyPlatformPage();
  }

  public void verifyPlatformPage() {
    openStartUrl();
    PlatformHomePage platformPage = new PlatformHomePage(driver());
    // no AB-test variant should be loaded
    disableAbtestvariant();

    assertTrue("Categories module is NOT present", platformPage.isCategoryModulePresent());
    assertTrue("New Apps module is NOT present", platformPage.isNewAppsModulePresent());
    assertTrue("Trending Apps module is NOT present", platformPage.isTrendingAppsModulePresent());
    assertTrue("Latest Apps module is NOT present", platformPage.areLatestAppsPresent());
    assertTrue("Updated Apps module is NOT present", platformPage.areUpdatedAppsPresent());
    assertTrue("Top Downloads is NOT present", platformPage.isTopDownloadsPresent());
    if (instance().getName().equalsIgnoreCase("EN")) {
    assertTrue("Related topics are NOT present", platformPage.areRelatedTopicsPresent());
    if (!SUITE_ENVIRONMENT.getParameters().getExecution().equals("VOLATILE")) {
    assertTrue("Recommended articles are NOT present", platformPage.isRecommendedArticlesPresent());
      }
    }
  }
}
