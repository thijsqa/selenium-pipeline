package com.softonicweb.container.test;

import com.softonicweb.container.framework.Runner;

/**
 * Java entry class. Runs TestNG programatically.
 *
 * @author ivan.solomonoff
 */
public final class Main {

  private Main() {
  }

  public static void main(String[] args) {
    Runner.execute();
  }
}
