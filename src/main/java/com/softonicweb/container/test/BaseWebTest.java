package com.softonicweb.container.test;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.http.HttpStatus;
import org.openqa.selenium.Cookie;
import org.testng.SkipException;

import com.softonicweb.container.seleniumframework.Browser;
import com.softonicweb.container.seleniumframework.RemoteWebDriverSession;
import com.softonicweb.container.seleniumframework.BaseWebDriverTest;
import com.softonicweb.container.seleniumframework.SeleniumFileDownloaderUtil;
import com.softonicweb.container.framework.FileDownloaderUtil;
import com.softonicweb.container.framework.HttpResource;
import com.softonicweb.container.framework.HttpUtil;
import com.softonicweb.container.pages.AbstractSoftonicWebPage;

/**
 * Base class for all web suite tests.
 *
 * @author carlos.silva
 * @author thijs.vandewynckel
 */
public class BaseWebTest extends BaseWebDriverTest {

  private static final int SUSPICIOUS_FILE_SIZE_LOWER_BOUND = 12000; // 12kb

  protected static final long IE_DOWNLOAD_TIMEOUT = 5000;
  protected static final long EMAIL_WAIT_TIME = 15000;
  protected static final long LONG_TEST_TIMEOUT = 240000; // ms
  protected static final long EXTRA_LONG_TEST_TIMEOUT = 320000; // ms

  @Override
  protected void openStartUrl() {
    super.openStartUrl();
    RemoteWebDriverSession.acceptAlertIfPresent(driver());
    driver().get(testEnvironment().getStartUrl());
  }

  protected boolean isDownloadLinkFromSoftonicServers(String url) {
    return url.startsWith("http://sd-cf") || url.startsWith("http://gsf-cf.softonic")
        || url.startsWith("https://gsf-cf.softonic") || url.startsWith("https://gsf-fl.softonic")
        || url.startsWith("http://global-shared-files-l3.softonic")
        || url.startsWith("http://global-shared-files-lw.softonic")
        || url.contains("universaldownloader") || url.contains("universal-downloader")
        || url.contains("dplus");
  }

  protected void checkFileProperties(HttpResource fileProperties) {
    String filePropertiesContentType = fileProperties.getContentType();
    logComment("File properties: " + filePropertiesContentType);
    assertFalse("Content is not a file but: " + filePropertiesContentType,
        filePropertiesContentType.contains("xml") || filePropertiesContentType.contains("text"));

    assertEquals("Response Code is NOT 200.", "200", fileProperties.getResponseCode());

    assertTrue(
        "Unexpected file content-type: " + filePropertiesContentType,
        filePropertiesContentType.contains("application/octet-stream")
            || filePropertiesContentType.contains("application/zip"));

    assertTrue("Suspicious file size (< " + SUSPICIOUS_FILE_SIZE_LOWER_BOUND + " bytes): ",
        fileProperties.getContentLength() > SUSPICIOUS_FILE_SIZE_LOWER_BOUND);
  }

  /**
   * Gets file properties for provided downloadLink.
   */
  protected HttpResource getFileProperties(String downloadLink) {
    HttpResource contentProperties = new HttpUtil(downloadLink).getContentProperties();
    logComment("Content Properties: " + "File Server = '" + contentProperties.getServer() + "'"
        + "  Type = '" + contentProperties.getContentType() + "'" + "  File Size = "
        + FileUtils.byteCountToDisplaySize(contentProperties.getContentLength())
        + "  Response Code = " + contentProperties.getResponseCode());
    return contentProperties;
  }

  protected void assertDownloadedFile() {
    String downloadUrl = driver().getCurrentUrl();
    FileDownloaderUtil downloaderUtil = new FileDownloaderUtil();
    downloaderUtil.setFollowRedirects(true);
    assertTrue("Download failed",
        downloaderUtil.downloadFromUrl(downloadUrl, SUITE_ENVIRONMENT.getDownloadsFolder()));
  }

  protected void disableAbtestvariant() {
    String actualUrl = driver().getCurrentUrl();
    logComment("Current url is: " + actualUrl);
    try {
      // Delete possible AB-test session cookie
      Cookie oldSwoCookie = driver().manage().getCookieNamed("_swo_pos");
      String swoValue = oldSwoCookie.getValue();
      logComment("Swo cookie value is: " + swoValue);
      driver().manage().deleteCookie(oldSwoCookie);
      // set cookie to disable any AB-test variant to be loaded
      Cookie newSwoCookie = new Cookie("_swo_pos", "-1");
      driver().manage().addCookie(newSwoCookie);
      Cookie newObtainedSwoCookie = driver().manage().getCookieNamed("_swo_pos");
      String newObtainedSwoCookieValue = newObtainedSwoCookie.getValue();
      logComment("New swo cookie value was set to: " + newObtainedSwoCookieValue);
      String keyword = "\\?ex=";
      String[] strArray = actualUrl.split(keyword);
      String trimUrl = strArray[0];
      driver().get(trimUrl);
      new AbstractSoftonicWebPage(driver());
    } catch (NullPointerException e) {
      logComment("Swo cookie NOT found!");
    }
    try {
      Cookie optimize = driver().manage().getCookieNamed("_gaexp");
      String optimizeValue = optimize.getValue();
      logComment("Optimize cookie value is: " + optimizeValue);
      driver().manage().deleteCookie(optimize);
      String disabledOptimize = optimizeValue.substring(0, optimizeValue.length() - 1) + "0";
      Cookie newOptimizeCookie = new Cookie("_gaexp", disabledOptimize);
      driver().manage().addCookie(newOptimizeCookie);
      Cookie newObtainedOptimizeCookie = driver().manage().getCookieNamed("_gaexp");
      String newObtainedOptimizeCookieValue = newObtainedOptimizeCookie.getValue();
      logComment("New optimize cookie value was set to: " + newObtainedOptimizeCookieValue);
      String keyword = "ex=";
      String[] strArray = actualUrl.split(keyword);
      String trimUrl = strArray[0];
      driver().get(trimUrl);
    } catch (NullPointerException e) {
      logComment("Optimize cookie NOT found!");
    }
  }

  protected void assertDownloadedFile(String downloadLink) {
    try {
      logComment("Download file link " + downloadLink);
      URL currentUrl = new URL(downloadLink);
      HttpURLConnection connection = (HttpURLConnection) currentUrl.openConnection();
      printHeadersFromRequest(downloadLink);
      HttpResource fileProperties = getFileProperties(downloadLink);
      String fileContentType = fileProperties.getContentType();
      assertTrue("Content type is not application: " + fileContentType,
          fileContentType.contains("application") || fileContentType.contains("text/html")
              || fileContentType.contains("text/xml")
              || fileContentType.contains("binary/octet-stream"));
      downloadFile(connection.getURL().toString());
    } catch (MalformedURLException e) {
      throw new RuntimeException(e);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public void downloadFile(String downloadUrl) {
    FileDownloaderUtil downloadUtil =
        new SeleniumFileDownloaderUtil(driver().manage().getCookies());
    assertTrue("Download failed",
        downloadUtil.downloadFromUrl(downloadUrl, SUITE_ENVIRONMENT.getDownloadsFolder()));
    printHeadersFromRequest(downloadUrl);
    try {
      downloadUtil.deleteLastDownloadFile();
    } catch (RuntimeException e) {
      logger().logComment("There was a problem deleting the downloaded file...");
    }
  }

  public void printHeadersFromRequest(String downloadLink) {
    try {
      URL obj = new URL(downloadLink);
      URLConnection conn = obj.openConnection();
      // get all headers
      Map<String, List<String>> map = conn.getHeaderFields();
      for (Map.Entry<String, List<String>> entry : map.entrySet()) {
        logComment("Key : " + entry.getKey() + " ,Value : " + entry.getValue());
      }
    } catch (MalformedURLException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  protected void skipTestForIE() {
    if (platform().equals(Browser.IE11) || platform().equals(Browser.IE9)
        || platform().equals(Browser.IE8)) {
      throw new SkipException("Test is not ready for Internet Explorer");
    }
  }

  protected void assertLinkResponseOk(String link) {
    logComment("Link to check: " + link);
    HttpUtil httpUtil = new HttpUtil(link);

    assertEquals("Response Code is NOT 200.", String.valueOf(HttpStatus.SC_OK),
        httpUtil.getResponseCode());
  }

  protected void assertBinaryLinkResponseOk(String link) {
    logComment("Link to check: " + link);
    HttpUtil httpUtil = new HttpUtil(link);
    assertTrue(
        "Response Code is NOT 200, 302 or 301.",
        String.valueOf(HttpStatus.SC_OK).equals(httpUtil.getResponseCode())
            || String.valueOf(HttpStatus.SC_MOVED_PERMANENTLY).equals(httpUtil.getResponseCode())
            || String.valueOf(HttpStatus.SC_MOVED_TEMPORARILY).equals(httpUtil.getResponseCode()));
  }
}
