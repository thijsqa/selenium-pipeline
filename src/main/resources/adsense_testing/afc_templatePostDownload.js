{
	"window.google_ad_client" = "%CLIENT%",
	"window.google_ad_output" = "js",
	"window.google_max_num_ads"   = %MAXADS%,
	"window.google_safe"          = "high",
	"window.google_feedback"      = "on",
	"window.google_ad_type"       = "text",
	"window.google_targeting" = "site_content",
	"window.google_page_url"      = "%URL%",
	"window.google_ad_channel"    = "%CHANNEL%",
	"window.google_adtest"        = "off"
}