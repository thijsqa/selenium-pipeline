{
	"oAdsStylesTop": {
		"adLoadedCallback" : fpEmptyAdsCleaner,
		"colorAdSeparator" : "#DCEEFB",
		"colorBackground" : "#FFFFFF",
		"colorDomainLink" : "#539B32",
		"colorTitleLink" : "#2A435F",
		"container" : "afs-container-top",
		"detailedAttribution": false,
		"fontFamily" : "Arial",
		"fontSizeAttribution" : 12,
		"fontSizeDescription" : 12,
		"fontSizeTitle" : 16,
		"longerHeadlines" : true,
		"maxTop" : 3,
		"titleBold" : true,
		"width" : "100%"
	}
}