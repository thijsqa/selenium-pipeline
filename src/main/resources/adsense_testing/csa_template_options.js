{
	"oAdsOptions": {
		"adPage" : "1",
		"numRepeated" : %NUMREPEATED%,
		"query" : "%QUERY%",
		"adtest" : "off",
		"rightHandAttribution" : false,
		"hl": "%HL%",
		"oe": "utf8",
		"adsafe": "medium",
		"clickableBackgrounds": "true",
		"pubId": "%PUBID%",
		"ie": "utf8",
		"linkTarget": "_blank",
		"channel": "%CHANNEL%",
		"rolloverLinkUnderline": true
	}
}