{
	"oAdsStylesBottom": {
		"lineHeightDomainLink" : 20,
		"colorDomainLink" : "#539B32",
		"colorTitleLink" : "#2a435f",
		"width" : "100%",
		"colorBackground":"#FDFFF1",
		"domainLinkAboveDescription" : "false",
		"number": "3",
		"titleBold": "true",
		"colorAdSeparator": "#DCEEFB",
		"fontSizeDescription": 12,
		"lineHeightDescription": 20,
		"fontSizeAttribution": 12,
		"fontSizeTitle": 20,
		"noTitleUnderline": "true",
		"container": "ads-container-bottom",
		"fontFamily": "Verdana",
		"verticalSpacing": 12,
		"lineHeightTitle": 35
	}
}